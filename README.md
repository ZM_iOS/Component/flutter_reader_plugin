# flutter_reader_plugin

Flutter read plugin.

### func 

#### flutter 调用 native

```
// 上一页
reversePage
{

}
```
```
// 下一页
forwardPage
 {

 }
```
// 页面信息
pageMsg
 {
    'pageIndex':'' // 可选，没有pageIndex时候获取当前页信息
 }
```

```
// 设置变更
updateConfig
{
    'contentFontSize': '16',
    'contentFontName': '',
    'contentColor': '0x0ffffff',

    'titleFontSize': '16',
    'titleFontName': '',
    'titleColor': '0x0ffffff',

    'backgroundColor': '0x000000',
}
```
```
// 内容变更
updateContent
{
    'title':'title'
    'content':'content'
}
```
```
// 翻页方式改变
transitionStyleChange
{
    'style':'0' // 0 防真翻页；1 横向翻页；2纵向翻页
}
```

#### native 调用 flutter

loadNextChapter

```
// 上一章
loadPreChapter
{
    
}
```

```
// 下一章
loadNextChapter
{
    
}
```

// 当前展示页面变更
currentPageChange
 {
    'pageIndex':'1'
 }
```