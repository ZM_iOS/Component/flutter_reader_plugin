package com.chineseall.reader.utils;

import android.content.Context;

import java.util.Map;

public class Protect {

    static {
        System.loadLibrary("native-lib");
    }


    public static native String encode(String key);
    public static native String encodeMap(Context context,Map<String, String> body);


}
