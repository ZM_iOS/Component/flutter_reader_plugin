package com.chineseall.reader17ksdk.utils;



import android.content.Context;

import com.chineseall.reader.lib.reader.config.TangYuanSharedPrefUtils;
import com.col.lib_book.ReadManager;


/**
 * <pre>
 *     author : Freedom
 *     e-mail : burst3166@gmail.com
 *     time   : 2020/11/19
 *     desc   :
 *     version: 1.0
 * </pre>
 */
public class GlobalConfig {

    public static final long DEFAULT_READER_FIRST_DIALOG_AD_INTERVAL = 1000 * 60 * 5;
    public static final long DEFAULT_READER_DIALOG_AD_INTERVAL = 1000 * 60 * 30;
    public static final int DEFAULT_READER_AD_INTERVAL_NUM = 3;

    public static String getAppId() {
        return ReadManager.INSTANCE.getAppId();
    }

    public static String getSecret() {
        return ReadManager.INSTANCE.getSecret();
    }

    public static String getUserId() {
        return TangYuanSharedPrefUtils.getInstance().getString("userId", "0");
    }

    public static String getRequestSign(String param) {
        return "12";
    }

    public static String getTimestamp() {
        return System.currentTimeMillis()/1000+"";
    }

    public static Context getContext() {
        return ReadManager.INSTANCE.getGetApplication();
    }
}