package com.col.lib_book;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.chineseall.reader.lib.reader.entities.Chapter;
import com.col.lib_book.utils.DensityUtil;
import com.col.lib_book.utils.ScreenUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import skin.support.content.res.SkinCompatResources;
import skin.support.widget.SkinCompatHelper;

/**
 * Created by huangzhengneng on 2020/4/20.
 */
public class CatalogAdapter extends RecyclerView.Adapter<CatalogAdapter.ViewHolder> {

    private List<Chapter> chapters = new ArrayList<>();
    private View.OnClickListener mOnClickListener;
    private Chapter mCurrChapter;

    private int itemCountInHalfScreen = 10;

    public CatalogAdapter() {
    }

    public CatalogAdapter(List<Chapter> chapters) {
        if (chapters != null) {
            this.chapters.addAll(chapters);
        }

        itemCountInHalfScreen = (ScreenUtils.getScreenHeight(ReadManager.INSTANCE.getGetApplication())
                - DensityUtil.dip2px(ReadManager.INSTANCE.getGetApplication(), 50)
                - ScreenUtils.getStatusHeight(ReadManager.INSTANCE.getGetApplication()))
                / DensityUtil.dip2px(ReadManager.INSTANCE.getGetApplication(), 50) / 2;
    }

    public void reverse() {
        Collections.reverse(chapters);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.col_layout_catalog_item, null));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.tv_name.setText(chapters.get(position).getChapterName());
        holder.tv_name.setTag(position);
        holder.tv_name.setOnClickListener(mOnClickListener);

        holder.tv_name.setTextColor(getSkinColor(chapters.get(position).equals(mCurrChapter) ? R.color.col_color_5DA9FA : R.color.col_reader_catalog_item_text_normal));
    }

    private int getSkinColor(int resId) {
        int checkedColor = SkinCompatHelper.checkResourceId(resId);
        if (checkedColor != 0) {
            return SkinCompatResources.getColor(ReadManager.INSTANCE.getGetApplication(), checkedColor);
        } else {
            return ReadManager.INSTANCE.getGetApplication().getResources().getColor(resId);
        }
    }

    @Override
    public int getItemCount() {
        return chapters.size();
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.mOnClickListener = v -> {
            if (onItemClickListener != null) {
                Object tag = v.getTag();
                if (tag instanceof Integer) {
                    onItemClickListener.onItemClick((Integer) tag);
                }
            }
        };
    }

    public int setCurrentChapter(Chapter chapter) {
        if (chapter == null || chapters.size() == 0) {
            return 0;
        }
        int res = 0;
        int curr = 0;
        for (int i = 0; i < chapters.size(); i++) {
            if (TextUtils.equals(chapter.getChapterId(), chapters.get(i).getChapterId())) {
                curr = i;
            }
            if (mCurrChapter != null && TextUtils.equals(mCurrChapter.getChapterId(), chapters.get(i).getChapterId())) {
                res = i;
            }
        }
        mCurrChapter = chapters.get(curr);
        notifyItemChanged(res);
        notifyItemChanged(curr);

        if (curr - itemCountInHalfScreen >= 0) {
            curr -= itemCountInHalfScreen;
        } else {
            curr = 0;
        }

        return curr;
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        TextView tv_name;

        ViewHolder(@NonNull View itemView) {
            super(itemView);
            tv_name = itemView.findViewById(R.id.tv_name);
        }
    }

    public interface OnItemClickListener {
        void onItemClick(int position);
    }
}
