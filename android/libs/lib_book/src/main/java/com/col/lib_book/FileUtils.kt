package com.col.lib_book

import splitties.init.appCtx
import java.io.BufferedReader
import java.io.InputStreamReader

object FileUtils {

    /**
     * 资源文件获取
     */
    fun getStringFromAssets(fileName: String?): String? {
        try {
            val inputReader = InputStreamReader(
                fileName?.let { appCtx.resources.assets.open(it) }
            )
            val bufReader = BufferedReader(inputReader)
            var line: String? = ""
            val result = StringBuilder()
            while (bufReader.readLine().also { line = it } != null) {
                result.append(line)
            }
            inputReader.close()
            return result.toString()
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return fileName
    }
}