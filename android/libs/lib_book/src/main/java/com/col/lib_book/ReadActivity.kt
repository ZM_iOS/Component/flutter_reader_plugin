package com.col.lib_book

import android.app.Activity
import android.app.AlarmManager
import android.app.PendingIntent
import android.content.*
import android.os.*
import android.telephony.PhoneStateListener
import android.telephony.TelephonyManager
import android.text.TextUtils
import android.view.MotionEvent
import android.view.View
import android.view.View.INVISIBLE
import android.view.ViewGroup
import android.view.ViewTreeObserver.OnGlobalLayoutListener
import android.view.WindowManager
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatDelegate
import androidx.appcompat.app.SkinAppCompatDelegateImpl
import androidx.core.view.GravityCompat
import androidx.core.view.isVisible
import androidx.drawerlayout.widget.DrawerLayout
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.chineseall.reader.lib.reader.ad.ReaderAdManager
import com.chineseall.reader.lib.reader.callbacks.OnBottomMenuListener
import com.chineseall.reader.lib.reader.config.IReadConfig
import com.chineseall.reader.lib.reader.config.ReaderConfigWrapper
import com.chineseall.reader.lib.reader.config.TangYuanSharedPrefUtils
import com.chineseall.reader.lib.reader.entities.Chapter
import com.chineseall.reader.lib.reader.entities.Paragraph
import com.chineseall.reader.lib.reader.utils.PaintHelper
import com.chineseall.reader.lib.reader.view.ReaderView
import com.chineseall.reader.lib.reader.view.horizontal.PaperView
import com.col.lib_book.ext.exceptionHandler
import com.col.lib_book.ext.logi
import com.col.lib_book.ext.toast
import com.col.lib_book.base.BaseActivity
import com.col.lib_book.callbacks.OnTouchMoveListener
import com.col.lib_book.data.*
import com.col.lib_book.database.AppDatabase
import com.col.lib_book.database.ExploreHistoryDao
import com.col.lib_book.utils.*
import com.col.lib_book.utils.ScreenUtils.getScreenBrightness
import com.col.lib_book.utils.voice.*
import com.col.lib_book.widget.*
import com.col.lib_book.widget.dialog.AddBookShelfReminderDialog
import com.col.lib_book.widget.dialog.ReminderCallBack

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import skin.support.SkinCompatManager
import splitties.init.appCtx
import java.util.*


/**
 * Created by huangzhengneng on 2020/11/11.
 */
@Suppress("DEPRECATION")
class ReadActivity : BaseActivity(), ReaderView.OnRectClickCallback, ReaderView.OnChangedListener {

    private lateinit var drawerLayout: DrawerLayout
    private lateinit var rvCatalog: RecyclerView
    private lateinit var paperView: PaperView
    private lateinit var tvSort: TextView
    private lateinit var rl_mulu_head: RelativeLayout
    private lateinit var ivIcon: ImageView

    private lateinit var flTopMenuContainer: FrameLayout
    private lateinit var flBottomMenuContainer: FrameLayout
    private lateinit var flVoiceReaderContainer: FrameLayout
    private lateinit var flADCenterContainer: CustomRelativeLayout
    private lateinit var mTopMenuBar: TopMenuBar
    private lateinit var mBottomMenuBar: BottomMenuBar
    private lateinit var mBottomSettingBar: BottomSettingBar

    private lateinit var mVoiceControllerView: VoiceControllerView
    private var mBookName = ""

    private lateinit var mCatalogAdapter: CatalogAdapter

    private var mOpenSuccess = false
    private var lastMenuChangeTime: Long = 0
    private val menuChangeIntervalTime = 500
    private lateinit var sharedPrefUtil: TangYuanSharedPrefUtils
    private var moveStep = 0f
    private val chapters = ArrayList<Chapter>()

    private var isReadModeOn = false
    private var mReaderTime = 0

    private var customPhoneStateListener: CustomPhoneStateListener? = null

    private lateinit var mMenuManager: ReadPageMenuManager

    private lateinit var batteryReceiver: BroadcastReceiver

    private lateinit var viewModel: ReadViewModel
    private var currentReadParagraph: Paragraph? = null
    private var currentReadParagraphIndex = 0
    private var mSkinStatus = 0
    private var isLaunchingVoice = false
    private var mEnterTime = 0L
    private var mStartReadTime = 0L
    private var mReadChapterCount = 0
    private var mReadPageCount = 0

    private var mProgress = 0
    private var mChapterId = ""

    private lateinit var mReadPageAdManager: ReadPageAdManager
    private val mHandler = Handler(Looper.getMainLooper()) { message ->
        if (message.what == 1) {
            updateChapterProgress()
        }
        false
    }

    private val skinLoadListener = object : SkinCompatManager.SkinLoaderListener {
        override fun onStart() {
            mSkinStatus = 1
        }

        override fun onSuccess() {
            mSkinStatus = 0
            ReaderConfigWrapper.init(TangYuanReadConfig(appCtx))
            synchronized(this@ReadActivity) {
                PaintHelper.getInstance().resetConfig(ReaderConfigWrapper.getInstance())
                getReaderView().requestRepaint(null)
            }

            val sceneMode =
                TangYuanSharedPrefUtils.getInstance()
                    .getSceneMode(IReadConfig.SKIN_THEME_DAYTIME)
            if (sceneMode == ReaderConfigWrapper.SKIN_THEME_NIGHT) {
                setNightModeBtnStatus(true)
            } else {
                setNightModeBtnStatus(false)
            }
            setThemeGroupEnable(true)
        }

        override fun onFailed(errMsg: String?) {
            mSkinStatus = -1
            setThemeGroupEnable(true)
        }
    }

    private fun setNightModeBtnStatus(isNightMode: Boolean) {
        mBottomMenuBar.setNightMode(isNightMode)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        ReadManager.initSdk(application)
        super.onCreate(savedInstanceState)
        window.clearFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN)
        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
        setContentView(R.layout.activity_read)
        val aInt: Int = TangYuanSharedPrefUtils.getInstance()
            .getInt(READER_THEME_DAY_TIME, 0)
        if (aInt == 0) {
            SkinCompatManager.getInstance().loadSkin(
                "yellow.skin",
                skinLoadListener,
                SkinCompatManager.SKIN_LOADER_STRATEGY_ASSETS
            )
            TangYuanSharedPrefUtils.getInstance()
                .putInt(READER_THEME_DAY_TIME, R.id.radio_read_menu_skin_theme_yellow)
        }
        val bookId = intent.getStringExtra(BOOK_ID)
        if (savedInstanceState != null) {
            mProgress = savedInstanceState.getInt(PROGRESS)
            mChapterId = savedInstanceState.getString(CHAPTER_ID, "")
        } else {
            mChapterId = intent.getStringExtra(CHAPTER_ID) ?: ""
            mProgress = intent.getIntExtra(PROGRESS, 0)
        }

        viewModel =
            ReadViewModel(BookContentRespository(AppDatabase.getInstance(appCtx).bookshelfDao()))
        exploreHistoryDao = AppDatabase.getInstance(appCtx).exploreHistoryDao();

        viewModel.mBookId.value = bookId
        flTopMenuContainer = findViewById(R.id.fl_top_menu_container)
        flBottomMenuContainer = findViewById(R.id.fl_bottom_menu_container)
        flVoiceReaderContainer = findViewById(R.id.reader_voice_reader_container)
        flADCenterContainer = findViewById(R.id.fl_ad_center_container)
        drawerLayout = findViewById(R.id.drawer_layout)
        rvCatalog = findViewById(R.id.rv_catalog)

        paperView = findViewById(R.id.paperView)
        rl_mulu_head = findViewById(R.id.rl_mulu_head)
        tvSort = findViewById(R.id.tv_sort)
        ivIcon = findViewById(R.id.iv_icon)
        rl_mulu_head.setOnClickListener(View.OnClickListener { })
        sharedPrefUtil = TangYuanSharedPrefUtils.getInstance()

        EventBus.getDefault().register(this)

        flADCenterContainer.setOnTouchMoveListener(object : OnTouchMoveListener {

            override fun onTouch(event: MotionEvent?) {
                paperView.onTouch(paperView, event)
            }
        })

        viewModel.mChapterList.observe(this) {

        }
        viewModel.mChapterContent.observe(this) {

        }

        viewModel.mInBookShelf.observe(this) {
            if (mTopMenuBar != null) {
                mTopMenuBar.setAddBookShlefButtonVisible(!it)
            }
        }

        paperView.activity = this
        paperView.adViewContainer = findViewById<FrameLayout>(R.id.fl_ad_container)
        paperView.setOnRectClickCallback(this)
        paperView.setOnChangedListener(this)

        ReaderConfigWrapper.init(TangYuanReadConfig(applicationContext))
        PaintHelper.init(ReaderConfigWrapper.getInstance())
        loadConfig()

        initTopMenuBar()
        initBottomMenuBar()
        initBottomSettingBar()
        initVoiceView()
        initHardWare()
        loadChapterList()
        initBroadcast()

        ReadManager.getOnReadNightModeChangeListener()?.onNightModeChanged(
            TangYuanSharedPrefUtils.getInstance()
                .getSceneMode(IReadConfig.SKIN_THEME_DAYTIME) == IReadConfig.SKIN_THEME_NIGHT
        )

        mMenuManager = ReadPageMenuManager(
            flTopMenuContainer,
            flBottomMenuContainer,
            mTopMenuBar,
            mBottomMenuBar,
            mBottomSettingBar
        )

        lifecycleScope.launch(Dispatchers.IO + exceptionHandler()) {
            viewModel.checkInBookShelf()
        }
        mReadPageAdManager = ReadPageAdManager()
        mReadPageAdManager.init(this)
    }

    private fun startTimer() {
        try {
            ReadManager.getOnReadCallBack()?.apply {
                if (ReadManager.getReadCallBackPeriod() > 0) {
                    mHandler.postDelayed({
                        startTimer()
                        val result = mapOf<String, Any>(
                            "sessionId" to mHandler.hashCode(),
                            "time" to SystemClock.elapsedRealtime() - mEnterTime + mReadedMillisecond,
                            "bookId" to (viewModel.mBookInfo.value?.bookId ?: ""),
                            "bookName" to (viewModel.mBookInfo.value?.bookName ?: ""),
                            "authorPenname" to (viewModel.mBookInfo.value?.authorPenname ?: ""),
                            "bookStatus" to (viewModel.mBookInfo.value?.bookStatus ?: ""),
                            "wordCount" to (viewModel.mBookInfo.value?.wordCount ?: ""),
                            "chapterName" to (getReaderView().currentChapter?.chapterName ?: ""),
                            "pageInChapter" to (getReaderView().article?.currPageInChapter ?: 1),
                            "chapterTotalPages" to (getReaderView().article?.currChapterTotalPages
                                ?: 1)
                        )
                        onReceive(result)
                    }, ReadManager.getReadCallBackPeriod().toLong())
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    /**
     * 隐藏广告
     */
    private fun hideAdView() {
        if (flADCenterContainer != null && flADCenterContainer.isVisible) {
            flADCenterContainer.setVisibility(INVISIBLE)
            flADCenterContainer.removeAllViews();
        }
    }

    /**
     * 隐藏和重置广告
     */
    private fun hideAndResetAdView() {
        hideAdView();
    }

    /**
     * 在第二次调用绘制时才 显示浮层 来自广告绘制
     * 1.首次进入到阅读器 当前页有广告
     * 2.首次进入到阅读器 上一页或下一页有广告 点击切换页面 浮层显示问题
     * 3.切换章节 当前第一页有广告
     */
//    private var onlyTwoDo = 0

    /**
     * 首次进入到阅读器 下一页或上一页有广告 滑动翻页再松开 浮层快速显示问题优化
     */
    private var movePage = false

    /**
     * 根据当前章节信息判断 是否显示广告浮层
     */
    private fun isShowAdViewFromCurrentChapter() {
        Handler(Looper.getMainLooper()).post {
            val chapter = getReaderView().currentChapter
            if (chapter != null) {
                val adKey = chapter.adViewKeyList.get(chapter.currentPageIndex)
                if (adKey != null) {
                    val ttAd = ReaderAdManager.getInstance().getTTAd(adKey)
                    if (ttAd != null) {
                        showAdView(ttAd)
                        movePage = true
                    }
                }
            }
        }
    }

    /**
     * 显示广告布局
     */
    private fun showAdView(adView: View) {
        if (flADCenterContainer != null) {
            if (!flADCenterContainer.isVisible || (flADCenterContainer.isVisible && flADCenterContainer.getChildCount() == 0)) {
                flADCenterContainer.setVisibility(View.VISIBLE);

                if (flADCenterContainer.getChildCount() > 0) {
                    flADCenterContainer.removeAllViews();
                }

                // 广告可见才会产生曝光，否则将无法产生收益。
                if (adView.getParent() != null) {
                    (adView.getParent() as ViewGroup).removeView(adView)
                }
                flADCenterContainer.addView(adView);
            }
        }
    }

    override fun getDelegate(): AppCompatDelegate {
        return SkinAppCompatDelegateImpl.get(this, this)
    }

    private fun initVoiceView() {
        mVoiceControllerView = VoiceControllerView(this)
        flVoiceReaderContainer.addView(mVoiceControllerView)
        mVoiceControllerView.setCallBack { time -> mReaderTime = time }
        if (sharedPrefUtil.isReadModeOn) {
            sharedPrefUtil.isReadModeOn = false
        }
    }

    private var job: Job? = null

    /**
     * 初始化语音
     */
    private fun initVoiceManager(lastAnimCheckID: Int) {
        registerPhoneStateListener()
        job = lifecycleScope.launch(Dispatchers.IO + exceptionHandler()) {
            isReadModeOn = true
            sharedPrefUtil.isReadModeOn = true
            sharedPrefUtil.putInt(
                VoiceControllerView.AUTO_VOICE_LAST_CHECK_RADIO_ID,
                lastAnimCheckID
            )
            VoiceManager.getInstance(this@ReadActivity).start(object : VoiceCallBack {

                override fun turnPage(next: Boolean) {
                    runOnUiThread {
                        hideAndResetAdView() //朗读翻页
                        getReaderView().trunpage(next, false)
                        mReadPageCount++
                    }
                }

                override fun gotoNextChapter() {
                    runOnUiThread {
                        this@ReadActivity.goToNextChapter()
                    }
                }

                override fun getCurrentParagraph(): Paragraph {
                    return currentReadParagraph ?: Paragraph()
                }

                override fun setCurrentParagraph(paragraph: Paragraph) {
                    currentReadParagraph = paragraph
                }

                override fun getCurrentParagraphIndex(): Int {
                    return currentReadParagraphIndex
                }

                override fun setCurrentParagraphIndex(index: Int) {
                    currentReadParagraphIndex = index
                }

                override fun getCurrentChapter(): Chapter? {
                    return getReaderView().currentChapter
                }

                override fun getNextChapter(): Chapter? {
                    return getReaderView().nextChapter
                }

                override fun invalidateReaderView() {
                    runOnUiThread {
                        getReaderView().requestRepaint("")
                    }
                }

                override fun exit() {
                    runOnUiThread {
                        exitVoiceRead()
                    }
                }

                override fun initResult(success: Boolean) {
                    isLaunchingVoice = false
                }
            })
        }
    }

    /**
     * 退出语音朗读
     */
    private fun exitVoiceRead() {
        mVoiceControllerView.checkTimer(R.id.rb_reader_timer_off)
        isReadModeOn = false
        sharedPrefUtil.isReadModeOn = false
        flVoiceReaderContainer.visibility = View.GONE
        val anInt = sharedPrefUtil.getInt(
            VoiceControllerView.AUTO_VOICE_LAST_CHECK_RADIO_ID,
            R.id.rb_noanim
        )
        if (anInt != R.id.rb_noanim) {
            mBottomSettingBar.setAnim(anInt)
        }
    }

    private fun goToLastChapter() {
        val currentChapter: Chapter? = getReaderView().currentChapter
        if (currentChapter?.preId ?: 0 > 0) {
//            setReaderProgress(readerView.getArticle().getPreviousChapter().getChapterName())
//            chapterId = currentChapter?.preId
//            mProgress = 0
            getReaderView().loadPreChapter()
        }
    }

    private fun goToNextChapter() {
        val currentChapter: Chapter? = getReaderView().currentChapter
        if (currentChapter?.nextId ?: 0 > 0) {
//            setReaderProgress(getNextChapter().getChapterName())
            getReaderView().loadNextChapter()
        }
        mReadPageCount++
        mReadChapterCount++
    }

    private fun registerPhoneStateListener() {
        if (customPhoneStateListener == null) {
            customPhoneStateListener = CustomPhoneStateListener(this)
        }
        val telephonyManager = getSystemService(TELEPHONY_SERVICE) as TelephonyManager
        telephonyManager.listen(customPhoneStateListener, PhoneStateListener.LISTEN_CALL_STATE)
    }

    private fun unregisterPhoneStateListener() {
        val telephonyManager = getSystemService(TELEPHONY_SERVICE) as TelephonyManager
        if (customPhoneStateListener != null) {
            telephonyManager.listen(customPhoneStateListener, PhoneStateListener.LISTEN_NONE)
        }
    }

    override fun hideMenu() {
        logi("OnRCC=hideMenu")
        switchMenus(false)
        switchDrawer(false)
    }

    override fun menuRectClick() {
        if (!mOpenSuccess) return
        if (System.currentTimeMillis() - lastMenuChangeTime < menuChangeIntervalTime) {
            return
        }
        lastMenuChangeTime = System.currentTimeMillis()
        switchDrawer(false)
        if (isReadModeOn) {
            if (flVoiceReaderContainer.visibility == View.VISIBLE) {
                flVoiceReaderContainer.visibility = View.GONE
                VoiceManager.getInstance(this).resume()
                checkIsStartTaskReader()
            } else {
                flVoiceReaderContainer.visibility = View.VISIBLE
                mVoiceControllerView.resetColor()
                VoiceManager.getInstance(this).pause()
            }
        } else {
            switchMenus(!isMenuShowing)
        }
    }

    /**
     * 检查定时朗读是否开启,开启则启动
     */
    private fun checkIsStartTaskReader() {
        if (mReaderTime > 0) {
            startTask()
            mReaderTime = 0
        } else if (mReaderTime == -1) {
            cancelTask()
        }
    }

    /**
     * 启动朗读定时任务
     */
    private fun startTask() {
        val am = getSystemService(ALARM_SERVICE) as AlarmManager
        val intent = Intent(this, VoiceTaskReceiver::class.java)
        val currentTimeMillis = System.currentTimeMillis() + mReaderTime
        val pendingIntent =
            PendingIntent.getBroadcast(this, 0, intent, PendingIntent.FLAG_CANCEL_CURRENT)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            am.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, currentTimeMillis, pendingIntent)
        }
        am.setExact(AlarmManager.RTC_WAKEUP, currentTimeMillis, pendingIntent)
    }

    /**
     * 取消朗读定时任务
     */
    private fun cancelTask() {
        val am = getSystemService(ALARM_SERVICE) as AlarmManager
        val intent = Intent(this, VoiceTaskReceiver::class.java)
        val pendingIntent =
            PendingIntent.getBroadcast(this, 0, intent, PendingIntent.FLAG_NO_CREATE)
        if (pendingIntent != null) {
            am.cancel(pendingIntent)
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onGetCancelVoiceTaskEvent(event: CancelVoiceTaskEvent?) {
        VoiceManager.getInstance(this).exitVoiceRead(true)
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun pauseVoiceReaderEvent(event: PauseVoiceReaderEvent?) {
        if (isReadModeOn) {
            flVoiceReaderContainer.visibility = View.VISIBLE
            VoiceManager.getInstance(this).pause()
        }
    }

    /**
     * w:
     * 仿真动画
     * 1点击翻页
     * 2滑动一段距离翻页（2->1）
     * 无动画
     * 3.滑动翻页
     * 4.点击翻页
     */
    override fun nextPageRectClick(w: Int) {
        switchMenus(false)
        switchDrawer(false)
        mReadPageCount++
    }

    override fun previousPageRectClick(w: Int) {
        switchMenus(false)
        switchDrawer(false)
        mReadPageCount++
    }

    var isUpAniming = false
    override fun adGone(w: Int) {
        logi("OnRCC=adGone$w")
        if (w == 1) {
            isUpAniming = true
        }
        hideAdView()
    }

    /**
     * w:
     * 1 松开手 动画执行出屏幕后
     * 5 广告绘制完
     */
    override fun adVisible(w: Int) {
        //  刚打开阅读器 没有翻页逻辑 导致广告浮层不显示问题
        logi("OnRCC=adVisible" + w)
        if (w == 1) {
            isUpAniming = false
        }
        if (isUpAniming) {
            return
        }
        isShowAdViewFromCurrentChapter()
    }

    override fun isMenuShowing(): Boolean {
        return mMenuManager.isMenuOnShow
    }

    private fun loadConfig() {
        // 根据用户系统亮度操作 ,来还原上次亮度等设置
        if (sharedPrefUtil.getBoolean(IS_FOLLOW_SYSTEM, true)) {
            ScreenUtils.startAutoBrightness(this)
        } else {
            val brightness: Int =
                sharedPrefUtil.getBrightness(getScreenBrightness(this, 120))
            ScreenUtils.setScreenBritness(this, brightness)
        }
    }

    private fun loadChapterList() {
        viewModel.mBookId.value?.let {
            lifecycleScope.launch(Dispatchers.IO) {
                try {
                    val chapterBeanList = viewModel.getChapterList(it)
                    runOnUiThread {
                        openBookContent(chapterBeanList, it)
                    }
                } catch (e: Exception) {
                    launch(Dispatchers.Main) {
                        showErrorDialog(ReadManager.getErrorMsg(e))
                    }
                }
            }
        }

    }

    private fun showErrorDialog(msg: String?) {
        DialogUtil.showDialog(
            this@ReadActivity,
            "",
            msg,
            null,
            null,
            "我知道了"
        ) { _: DialogInterface?, _: Int ->
            exit()
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        val currentReadChapterId: String = getReaderView().currentChapter?.chapterId ?: "0"
        if (currentReadChapterId != null) {
            outState.putString(CHAPTER_ID, currentReadChapterId)
        }
        outState.putInt(PROGRESS, getReaderView().progress)
    }


    private fun openBookContent(
        chapterBeanList: List<ChapterBean>,
        it: String,
    ) {
        if (TextUtils.isEmpty(mChapterId) || "0".equals(mChapterId)) {
            GlobalScope.launch(Dispatchers.IO + exceptionHandler()) {
                val findById = exploreHistoryDao.findById(it)
                if (findById != null) {
                    mChapterId = findById.chapterId.toString()
                    mProgress = findById.progress ?: 0
                }
                runOnUiThread {
                    dealChapterList(chapterBeanList, it, mChapterId, mProgress)
                }
            }
        } else {
            dealChapterList(chapterBeanList, it, mChapterId, mProgress)
        }
    }

    private fun dealChapterList(
        chapterBeanList: List<ChapterBean>,
        it: String,
        chapterId: String,
        progress: Int,
    ) {
        val chapterList: MutableList<Chapter> = mutableListOf()
        var targetChapter: Chapter? = null
        chapterBeanList.forEachIndexed { index, chapterBean ->
            val chapter = Chapter()
            chapter.index = index
            //                    chapter.offset = chapterBean.offset
            //                    chapter.setIs_jingpin(isJinpin)
            //                    chapter.lineNumber = item.line_number
            chapter.bookId = it
            chapter.bookName = chapterBean.bookName
            chapter.chapterId = "${chapterBean.chapterId}"
            chapter.timeStamp_value = chapterBean.updateTimeValue
            chapter.publishTime = chapterBean.updateTimeValue
            var chapterName: String = chapterBean.chapterName
            if (TextUtils.isEmpty(chapterName)) {
                chapterName = "无标题"
            }

            chapter.nextId =
                if (index >= chapterBeanList.size - 1) 0 else chapterBeanList[index + 1].chapterId.toLong()
            chapter.preId =
                if (index <= 0) 0 else chapterBeanList[index - 1].chapterId.toLong()
            chapter.chapterName = chapterName

            //                    if (chapterBean.chapterId === chapterId) {
            //                        hasChapterId = true
            //                            mCurrentChapter = chapter
            //                    }
            chapterList.add(chapter)

            if (chapter.chapterId == chapterId) {
                targetChapter = chapter
            }
        }

        if (chapterList.isNotEmpty()) {

            viewModel.mChapterBeanList.value = (chapterBeanList)
            viewModel.mChapterList.value = (chapterList)

            this@ReadActivity.chapters.clear()
            this@ReadActivity.chapters.addAll(chapterList)
            loadArticle(
                chapterList as ArrayList<Chapter>,
                targetChapter ?: chapterList[0],
                progress
            )
            initCatalogAdapter(chapters)
            mOpenSuccess = true
        } else {
            showErrorDialog("书内容为空")
        }
    }

    private fun initCatalogAdapter(chapters: List<Chapter>) {
        mCatalogAdapter = CatalogAdapter(chapters)
        mCatalogAdapter.setOnItemClickListener { position ->
            val posSort = "倒序" == tvSort.text
            val chapter = chapters[if (posSort) position else chapters.size - position - 1]
            if (!TextUtils.isEmpty(chapter.chapterId)) {
                switchDrawer(false)
                getReaderView().setNorepaint(false)
                getReaderView().jumpChapter(chapter.chapterId, 0)
                hideAndResetAdView() //目录章节点击后跳转
            } else {
                LogUtils.d("target chapter is null or chapter id is empty...")
            }
        }
        drawerLayout.addDrawerListener(object : DrawerLayout.DrawerListener {
            override fun onDrawerSlide(drawerView: View, slideOffset: Float) {}

            override fun onDrawerOpened(drawerView: View) {
                scrollToCurrentChapter()
            }

            override fun onDrawerClosed(drawerView: View) {
            }

            override fun onDrawerStateChanged(newState: Int) {
            }
        })
        rvCatalog.adapter = mCatalogAdapter
        rvCatalog.layoutManager =
            LinearLayoutManager(applicationContext, RecyclerView.VERTICAL, false)
        tvSort.setOnClickListener {
            mCatalogAdapter.reverse()
            scrollToCurrentChapter()
            val posSort = "倒序" == tvSort.text
            tvSort.text = if (posSort) "正序" else "倒序"
            if (Build.VERSION.SDK_INT >= 21) {
                ivIcon.setImageDrawable(ColorUtil.getSkinDrawable(if (posSort) R.drawable.col_ic_read_sort_pos else R.drawable.col_ic_read_sort_nav))
            }
//            tvSort.setCompoundDrawablesRelativeWithIntrinsicBounds(
//                ColorUtil.getSkinDrawable(if (posSort) R.drawable.col_ic_read_sort_pos else R.drawable.col_ic_read_sort_nav),
//                null,
//                null,
//                null
//            )
        }
    }

    private fun getReaderView(): ReaderView {
//        val orientation = sharedPrefUtil.getReaderPageOrientation(34)
//        return if (orientation != 1) paperView else yViewBiz
        return paperView
    }

    override fun onDestroy() {
        super.onDestroy()

//        if (mNeedSaveProgress) {
//            saveProgress()
//        }
        mHandler.removeCallbacksAndMessages(null)
        if (paperView.article != null) {
            paperView.article?.freeMemory()
            paperView.destroy()
            paperView.freeMemory()
        }
        if (isReadModeOn) {
            exitVoiceRead()
        }

        traceReadExit()

//        SkinCompatManager.getInstance().restoreDefaultTheme()
        dismissProgress()
        EventBus.getDefault().unregister(this)
        mMenuManager.cancelMenuAnim()
        job?.cancel()
        VoiceManager.getInstance(this).release()
        unregisterPhoneStateListener()
        unregisterReceiver(batteryReceiver)
        mReadPageAdManager?.destory()
        ReaderAdManager.getInstance().clearAdList()
    }

    private fun updateChapterProgress() {
        LogUtils.d("TAG=updateChapterProgress=saveProgress")
        saveProgress()
        mHandler.sendEmptyMessageDelayed(1, 30000)
    }

    private fun traceReadExit() {
        val diff = SystemClock.elapsedRealtime() - mStartReadTime
//        val readTime = formatReadTime(diff)

        viewModel.mBookInfo.value?.apply {

        }
    }

    private fun saveProgress() {
        GlobalScope.launch(Dispatchers.IO + exceptionHandler()) {
            val ans = viewModel.addBookShelf(
                getReaderView().currentChapter?.chapterId ?: "0",
                if (getReaderView().progress >= 0) getReaderView().progress else 0
            )
            logi("更新结果：$ans")
        }
    }

    private fun initHardWare() {
        if (Build.VERSION.SDK_INT >= 21) {
            window.setFlags(
                WindowManager.LayoutParams.FLAG_HARDWARE_ACCELERATED,
                WindowManager.LayoutParams.FLAG_HARDWARE_ACCELERATED
            )
        }
    }

    /**
     * 初始化电量显示广播
     */
    private fun initBroadcast() {
        val intentFilter = IntentFilter(
            "android.intent.action.BATTERY_CHANGED"
        )
        batteryReceiver = object : BroadcastReceiver() {
            override fun onReceive(context: Context?, intent: Intent?) {
                if ("android.intent.action.BATTERY_CHANGED" == intent?.action) {
                    val intExtra = intent.getIntExtra("level", 0)
                    val intExtra2 = intent.getIntExtra("scale", 100)
                    paperView.batteryLevel = intExtra * 100 / intExtra2
//                    yViewBiz.batteryLevel = intExtra * 100 / intExtra2
                }
            }
        }
        registerReceiver(this.batteryReceiver, intentFilter)
    }

    private fun loadArticle(chapters: ArrayList<Chapter>, self: Chapter, progress: Int) {
        mBookName = self.bookName
        mTopMenuBar.setTitle(mBookName)
        if (getReaderView().width <= 0) {
            getReaderView().viewTreeObserver?.addOnGlobalLayoutListener(object :
                OnGlobalLayoutListener {
                override fun onGlobalLayout() {
                    getReaderView().viewTreeObserver?.removeOnGlobalLayoutListener(this)
                    loadArtical(self, chapters, progress)
                }
            })
        } else {
            loadArtical(self, chapters, progress)
        }
    }

    private fun loadArtical(
        self: Chapter,
        chapters: ArrayList<Chapter>,
        progress: Int,
    ) {
        getReaderView().loadArticle(
            self.bookId.toInt(),
            self.chapterId,
            chapters,
            progress
        ) { articl ->
            ChapterHelperImpl(
                articl,
                viewModel.getRespository(),
                this@ReadActivity
            ) {
                if (intent?.getBooleanExtra(START_WITH_VOICE, false) == true) {
                    it?.apply {
                        if (!isLaunchingVoice) {
                            isLaunchingVoice = true
                            launchVoice()
                        }
                    }
                }
            }
        }
        mHandler.removeCallbacksAndMessages(null)
        startTimer()
    }

    private fun switchMenus(show: Boolean) {
        if (!show) {
            mBottomMenuBar.setReaderProgress("", false)
        }
        if (show != mMenuManager.isMenuOnShow) {
            mMenuManager.switchMenus(
                show, getReaderView().currentChapter,
                (chapters.size - 1).coerceAtLeast(0),
                object : OnBottomMenuListener {
                    override fun onHideBottomBar() {
                    }
                }
            )
            if (show) {
                isShowAdViewFromCurrentChapter()
            }
        }
    }

    private fun switchDrawer(show: Boolean) {
        if (show) {
            if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
                return
            }
            drawerLayout.openDrawer(GravityCompat.START)
        } else {
            if (!drawerLayout.isDrawerOpen(GravityCompat.START)) {
                return
            }
            drawerLayout.closeDrawer(GravityCompat.START)
        }
    }

    private fun scrollToCurrentChapter() {
        val chapter = getReaderView().currentChapter
        val position: Int = mCatalogAdapter.setCurrentChapter(chapter)
        val lm = rvCatalog.layoutManager as LinearLayoutManager
        lm.scrollToPositionWithOffset(position, 0)
        mCatalogAdapter.notifyDataSetChanged()
    }

    private fun initTopMenuBar(): TopMenuBar {
        mTopMenuBar = TopMenuBar(this)
        mTopMenuBar.setClickListener(object : TopMenuBar.ClickListener {
            override fun addBookToShelf() {
                mTopMenuBar.setAddBookShlefButtonVisible(false)
                showProgress()
                lifecycleScope.launch(Dispatchers.IO + exceptionHandler()) {
                    val ans = viewModel.addBookShelf(
                        getReaderView().currentChapter?.chapterId ?: "0",
                        if (getReaderView().progress >= 0) getReaderView().progress else 0
                    )
                    runOnUiThread {
                        dismissProgress()
                        viewModel.mInBookShelf.value = ans
                        if (ans) {
                            toast("加入书架成功")
                        } else {
                            toast("加入失败~")
                        }
                    }
                }
            }

            override fun back() {
                exitWithoutMenuCheck()
            }

            override fun more() {

            }
        })
        mTopMenuBar.setTitle(mBookName)
        flTopMenuContainer.addView(mTopMenuBar)
        return mTopMenuBar
    }

    private fun getBottomMenuBar(): BottomMenuBar? {
        return mBottomMenuBar
    }

    override fun onBackPressed() {
        if (this.flTopMenuContainer.visibility == View.VISIBLE
            || this.flBottomMenuContainer.visibility == View.VISIBLE
        ) {
            switchMenus(false)
            return
        }
        exitWithoutMenuCheck()
    }

    private fun exitWithoutMenuCheck() {
        if (viewModel.mInBookShelf.value == false) {
            showAddBookShelfReminder()
            return
        }
        exit()
    }

    private fun showAddBookShelfReminder() {
        val dialog = AddBookShelfReminderDialog(this)
        dialog.setCancelable(false)
        dialog.callBack = object : ReminderCallBack {
            override fun ok() {
                dialog.dismiss()
                showProgress()
                lifecycleScope.launch(Dispatchers.IO + exceptionHandler()) {
                    val ans = viewModel.addBookShelf(
                        getReaderView().currentChapter?.chapterId ?: "0",
                        if (getReaderView().progress >= 0) getReaderView().progress else 0
                    )
                    runOnUiThread {
                        dismissProgress()
                        if (ans) {
                            toast("加入书架成功")
                            exit()
                        } else {
                            toast("加入失败~")
                        }
                    }
                }
                viewModel.mBookInfo.value?.apply {

                }
            }

            override fun cancel() {
                dialog.dismiss()
                exit()
            }
        }
        dialog.show()
    }

    private fun exit() {
        super.onBackPressed()
    }

    override fun onResume() {
        super.onResume()
        mStartReadTime = SystemClock.elapsedRealtime()

        mReadChapterCount = 0
        mReadPageCount = 0

        mEnterTime = SystemClock.elapsedRealtime()
        mHandler.removeCallbacksAndMessages(null)
        startTimer()

//        mReadPageAdManager.resume(this)
        readStartTime = System.currentTimeMillis()
    }

    private var mReadedMillisecond = 0L
    override fun onPause() {
        super.onPause()
        mReadedMillisecond += SystemClock.elapsedRealtime() - mEnterTime
        mHandler.removeCallbacksAndMessages(null)
        saveToHistory()
        if (viewModel.mInBookShelf.value == true) {
            // 在书架的书才保存进度
            TangYuanSharedPrefUtils.getInstance()
                .putBoolean("ReadBook" + viewModel.mBookId.value, true)
            saveProgress()
        }

        mReadPageAdManager.stopTimer()
        dealReadTime()
    }


    lateinit var exploreHistoryDao: ExploreHistoryDao

    private fun saveToHistory() {
        var chapterId = getReaderView().currentChapter?.chapterId ?: "0"
        var progress = if (getReaderView().progress >= 0) getReaderView().progress else 0
        GlobalScope.launch(Dispatchers.IO + exceptionHandler()) {
            val findById = exploreHistoryDao.findById(viewModel.mBookId.toString())
            if (findById != null) {
                findById.openTime = System.currentTimeMillis()
                findById.chapterId = chapterId.toLong()
                findById.progress = progress
                exploreHistoryDao.update(findById)
            } else {
                val book = viewModel.mBookInfo.value
                if (book != null) {
                    var item = ExploreHistory(
                        book.bookId,
                        progress,
                        ReadManager.getUserId().toInt(),
                        book.bookName,
                        book.coverImageUrl,
                        chapterId.toLong(),
                        System.currentTimeMillis(),
                    )
                    exploreHistoryDao.insert(item)
                }
            }
        }
    }

    private fun formatReadTime(diff: Long): String {
        val hour = diff / (1000 * 60 * 60)
        val hourStr = if (hour >= 10) "$hour" else "0${hour}"
        val minute = (diff - (hour * 60 * 60 * 1000)) / (1000 * 60)
        val minuteStr = if (minute >= 10) "$minute" else "0${minute}"
        val second = (diff - (hour * 60 * 60 * 1000) - (minute * 60 * 1000)) / 1000
        val secondStr = if (second >= 10) "$second" else "0${second}"

        return "$hourStr:$minuteStr:$secondStr"
    }

    private fun initBottomMenuBar() {
        mBottomMenuBar = BottomMenuBar(this)
        mBottomMenuBar.setCallBack(object : BottomMenuBar.CallBack {
            private var lastChanpterChangeTime: Long = 0

            override fun directory() {
                switchMenus(false)
                switchDrawer(true)
            }

            override fun nightMode(night: Boolean) {
                if (night) { // 设置为夜间模式
                    mBottomSettingBar.setTheme(mSkinStatus, R.id.radio_read_menu_skin_theme_night)
                } else { // 设置为日间模式
                    val aInt: Int = TangYuanSharedPrefUtils.getInstance()
                        .getInt(READER_THEME_DAY_TIME, 0)
                    mBottomSettingBar.setTheme(
                        mSkinStatus,
                        if (aInt > 0) aInt else R.id.radio_read_menu_skin_theme_gray
                    )
                }
            }

            override fun setting() {
                mMenuManager.showSettingBar(sharedPrefUtil.getFontSize(22))
            }

            override fun chapterChanged(online: Boolean, next: Boolean) {
                if (System.currentTimeMillis() - lastChanpterChangeTime < 1000) {
                    return
                }
                lastChanpterChangeTime = System.currentTimeMillis()
                if (next) {
                    goToNextChapter()
                } else {
                    goToLastChapter()
                }
                mBottomMenuBar.refreshStatus(
                    (chapters.size - 1).coerceAtLeast(0),
                    getReaderView().currentChapter?.index ?: 0
                )
            }

            override fun chapterChangedBySeek(
                online: Boolean,
                percent: Int,
                stop: Boolean,
                fromUser: Boolean,
            ) {
                if (!stop) {
                    if (fromUser) {
                        mBottomMenuBar.setReaderProgress(chapters[percent].chapterName, true)
                    }
                } else {
                    val chapter = chapters[percent]
                    getReaderView().jumpChapter(chapter.chapterId, 0)
                    hideAndResetAdView() //底部菜单栏 SeekBar 拖动切换章节
                }
            }

            override fun openVoice() {
                launchVoice()
            }

            private fun goToLastChapter() {
                val currentChapter = getReaderView().currentChapter
                if (currentChapter?.preId ?: 0 > 0) {
                    mBottomMenuBar.setReaderProgress(
                        getReaderView().article.previousChapter?.chapterName,
                        true
                    )
                    getReaderView().loadPreChapter()
                    hideAndResetAdView() //底部菜单栏 上一章 切换章节
                }
            }

            private fun goToNextChapter() {
                val currentChapter = getReaderView().currentChapter
                if (currentChapter?.nextId ?: 0 > 0) {
                    mBottomMenuBar.setReaderProgress(
                        getReaderView().article.nextChapter?.chapterName,
                        true
                    )
                    getReaderView().loadNextChapter()
                    hideAndResetAdView() //底部菜单栏 下一章 切换章节
                }
            }
        })
        mBottomMenuBar.progressSeekBar?.max = chapters.size
        val chapterIndex = chapters.indexOf(getReaderView().currentChapter)
        mBottomMenuBar.progressSeekBar?.progress = chapterIndex.coerceAtLeast(0)
//        mBottomMenuBar.setNightMode(
//            TangYuanSharedPrefUtils.getInstance()
//                .getSceneMode(IReadConfig.SKIN_THEME_DAYTIME) == IReadConfig.SKIN_THEME_NIGHT
//        )
        flBottomMenuContainer.addView(mBottomMenuBar)
    }

    private fun launchVoice() {
        switchMenus(false)
        initVoiceManager(mBottomSettingBar.animCheckID)
    }

    private fun initBottomSettingBar() {
        mBottomSettingBar = BottomSettingBar(this)
        mBottomSettingBar.setCallBack(object : BottomSettingBar.CallBack {

            private var lastFontChangedTime = 0L

            override fun followSystemBrightness(checked: Boolean) {
                if (checked) {
                    // 跟随系统亮度 ,  设置成跟随亮度
                    ScreenUtils.startAutoBrightness(this@ReadActivity)
                    sharedPrefUtil.putBoolean(IS_FOLLOW_SYSTEM, true)
                } else {
                    // 关闭跟随系统亮度 , 设置成当前用户调节亮度
                    ScreenUtils.setScreenBritness(
                        this@ReadActivity,
                        this@ReadActivity.sharedPrefUtil.getBrightness(
                            getScreenBrightness(
                                applicationContext,
                                120
                            )
                        )
                    )
                    sharedPrefUtil.putBoolean(IS_FOLLOW_SYSTEM, false)
                }
            }

            override fun brightnessChanged(
                pro: Int,
                fromUser: Boolean,
                followSystem: Boolean,
            ) {
                var progress = pro
                if (fromUser) {
                    //判断跟随系统 是否打开(如果打开 自动关闭)
                    if (followSystem) {
                        mBottomSettingBar.setFollowSystem(false)
                        sharedPrefUtil.putBoolean(IS_FOLLOW_SYSTEM, false)
                    }
                    sharedPrefUtil.putBrightness(progress)
                    sharedPrefUtil.isAppAloneBrightness = true
                    if (progress <= 10) {
                        progress = 10
                    }
                    val attributes: WindowManager.LayoutParams = window.attributes
                    attributes.screenBrightness = (progress.toDouble() / 255.0).toFloat()
                    window.attributes = attributes
                }
            }

            override fun themeChanged(skinName: String?, id: Int) {
                setThemeGroupEnable(false)
                if (skinName != null) {
                    SkinCompatManager.getInstance().loadSkin(
                        skinName,
                        skinLoadListener,
                        SkinCompatManager.SKIN_LOADER_STRATEGY_ASSETS
                    )
                } else {
                    SkinCompatManager.getInstance().loadSkin(
                        "",
                        skinLoadListener,
                        SkinCompatManager.SKIN_LOADER_STRATEGY_ASSETS
                    )
                }
                if (sharedPrefUtil.getSceneMode(IReadConfig.SKIN_THEME_DAYTIME) != id) {
                    sharedPrefUtil.putSceneMode(id)
                }
            }

            override fun fontSizeChanged(reduce: Boolean) {
                if (System.currentTimeMillis() - lastFontChangedTime < 1000) {
                    return
                }
                getReaderView().currentChapter?.clearAdViewKeyList()
                lastFontChangedTime = System.currentTimeMillis()
                val drawing = getReaderView().isDrawing
                if (drawing) return
                if (reduce) {
                    mBottomSettingBar.setEnlargeFontSize(true)
                    val fontSize =
                        sharedPrefUtil.getFontSize(ReaderConfigWrapper.TEXT_SIZE_DEFAULT)
                    val nextReduceFontSize = getNextReduceFontSize(fontSize)
                    if (nextReduceFontSize != fontSize) {
                        if (nextReduceFontSize == ReaderConfigWrapper.TEXT_SIZE_MIN) {
                            mBottomSettingBar.setReduceFontSize(false)
                        }
                        sharedPrefUtil.setFontSize(nextReduceFontSize)
                        ReaderConfigWrapper.init(TangYuanReadConfig(applicationContext))
                        resetFont()
                    }
                } else {
                    mBottomSettingBar.setReduceFontSize(true)
                    val fontSize =
                        sharedPrefUtil.getFontSize(ReaderConfigWrapper.TEXT_SIZE_DEFAULT)
                    val nextEnlargeFontSize = getNextEnlargeFontSize(fontSize)
                    if (nextEnlargeFontSize != fontSize) {
                        if (nextEnlargeFontSize == ReaderConfigWrapper.TEXT_SIZE_MAX) {
                            mBottomSettingBar.setEnlargeFontSize(false)
                        }
                        sharedPrefUtil.setFontSize(nextEnlargeFontSize)
                        ReaderConfigWrapper.init(TangYuanReadConfig(applicationContext))
                        resetFont()
                    }
                }
            }

            override fun fontTypeChanged(font: String) {
                if (System.currentTimeMillis() - lastFontChangedTime < 1000) {
                    return
                }
                lastFontChangedTime = System.currentTimeMillis()
                resetFont()
            }

            private fun getNextReduceFontSize(size: Int): Int {
                var i = size
                return if (i > ReaderConfigWrapper.TEXT_SIZE_MIN) {
                    i -= 2
                    i
                } else {
                    i
                }
            }

            private fun getNextEnlargeFontSize(size: Int): Int {
                var i = size
                return if (i < ReaderConfigWrapper.TEXT_SIZE_MAX) {
                    i += 2
                    i
                } else {
                    i
                }
            }

            override fun onAnimChanged(orientation: Int, mode: Int) {
                val readerPagerAnim =
                    TangYuanSharedPrefUtils.getInstance().getReaderPagerAnim(34)
                if (orientation == 2 && readerPagerAnim != mode) {
                    sharedPrefUtil.saveReaderPagerAnim(mode)
                }
            }
        })
        if (sharedPrefUtil.getBoolean(IS_FOLLOW_SYSTEM, true)) {
            mBottomSettingBar.setFollowSystem(true)
        } else {
            mBottomSettingBar.setFollowSystem(false)
            val brightness =
                sharedPrefUtil.getBrightness(getScreenBrightness(this, 120))
            ScreenUtils.setScreenBritness(this, brightness)
            mBottomSettingBar.setBrightness(brightness)
        }
        var readerPagerAnim: Int
        if (2 == this.sharedPrefUtil.getReaderPageOrientation(2)) {
            readerPagerAnim = this.sharedPrefUtil.getReaderPagerAnim(34)
            val checkId: Int = when (readerPagerAnim) {
                33 -> {
                    R.id.rb_noanim
                }
                34 -> {
                    R.id.rb_fz
                }
                35 -> {
                    R.id.rb_fg
                }
                else -> {
                    R.id.rb_noanim
                }
            }
            mBottomSettingBar.setAnim(checkId)
        } else {
            mBottomSettingBar.setAnim(R.id.rb_sx)
        }
        flBottomMenuContainer.addView(mBottomSettingBar)
    }

    private fun resetFont() {
        synchronized(this@ReadActivity) {
            PaintHelper.getInstance().resetConfig(ReaderConfigWrapper.getInstance())
            getReaderView().reload(true, 0)
            hideAndResetAdView() //设置 字号调整大小
        }
    }

    fun getBookId(): String? {
        return viewModel.mBookId.value ?: ""
    }

    var isFirstChapterChange = true
    var isFirstPageChange = true
    var readStartTime = 0L

    /**
     * 处理真实阅读时间
     */
    fun dealReadTime(): Long {
        var timeCha = 0L
        if (ReadManager.getVideoAdRealReadMode()) {
            if (readStartTime > 0) {
                timeCha = System.currentTimeMillis() - readStartTime
                readStartTime = System.currentTimeMillis()
                TangYuanSharedPrefUtils.getInstance().putLong(
                    "realReadTime",
                    TangYuanSharedPrefUtils.getInstance().getLong(
                        "realReadTime",
                        0
                    ) - timeCha
                )
            }
            LogUtils.d(
                "TAG=realReadTime=" + TangYuanSharedPrefUtils.getInstance().getLong(
                    "realReadTime",
                    0
                )
            )
        }
        return timeCha
    }

    private var time = 0L
    override fun onChapterChanged() {
        LogUtils.d("test changed: onChapterChanged1")
        if (System.currentTimeMillis() - time < 3000) {
            return
        }
        time = System.currentTimeMillis();
        LogUtils.d("test changed: onChapterChanged2222222222")
        mReadChapterCount++
        mReadPageAdManager.requestDialogAd(this, dealReadTime()) {
            exitWithoutMenuCheck()
        }
        if (!isFirstChapterChange) {
            ReadManager.getOnPageChangeListener()?.onChapterChanged()
        } else {
            isFirstChapterChange = false
        }
        mHandler.removeMessages(1)
        mHandler.sendEmptyMessageDelayed(1, 5000)
    }


    override fun onPageChanged() {
        LogUtils.d("test changed: onPageChanged")
        mReadPageCount++
        if (!isFirstPageChange) {
            ReadManager.getOnPageChangeListener()?.onPageChanged()
        } else {
            isFirstPageChange = false
            mHandler.sendEmptyMessageDelayed(1, 30000)
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onReadCompleteEvent(event: ReadCompleteEvent) {
        if (event.bookId == viewModel.mBookId.value) {

        }
    }

    private fun setThemeGroupEnable(enable: Boolean) {
        mBottomSettingBar.setThemeEnable(enable)
    }

    companion object {
        private const val IS_FOLLOW_SYSTEM = "is_follow_system"
        private const val BOOK_ID: String = "bookId"
        private const val CHAPTER_ID: String = "chapterId"
        private const val PROGRESS: String = "progress"
        private const val START_WITH_VOICE: String = "startWithVoice"

        fun start(
            context: Context,
            bookId: String,
            chapterId: String = "",
            progress: Int = 0,
            startWithVoice: Boolean = false,
        ) {
            val intent = Intent(context, ReadActivity::class.java)
            intent.putExtra(BOOK_ID, bookId)
            intent.putExtra(CHAPTER_ID, chapterId)
            intent.putExtra(PROGRESS, progress)
            intent.putExtra(START_WITH_VOICE, startWithVoice)

            val activityByClass: Activity? = ActivityStackManager.getInstance().getActivityByClass(
                ReadActivity::class.java
            )

            if (activityByClass is ReadActivity) {
                // 阅读页只存在一个
//                if (activityByClass.getBookId() == bookId) {
                activityByClass.finish()
//                }
            }
            context.startActivity(intent)
        }
    }
}