package com.col.lib_book

import android.app.Application
import android.content.Context
import android.os.Build
import android.text.TextUtils
import android.view.MotionEvent
import android.view.View
import com.chineseall.reader.lib.reader.callbacks.OnAdViewCallBack
import com.chineseall.reader.lib.reader.callbacks.OnAdViewReceiver
import com.chineseall.reader.lib.reader.callbacks.OnLockViewCallBack
import com.chineseall.reader.lib.reader.config.ReaderConfigWrapper
import com.chineseall.reader.lib.reader.config.TangYuanSharedPrefUtils
import com.chineseall.reader.lib.reader.core.ReaderClient
import com.chineseall.reader.lib.reader.entities.Chapter
import com.chineseall.reader.lib.reader.utils.PaintHelper
import com.chineseall.reader.lib.reader.view.ILockView
import com.chineseall.reader.lib.reader.view.ReaderView
import com.col.lib_book.ext.toast
import com.col.lib_book.base.ApiCodeException
import com.col.lib_book.base.BaseActivity
import com.col.lib_book.callbacks.*
import com.col.lib_book.utils.ActivityStackManager
import com.col.lib_book.utils.LogUtils
import com.col.lib_book.utils.TangYuanReadConfig
import com.col.lib_book.utils.Utils
import kotlinx.coroutines.CoroutineExceptionHandler
import retrofit2.HttpException
import skin.support.SkinCompatManager
import splitties.init.appCtx
import java.net.ConnectException
import java.net.SocketException
import java.net.SocketTimeoutException
import java.net.UnknownHostException
import java.util.*

object ReadManager {
    var mAppId: String = "1207"
    var secret: String = "asdf!@"

    val DEFAULT_READER_AD_INTERVAL_NUM: Int = 3
    val isDebug: Boolean = BuildConfig.DEBUG
    private var mOnReadNightModeChangeListener: OnReadNightModeChangeListener? = null
    private var mOnActivityListener: OnActivityListener? = null
    private var mOnBaseListener: OnBaseListener? = null
    private var mOnPageChangeListener: OnPageChangeListener? = null
    private var mOnReadCallBack: OnReadCallBack? = null
    private var mReadCallBackPeriod: Int = -1
    private var adProvider: AdProvider? = null
    private var readPageVideoADLoadErrorIsCloseDialog = true
    private lateinit var mDeviceInfo: Map<String, Any>

    init {
        initDeviceInfo()
    }


    /**
     * 阅读器看完视频广告 真实阅读时长统计规则模式
     * false 自然时长
     * true 在阅读器内真实阅读累计时长
     */
    private var realReadMode = false
    var exceptionHandler = CoroutineExceptionHandler { _, throwable ->
        LogUtils.d("11111---------发生未知错误---------")
        throwable.printStackTrace()
        val act = ActivityStackManager.getInstance().topActivity
        if (act is BaseActivity) {
            act.handleApiException(throwable)
        } else {
            handleException(throwable)
        }
    }

    private fun initDeviceInfo() {
        val deviceInfo: MutableMap<String, Any> = HashMap()
        run {
            deviceInfo["os"] = "Android"
            deviceInfo["os_version"] =
                if (Build.VERSION.RELEASE == null) "UNKNOWN" else Build.VERSION.RELEASE
            deviceInfo["model"] = if (Build.MODEL == null) "UNKNOWN" else Build.MODEL
            try {
                deviceInfo["app_version"] = "1.1.0"
            } catch (e: Exception) {
            }
            if (Utils.checkHasPermission(
                    appCtx,
                    "android.permission.READ_PHONE_STATE"
                )
            ) {
                val provider: String =
                    Utils.getProvider(appCtx)
                if (!TextUtils.isEmpty(provider)) {
                    deviceInfo["carrier"] = provider
                }
            }
            val androidID: String =
                Utils.getAndroidID(appCtx)
            if (!TextUtils.isEmpty(androidID)) {
                deviceInfo["unique_id"] = androidID
            }
            deviceInfo.put(
                "brand",
                if (Build.BRAND == null) "UNKNOWN" else Build.BRAND
            )
        }
        mDeviceInfo = Collections.unmodifiableMap(deviceInfo)
    }

    fun getDeviceInfo(): Map<String, Any> {
        return mDeviceInfo
    }


    /**
     * 阅读页夜间模式切换回调
     */
    fun setOnReadNightModeChangeListener(onReadNightModeChangeListener: OnReadNightModeChangeListener) {
        mOnReadNightModeChangeListener = onReadNightModeChangeListener;
    }

    fun getOnReadNightModeChangeListener(): OnReadNightModeChangeListener? {
        return mOnReadNightModeChangeListener
    }

    var getApplication = appCtx

    fun handleException(throwable: Any) {
        val msg: String = getErrorMsg(throwable)
        toast(msg)
    }

    fun getErrorMsg(throwable: Any): String {
        val msg: String
        when (throwable) {
            is HttpException -> {
                msg = "网络错误"
            }
            is ConnectException -> {
                msg = "连接错误"
            }
            is UnknownHostException -> {
                msg = "域名解析错误"
            }
            is SocketTimeoutException -> {
                msg = "连接超时"
            }
            is SocketException -> {
                msg = "网络错误"
            }
            is ApiCodeException -> {
                msg = throwable.localizedMessage ?: "未知错误"
            }
            else -> {
                msg = "未知错误"
            }
        }
        return msg
    }

    fun setActivityListener(onActivityListener: OnActivityListener) {
        mOnActivityListener = onActivityListener
    }

    fun getActivityListener(): OnActivityListener? {
        return mOnActivityListener
    }

    fun setOnBaseListener(onBaseListener: OnBaseListener) {
        mOnBaseListener = onBaseListener;
    }

    fun getOnBaseListener(): OnBaseListener? {
        return mOnBaseListener;
    }

    /**
     * 设置阅读页看完视频广告 阅读时长统计规则模式
     */
    fun setVideoADRealReadMode(mode: Boolean) {
        this.realReadMode = mode
    }

    fun getVideoAdRealReadMode(): Boolean {
        return realReadMode
    }

    /**
     * 阅读页翻页实时监听
     */
    fun setOnPageChangeListener(onPageChangeListener: OnPageChangeListener) {
        mOnPageChangeListener = onPageChangeListener
    }

    fun getOnPageChangeListener(): OnPageChangeListener? {
        return mOnPageChangeListener
    }


    /**
     * 阅读时的回调
     * @param callBack 回调函数
     * @param period 回调周期，每间隔period时间回调一次，单位毫秒
     */
    fun setOnReadCallBack(callBack: OnReadCallBack, period: Int) {
        mOnReadCallBack = callBack
        mReadCallBackPeriod = period
    }

    fun getOnReadCallBack(): OnReadCallBack? {
        return mOnReadCallBack
    }

    fun getReadCallBackPeriod(): Int {
        return mReadCallBackPeriod
    }

    fun getUserId(): String {
        return "11111"
    }


    fun getAppId(): String {
        return mAppId
    }

    fun setAppId(appId: String) {
        mAppId = appId
    }

    fun getTimestamp(): String {
        return (System.currentTimeMillis() / 1000).toString()
    }

    fun getAdProvider(): AdProvider? {
        return adProvider
    }

    /**
     * 设置阅读页广告弹窗 点击观看视频，广告请求失败情况下是否关闭弹窗 （默认关闭弹窗，关闭弹窗用户可以继续阅读）
     */
    fun setReadPageVideoADLoadErrorIsCloseDialog(isClose: Boolean) {
        readPageVideoADLoadErrorIsCloseDialog = isClose
    }

    fun getReadPageVideoADLoadErrorIsCloseDialog(): Boolean {
        return readPageVideoADLoadErrorIsCloseDialog
    }

    fun initSdk(application: Application, appId: String = "1207", appSecret: String = "asdf!@") {
        this.mAppId = appId
        this.secret = appSecret
        SkinCompatManager.init(application)
        SkinCompatManager.withoutActivity(application)
            .setSkinStatusBarColorEnable(false)
            .loadSkin()
        TangYuanSharedPrefUtils.initSharePref(appCtx)
        ReaderConfigWrapper.init(TangYuanReadConfig(appCtx))
        PaintHelper.init(ReaderConfigWrapper.getInstance())
        ReaderClient.Builder(appCtx)
            .drawLockView(object : OnLockViewCallBack {
                override fun draw(
                    readerView: ReaderView,
                    lockView: ILockView,
                    chapter: Chapter,
                ) {
                }

                override fun initInstance(context: Context): ILockView {
                    return object : ILockView {
                        override fun setCanDraw(flag: Boolean) {}

                        override fun getLockHeight(): Int {
                            return 0
                        }

                        override fun dispatchTouchEvent(ev: MotionEvent): Boolean {
                            return false
                        }
                    }
                }
            })
            .readComplete {

            }
            .log { tag, msg ->

            }
            .adViewProvider(object : OnAdViewCallBack {
                override fun get(receiver: OnAdViewReceiver?) {

                }

                override fun adClick(adView: View?) {

                }

                override fun adShow(adView: View?) {

                }
            })
            .build()
    }

}