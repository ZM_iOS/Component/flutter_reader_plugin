package com.col.lib_book

import android.app.Dialog
import android.content.Context
import android.os.Handler
import android.os.Looper
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.chineseall.reader.lib.reader.callbacks.OnAdViewReceiver
import com.chineseall.reader.lib.reader.config.TangYuanSharedPrefUtils
import com.chineseall.reader.lib.reader.core.Constants
import com.col.lib_book.ext.toast
import com.col.lib_book.utils.ActivityStackManager
import com.col.lib_book.utils.DateUtil
import com.col.lib_book.widget.dialog.ReaderAdDialog
import com.col.lib_book.widget.dialog.ThanksDialog

/**
 * Created by huangzhengneng on 12/19/20.
 */
class ReadPageAdManager {

    /**
     * //首次 弹框广告显示 完成时间
     */
    private var prevScreenAdTime = 0L

    /**
     * //首次 激励视频弹框  完成时间
     */
    private var lastFirstShowDialogTime = 0L
    private var adDialog: Dialog? = null
    private var adThanksDialog: Dialog? = null
    private var showThanksDialog = false

    /**
     * 首次 激励视频 标记
     */
    private var mFirstReadToday = true
    private var mShowDialogAd = false

    /**
     * // 30分钟 激励视频弹框 完成时间
     */
    private var prevDialogAdTime = 0L
    private var mNeedShowFirstDialog = false

    /**
     * 真实阅读时间
     */
    private var realReadTime = 0L
    private var realReadTimeMode = false

    private val mHandler = Handler(Looper.getMainLooper())

    /**
     * // 30分钟 弹窗广告显示间隔
     */
    private var dialogAdInterval = 0L


    /**
     * // 每日首次阅读x分钟显示弹窗广告
     */
    private var dialogAdIntervalFirst = 0L

    /**
     * // 检测间隔
     */
    private val timerPeriod = 1000 * 3L

    private var sharedPrefUtil: TangYuanSharedPrefUtils = TangYuanSharedPrefUtils.getInstance()
    private lateinit var mContext: Context

    fun init(context: Context) {
        mContext = context
        realReadTime = TangYuanSharedPrefUtils.getInstance().getLong(
            "realReadTime",
            0
        )
        realReadTimeMode = ReadManager.getVideoAdRealReadMode()
        dialogAdIntervalFirst = TangYuanSharedPrefUtils.getInstance().getLong(
            "readerFirstDialogAdInterval",
            0
        )
        dialogAdInterval = TangYuanSharedPrefUtils.getInstance()
            .getLong("readerDialogAdInterval", 0)

        Constants.CHAPTERAD_INTERVALNUM = TangYuanSharedPrefUtils.getInstance()
            .getInt("readerAdIntervalNum", ReadManager.DEFAULT_READER_AD_INTERVAL_NUM)

        prevDialogAdTime =
            sharedPrefUtil.getLong("prevShowReadDialogAdTime", System.currentTimeMillis())
        lastFirstShowDialogTime = sharedPrefUtil.getLong("lastFirstShowDialogTime", 0)
        prevScreenAdTime = sharedPrefUtil.getLong("prevShowReadScreenAdTime", 0)

        if (lastFirstShowDialogTime != 0L && DateUtil.isToday(lastFirstShowDialogTime)) {
            mFirstReadToday = false
        } else {
            lastFirstShowDialogTime = 0
        }
        if (lastFirstShowDialogTime == 0L && prevScreenAdTime > 0 && DateUtil.isToday(
                prevScreenAdTime
            )
        ) {
            lastFirstShowDialogTime = prevScreenAdTime
        }
        if (!DateUtil.isToday(prevDialogAdTime)) {
            // 次日清零，如果不是今日，按每日首次5分钟开始算
            // 修改上次显示弹窗广告时间为现在
            sharedPrefUtil.putLong(
                "prevShowReadDialogAdTime",
                System.currentTimeMillis()
            )
        }

        checkScreenAd(context)
    }

    private fun check() {
        if (!mShowDialogAd) {
            if (mFirstReadToday) {
                // 阅读时长使用的是开机时间来计算的，这里也需要用开机时间去计算，不影响30分钟间隔的弹框广告
                if (lastFirstShowDialogTime == 0L) {
                    return
                }
                if (System.currentTimeMillis() - lastFirstShowDialogTime >= dialogAdIntervalFirst && dialogAdIntervalFirst > 0) {
                    // 每日首次阅读5分钟后，下一次章节变化时弹出
                    mShowDialogAd = true
                    mFirstReadToday = false
                    mNeedShowFirstDialog = true
                }
            } else {
                if (realReadTimeMode) {
                    if (realReadTime <= 0) {
                        mShowDialogAd = true
                    }
                } else {
                    prevDialogAdTime = sharedPrefUtil.getLong("prevShowReadDialogAdTime", 0)
                    checkFirstRead(prevDialogAdTime)
                    if (System.currentTimeMillis() - prevDialogAdTime >= dialogAdInterval && dialogAdInterval > 0) {
                        // 首次5分钟弹窗后，下一次30分钟后章节变化时弹出
                        mShowDialogAd = true
                    }
                }
            }
        }
    }

    fun resume(context: Context) {
        startAdTimer()

        if (showThanksDialog) {
            showThanksDialog(context)
        }
    }

    fun stopTimer() {
        mHandler.removeCallbacksAndMessages(null)
    }

    fun destory() {
        stopTimer()
        if (adDialog?.isShowing == true) {
            adDialog?.dismiss()
        }
    }

    fun requestDialogAd(context: Context, readTimeCha: Long, block: () -> Unit) {
        if (realReadTimeMode) {
            realReadTime -= readTimeCha
        }
        check()
        if (mShowDialogAd
            && (adDialog == null || adDialog?.isShowing == false)
        ) {
            var adGetSuccess = false
            showThanksDialog = false
            val builder = ReaderAdDialog.Builder(context)
                .setTitle("观看1次视频广告，可免费阅读" + (dialogAdInterval / 60 / 1000) + "分钟")
                .setNegativeButton("取消") { _, _ ->
                    // 取消直接退出页面
                    block()
                }
                .setPositiveButton("观看视频") { _, _ ->
                    onAdDialogConfirmClick(adGetSuccess, context)
                }
            adDialog = builder.createNew()


            ReadManager.getAdProvider()
                ?.getReaderDialogAd(builder.adViewContainer, object : OnAdViewReceiver {
                    override fun onReceive(success: Boolean, view: View?) {
                        if (inReadActivity()) {
                            adGetSuccess = success
                            if (adDialog?.isShowing == false) {
                                adDialog?.show()
                            }
                        }
                    }

                    override fun onAdClosed() {
                    }

                }, adDialog)
        }
    }

    private fun checkScreenAd(context: Context) {

        if (!DateUtil.isToday(prevScreenAdTime)) {
            val builder = ReaderAdDialog.Builder(context)
            val dialog = builder.create()

            ReadManager.getAdProvider()
                ?.getReaderScreenAd(builder.adViewContainer, object : OnAdViewReceiver {
                    override fun onReceive(success: Boolean, view: View?) {
                        if (success && view != null && inReadActivity()) {
                            if (view.parent != null) {
                                (view.parent as ViewGroup).removeView(view)
                            }
                            dialog.show()
                            builder.adViewContainer.addView(view)

                        }
                        sharedPrefUtil.putLong(
                            "prevShowReadScreenAdTime",
                            System.currentTimeMillis()
                        )
                        lastFirstShowDialogTime = System.currentTimeMillis()
                    }

                    override fun onAdClosed() {
                    }
                }, dialog)
        }
    }

    private fun inReadActivity() =
        ActivityStackManager.getInstance().topActivity is ReadActivity && !ActivityStackManager.getInstance().topActivity.isFinishing

    private fun startAdTimer() {
        mHandler.postDelayed({
            check()
            startAdTimer()
        }, timerPeriod)
    }


    private fun checkFirstRead(prevDialogAdTime: Long) {
//        if(mShowDialogAd)
//            return
//        mShowDialogAd = System.currentTimeMillis() - prevDialogAdTime >= dialogAdInterval
    }

    /**
     * 点击
     */
    private fun onAdDialogConfirmClick(success: Boolean, context: Context) {
        if (success) {
            prevDialogAdTime = System.currentTimeMillis()
            mShowDialogAd = false
            ReadManager.getAdProvider()?.onAdDialogPositiveClick(adDialog) {
                if (adDialog?.isShowing == true) {
                    adDialog?.dismiss()
                }
                prevDialogAdTime = System.currentTimeMillis()
                mShowDialogAd = false
                sharedPrefUtil.putLong(
                    "prevShowReadDialogAdTime",
                    System.currentTimeMillis()
                )
                realReadTime = dialogAdInterval
                sharedPrefUtil.putLong(
                    "realReadTime",
                    dialogAdInterval
                )
                if (mNeedShowFirstDialog) {
                    mNeedShowFirstDialog = false
                    sharedPrefUtil.putLong(
                        "lastFirstShowDialogTime",
                        System.currentTimeMillis()
                    )
                }
                checkFirstRead(sharedPrefUtil.getLong("prevShowReadDialogAdTime", 0))
                showThanksDialog = true

                if (context != null) {
                    resume(context)
                }

            }

//            if (!NetworkUtils.isNetworkAvailable(mContext)) {
//                ToastUtil.showSystemToast("网络不可用，请检查网络")
//            }
        } else {
            if (ReadManager.getReadPageVideoADLoadErrorIsCloseDialog()) {
                mShowDialogAd = false
                if (adDialog?.isShowing == true) {
                    adDialog?.dismiss()
                }
                toast("视频广告播放失败！")
            } else {
                Toast.makeText(mContext, "视频广告播放失败！", Toast.LENGTH_LONG).show();
            }
        }

    }

    private fun showThanksDialog(context: Context) {
        if (adDialog?.isShowing == true) {
            adDialog?.dismiss()
        }

        val builder = ThanksDialog.Builder(context)
            .setTitle("恭喜解锁" + (dialogAdInterval / 60 / 1000) + "分钟，继续免费阅读吧~")
            .setPositiveButton("我知道了") { _, _ ->
                adThanksDialog?.dismiss()
            }
        adThanksDialog = builder.createThanks()
        if (inReadActivity()) {
            adThanksDialog?.show()
        }
        showThanksDialog = false
    }
}