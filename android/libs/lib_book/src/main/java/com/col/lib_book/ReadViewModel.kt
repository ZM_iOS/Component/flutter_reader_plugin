package com.col.lib_book


import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.chineseall.reader.lib.reader.entities.Chapter
import com.col.lib_book.data.BookContentRespository
import com.col.lib_book.data.BookDTO
import com.col.lib_book.data.ChapterBean
import com.col.lib_book.data.ChapterContent
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

/**
 * Created by huangzhengneng on 2020/11/11.
 */
class ReadViewModel  constructor(private val respository: BookContentRespository) :
    ViewModel() {

    var mBookId: MutableLiveData<String> = MutableLiveData()

    var mChapterContent: MutableLiveData<ChapterContent> = MutableLiveData()
    var mChapterBeanList: MutableLiveData<List<ChapterBean>> = MutableLiveData()
    var mChapterList: MutableLiveData<List<Chapter>> = MutableLiveData()
    var mCurrChapter: MutableLiveData<ChapterBean> = MutableLiveData()

    var mInBookShelf: MutableLiveData<Boolean> = MutableLiveData()
    var mBookInfo: MutableLiveData<BookDTO> = MutableLiveData()

    fun getRespository(): BookContentRespository {
        return respository
    }

    suspend fun getBookContent(
        bookId: String,
        chapterId: String,
    ) {
        val chapterContent = respository.getChapterContent(bookId, chapterId)
        mChapterContent.postValue(chapterContent)
    }

    suspend fun getChapterList(bookId: String): List<ChapterBean> {

        val response = respository.getChapterList(bookId)
        val chapters = mutableListOf<ChapterBean>()
        response.volumes.forEach {
            if (it.chapterList.isNotEmpty()) {
                it.chapterList.forEach { chapter ->
                    chapter.bookName = it.bookName
                    chapters.add(chapter)
                }
            }
        }
        mBookInfo.postValue(response.bookInfo)

        return chapters
    }

    suspend fun checkInBookShelf() {
        val ans = respository.checkInBookShelf(mBookId.value ?: "-1")
        withContext(Dispatchers.Main) {
            mInBookShelf.value = ans
        }
    }

    suspend fun addBookShelf(chapterId: String, progress: Int): Boolean {
        val chapterNum= mBookInfo.value?.chapterNum?:0

        var chapter: ChapterBean = ChapterBean(mBookId.value?.toLong() ?: 0,
            chapterId.toLong(),
            0,
            "",
            0,
            "",
            "",
            0,
            0,
            0,
            ""
        )
        mChapterBeanList.value?.forEach {
            if (it.chapterId == chapterId.toLong()) {
                chapter = it
            }
        }

        return respository.addBookShelf(mBookId.value ?: "", chapterId, progress, chapter,chapterNum.toInt())
    }
}