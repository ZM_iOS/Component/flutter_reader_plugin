package com.col.lib_book.api

import com.col.lib_book.ReadManager
import com.col.lib_book.data.*
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*
import java.util.concurrent.TimeUnit

/**
 * <pre>
 *     author : Freedom
 *     e-mail : burst3166@gmail.com
 *     time   : 2020/11/09
 *     desc   :
 *     version: 1.0
 * </pre>
 */

interface BookService {



    @POST("/union/{partnerId}/collect/book/{timestamp}")
    suspend fun postAddBookshelf(
        @Path("partnerId") partnerId: String,
        @Path("timestamp") timestamp: String,
        @Body() book: BookShelfItem
    ): BaseBean<BookShelfItem>

    @FormUrlEncoded
    @POST("/union/{partnerId}/collect/book/remove/{timestamp}")
    suspend fun postDelBookshelf(
        @Path("partnerId") partnerId: String,
        @Path("timestamp") timestamp: String,
        @Field("userId") userId: String,
        @Field("bookIds") bookIds: String,
        @Field("categoryId") categoryId: String,
    ): BaseBean<Boolean>

    @GET("/union/{partnerId}/collect/book/list/{timestamp}")
    suspend fun getUserBookshelf(
        @Path("partnerId") partnerId: String,
        @Path("timestamp") timestamp: String,
        @Query("userId") userId: String,
        @Query("categoryId") categoryId: String,
        @Query("offset") offset: Int = 0,
        @Query("count") count: Int = 36,
    ): BaseBean<BaseListData<BookShelfItem>>

    @GET("/lisToRead/sdk/{partnerId}/book/{bookId}/home/{timestamp}")
    suspend fun getBookDetail(
        @Path("partnerId") partnerId: String,
        @Path("timestamp") timestamp: String,
        @Path("bookId") bookId: String,
        @Query("page") page: Int,
        @Query("userId") userId: String,
        @Query("firstChapterFlag") firstChapterFlag: Boolean = true,
    ): BaseBean<List<BookDetailItem>>

    @GET("/lisToRead/sdk/{partnerId}/chapter/{bookId}/{chapterId}/content/{timestamp}")
    suspend fun getChapterContent(
        @Path("partnerId") partnerId: String,
        @Path("timestamp") timestamp: String,
        @Path("bookId") bookId: String,
        @Path("chapterId") chapterId: String,
    ): ChapterContentResponse

    @GET("/lisToRead/sdk/{partnerId}/chapter/{bookId}/list/{timestamp}")
    suspend fun getChapterList(
        @Path("partnerId") partnerId: String,
        @Path("timestamp") timestamp: String,
        @Path("bookId") bookId: String,
    ): ChapterListResponse

    @GET("/lisToRead/sdk/{partnerId}/book/endPageRecommend/{timestamp}")
    suspend fun getBookEndRecommendBooks(
        @Path("partnerId") partnerId: String,
        @Path("timestamp") timestamp: String,
        @Query("bookId") bookId: String,
        @Query("userId") userId: String
    ): BaseBean<List<BookDTO>>


    @GET("/lisToRead/sdk/{partnerId}/book/guessLike/{timestamp}")
    suspend fun getGuessYouLikeListInSearch(
        @Path("partnerId") partnerId: String,
        @Path("timestamp") timestamp: String,
        @Query("userId") userId: String
    ): BaseBean<List<BookDTO>>


    @GET("/lisToRead/sdk/{partnerId}/book/associative/{timestamp}")
    suspend fun getAssociAtiveListInSearch(
        @Path("partnerId") partnerId: String,
        @Path("timestamp") timestamp: String,
        @Query("searchName") searchName: String
    ): BaseBean<List<BookDTO>>



    companion object {
        private const val BASE_URL = "http://league.17k.com//"

        @Volatile private var instance: BookService? = null

        fun getInstance(): BookService {
            return instance ?: synchronized(this) {
                instance ?: create().also { instance = it }
            }
        }

        private fun create(): BookService {
            val logger =
                HttpLoggingInterceptor().apply {
                    level =
                        if (ReadManager.isDebug) HttpLoggingInterceptor.Level.BASIC
                        else HttpLoggingInterceptor.Level.NONE
                }

            val headerInterceptor = HeaderInterceptor()
            val client = OkHttpClient.Builder()
                .connectTimeout(15, TimeUnit.SECONDS)
                .readTimeout(15, TimeUnit.SECONDS)
                .writeTimeout(15, TimeUnit.SECONDS)
                .addInterceptor(headerInterceptor)
                .addInterceptor(logger)
                .build()

            return Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(client)
                .addCallAdapterFactory(LiveDataCallAdapterFactory())
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(BookService::class.java)
        }
    }
}