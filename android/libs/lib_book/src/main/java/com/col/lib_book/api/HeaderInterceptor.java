package com.col.lib_book.api;


import android.net.Uri;
import android.text.TextUtils;

import com.chineseall.reader.utils.Protect;
import com.col.lib_book.ReadManager;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.reflect.TypeToken;
import com.warkiz.widget.BuildConfig;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import okhttp3.FormBody;
import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okio.Buffer;

/**
 * Retrofit2 Cookie拦截器。用于保存和设置Cookies
 *
 * @author yuyh.
 * @date 16/8/6.
 */
public final class HeaderInterceptor implements Interceptor {

    private Map<String, String> commonParams = new HashMap<>();

    private void initCommonParams() {
        Map<String, Object> deviceInfo = ReadManager.INSTANCE.getDeviceInfo();
        for (Map.Entry<String, Object> item : deviceInfo.entrySet()) {
            commonParams.put(item.getKey(), item.getValue() + "");

        }
        String loginId = ReadManager.INSTANCE.getUserId();
        if (!TextUtils.isEmpty(loginId)) {
            commonParams.put("user_key", loginId +"");
            commonParams.put("userId", loginId +"");
        }
        commonParams.put("channel_code", ReadManager.INSTANCE.getAppId() + "");
        commonParams.put("platform", "2");//android 2
        commonParams.put("visit_time", System.currentTimeMillis() + "");
        commonParams.put("sdk_version", BuildConfig.VERSION_CODE + "");
    }

    @Override
    public Response intercept(Chain chain) throws IOException {
        initCommonParams();
        Request oldRequest = chain.request();
        Request.Builder newRequestBuild = null;
        String method = oldRequest.method();
        Request newRequest;
//        boolean hasToken = false;

//        String uri = Uri.decode(oldRequest.url().encodedPath());
        RequestBody body = oldRequest.body();
        if ("GET".equals(method) || "DELETE".equals(method) || body instanceof MultipartBody) {

            HttpUrl.Builder commonParamsUrlBuilder = oldRequest.url()
                    .newBuilder()
                    .scheme(oldRequest.url().scheme())
                    .host(oldRequest.url().host());
            for (Map.Entry<String, String> item : commonParams.entrySet()) {
                commonParamsUrlBuilder.addQueryParameter(item.getKey(), item.getValue());
            }
            String encode = Protect.encode(Uri.decode(oldRequest.url().encodedPath()));
            commonParamsUrlBuilder.addQueryParameter("sign", encode);
//            String accessToken = oldRequest.url().queryParameter(StringConstants.ACCESS_TOKEN);
//            if (!TextUtils.isEmpty(accessToken)) {
//                hasToken = true;
//                         commonParamsUrlBuilder.addQueryParameter("accessToken","1");
//            }
            newRequestBuild = oldRequest.newBuilder()
                    .method(oldRequest.method(), body)
                    .url(commonParamsUrlBuilder.build());

        } else {

            if (body instanceof FormBody) {
                FormBody.Builder newFormBody = new FormBody.Builder();
                FormBody oldFormBody = (FormBody) body;
                for (int i = 0; i < oldFormBody.size(); i++) {
                    String paramName = oldFormBody.encodedName(i);
//                    if (StringConstants.ACCESS_TOKEN.equals(paramName)) {
//                    newFormBody.addEncoded("accessToken",  "1");
//                        hasToken = true;
//                    } else {
                    newFormBody.addEncoded(paramName, oldFormBody.encodedValue(i));
//                    }

                }
                for (Map.Entry<String, String> item : commonParams.entrySet()) {
                    newFormBody.add(item.getKey(), item.getValue());
                }
                newFormBody.add("sign", Protect.encode(oldRequest.url().uri().getPath()));


                newRequestBuild = oldRequest.newBuilder().method(oldRequest.method(), newFormBody.build());
            } else if (body instanceof RequestBody) {
                //buffer流
                TreeMap<String, Object> rootMap = null;
                Gson mGson = new GsonBuilder()
                        .registerTypeAdapter(
                                new TypeToken<TreeMap<String, Object>>(){}.getType(),
                                new JsonDeserializer<TreeMap<String, Object>>() {
                                    @Override
                                    public TreeMap<String, Object> deserialize(
                                            JsonElement json, Type typeOfT,
                                            JsonDeserializationContext context) throws JsonParseException {


                                        TreeMap<String, Object> treeMap = new TreeMap<>();
                                        JsonObject jsonObject = json.getAsJsonObject();
                                        Set<Map.Entry<String, JsonElement>> entrySet = jsonObject.entrySet();
                                        for (Map.Entry<String, JsonElement> entry : entrySet) {
                                            treeMap.put(entry.getKey(), entry.getValue());
                                        }
                                        return treeMap;
                                    }
                                }).create();
                Buffer buffer = new Buffer();
                body.writeTo(buffer);
                String oldParamsJson = buffer.readUtf8();
                rootMap = mGson.fromJson(oldParamsJson, new TypeToken<TreeMap<String, Object>>(){}.getType());  //原始参数
                if (rootMap == null) {
                    rootMap = new TreeMap<>();
                }

                for (Map.Entry<String, String> item : commonParams.entrySet()) {
                    rootMap.put(item.getKey(), item.getValue());
                }
                String encode = Protect.encode(oldRequest.url().uri().getPath());
                rootMap.put("sign", encode);
                String newJsonParams = mGson.toJson(rootMap);  //装换成json字符串
                HttpUrl.Builder commonParamsUrlBuilder = oldRequest.url()
                        .newBuilder()
                        .scheme(oldRequest.url().scheme())
                        .host(oldRequest.url().host());
                commonParamsUrlBuilder.addQueryParameter("sign",encode).addQueryParameter("platform","2");
                newRequestBuild = oldRequest.newBuilder().url(commonParamsUrlBuilder.build()).post(RequestBody.create(MediaType.parse("application/json; charset=utf-8"), newJsonParams));
            }
        }

        newRequest = newRequestBuild
                .addHeader("Connection", "close")
                .build();

        return chain.proceed(newRequest);
    }
}
