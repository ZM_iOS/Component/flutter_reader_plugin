package com.col.lib_book.api

import androidx.paging.LoadState
import com.col.lib_book.ext.toast
import com.col.lib_book.base.ApiCodeException
import retrofit2.HttpException
import java.net.ConnectException
import java.net.SocketException
import java.net.SocketTimeoutException
import java.net.UnknownHostException

/**
 * Created by huangzhengneng on 12/7/20.
 */
object PagingAdapterLoadStateHandler  {

    fun handleLoadStateError(refresh: LoadState.Error) {
        when (refresh?.error) {
            is HttpException -> {
                toast("网络错误")
            }

            is ConnectException -> {

                toast("连接错误")
            }

            is UnknownHostException -> {

                toast("域名解析错误")
            }


            is SocketTimeoutException -> {

                toast("连接超时")
            }

            is SocketException -> {

                toast("网络错误")
            }
            is ApiCodeException ->{
                toast(refresh?.error?.localizedMessage+"")
            }
            else ->{
                toast("未知错误")
            }
        }
    }

}