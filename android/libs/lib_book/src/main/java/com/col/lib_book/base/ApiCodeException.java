package com.col.lib_book.base;


public class ApiCodeException extends RuntimeException {
    public int getErrorCode() {
        return mErrorCode;
    }

    public void setErrorCode(int errorCode) {
        mErrorCode = errorCode;
    }

    private int mErrorCode;

    public ApiCodeException(int errorCode, String errorMessage) {
        super(errorMessage);
        mErrorCode = errorCode;
    }


}