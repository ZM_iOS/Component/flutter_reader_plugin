package com.col.lib_book.base

import android.app.Activity
import android.os.Build
import android.os.Bundle
import android.view.View
import android.view.WindowInsetsController
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import androidx.appcompat.app.SkinAppCompatDelegateImpl
import com.col.lib_book.ReadManager
import com.col.lib_book.utils.ActivityStackManager
import com.col.lib_book.utils.statusbar.StatusBarUtil
import com.col.lib_book.widget.dialog.CustomProgressDialog
import skin.support.app.SkinActivityLifecycle
import java.lang.ref.WeakReference

/**
 * <pre>
 *     author : Freedom
 *     e-mail : burst3166@gmail.com
 *     time   : 2020/12/04
 *     desc   :
 *     version: 1.0
 * </pre>
 */
open class BaseActivity: AppCompatActivity() {
    private var mWeakReference: WeakReference<Activity>? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        ReadManager.getOnBaseListener()?.onBaseActivityOnCreateBefore()
        SkinActivityLifecycle.init().onActivityCreated(this)
        super.onCreate(savedInstanceState)
        mWeakReference = WeakReference<Activity>(this)
        ActivityStackManager.getInstance().addActivity(mWeakReference)
        ReadManager.getActivityListener()?.onCreate(this)
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.R) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
            }
        } else {
            try {
                window.setDecorFitsSystemWindows(false)
                val controller = window.insetsController
                controller?.setSystemBarsAppearance(
                    WindowInsetsController.APPEARANCE_LIGHT_STATUS_BARS,
                    WindowInsetsController.APPEARANCE_LIGHT_STATUS_BARS
                )
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

        StatusBarUtil.setStatusBarDarkTheme(this, isStatusBarDarkMode())
    }

    override fun onResume() {
        super.onResume()
        SkinActivityLifecycle.init().onActivityResumed(this)
        ReadManager.getActivityListener()?.onResume(this)
    }

    override fun onStop() {
        super.onStop()
        ReadManager.getActivityListener()?.onStop(this)
    }

    override fun onDestroy() {
        SkinActivityLifecycle.init().onActivityDestroyed(this)
        super.onDestroy()
        ReadManager.getActivityListener()?.onDestroy(this)
        ActivityStackManager.getInstance().removeActivity(mWeakReference)
    }

    private var dialog: CustomProgressDialog? = null

    fun dismissProgress() {
        if (dialog?.isShowing == true) {
            dialog?.dismiss()
        }
    }

    fun showProgress() {
        if (dialog == null) {
            dialog = CustomProgressDialog(
                this
            ).instance(this)
        }
        if (dialog?.isShowing == false) {
            dialog?.show()
        }
    }

    open fun handleApiException(throwable: Throwable) {
        dismissProgress()
        ReadManager.handleException(throwable)
    }

    override fun getDelegate(): AppCompatDelegate {
        return SkinAppCompatDelegateImpl.get(this, this);
    }

    open fun isStatusBarDarkMode(): Boolean {
        return true
    }
}