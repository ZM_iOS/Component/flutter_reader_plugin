package com.col.lib_book.callbacks

import android.app.Dialog
import android.view.View
import com.chineseall.reader.lib.reader.callbacks.OnAdViewReceiver
import com.chineseall.reader.lib.reader.callbacks.OnListAdViewReceiver

/**
 * Created by huangzhengneng on 12/18/20.
 */
interface AdProvider {

    /**
     * 阅读页翻页广告
     */
    fun getReaderPageAd(receiver: OnAdViewReceiver)

    /**
     * 阅读页开屏广告
     * @param adViewContainer 广告view的容器
     */
    fun getReaderScreenAd(adViewContainer: View, receiver: OnAdViewReceiver, dialog: Dialog?)

    /**
     * 阅读页定时弹窗广告
     * @param adViewContainer 广告view的容器
     */
    fun getReaderDialogAd(
        adViewContainer: View,
        receiver: OnAdViewReceiver,
        dialog: Dialog?
    )

    /**
     * 阅读页弹框广告确认按钮的点击回调
     * @param dialog
     * @param block 对话框关闭回调，必须调用，否则计时每次切换章节都会出现弹窗
     */
    fun onAdDialogPositiveClick(dialog: Dialog?, block: () -> Unit)

    /**
     * 获取书末页广告
     */
    fun getReaderEndPageAd(receiver: OnAdViewReceiver)

    /**
     * 获取列表页广告
     */
    fun getListAd( adKey:String,receiver: OnListAdViewReceiver)
}