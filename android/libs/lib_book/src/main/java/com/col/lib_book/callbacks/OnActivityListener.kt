package com.col.lib_book.callbacks

import android.app.Activity

/**
 * <pre>
 *     author : xuxinghai
 *     e-mail : xuxh_6@163.com
 *     time   : 2021/09/18
 *     desc   :
 *     version: 1.0
 * </pre>
 */
interface OnActivityListener {
    fun onCreate(activity: Activity)
    fun onResume(activity: Activity)
    fun onStop(activity: Activity)
    fun onDestroy(activity: Activity)
}