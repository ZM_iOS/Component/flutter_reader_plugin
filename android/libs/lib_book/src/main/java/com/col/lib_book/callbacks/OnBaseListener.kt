package com.col.lib_book.callbacks

/**
 * <pre>
 *     author : xuxinghai
 *     e-mail : xuxh_6@163.com
 *     time   : 2021/09/18
 *     desc   :
 *     version: 1.0
 * </pre>
 */
interface OnBaseListener {
    fun onBaseActivityOnCreateBefore()
}