package com.col.lib_book.callbacks;

/**
 * <pre>
 *     author : xuxinghai
 *     e-mail : xuxh_6@163.com
 *     time   : 2021/06/17
 *     desc   :
 *     version: 1.0
 * </pre>
 */
public interface OnPageChangeListener {
    void onChapterChanged();
    void onPageChanged();
}
