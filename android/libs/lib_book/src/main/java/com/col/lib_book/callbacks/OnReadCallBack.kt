package com.col.lib_book.callbacks

/**
 * Created by huangzhengneng on 1/25/21.
 */
interface OnReadCallBack {

    fun onReceive(result: Map<String, Any>)

}