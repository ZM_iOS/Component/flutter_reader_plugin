package com.col.lib_book.callbacks;

/**
 * <pre>
 *     author : xuxinghai
 *     e-mail : xuxh_6@163.com
 *     time   : 2021/10/18
 *     desc   : 阅读页夜间模式切换监听
 *     version: 1.0
 * </pre>
 */
public interface OnReadNightModeChangeListener {
    void onNightModeChanged(boolean isNight);
}
