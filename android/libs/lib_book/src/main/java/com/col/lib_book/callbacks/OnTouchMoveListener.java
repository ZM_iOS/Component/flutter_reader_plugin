package com.col.lib_book.callbacks;

import android.view.MotionEvent;

/**
 * <pre>
 *     author : xuxinghai
 *     e-mail : xuxh_6@163.com
 *     time   : 2021/05/13
 *     desc   :
 *     version: 1.0
 * </pre>
 */
public interface OnTouchMoveListener {
    void onTouch(MotionEvent event);
}
