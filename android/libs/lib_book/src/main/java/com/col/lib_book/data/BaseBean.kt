package com.col.lib_book.data

import java.io.Serializable

/**
 * Created by Freedom on 16/10/18.
 */
class BaseBean<T> (
    var msg: String?,
    var code:Int?,
    var data: T?,
    var exception: String?,
    var sysTime: String?
):Serializable