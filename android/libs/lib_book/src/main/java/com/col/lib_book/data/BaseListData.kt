package com.col.lib_book.data

data class BaseListData<T>(
    val count: Int,
    val hasMore: Boolean,
    val items: ArrayList<T>,
    val offset: Int,
    val total: Int
)