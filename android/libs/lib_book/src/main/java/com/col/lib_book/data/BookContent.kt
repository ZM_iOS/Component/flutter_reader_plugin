package com.col.lib_book.data


import com.col.lib_book.api.BookService
import com.col.lib_book.ReadManager
import com.col.lib_book.base.ApiCodeException
import com.col.lib_book.database.BookshelfDao

/**
 * Created by huangzhengneng on 2020/11/11.
 */
data class ChapterListResponse(
    val code: Int,
    val `data`: DataBean,
    val exception: Any,
    val msg: Any,
    val sysTime: Long
)

data class DataBean(
    val bookInfo: BookDTO,
    val volumes: List<ChapterList>
)

data class ChapterList(
    val bookId: Long,
    val chapterList: List<ChapterBean>,
    val volumeId: Long,
    val volumeName: String,
    val bookName: String?
)

data class ChapterBean(
    val bookId: Long,
    val chapterId: Long,
    val chapterIndex: Int,
    val chapterName: String,
    val createTimeValue: Long,
    val isVIP: String,
    val updateDate: String,
    val updateTimeValue: Long,
    val volumeCode: Int,
    val volumeId: Long,
    var bookName: String?
) {
    companion object {
        fun newInstance(
            bookId: Long,
            chapterId: Long,
        ): ChapterBean {
            return ChapterBean(
                bookId, chapterId, 0, "", 0, "", "", 0, 0, 0, null
            )
        }
    }
}

data class ChapterContentResponse(
    val code: Int,
    val `data`: ChapterContent,
    val exception: Any,
    val msg: Any,
    val sysTime: Long
)

data class ChapterContent(
    val chapterContent: String,
    val chapterIndex: Int,
    val chapterName: String,
    val isRecommend: String,
    val isVIP: String,
    val wordCount: Int
)

class BookContentRespository constructor(
    private val bookshelfDao: BookshelfDao
) {

    suspend fun getChapterContent(
        bookId: String,
        chapterId: String,
    ): ChapterContent {
        val response = BookService.getInstance().getChapterContent(
            ReadManager.getAppId(),
            ReadManager.getTimestamp(),
            bookId,
            chapterId,
        )
        @Suppress("SENSELESS_COMPARISON")
        if (response.code == 0 && response.data != null) {
            return response.data
        } else {
            throw ApiCodeException(response.code, "${response.msg}")
        }
    }

    suspend fun getChapterList(bookId: String): DataBean {
        val response =
            BookService.getInstance().getChapterList(
                ReadManager.getAppId(),
                ReadManager.getTimestamp(),
                bookId,
            )
        @Suppress("SENSELESS_COMPARISON")
        if (response.code == 0 && response.data != null) {
            return response.data
        } else {
            throw ApiCodeException(response.code, "${response.msg}")
        }
    }

    @Suppress("SENSELESS_COMPARISON")
    fun checkInBookShelf(bookId: String): Boolean {
        return bookshelfDao.findById(bookId.toLong(), ReadManager.getUserId()) != null
    }

    suspend fun addBookShelf(
        bookId: String,
        chapterId: String,
        progress: Int,
        chapterBean: ChapterBean,
        chapterNum: Int
    ): Boolean {
        val bookShelfItem = BookShelfItem(
            BookDTO.newInstanceWithBookId(bookId.toLong()),
            0,
            ChapterBean.newInstance(bookId = bookId.toLong(), chapterId = chapterId.toLong()),
            0,
            progress,
            ReadManager.getUserId().toInt(),
            null,
            null,
            0
        )
        val response = BookService.getInstance().postAddBookshelf(
            ReadManager.getAppId(),
            ReadManager.getTimestamp(),
            bookShelfItem
        )

        @Suppress("SENSELESS_COMPARISON")
        if (response.code == 0 && response.data != null) {
            val ans = response.data
            if (ans != null) {
                // 更新数据库
                var item = bookshelfDao.findById(bookId.toLong(), ReadManager.getUserId())
                val bookmarkChapter = ans.bookmarkChapter
                val lastUpdateChapter = ans.lastUpdateChapter
                val chapterIndex = bookmarkChapter?.chapterIndex ?: 0
                var unReadChapterCount = chapterNum - chapterIndex;
                if (unReadChapterCount < 0) {
                    unReadChapterCount = 0
                }
                var lastChapterName = "";
                if (bookmarkChapter == null) {
                    lastChapterName = "未开始阅读"
                } else if (unReadChapterCount == 0) {
                    lastChapterName = "已读完最新章节"
                } else {
                    lastChapterName = unReadChapterCount.toString() + "章未读"
                }
                if (item == null) {
                    item = BookshelfLocal(
                        ans.book.bookId,
                        ans.bookAuthorId,
                        ans.categoryId,
                        ans.progress,
                        ans.userId,
                        ans.book.authorPenname,
                        ans.book.bookName,
                        ans.book.bookStatus,
                        ans.book.categoryName,
                        ans.book.channelName,
                        ans.book.className,
                        ans.book.coverImageUrl,
                        ans.book.introduction,
                        ans.book.keyWord,
                        ans.book.lastUpdateChapterDate,
                        ans.book.status,
                        ans.book.wordCount,
                        ans.bookmarkChapter?.chapterId,
                        ans.bookmarkChapter?.chapterIndex,
                        ans.bookmarkChapter?.chapterName,
                        ans.bookmarkChapter?.createTimeValue,
                        ans.bookmarkChapter?.isVIP,
                        ans.bookmarkChapter?.updateDate,
                        ans.bookmarkTimeValue ?: System.currentTimeMillis(),
                        ans.bookmarkChapter?.volumeCode,
                        ans.bookmarkChapter?.volumeId,
                        false,
                        lastChapterName,
                        lastUpdateChapter?.chapterId ?: 0,
                        lastUpdateChapter?.updateTimeValue,
                    )
                    bookshelfDao.insert(item)
                } else {
                    item.progress = progress
                    item.chapterId = chapterId.toLong()
                    item.chapterIndex = chapterBean.chapterIndex
                    item.chapterName = chapterBean.chapterName
                    item.volumeId = chapterBean.volumeId
                    item.volumeCode = chapterBean.volumeCode
                    item.update = false
                    item.lastUpdateChapterName = lastChapterName
                    item.updateTimeValue = System.currentTimeMillis()
                    bookshelfDao.update(item)
                }

                var addItem = bookshelfDao.findById(-1, ReadManager.getUserId())
                if (addItem == null) {
                    //添加+号
                    val addData = item
                    addData?.bookId = -1
                    addData?.updateTimeValue = 0
                    bookshelfDao.insert(addData)
                }
                return true
            }
        } else {
            throw ApiCodeException(response.code ?: -1, "${response.msg}")
        }
        return false
    }
}