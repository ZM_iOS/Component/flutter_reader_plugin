package com.col.lib_book.data

import com.google.gson.annotations.SerializedName
import java.io.Serializable

/**
 * 书单列表中的书
 *
 * @author: p_x_c (pengch at col dot com)
 */
data class BookDTO(


    val topBarStart: String?,
    val topBarEnd: String?,
    val contentBg: String?,
    val numberBg: String?,
    val moduleDesc: String?,
    val moduleType: String?,
    @SerializedName("imgPath", alternate = ["cover"])
    var imgPath: String?,
    var targetPath: String?,
    @SerializedName("name", alternate = ["title"])
    var name: String?,
    var buttonName: String?,
    var buttonImgPath: String?,
    var firstChapter: ChapterDto?,
    var pageViewNum: Long?,
    var chapterNum: Long?,
    var copyRight: Copyright?,
    var progress: Int?,
    var userId: Int?,
    /**
     * 作品ID
     */
    var bookId: Long,
    var id: Long,

    /**
     * 作品名称
     */
    var bookName: String,

    /**
     * 作品频道
     */
    var channelName: String?,

    /**
     * 作品类别名
     */
    var categoryName: String?,

    /**
     * 作品封面
     */
    var coverImageUrl: String?,

    /**
     * 作品简介
     */
    var introduction: String?,

    /**
     * 作者名
     */
    var authorPenname: String?,
    val items: List<BookDTO>?,

    /**
     * 当前字数
     */
    var wordCount: Long = 0,

    /**
     * 最新更新章节时间
     */
    var lastUpdateChapterDate: String?,
    var lastUpdateChapterTime: Long?,

    /**
     * 书状态(1：可抓取  2：屏蔽或下架，非正)
     */
    var status: Int?,

    /**
     * 书连载状态(01:连载, 02:暂停, 03:完本)
     */
    var bookStatus: String?,
    var className: String?,
    var keyWord: String?,
    var topLabelType: Int?,
    var targetBillboardId: Int?,

    var source: SoureDto?,
    var showBookData: ShowBookData?

) : Serializable {


    companion object {
        fun newInstanceWithBookId(bookId: Long): BookDTO {
            return BookDTO(
                bookId = bookId,
                topBarStart = "",
                topBarEnd = "",
                contentBg = "",
                numberBg = "",
                moduleDesc = "",
                moduleType = "",
                imgPath = "",
                targetPath = "",
                name = "",
                buttonName = "",
                buttonImgPath = "",
                firstChapter = null,
                pageViewNum = 0,
                chapterNum = 0,
                copyRight = null,
                progress = 0,
                userId = 0,
                bookName = "",
                channelName = "",
                categoryName = "",
                coverImageUrl = "",
                introduction = "",
                authorPenname = "",
                items = listOf(),
                wordCount = 0,
                lastUpdateChapterDate = "",
                status = 1,
                bookStatus = "01",
                className = "",
                keyWord = "",
                topLabelType = 1,
                targetBillboardId = 1,
                id = 0,
                lastUpdateChapterTime = 0,
                source = null,
                showBookData = null
            )
        }
    }
}