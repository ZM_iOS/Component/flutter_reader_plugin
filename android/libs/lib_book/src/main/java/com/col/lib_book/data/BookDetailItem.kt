package com.col.lib_book.data

data class BookDetailItem(
    val `data`: BookDTO,
    val moduleType: String
)