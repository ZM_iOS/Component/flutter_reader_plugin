package com.col.lib_book.data

data class BookShelfItem(
    var book: BookDTO,
    val bookAuthorId: Int,
    val bookmarkChapter: ChapterBean?,
    val categoryId: Int,
    val progress: Int,
    val userId: Int,
    var lastUpdateChapter:ChapterBean?,
    var bookmarkTimeValue:Long?,
    var unReadChapterCount:Int?
)