package com.col.lib_book.data

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

/**
 * <pre>
 *     author : Freedom
 *     e-mail : burst3166@gmail.com
 *     time   : 2020/11/13
 *     desc   :
 *     version: 1.0
 * </pre>
 */


@Entity(tableName = "bookshelf_table")
data class BookshelfLocal(



    @ColumnInfo
    var bookId: Long,
    @ColumnInfo
    var bookAuthorId: Int?,
    @ColumnInfo
    var categoryId: Int?,
    @ColumnInfo
    var progress: Int?,
    @ColumnInfo
    var userId: Int?,
    @ColumnInfo
    var authorPenname: String?,
    @ColumnInfo
    var bookName: String,
    @ColumnInfo
    var bookStatus: String?,
    @ColumnInfo
    var categoryName: String?,
    @ColumnInfo
    var channelName: String?,
    @ColumnInfo
    var className: String?,
    @ColumnInfo
    var coverImageUrl: String?,
    @ColumnInfo
    var introduction: String?,
    @ColumnInfo
    var keyWord: String?,
    @ColumnInfo
    var lastUpdateChapterDate: String?,
    @ColumnInfo
    var status: Int?,
    @ColumnInfo
    var wordCount: Long?,



    @ColumnInfo
    var chapterId: Long? ,
    @ColumnInfo
    var chapterIndex: Int?,
    @ColumnInfo
    var chapterName: String?,
    @ColumnInfo
    var createTimeValue: Long?,
    @ColumnInfo
    var isVIP: String?,
    @ColumnInfo
    var updateDate: String?,
    @ColumnInfo
    var updateTimeValue: Long?,
    @ColumnInfo
    var volumeCode: Int?,
    @ColumnInfo
    var volumeId: Long?,

    @ColumnInfo
    var update: Boolean?,

    @ColumnInfo
    var lastUpdateChapterName: String?,
    @ColumnInfo
    var lastUpdateChapterId: Long?,
    @ColumnInfo
    var lastUpdateChapterTimeValue: Long?,


){
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo()
    var id: Long = 0
}