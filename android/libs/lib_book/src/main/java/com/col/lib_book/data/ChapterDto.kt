package com.col.lib_book.data

data class ChapterDto(
    val bookId: Long,
    val chapterId: Long,
    val chapterIndex: Int,
    val chapterName: String,
    val content: String,
    val createTimeValue: Long,
    val isVIP: String,
    val updateDate: String,
    val updateTimeValue: Long,
    val volumeCode: Int,
    val volumeId: Long,
    val nextChapterId: Long,

)