package com.col.lib_book.data

data class Copyright(
    val authorPenName: String,
    val copy_right: String,
    val description: String,
    val law: String,
    val publisher: String
)