package com.col.lib_book.data

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

/**
 * <pre>
 *     author : Freedom
 *     e-mail : burst3166@gmail.com
 *     time   : 2020/11/17
 *     desc   :
 *     version: 1.0
 * </pre>
 */
@Entity(tableName = "explore_history_table")
data class ExploreHistory(

    @PrimaryKey()
    var bookId: Long,
    @ColumnInfo
    var progress: Int?,
    @ColumnInfo
    var userId: Int?,
    @ColumnInfo
    var bookName: String,
    @ColumnInfo
    var coverImageUrl: String?,
    @ColumnInfo()
    var chapterId: Long? ,
    @ColumnInfo()
    var openTime: Long? ,



)