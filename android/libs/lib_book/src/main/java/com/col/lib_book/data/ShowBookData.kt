package com.col.lib_book.data

data class ShowBookData(
    val dataNum: Long,
    val showUnit: String,
    val simpleDataNum: String
)