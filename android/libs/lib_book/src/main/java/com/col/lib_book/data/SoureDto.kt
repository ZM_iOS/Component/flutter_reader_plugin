package com.col.lib_book.data

data class SoureDto(
    val orderNo: Int,
    val sourceModelId: String,
    val sourceModelName: String,
    val sourcePageName: String,
    val sourceType: String
)