package com.col.lib_book.database

import androidx.lifecycle.LiveData
import androidx.paging.PagingSource
import androidx.room.*
import com.col.lib_book.data.BookshelfLocal

/**
 * <pre>
 *     author : Freedom
 *     e-mail : burst3166@gmail.com
 *     time   : 2020/11/13
 *     desc   :
 *     version: 1.0
 * </pre>
 */
@Dao
interface BookshelfDao {



    @Insert
    suspend fun insert (book: BookshelfLocal):Long

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll (books: List<BookshelfLocal>)

    @Update
    suspend fun update (book: BookshelfLocal):Int

    @Delete
    suspend fun delete (book: BookshelfLocal):Int

    @Delete
    suspend fun deleteSome (books: List<BookshelfLocal>):Int

    @Query("DELETE FROM bookshelf_table WHERE userId = :userId")
    suspend fun deleteByUserId (userId: String):Int

    @Query("DELETE FROM bookshelf_table")
    suspend fun deleteAll ():Int



    @Query("SELECT * FROM bookshelf_table  WHERE userId = :userId ORDER BY updateTimeValue Desc ,lastUpdateChapterTimeValue Desc")
    fun findBookByUserId(userId: String): PagingSource<Int, BookshelfLocal>

    @Query("SELECT count(*) FROM bookshelf_table  WHERE userId = :userId AND bookId !=-1")
    suspend fun findBookByUserIdNoAddDataSize(userId: String): Int

    @Query("DELETE FROM bookshelf_table WHERE userId = :userId AND bookId = -1")
    suspend fun deleteBookAddData(userId: String):Int

    @Query("SELECT * FROM bookshelf_table  WHERE userId = :userId ORDER BY updateTimeValue Desc ,lastUpdateChapterTimeValue Desc")
    fun findBookByUserIdList(userId: String): LiveData<List<BookshelfLocal>>

    @Query("SELECT * FROM bookshelf_table  WHERE userId = :userId AND bookId !=-1 ORDER BY updateTimeValue Desc ,lastUpdateChapterTimeValue Desc")
    fun findBookByUserIdListNoAddData(userId: String): LiveData<List<BookshelfLocal>>

    @Query("SELECT * FROM bookshelf_table WHERE bookId = :bookId AND userId = :userId")
    fun findById (bookId:Long, userId: String): BookshelfLocal

    @Query("DELETE FROM bookshelf_table")
    suspend fun clearAll()

}