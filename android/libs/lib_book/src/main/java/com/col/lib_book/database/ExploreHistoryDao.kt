package com.col.lib_book.database

import androidx.lifecycle.LiveData
import androidx.paging.PagingSource
import androidx.room.*
import com.col.lib_book.data.ExploreHistory

/**
 * <pre>
 *     author : Freedom
 *     e-mail : burst3166@gmail.com
 *     time   : 2020/11/17
 *     desc   :
 *     version: 1.0
 * </pre>
 */
@Dao
interface ExploreHistoryDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert (book: ExploreHistory):Long

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll (books: List<ExploreHistory>)

    @Update
    suspend fun update (book: ExploreHistory):Int

    @Delete
    suspend fun delete (book: ExploreHistory):Int

    @Delete
    suspend fun deleteSome (books: List<ExploreHistory>)

    @Query("DELETE FROM explore_history_table WHERE userId = :userId")
    suspend fun deleteByUserId (userId: String):Int

    @Query("DELETE FROM explore_history_table")
    suspend fun deleteAll ():Int



    @Query("SELECT * FROM explore_history_table  WHERE userId = :userId ORDER BY openTime Desc")
    fun findBookByUserId(userId: String): PagingSource<Int, ExploreHistory>

    @Query("SELECT * FROM explore_history_table  WHERE userId = :userId ORDER BY openTime Desc")
    fun findBookByUserIdList(userId: String): LiveData<List<ExploreHistory>>

    @Query("SELECT * FROM explore_history_table WHERE bookId = :bookId")
    fun findById (bookId:String): ExploreHistory

    @Query("DELETE FROM explore_history_table")
    suspend fun clearAll()
}