@file:Suppress("unused")

package com.col.lib_book.ext

import android.os.Handler
import android.os.Looper
import android.text.TextUtils
import android.util.Log
import com.col.lib_book.ReadManager
import com.col.lib_book.utils.ToastUtil
import kotlinx.coroutines.CoroutineExceptionHandler

/**
 * Created by huangzhengneng on 2020/11/12.
 */

fun Any.toast(msg: String) {
    if (TextUtils.isEmpty(msg)) {
        return
    }
    if (Looper.myLooper() != Looper.getMainLooper()) {
        Handler(Looper.getMainLooper()).post {
            ToastUtil.showToast(msg)
        }
    } else {
        ToastUtil.showToast(msg)
    }
}

fun Any.logi(msg: String) {
    Log.i("oklog",msg)
}

fun Any.loge(msg: String) {
    Log.e("oklog",msg)
}

fun Any.loge(e: Exception) {
   e.printStackTrace()
}

fun Any.exceptionHandler(): CoroutineExceptionHandler {
    return ReadManager.exceptionHandler
}

/// 返回指定大小的随机List
fun <T : Any> List<T>.randomList(targetSize: Int = 1): List<T> {
    val ans = mutableListOf<T>()
    if (size <= targetSize) {
        ans.addAll(this)
    } else {
        while (ans.size < targetSize
        ) {
            val t = random()
            if (!ans.contains(t)) {
                ans.add(t)
            }
        }
    }
    return ans
}