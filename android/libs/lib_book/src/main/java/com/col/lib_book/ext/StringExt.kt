@file:Suppress("unused")

package com.col.lib_book.ext
/**
 * 以万为单位显示 保留1位小数 只舍不入
 */
fun String.formatNumberWithW(number: Int): String? {
    return if (number < 10000) {
        number.toString() + ""
    } else {
        (number / 10000).toString() + "." + number % 10000 / 1000 + "万"
    }
}

fun String.substringWithEnd(str: String?, len: Int): String? {
    if (str?.isEmpty() == true) {
        return ""
    }
    return if (str?.length ?: 0 <= len) {
        str
    } else {
        str?.substring(0, len) + "..."
    }
}