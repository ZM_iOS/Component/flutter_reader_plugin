package com.col.lib_book.utils;

import android.app.Activity;
import android.content.Context;
import android.os.Process;

import java.lang.ref.WeakReference;
import java.util.Iterator;
import java.util.ListIterator;
import java.util.Stack;

public class ActivityStackManager {
    private static final String TAG = "ActivityStackManager";
    /**
     * Activity栈
     */
    private Stack<WeakReference<Activity>> mActivityStack;
    private static ActivityStackManager activityStackManager = new ActivityStackManager();

    private ActivityStackManager() {
    }

    /***
     * 获得AppManager的实例
     *
     * @return AppManager实例
     */
    public static ActivityStackManager getInstance() {
        if (activityStackManager == null) {
            activityStackManager = new ActivityStackManager();
        }
        return activityStackManager;
    }

    /***
     * 栈中Activity的数
     *
     * @return Activity的数
     */
    public int stackSize() {
        return mActivityStack.size();
    }

    /***
     * 获得Activity栈
     *
     * @return Activity栈
     */
    public Stack<WeakReference<Activity>> getStack() {
        return mActivityStack;
    }

    /**
     * 添加Activity到堆栈
     */
    public void addActivity(WeakReference<Activity> activity) {
        if (mActivityStack == null) {
            mActivityStack = new Stack<>();
        }
        mActivityStack.push(activity);
    }

    /**
     * 删除ac
     */
    public void removeActivity(WeakReference<Activity> activity) {
        if (mActivityStack != null) {
            mActivityStack.remove(activity);
        }
    }

    public void removeActivity(Activity act) {
        if (mActivityStack == null)
            return;
        int size = mActivityStack.size();
        ListIterator<WeakReference<Activity>> it = mActivityStack.listIterator(size);
        while (it.hasPrevious()) {
            WeakReference<Activity> activity = it.previous();
            Activity activityInstance = activity.get();
            if (activityInstance == null) {
                it.remove();
            } else if (activityInstance.equals(act)) {
                it.remove();
                break;
            }
        }

    }

    /***
     * 获取栈顶Activity（堆栈中最后一个压入的）
     *
     * @return Activity
     */
    public Activity getTopActivity() {
        if (mActivityStack == null || mActivityStack.empty())
            return null;
        WeakReference<Activity> activityWeakReference = mActivityStack.lastElement();
        if (activityWeakReference != null) {
            Activity activity = activityWeakReference.get();
            if (null == activity) {
                return null;
            } else {
                return mActivityStack.lastElement().get();
            }
        }
        return null;
    }

    public Activity getSecondFromLastActivity() {
        Activity return_activity = null;
        int size = mActivityStack.size();
        ListIterator<WeakReference<Activity>> it = mActivityStack.listIterator(size);
        int index = 0;
        while (it.hasPrevious()) {
            WeakReference<Activity> activity = it.previous();
            Activity activityInstance = activity.get();
            if (activityInstance == null) {
                it.remove();
            } else {
                index++;
                if (index == 2) {
                    return_activity = activity.get();
                    break;
                }
            }

        }
        if (null == return_activity) {
            return mActivityStack.lastElement().get();
        } else {
            return return_activity;
        }
    }


    /***
     * 通过class 获取栈顶Activity
     *
     * @param cls
     * @return Activity
     */
    public Activity getActivityByClass(Class<?> cls) {
        Activity return_activity = null;
        if (mActivityStack == null)
            return null;
        int size = mActivityStack.size();
        ListIterator<WeakReference<Activity>> it = mActivityStack.listIterator(size);

        while (it.hasPrevious()) {
            WeakReference<Activity> activity = it.previous();
            Activity activityInstance = activity.get();
            if (activityInstance == null) {
                it.remove();
            } else if (activityInstance.getClass().equals(cls)) {
                return_activity = activity.get();
                break;
            }
        }

        return return_activity;
    }

    /**
     * 结束栈顶Activity（堆栈中最后一个压入的）
     */
    public void killTopActivity() {
        try {
            WeakReference<Activity> activity = mActivityStack.lastElement();
            killActivity(activity);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /***
     * 结束指定的Activity
     */
    public void killActivity(WeakReference<Activity> activity) {
        try {
            Iterator<WeakReference<Activity>> iterator = mActivityStack.iterator();
            while (iterator.hasNext()) {
                WeakReference<Activity> stackActivity = iterator.next();
                if (stackActivity.get() == null) {
                    iterator.remove();
                    continue;
                }
                if (stackActivity.get().getClass().getName().equals(activity.get().getClass().getName())) {
                    iterator.remove();
                    stackActivity.get().finish();
                    break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /***
     * 结束指定类名的Activity
     */
    public void killActivity(Class<?> cls) {
        try {
            ListIterator<WeakReference<Activity>> listIterator = mActivityStack.listIterator();
            while (listIterator.hasNext()) {
                Activity activity = listIterator.next().get();
                if (activity == null) {
                    listIterator.remove();
                    continue;
                }
                if (activity.getClass() == cls) {
                    listIterator.remove();
                    if (activity != null) {
                        activity.finish();
                    }
                    break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 结束所有Activity
     */
    public void killAllActivity() {
        try {
            ListIterator<WeakReference<Activity>> listIterator = mActivityStack.listIterator();
            while (listIterator.hasNext()) {
                Activity activity = listIterator.next().get();
                if (activity != null) {
                    activity.finish();
                }
                listIterator.remove();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 移除除了某个activity的其他所有activity
     *
     * @param cls 界面
     */
    public void killAllActivityExceptOne(Class cls) {
        try {

            Iterator<WeakReference<Activity>> it = mActivityStack.iterator();
            while (it.hasNext()) {
                WeakReference<Activity> activity = it.next();
                if (activity != null && activity.get().getClass().equals(cls)) {
                    continue;
                }
                if (activity.get() != null) {
                    it.remove();
                    activity.get().finish();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 退出应用程序
     */
    public void AppExit(Context context) {
        killAllActivity();
        Process.killProcess(Process.myPid());

    }
}