/**
 * Copyright 2016 JustWayward Team
 *
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.col.lib_book.utils

import android.content.Context
import kotlin.jvm.Synchronized
import android.content.res.AssetManager
import android.os.Environment
import splitties.init.appCtx
import java.io.*
import java.lang.Exception
import java.lang.StringBuilder
import java.text.DecimalFormat
import java.util.ArrayList
import kotlin.Throws

/**
 * @author yuyh.
 * @date 16/4/9.
 */
object CacheFileUtils {

    const val ENCRYPT_PASSWORD = "1234567890abcDEF"
    const val CHARSET_NAME = "UTF-8"
    var BASE_PATH = createRootPath(appCtx) + "/book/"


    /**
     * 递归获取所有文件
     *
     * @param root
     * @param ext  指定扩展名
     */
    @Synchronized
    private fun getAllFiles(root: File, ext: String) {
        val list: MutableList<File> = ArrayList()
        val files = root.listFiles()
        if (files != null) {
            for (f in files) {
                if (f.isDirectory) {
                    getAllFiles(f, ext)
                } else {
                    if (f.name.endsWith(ext) && f.length() > 50) list.add(f)
                }
            }
        }
    }



    fun getChapterPath(bookId: String, chapter: Long, isFree: String): String {
        return BASE_PATH + bookId + File.separator + chapter + "_" + isFree + ".bin"
    }

    fun getChapterJsonPath(bookId: String, chapter: Long): String {
        return BASE_PATH + bookId + File.separator + chapter + "json" + ".bin"
    }

    fun isExistsInAssets(am: AssetManager, fileName: String): Boolean {
        try {
            val names = am.list("fonts")
            for (name in names!!) {
                if (name == fileName.trim { it <= ' ' }) {
                    return true
                }
            }
        } catch (e: IOException) {
            e.printStackTrace()
        }
        return false
    }

    @JvmStatic
    fun getChapterFile(
        bookId: String,
        chapter: Long,
        isFree: String,
        createFile: Boolean
    ): File {
        val file = File(getChapterPath(bookId, chapter, isFree))
        if (!file.exists() && createFile) {
            createFile(file)
        }
        return file
    }

    @JvmStatic
    fun getChapterJsonFile(bookId: String, chapter: Long, createFile: Boolean): File {
        val file = File(getChapterJsonPath(bookId, chapter))
        if (!file.exists() && createFile) {
            createFile(file)
        }
        return file
    }

    @JvmStatic
    fun getChapterFile2(
        bookId: String,
        chapter: Long,
        isFree: String,
        createFile: Boolean
    ): File {
        return File(getChapterPath(bookId, chapter, isFree))
    }

    fun getBookDir(bookId: String): File {
        return File(BASE_PATH + bookId)
    }

    /**
     * 读取Assets文件
     *
     * @param fileName
     * @return
     */
    fun readAssets(fileName: String?): ByteArray? {
        if (fileName == null || fileName.isEmpty()) {
            return null
        }
        var buffer: ByteArray? = null
        try {
            val fin: InputStream = appCtx.assets.open(
                "uploader$fileName"
            )
            val length = fin.available()
            buffer = ByteArray(length)
            fin.read(buffer)
            fin.close()
        } catch (e: Exception) {
            e.printStackTrace()
        } finally {
            return buffer
        }
    }

    /**
     * 创建根缓存目录
     *
     * @return
     */
    fun createRootPath(context: Context): String {
        var cacheRootPath = ""
        cacheRootPath = if (isSdCardAvailable) {
            // /sdcard/Android/data/<application package>/cache
            val externalCacheDir = context.externalCacheDir
            if (externalCacheDir == null) {
                context.cacheDir.path
            } else {
                externalCacheDir.path
            }
        } else {
            // /data/data/<application package>/cache
            context.cacheDir.path
        }
        return cacheRootPath
    }

    val isSdCardAvailable: Boolean
        get() = Environment.MEDIA_MOUNTED == Environment.getExternalStorageState()

    /**
     * 递归创建文件夹
     *
     * @param dirPath
     * @return 创建失败返回""
     */
    fun createDir(dirPath: String): String {
        try {
            val file = File(dirPath)
            if (file.parentFile.exists()) {
                file.mkdir()
                return file.absolutePath
            } else {
                createDir(file.parentFile.absolutePath)
                file.mkdir()
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return dirPath
    }

    /**
     * 递归创建文件夹
     *
     * @param file
     * @return 创建失败返回""
     */
    fun createFile(file: File): String {
        try {
            if (file.parentFile.exists()) {
//                Logger.i("----- 创建文件" + file.getAbsolutePath());
                file.createNewFile()
                return file.absolutePath
            } else {
                createDir(file.parentFile.absolutePath)
                file.createNewFile()
                //                Logger.i("----- 创建文件" + file.getAbsolutePath());
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return ""
    }

    /**
     * 将内容写入文件
     *
     * @param filePath eg:/mnt/sdcard/demo.txt
     * @param content  内容
     * @param isAppend 是否追加
     */
    fun writeFile(filePath: String?, content: String, isAppend: Boolean) {
//        Logger.i("save:" + filePath);
        try {
            val fout = FileOutputStream(filePath, isAppend)
            val bytes = content.toByteArray()
            fout.write(bytes)
            fout.close()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun writeFile(filePath: String?, bytes: ByteArray?, isAppend: Boolean) {
//        Logger.i("save:" + filePath);
        var fout: FileOutputStream? = null
        try {
            fout = FileOutputStream(filePath, isAppend)
            fout.write(bytes)
        } catch (e: Exception) {
            e.printStackTrace()
        } finally {
            if (fout != null) {
                try {
                    fout.close()
                } catch (e: IOException) {
                    e.printStackTrace()
                }
            }
        }
    }

    fun writeFile(filePathAndName: String?, fileContent: String?) {
        try {
            val outstream: OutputStream = FileOutputStream(filePathAndName)
            val out = OutputStreamWriter(outstream)
            out.write(fileContent)
            out.close()
        } catch (e: IOException) {
            e.printStackTrace()
        }
    }

    /**
     * 获取Raw下的文件内容
     *
     * @param context
     * @param resId
     * @return 文件内容
     */
    fun getFileFromRaw(context: Context?, resId: Int): String? {
        if (context == null) {
            return null
        }
        var br: BufferedReader? = null
        val s = StringBuilder()
        return try {
            val `in` = InputStreamReader(context.resources.openRawResource(resId))
            br = BufferedReader(`in`)
            var line: String?
            while (br.readLine().also { line = it } != null) {
                s.append(line)
            }
            s.toString()
        } catch (e: IOException) {
            e.printStackTrace()
            null
        } finally {
            try {
                br!!.close()
            } catch (e: IOException) {
                e.printStackTrace()
            }
        }
    }

    fun getBytesFromFile(f: File?): ByteArray? {
        if (f == null) {
            return null
        }
        try {
            val stream = FileInputStream(f)
            val out = ByteArrayOutputStream(1000)
            val b = ByteArray(1000)
            var n: Int
            while (stream.read(b).also { n = it } != -1) {
                out.write(b, 0, n)
            }
            stream.close()
            out.close()
            return out.toByteArray()
        } catch (e: IOException) {
        }
        return null
    }

    /**
     * 文件拷贝
     *
     * @param src  源文件
     * @param desc 目的文件
     */
    fun fileChannelCopy(src: File?, desc: File) {
        //createFile(src);
        createFile(desc)
        var fi: FileInputStream? = null
        var fo: FileOutputStream? = null
        try {
            fi = FileInputStream(src)
            fo = FileOutputStream(desc)
            val `in` = fi.channel //得到对应的文件通道
            val out = fo.channel //得到对应的文件通道
            `in`.transferTo(0, `in`.size(), out) //连接两个通道，并且从in通道读取，然后写入out通道
        } catch (e: IOException) {
            e.printStackTrace()
        } finally {
            try {
                fo?.close()
                fi?.close()
            } catch (e: IOException) {
                e.printStackTrace()
            }
        }
    }

    /**
     * 转换文件大小
     *
     * @param fileLen 单位B
     * @return
     */
    @JvmStatic
    fun formatFileSizeToString(fileLen: Long): String {
        val df = DecimalFormat("0.00")
        var fileSizeString = ""
        fileSizeString = if (fileLen < 1024) {
            df.format(fileLen.toDouble()) + "B"
        } else if (fileLen < 1048576) {
            df.format(fileLen.toDouble() / 1024) + "K"
        } else if (fileLen < 1073741824) {
            df.format(fileLen.toDouble() / 1048576) + "M"
        } else {
            df.format(fileLen.toDouble() / 1073741824) + "G"
        }
        return fileSizeString
    }

    /**
     * 删除指定文件
     *
     * @param file
     * @return
     * @throws IOException
     */
    @Throws(IOException::class)
    fun deleteFile(file: File?): Boolean {
        return deleteFileOrDirectory(file)
    }

    /**
     * 删除指定文件，如果是文件夹，则递归删除
     *
     * @param file
     * @return
     * @throws IOException
     */
    @Throws(IOException::class)
    fun deleteFileOrDirectory(file: File?): Boolean {
        try {
            if (file != null && file.isFile) {
                return file.delete()
            }
            if (file != null && file.isDirectory) {
                val childFiles = file.listFiles()
                // 删除空文件夹
                if (childFiles == null || childFiles.size == 0) {
                    return file.delete()
                }
                // 递归删除文件夹下的子文件
                for (i in childFiles.indices) {
                    deleteFileOrDirectory(childFiles[i])
                }
                return file.delete()
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return false
    }

    /**
     * 获取文件夹大小
     *
     * @return
     * @throws Exception
     */
    @JvmStatic
    @Throws(Exception::class)
    fun getFolderSize(dir: String?): Long {
        val file = File(dir)
        var size: Long = 0
        try {
            val fileList = file.listFiles()
            for (i in fileList.indices) {
                // 如果下面还有文件
                size = if (fileList[i].isDirectory) {
                    size + getFolderSize(fileList[i].absolutePath)
                } else {
                    size + fileList[i].length()
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return size
    }

    /***
     * 获取文件扩展名
     *
     * @param filename 文件名
     * @return
     */
    fun getExtensionName(filename: String?): String? {
        if (filename != null && filename.length > 0) {
            val dot = filename.lastIndexOf('.')
            if (dot > -1 && dot < filename.length - 1) {
                return filename.substring(dot + 1)
            }
        }
        return filename
    }

    /**
     * 获取文件内容
     *
     * @param path
     * @return
     */
    fun getFileOutputString(path: String?, charset: String?): String? {
        try {
            val file = File(path)
            val bufferedReader =
                BufferedReader(InputStreamReader(FileInputStream(file), charset), 8192)
            val sb = StringBuilder()
            var line: String? = null
            while (bufferedReader.readLine().also { line = it } != null) {
                sb.append("\n").append(line)
            }
            bufferedReader.close()
            return sb.toString()
        } catch (e: IOException) {
            e.printStackTrace()
        }
        return null
    }

    fun readFileData(file: File?): ByteArray? {
        var input: InputStream? = null
        try {
            var readBuffer: ByteArray? = ByteArray(1024)
            val bos = ByteArrayOutputStream(1024)
            input = FileInputStream(file)
            var numRead = 0
            while (input.read(readBuffer, 0, readBuffer!!.size).also { numRead = it } >= 0) {
                bos.write(readBuffer, 0, numRead)
            }
            input.close()
            readBuffer = null
            return bos.toByteArray()
        } catch (e: FileNotFoundException) {
            e.printStackTrace()
        } catch (e: IOException) {
            e.printStackTrace()
        }
        return null
    }

    fun getCharset(fileName: String?): String {
        var bis: BufferedInputStream? = null
        var charset = "GBK"
        val first3Bytes = ByteArray(3)
        try {
            var checked = false
            bis = BufferedInputStream(FileInputStream(fileName))
            bis.mark(0)
            var read = bis.read(first3Bytes, 0, 3)
            if (read == -1) return charset
            if (first3Bytes[0] == 0xFF.toByte() && first3Bytes[1] == 0xFE.toByte()) {
                charset = "UTF-16LE"
                checked = true
            } else if (first3Bytes[0] == 0xFE.toByte()
                && first3Bytes[1] == 0xFF.toByte()
            ) {
                charset = "UTF-16BE"
                checked = true
            } else if (first3Bytes[0] == 0xEF.toByte() && first3Bytes[1] == 0xBB.toByte() && first3Bytes[2] == 0xBF.toByte()) {
                charset = "UTF-8"
                checked = true
            }
            bis.mark(0)
            if (!checked) {
                while (bis.read().also { read = it } != -1) {
                    if (read >= 0xF0) break
                    if (0x80 <= read && read <= 0xBF) // 单独出现BF以下的，也算是GBK
                        break
                    if (0xC0 <= read && read <= 0xDF) {
                        read = bis.read()
                        if (0x80 <= read && read <= 0xBF) // 双字节 (0xC0 - 0xDF)
                        // (0x80 - 0xBF),也可能在GB编码内
                            continue else break
                    } else if (0xE0 <= read && read <= 0xEF) { // 也有可能出错，但是几率较小
                        read = bis.read()
                        if (0x80 <= read && read <= 0xBF) {
                            read = bis.read()
                            if (0x80 <= read && read <= 0xBF) {
                                charset = "UTF-8"
                                break
                            } else break
                        } else break
                    }
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        } finally {
            if (bis != null) {
                try {
                    bis.close()
                } catch (e: IOException) {
                    e.printStackTrace()
                }
            }
        }
        return charset
    }

    fun getCharset1(fileName: String?): String {
        var bin: BufferedInputStream? = null
        var p = 0
        try {
            bin = BufferedInputStream(FileInputStream(fileName))
            p = (bin.read() shl 8) + bin.read()
        } catch (e: FileNotFoundException) {
            e.printStackTrace()
        } catch (e: IOException) {
            e.printStackTrace()
        } finally {
            try {
                bin!!.close()
            } catch (e: IOException) {
                e.printStackTrace()
            }
        }
        val code: String
        code = when (p) {
            0xefbb -> "UTF-8"
            0xfffe -> "Unicode"
            0xfeff -> "UTF-16BE"
            else -> "GBK"
        }
        return code
    }

    fun saveWifiTxt(src: String?, desc: String?) {
        val LINE_END = "\n".toByteArray()
        try {
            val isr = InputStreamReader(FileInputStream(src), getCharset(src))
            val br = BufferedReader(isr)
            val fout = FileOutputStream(desc, true)
            var temp: String
            while (br.readLine().also { temp = it } != null) {
                val bytes = temp.toByteArray()
                fout.write(bytes)
                fout.write(LINE_END)
            }
            br.close()
            fout.close()
        } catch (e: FileNotFoundException) {
            e.printStackTrace()
        } catch (e: IOException) {
            e.printStackTrace()
        }
    }
}
