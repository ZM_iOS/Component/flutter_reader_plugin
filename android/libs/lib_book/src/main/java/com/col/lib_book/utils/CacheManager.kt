/**
 * Copyright 2016 JustWayward Team
 *
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.col.lib_book.utils

import com.col.lib_book.utils.CacheFileUtils.getChapterFile2
import com.col.lib_book.utils.StringUtil.formatContent
import com.col.lib_book.utils.CacheFileUtils.getFolderSize
import com.col.lib_book.utils.CacheFileUtils.isSdCardAvailable
import com.col.lib_book.utils.CacheFileUtils.formatFileSizeToString
import com.col.lib_book.utils.CacheFileUtils.writeFile
import splitties.init.appCtx
import kotlin.jvm.Synchronized
import java.io.File
import java.io.UnsupportedEncodingException
import java.lang.Exception

/**
 * @author yuyh.
 * @date 2016/9/28.
 */
object CacheManager {
    private val searchHistoryKey: String
        private get() = "searchHistory"
    private val collectionKey: String
        private get() = "my_book_lists"

    private fun getTocListKey(bookId: String): String {
        return "$bookId-bookToc"
    }

    fun getChapterFile(bookId: String?, chapter: Long, isFree: String?): File? {
        val file = CacheFileUtils.getChapterFile(bookId!!, chapter, isFree!!, true)
        return if (file.length() > 2) file else null
    }

    fun getChapterJsonFile(bookId: String?, chapter: Long, creatFile: Boolean): File? {
        val file = CacheFileUtils.getChapterJsonFile(bookId!!, chapter, creatFile)
        return if (file != null && file.length() > 2) file else null
    }

    /**
     * 用于批量下载  不自动创建文件
     * @param bookId
     * @param chapter
     * @param isFree
     * @return
     */
    fun getChapterFile2(bookId: String?, chapter: Long, isFree: String?): File? {
        val file = getChapterFile2(bookId!!, chapter, isFree!!, true)
        return if (file != null && file.length() > 2) file else null
    }

    fun getChapterFile(bookId: String?, chapter: Long, isFree: String?, creatFile: Boolean): File? {
        val file = CacheFileUtils.getChapterFile(bookId!!, chapter, isFree!!, false)
        return if (file != null && file.length() > 2) file else null
    }

    fun saveChapterFile(bookId: String?, content: String?, chapterId: Long, isFree: String?) {
        val file = CacheFileUtils.getChapterFile(bookId!!, chapterId, isFree!!, true)
        val password = CacheFileUtils.ENCRYPT_PASSWORD
        val s = formatContent(content!!)
        var encryptResult: ByteArray? = null
        try {
            var buffer = s!!.toByteArray(Charsets.UTF_8)
            // long startTime = System.currentTimeMillis();
            if (buffer == null || buffer.size <= 0) {
                try {
                    buffer = "".toByteArray(Charsets.UTF_8)
                } catch (e: UnsupportedEncodingException) {
                    e.printStackTrace()
                }
            }
            encryptResult = AESUtil.encrypt(buffer, password)
        } catch (e: Exception) {
            // TODO Auto-generated catch block
            e.printStackTrace()
        }
        if (encryptResult == null) {
            return
        }
        writeFile(file.absolutePath, encryptResult, false)
    }

    fun saveChapterJsonFile(bookId: String?, content: String?, chapterId: Long) {
        val file = CacheFileUtils.getChapterJsonFile(bookId!!, chapterId, true)
        writeFile(file.absolutePath, content!!, false)
    }

    /**
     * 获取缓存大小
     *
     * @return
     */
    @get:Synchronized
    val cacheSize: String
        get() {
            var cacheSize: Long = 0
            try {
                val cacheDir: String = appCtx.cacheDir.getPath()
                cacheSize += getFolderSize(cacheDir)
                if (isSdCardAvailable) {
                    val extCacheDir: String = appCtx.externalCacheDir?.path ?: ""
                    cacheSize += getFolderSize(extCacheDir)
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
            return formatFileSizeToString(cacheSize)
        }

}