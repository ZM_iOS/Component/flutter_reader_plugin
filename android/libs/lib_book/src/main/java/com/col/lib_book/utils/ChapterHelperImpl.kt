package com.col.lib_book.utils

import android.os.Handler
import android.os.Looper
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import com.chineseall.reader.lib.reader.config.ReaderConfigWrapper
import com.chineseall.reader.lib.reader.config.TangYuanSharedPrefUtils
import com.chineseall.reader.lib.reader.entities.*
import com.chineseall.reader.lib.reader.utils.IChapterHelper
import com.chineseall.reader.lib.reader.utils.PaintHelper
import com.chineseall.reader.lib.reader.utils.StringUtil
import com.col.lib_book.ext.loge
import com.col.lib_book.data.BookContentRespository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.io.File
import java.util.*
import java.util.concurrent.CopyOnWriteArrayList
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors

/**
 * Created by huangzhengneng on 2020/4/16.
 */
open class ChapterHelperImpl constructor(
    private val article: Article,
    private val respository: BookContentRespository,
    private val activity: AppCompatActivity,
    private val block: (chapter: Chapter?) -> Unit,
) : IChapterHelper {

    var threadpool: ExecutorService = Executors.newCachedThreadPool()
    private val mHandler: Handler = Handler(Looper.getMainLooper())

    override fun isVerticalScrollMode(): Boolean {
        return article.isVerticalScrollMode
    }

    override fun loadChapter(self: Chapter?, next: Chapter?, progress: Int) {

        if (self?.status != ReaderConfigWrapper.STATUS_LOADING) {
            self?.status = ReaderConfigWrapper.STATUS_LOADING
            // 检查本地缓存
            // 网络拉取 -> 缓存本地
            // loadFromFile
            activity.lifecycleScope.launch(Dispatchers.IO) {
                self?.let {
                    var chapterFile = CacheManager.getChapterFile(
                        self.bookId,
                        self.chapterId.toLong(),
                        (if (self.isVip) "Vip" else "Free") + self.timeStamp_value
                    )
                    var exc = Exception("")
                    if (chapterFile == null) {
                        try {
                            val contentResponse =
                                respository.getChapterContent(self.bookId, self.chapterId)

                            if (contentResponse.chapterContent.isNotEmpty()) {
                                CacheManager.saveChapterFile(
                                    self.bookId,
                                    contentResponse.chapterContent,
                                    self.chapterId.toLong(),
                                    (if (self.isVip) "Vip" else "Free") + self.timeStamp_value
                                )
                            }

                            chapterFile = CacheManager.getChapterFile(
                                self.bookId,
                                self.chapterId.toLong(),
                                (if (self.isVip) "Vip" else "Free") + self.timeStamp_value
                            )
                        } catch (e: Exception) {
//                            e.printStackTrace()
                            exc = e
                        }
                    }
                    if (chapterFile != null) {
                        loadFromFile(chapterFile, self, next, progress)
                    } else {
                        loadContentFailed(self, exc)
                    }
                }
            }
        }
    }

    private fun loadContentFailed(self: Chapter?, exception: Any) {
        self?.status = ReaderConfigWrapper.STATUS_FAILED
        mHandler.post {
            article.view.requestRepaint("确保刷新")
        }
    }

    private fun readCacheChapterContent(file: File, self: Chapter): String {
        val fileOutputString: String
        val originalData: ByteArray? = CacheFileUtils.readFileData(file)
        var decryptResult: ByteArray? = null
        if (originalData?.isNotEmpty() == true) {
            try {
                decryptResult = AESUtil.decrypt(
                    originalData,
                    CacheFileUtils.ENCRYPT_PASSWORD
                )
            } catch (e: Exception) {
                loge(e)
            }
        }
        if (decryptResult == null) {
            self.status = ReaderConfigWrapper.STATUS_FAILED
            return ""
        }
        fileOutputString = String(decryptResult, charset(CacheFileUtils.CHARSET_NAME))
        return fileOutputString.replace("<br><br>", "\n")
    }

    /**
     * @param self
     * @param next
     * @param progress -1表示需要强制刷新
     */
    private fun loadFromFile(file: File, self: Chapter, next: Chapter?, progress: Int) {
        val paragraphs: List<Paragraph> = CopyOnWriteArrayList()
        try {

            val fileOutputString = readCacheChapterContent(file, self)
            val padding = ReaderConfigWrapper.getInstance().padding
            val elements = ArrayList<RElement>()


            elements.add(RText(PaintHelper.getInstance().contentPaint, fileOutputString))

            var status = ReaderConfigWrapper.STATUS_LOADED
            if (self.isVip) {
                status = ReaderConfigWrapper.STATUS_LOCK
            }
            if (status == ReaderConfigWrapper.STATUS_LOADED || status == ReaderConfigWrapper.STATUS_LOCK) {
                elements.add(0, RTitle(PaintHelper.getInstance().titlePaint, self.chapterName))

//                elements.add(RAD())

                /** 连续空行数>=1, 保留一个  */
                var previousElementIsBlankLine = false //上一个是否是空行
                for (ele in elements) {
                    if (ele is RText) {
                        if (StringUtil.isBlank(ele.text)) {
                            if (!previousElementIsBlankLine) {
                                val blankTextBlock =
                                    LineBlock(article.view, self, LineBlock.TEXT, ele)
                                blankTextBlock.str = ""
                                blankTextBlock.height = 20 //文字有时太大了，所以写死
                            }
                            previousElementIsBlankLine = true
                        } else {
                            previousElementIsBlankLine = false
                        }
                    } else {
                        previousElementIsBlankLine = false
                    }
                    ele.layout(
                        article.view,
                        self,
                        paragraphs,
                        article.pageWidth - padding * 2
                    )
                }
                //可能会要求跳转
                val newOffsetY = if (isVerticalScrollMode) {
                    self.onePageLayout(
                        paragraphs,
                        article.pageWidth - padding * 2,
                        article.viewHeight,
                        next,
                        progress,
                        status
                    )
                } else {
                    self.morePagesLayout(
                        paragraphs,
                        article.pageWidth - padding * 2,
                        article.viewHeight,
                        next,
                        progress,
                        status
                    )
                }
                self.status = status
                if (progress > -1 && newOffsetY >= 0) { //跳转到进度
                    article.view.setOffsetY(newOffsetY + self.chapterTop)
                }
            } else {
                self.status = ReaderConfigWrapper.STATUS_FAILED
            }
            if (TangYuanSharedPrefUtils.getInstance()
                    .getReaderPagerAnim(ReaderConfigWrapper.PAGER_ANIM_BOOK) != ReaderConfigWrapper.PAGER_ANIM_COVER
            ) {
                mHandler.post {
                    article.view.requestRepaint("确保刷新")
                }
                mHandler.postDelayed({
                    block(self)
                }, 100)
            }
        } catch (e: Exception) {
            e.printStackTrace()
            self.status = ReaderConfigWrapper.STATUS_FAILED
            mHandler.post {
                article.view.requestRepaint("确保刷新")
            }
        }
    }
}