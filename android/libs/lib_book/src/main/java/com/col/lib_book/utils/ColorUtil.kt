package com.col.lib_book.utils

import android.content.Context
import android.graphics.drawable.Drawable
import androidx.annotation.ColorInt
import androidx.annotation.ColorRes
import skin.support.content.res.SkinCompatResources
import skin.support.widget.SkinCompatHelper
import splitties.init.appCtx

/**
 * Created by huangzhengneng on 2020/11/11.
 */
object ColorUtil {

    @JvmStatic
    @ColorInt
    fun getColor(context: Context, @ColorRes colorId: Int): Int {
        return if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            context.resources.getColor(colorId, context.theme)
        } else {
            context.resources.getColor(colorId)
        }
    }

    @JvmStatic
    fun getSkinColor(resId: Int): Int {
        val checkedColor = SkinCompatHelper.checkResourceId(resId)
        return if (checkedColor != 0) {
            SkinCompatResources.getColor(appCtx, checkedColor)
        } else {
            appCtx.resources.getColor(resId)
        }
    }

    @JvmStatic
    fun getSkinDrawable(resId: Int): Drawable {
        val checkedColor = SkinCompatHelper.checkResourceId(resId)
        return if (checkedColor != 0) {
            SkinCompatResources.getDrawable(appCtx, checkedColor)
        } else {
            appCtx.resources.getDrawable(resId)
        }
    }
}