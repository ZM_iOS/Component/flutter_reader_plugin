package com.col.lib_book.utils

import android.view.View

/**
 * <pre>
 *     author : Freedom
 *     e-mail : burst3166@gmail.com
 *     time   : 2020/11/16
 *     desc   :
 *     version: 1.0
 * </pre>
 */
val SHELF_MODE: String = "shelf_mode"
val USERID: String = "userId"
const val READER_THEME_DAY_TIME = "READER_THEME_DAY_TIME"
var mListAds:MutableMap<String, View> = mutableMapOf()
var mListCloseAds:MutableList<String> = mutableListOf()
