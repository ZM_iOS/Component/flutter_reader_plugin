package com.col.lib_book.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class DateUtil {
    //判断选择的日期是否是本周
    public static boolean isThisWeek(long time) {
        Calendar calendar = Calendar.getInstance();
        int currentWeek = calendar.get(Calendar.WEEK_OF_YEAR);
        calendar.setTime(new Date(time));
        int paramWeek = calendar.get(Calendar.WEEK_OF_YEAR);
        return paramWeek == currentWeek;
    }

    //判断选择的日期是否是今天  
    public static boolean isToday(long time) {
        return isThisTime(time, "yyyy-MM-dd");
    }

    //判断选择的日期是否是本月  
    public static boolean isThisMonth(long time) {
        return isThisTime(time, "yyyy-MM");
    }

    private static boolean isThisTime(long time, String pattern) {
        Date date = new Date(time);
        SimpleDateFormat sdf = new SimpleDateFormat(pattern);
        String param = sdf.format(date);//参数时间
        String now = sdf.format(new Date());//当前时间
        return param.equals(now);
    }

    /**
     * @param endTime 原来的会员到期时间
     * @param months  增加的月数
     * @return 返回增加天数后的 年月日 时分秒
     */
    public static String addMonthTime(long endTime, int months) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date(endTime));
        calendar.add(Calendar.MONTH, months);
        Date date = calendar.getTime();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return simpleDateFormat.format(date);
    }

    /**
     * @param months 增加的月数
     * @return 返回增加天数后的 年月日 时分秒
     */
    public static String addMonthTime(int months) {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.MONTH, months);
        Date date = calendar.getTime();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return simpleDateFormat.format(date);
    }

    /**
     * @return 返回现在的时间，精确到天数  yyyy-MM-dd
     */
    public static String getToday() {
        Calendar calendar = Calendar.getInstance();
        Date date = calendar.getTime();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        return simpleDateFormat.format(date);
    }

    /**
     * @return 返回现在的周， yyyy-week
     */
    public static String getWeek() {
        Calendar calendar = Calendar.getInstance();
        int currentWeek = calendar.get(Calendar.WEEK_OF_YEAR);
        Date date = calendar.getTime();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-");
        return simpleDateFormat.format(date) + currentWeek;
    }

    /**
     * 判断是否在有效期内
     *
     * @param startTime 开始时间，格式：2017-11-04 14:57:00
     * @param endTime   结束时间，格式：2017-11-04 14:57:00
     * @return
     */
    public static boolean isInTime(String startTime, String endTime) {
        long current = System.currentTimeMillis();
        long start = getTime(startTime);
        long end = getTime(endTime);
        return current > start && current < end;
    }

    // 将字符串转为时间戳
    public static long getTime(String user_time) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        long time = 0;
        try {
            Date d = sdf.parse(user_time);
            time = d.getTime();
        } catch (Exception e) {
        }
        return time;
    }

    public void testTime() {
        isToday(1416360654000L);
        isThisMonth(1416360654000L);
        isThisWeek(1416360654000L);
    }

    /**
     * 剩余时间格式装换
     *
     * @param endTime 结束时间  格式  yyyy-MM-dd HH:mm:ss
     */
    public static String getLeftTime(String endTime) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String timeStr = "00:00:00";
        int day = 0;
        int hour = 0;
        int minute = 0;
        int second = 0;
        try {
            Date date = sdf.parse(endTime);
            long end = date.getTime();
            long now = System.currentTimeMillis();
            //剩余时间 秒
            int time = (int) ((end - now) / 1000);
            if (time > 0) {
                minute = time / 60;
                if (minute < 60) {
                    second = time % 60;
                    timeStr = "00:" + unitFormat(minute) + ":" + unitFormat(second);
                } else {
                    hour = minute / 60;
                    minute = minute % 60;
                    if (hour < 24) {
                        second = time - hour * 3600 - minute * 60;
                        timeStr = unitFormat(hour) + ":" + unitFormat(minute) + ":" + unitFormat(second);
                    } else {
                        day = hour / 24;
                        hour = hour % 24;
                        second = time - day * 24 * 3600 - hour * 3600 - minute * 60;
                        timeStr = day + "天" + unitFormat(hour) + "小时" + unitFormat(minute) + "分" + unitFormat(second) + "秒";
                    }

                }
                return timeStr;
            } else {
                return timeStr;
            }

        } catch (ParseException e) {
            e.printStackTrace();
            return timeStr;
        }
    }

    public static String getLeftTime(long endTime) {
        String timeStr = "00:00:00";
        int day = 0;
        int hour = 0;
        int minute = 0;
        int second = 0;

        Date date = new Date(endTime);
        long end = date.getTime();
        long now = System.currentTimeMillis();
        //剩余时间 秒
        int time = (int) ((end - now) / 1000);
        if (time > 0) {
            minute = time / 60;
            if (minute < 60) {
                second = time % 60;
                timeStr = "00:" + unitFormat(minute) + ":" + unitFormat(second);
            } else {
                hour = minute / 60;
                minute = minute % 60;
                if (hour < 24) {
                    second = time - hour * 3600 - minute * 60;
                    timeStr = unitFormat(hour) + ":" + unitFormat(minute) + ":" + unitFormat(second);
                } else {
                    day = hour / 24;
                    hour = hour % 24;
                    second = time - day * 24 * 3600 - hour * 3600 - minute * 60;
                    timeStr = day + "天" + unitFormat(hour) + "小时" + unitFormat(minute) + "分" + unitFormat(second) + "秒";
                }

            }
            return timeStr;
        } else {
            return timeStr;
        }


    }

    private static String unitFormat(int i) {
        String retStr = null;
        if (i >= 0 && i < 10) {
            retStr = "0" + Integer.toString(i);
        } else {
            retStr = "" + i;
        }
        return retStr;
    }

    //获取当天的结束时间
    public static Date getDayEnd() {
        Calendar cal = new GregorianCalendar();
        cal.set(Calendar.HOUR_OF_DAY, 23);
        cal.set(Calendar.MINUTE, 59);
        cal.set(Calendar.SECOND, 59);
        return cal.getTime();
    }

    /**
     * 获取当前年份
     */
    public static int getYear() {
        return Calendar.getInstance().get(Calendar.YEAR);
    }

    /**
     * 获取当前月份
     */
    public static int getMonth() {
        return Calendar.getInstance().get(Calendar.MONTH) + 1;
    }
}  