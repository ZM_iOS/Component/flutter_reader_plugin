package com.col.lib_book.utils

import android.content.Context
import android.content.DialogInterface
import com.col.lib_book.widget.dialog.CustomDialog

/**
 * Created by huangzhengneng on 12/7/20.
 */
object DialogUtil {

    fun showDialog(
        context: Context?,
        title: String?,
        message: CharSequence?,
        negativeStr: String?,
        negativeListener: DialogInterface.OnClickListener?,
        positiveStr: String?,
        positiveListener: DialogInterface.OnClickListener?
    ): CustomDialog? {
        val builder: CustomDialog.Builder = CustomDialog.Builder(context)
        builder.setTitle(title)
        builder.setMessage(message)
        builder.setNegativeButton(negativeStr, negativeListener)
        builder.setPositiveButton(positiveStr, positiveListener)
        val dialog: CustomDialog = builder.create()
        dialog.show()
        return dialog
    }

}