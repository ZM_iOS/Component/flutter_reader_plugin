package com.col.lib_book.utils;

import android.content.res.AssetManager;
import android.text.TextUtils;

import com.chineseall.reader.lib.reader.entities.Chapter;
import com.col.lib_book.ReadManager;

import org.mozilla.universalchardet.UniversalDetector;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by huangzhengneng on 2020/4/17.
 */
public class FileUtils {


    public static List<Chapter> loadLocalTxtBook(String filePath) {
        File file = new File(filePath);
        if (!file.exists()) {
            throw new IllegalStateException("文件不存在");
        }
        String fileOutputString = FileUtils.getFileOutputString(filePath, getFileIncode(file));
        if (TextUtils.isEmpty(fileOutputString)) {
            throw new IllegalStateException("文件读取失败~");
        }
        String[] split = fileOutputString.split("\n");
        StringBuilder sb = new StringBuilder();

        Pattern compile;
        compile = Pattern
                .compile("^ {0,8}.{0,14}第\\s{0,6}[一二三四五六七八九十零百千万壹贰叁肆伍陆柒捌拾0-9１２３４５６７８９０]{1,8}\\s{0,6}[回章节卷部集册话篇].{0,20}");
        Matcher matcher = compile.matcher("");
        int lineNumber = 0;
        long offset = 0;
        int line_separator = 2;

        List<Chapter> chapterList = new ArrayList<>();

        Chapter chapter = null;

        for (String readLine : split) {
            sb.append(readLine);
            sb.append("\r\n");
            if (lineNumber == 0) {
                chapter = new Chapter();
                chapter.setBookName(getFileName(filePath));
                chapter.setFilePath(filePath);
                initChapter(offset, chapterList, chapter, readLine);
            }

            if (readLine.length() < 35) {
                boolean find = matcher.reset(readLine).find();
                if (find) {
                    chapter.setLineNumber(lineNumber);
                    lineNumber = 0;
                    chapterList.add(chapter);
                    chapter = new Chapter();
                    chapter.setBookName(getFileName(filePath));
                    chapter.setFilePath(filePath);
                    initChapter(offset, chapterList, chapter, readLine);
                }
            }
            offset += readLine.getBytes(StandardCharsets.UTF_8).length;
            offset += line_separator;
            lineNumber++;
        }

        FileUtils.writeFile(filePath, sb.toString(), false);
        if (chapter != null) {
            chapter.setLineNumber(lineNumber + 1);
            chapter.setNextId(-1);
            chapterList.add(chapter);
        }

        return chapterList;
    }

    private static void initChapter(long offset, List<Chapter> chapterList, Chapter chapter, String readLine) {
        if (readLine.length() > 30) {
            chapter.setChapterName(readLine.substring(0, 30) + "...");
        } else {
            if (TextUtils.isEmpty(readLine)) {
                readLine = "无标题";
            }
            chapter.setChapterName(readLine);
        }
        chapter.setLocal(true);
        chapter.setBookId("-1");
        chapter.setChapterName(chapter.getChapterName().trim());
        chapter.setChapterId(String.valueOf(chapterList.size() + 1));
        chapter.setNextId(chapterList.size() + 2);
        chapter.setPreId(chapterList.size());
        chapter.setOffset(offset);
        if (chapter.getPreId() < 0) {
            chapter.setPreId(0);
        }
    }

    /**
     * 获取文件内容
     *
     * @param path
     * @return
     */
    public static String getFileOutputString(String path, String charset) {
        try {
            File file = new File(path);
            InputStreamReader isr = new InputStreamReader(new FileInputStream(file), charset);
            BufferedReader bufferedReader = new BufferedReader(isr, 8192);
            StringBuilder sb = new StringBuilder();
            String line = null;
            while ((line = bufferedReader.readLine()) != null) {
                sb.append("\n").append(line);
            }
            bufferedReader.close();
            return sb.toString();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getFileIncode(File file) {

        if (!file.exists()) {
            LogUtils.d("getFileIncode: file not exists!");
            return null;
        }

//        try {
//            String encoding =  UniversalDetector.detectCharset(file);
//            if (encoding != null) {
//                System.out.println("Detected encoding = " + encoding);
//            } else {
//                System.out.println("No encoding detected.");
//                encoding = "gbk";
//            }
//            return encoding;
//        } catch (IOException e) {
//            Logger.e(e);
//        }

        byte[] buf = new byte[4096];
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(file);
            // (1)
            UniversalDetector detector = new UniversalDetector(null);

            // (2)
            int nread;
            while ((nread = fis.read(buf)) > 0 && !detector.isDone()) {
                detector.handleData(buf, 0, nread);
            }
            // (3)
            detector.dataEnd();

            // (4)
            String encoding = detector.getDetectedCharset();
            if (encoding != null) {
                LogUtils.d("Detected encoding = " + encoding);
            } else {
                LogUtils.d("No encoding detected.");
                encoding = "gbk";
            }

            // (5)
            detector.reset();
            fis.close();
            return encoding;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return "gbk";
    }

    /**
     * 将内容写入文件
     *
     * @param filePath eg:/mnt/sdcard/demo.txt
     * @param content  内容
     */
    public static synchronized void writeFile(String filePath, String content, boolean isAppend) {
//        Logger.i("save:" + filePath);
        try {
            FileOutputStream fout = new FileOutputStream(filePath, isAppend);
            byte[] bytes = content.getBytes();
            fout.write(bytes);
            fout.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String getFileName(String filePath) {
        if (TextUtils.isEmpty(filePath)) {
            return "";
        }
        File file = new File(filePath);
        if (!file.exists()) {
            return "";
        }
        String fileName = file.getName();
        int lastDotIndex = fileName.indexOf(".");
        if (lastDotIndex > 0) {
            return fileName.substring(0, lastDotIndex);
        }
        return "";
    }

    public static String TTS_PATH = CacheFileUtils.INSTANCE.createRootPath(ReadManager.INSTANCE.getGetApplication()) + "/tts/";

    public static String getTtsPath(String filename) {
        return TTS_PATH + filename;
    }

    public static void copyFromAssets(AssetManager assets, String source, String dest, boolean isCover) throws IOException {
        File file = new File(dest);
        if (!file.getParentFile().exists())
            file.getParentFile().mkdirs();
        if (isCover || (!isCover && !file.exists())) {
            InputStream is = null;
            FileOutputStream fos = null;
            try {
                is = assets.open(source);
                String path = dest;
                fos = new FileOutputStream(path);
                byte[] buffer = new byte[1024];
                int size = 0;
                while ((size = is.read(buffer, 0, 1024)) >= 0) {
                    fos.write(buffer, 0, size);
                }
            } finally {
                if (fos != null) {
                    try {
                        fos.close();
                    } finally {
                        if (is != null) {
                            is.close();
                        }
                    }
                }
            }
        }
    }

    /**
     * 资源文件获取
     */
    public static String getStringFromAssets(String fileName) {
        try {
            InputStreamReader inputReader = new InputStreamReader(
                    ReadManager.INSTANCE.getGetApplication().getResources().getAssets().open(fileName));
            BufferedReader bufReader = new BufferedReader(inputReader);
            String line = "";
            StringBuilder result = new StringBuilder();
            while ((line = bufReader.readLine()) != null) {
                result.append(line);
            }
            inputReader.close();
            return result.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return fileName;
    }

}
