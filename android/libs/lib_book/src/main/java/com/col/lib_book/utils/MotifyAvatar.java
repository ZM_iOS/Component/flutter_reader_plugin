package com.col.lib_book.utils;

public class MotifyAvatar {
    public static final int CAMERA_WITH_DATA = 1537;
    public static final int CROP_IMAGE = 1540;
    public static final int GALLERY_WITH_DATA = 1538;
    public static final int GALLERY_WITH_DATA_KITKAT = 1539;
}
