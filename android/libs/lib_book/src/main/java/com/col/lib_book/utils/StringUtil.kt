package com.col.lib_book.utils

import android.content.Context
import android.content.Intent
import android.graphics.Typeface
import android.net.Uri
import android.text.SpannableStringBuilder
import android.text.Spanned
import android.text.TextPaint
import android.text.TextUtils
import android.text.style.ClickableSpan
import android.text.style.URLSpan
import android.view.View
import androidx.annotation.NonNull


/**
 * Created by huangzhengneng on 2020/11/9.
 */
object StringUtil {

    /**
     * 格式化小说内容。
     *
     *
     *  * 小说的开头，缩进2格。在开始位置，加入2格空格。
     *  * 所有的段落，缩进2格。所有的\n,替换为2格空格。
     *
     * @param source
     * @return
     */
    @JvmStatic
    fun formatContent(source: String): String? {
        var str = source
        str = str.replace("[\u3000| ]+".toRegex(), " ") //替换来自服务器上的，特殊空格
        str = str.replace("\n\n", "\n")
        //        str = convertToSBC(str);
        return str
    }
    /**
     * 格式化超链接文本内容并设置点击处理
     */
    @JvmStatic
    fun getClickableHtml(html: CharSequence?): CharSequence? {
        if (TextUtils.isEmpty(html) || html !is Spanned) return html
        val clickableHtmlBuilder = SpannableStringBuilder(html)
        val urls = clickableHtmlBuilder.getSpans(
            0, html.length,
            URLSpan::class.java
        )
        for (span in urls) {
            setLinkClickable(clickableHtmlBuilder, span)
        }
        return clickableHtmlBuilder
    }

    /**
     * 设置点击超链接对应的处理内容
     */
    @JvmStatic
    private fun setLinkClickable(clickableHtmlBuilder: SpannableStringBuilder, urlSpan: URLSpan) {
        val start = clickableHtmlBuilder.getSpanStart(urlSpan)
        val end = clickableHtmlBuilder.getSpanEnd(urlSpan)
        val flags = clickableHtmlBuilder.getSpanFlags(urlSpan)
        val clickableSpan: ClickableSpan = object : ClickableSpan() {
            override fun onClick(@NonNull view: View) {
                val context: Context? = ActivityStackManager.getInstance().topActivity
                val uri: Uri = Uri.parse(urlSpan.url)
                val intent = Intent(Intent.ACTION_VIEW, uri)
                context?.startActivity(intent)
            }

            override fun updateDrawState(@NonNull ds: TextPaint) {
                super.updateDrawState(ds)
                ds.isUnderlineText = false
                ds.typeface = Typeface.DEFAULT_BOLD
            }
        }
        clickableHtmlBuilder.removeSpan(urlSpan)
        clickableHtmlBuilder.setSpan(clickableSpan, start, end, flags)
    }
}