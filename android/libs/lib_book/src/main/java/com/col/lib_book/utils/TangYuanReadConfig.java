package com.col.lib_book.utils;

import static com.chineseall.reader.lib.reader.core.Constants.AUTO_SCROLL_SPEED;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.util.DisplayMetrics;

import com.chineseall.reader.lib.reader.config.IReadConfig;
import com.chineseall.reader.lib.reader.config.ReaderConfigWrapper;
import com.chineseall.reader.lib.reader.config.TangYuanSharedPrefUtils;
import com.col.lib_book.R;

import skin.support.content.res.SkinCompatResources;
import skin.support.widget.SkinCompatHelper;

public class TangYuanReadConfig implements IReadConfig {

    private Context context;

    private int pageWidth; // 阅读页面绘制区域宽度 (默认屏幕宽度)
    private int pageHight;  // 阅读页面绘制区域高度 (默认屏幕高度)
    private float screenDensity = 1f; // 屏幕密度

    private int topBarHeight; // 顶部显示 章节进度，时间，电量的 bar 的高度
    private int bottomBarHeight; // 底部显示 页码进度 的 bar 的高度

    private int padding; // 页面内边距
    private int lineSpace; // 正文的行间距
    private int paragraphSpace; // 正文中的段落间距

    private int bgColor;  // 页面的背景色

    private int textSize;  // 正文字号大小
    private int textColor;  // 正文的文字颜色
    private int lineColor;
    private Typeface textTypeface = null;  // 正文的文字的字体

    private int titleTextSize;  // 正文标题的字号大小
    private int titleTextColor;  // 正文标题的文字颜色
    private Typeface titleTypeface = null; // 正文标题的字体

    private int tailTextSize;  // 作者有话说的字号大小
    private int tailTextColor;  //作者有话说的文字颜色
    private String tailTitle = "作者有话说:"; //作者有话说的作者名

    private int headerTextSize; // 页眉文字大小
    private int footerTextSize;  // 页脚文字大小
    private int pageTopBottomExtraTextColor;  //阅读页面顶部，章节信息，时间以及底部页码的文字颜色

    private int imageMargin;  // 图片绘制时，四周边距

    private int animateTime;  // 横向翻页时，翻页动画效果时长(毫秒)

    private int verticalChapterMargin;  // 垂直翻页时，两个章节的间距

    private int titleBetweenContentGapSpace; // 章节标题 与 正文之间的间距

    private boolean isCatalogPageShowBookInfoBlock; //在章节目录列表最上面，是否显示书籍简介信息块

    private boolean isShowChapterTransitPage;  //阅读过程中，章节结束，是否显示章节过渡页
    private boolean isShowBookEndRecommendPage;//阅读过程中，书籍结束，是否显示书尾推荐页

    private boolean isShowFloatCollectLayer; //阅读过程中，是否弹出喜欢收藏的浮层
    private boolean isShowCollectItemMenu; //是否显示收藏弹出菜单

    private boolean isShowReaderImage; // 阅读章节中的图片，是否显示
    private int defaultReaderImageResId = 0;  // 阅读章节中的图片如果不显示，替代的默认图显示
    private int defaultReaderImageWidth;
    private int defaultReaderImageHeight;

    public static final String AUTO_SCROLL_SPEED_LEVEL = "auto_scroll_speed_level";

    public TangYuanReadConfig(Context context) {
        this.context = context;

        DisplayMetrics display = context.getResources().getDisplayMetrics();
        pageWidth = display.widthPixels;
        pageHight = display.heightPixels;
        screenDensity = display.density;

        TangYuanSharedPrefUtils sharedPrefUtil = TangYuanSharedPrefUtils.getInstance();

        padding = DensityUtil.dip2px(context, 16);


//        paragraphSpace = DensityUtil.dip2px(context, 10);

        textSize = DensityUtil.dip2px(context, sharedPrefUtil.getFontSize(TEXT_SIZE_DEFAULT));

        Paint paint = new Paint();
        paint.setAntiAlias(true);
//        Typeface createFromAsset = Typeface.createFromAsset(ReaderApplication.getApp().getAssets(),"fzlth.ttf");
//        paint.setTypeface(createFromAsset);
        paint.setTextSize(textSize);
        int ceil = (int) paint.measureText("啊");
        int size = ceil * 2 / 3;

        lineSpace = sharedPrefUtil.getPageLineGap(size);
        paragraphSpace = lineSpace;// 段间距和行间距一样


        int speedLevel = sharedPrefUtil.getInt(AUTO_SCROLL_SPEED_LEVEL, 5);
        int lineNumber = 48;
        switch (speedLevel) {
            case 1:
                lineNumber = 35;
                break;
            case 2:
                lineNumber = 38;
                break;
            case 3:
                lineNumber = 42;
                break;
            case 4:
                lineNumber = 45;
                break;
            case 5:
                lineNumber = 48;
                break;
            case 6:
                lineNumber = 51;
                break;
            case 7:
                lineNumber = 54;
                break;
            case 8:
                lineNumber = 57;
                break;
            case 9:
                lineNumber = 60;
                break;
            case 10:
                lineNumber = 63;
                break;
        }
        sharedPrefUtil.putInt(AUTO_SCROLL_SPEED, lineNumber * (textSize + lineSpace) / 60);


        titleTextSize = DensityUtil.dip2px(context, sharedPrefUtil.getFontSize(TEXT_SIZE_DEFAULT) + 4);
        tailTextSize = DensityUtil.dip2px(context, sharedPrefUtil.getFontSize(TEXT_SIZE_DEFAULT));

        topBarHeight = DensityUtil.dip2px(context, 42);
        bottomBarHeight = DensityUtil.dip2px(context, 42);

        headerTextSize = DensityUtil.dip2px(context, 12);
        footerTextSize = DensityUtil.dip2px(context, 12);

        imageMargin = DensityUtil.dip2px(context, 18);

        animateTime = 1000;

        verticalChapterMargin = topBarHeight;

        titleBetweenContentGapSpace = DensityUtil.dip2px(context, 26);

        int sceneMode = sharedPrefUtil.getSceneMode(ReaderConfigWrapper.SKIN_THEME_DAYTIME);

        lineColor = getSkinColor(R.color.col_reader_voice_line_bg);


        bgColor = getSkinColor(R.color.col_reader_sence_bg_color);
        textColor = getSkinColor(R.color.col_reader_sence_text_color);
        titleTextColor = getSkinColor(R.color.col_reader_sence_title_color);
        tailTextColor = getSkinColor(R.color.col_reader_sence_tail_color);
        pageTopBottomExtraTextColor = getSkinColor(R.color.col_reader_sence_top_bottom_extra_txt_color);

        isCatalogPageShowBookInfoBlock = true;

        isShowChapterTransitPage = false;
        isShowBookEndRecommendPage = true;

        isShowFloatCollectLayer = true;
        isShowCollectItemMenu = true;


        int showImageType = sharedPrefUtil.getShowImageType(MotifyAvatar.CAMERA_WITH_DATA);
        if (showImageType == MotifyAvatar.CAMERA_WITH_DATA) {
            this.isShowReaderImage = true;
        } else if (showImageType == MotifyAvatar.GALLERY_WITH_DATA_KITKAT) {
            this.isShowReaderImage = false;
        } else if (showImageType == MotifyAvatar.GALLERY_WITH_DATA) {
            this.isShowReaderImage = true;
        }
        if (!this.isShowReaderImage) {
            initDefaultImageResouce();
        }
    }

    private int getSkinColor(int resId) {
        int checkedColor = SkinCompatHelper.checkResourceId(resId);
        if (checkedColor != 0) {
            return SkinCompatResources.getColor(context, checkedColor);
        } else {
            return context.getResources().getColor(resId);
        }
    }

    private void initDefaultImageResouce() {
        if (defaultReaderImageResId == 0) {
            defaultReaderImageResId = R.drawable.col_ic_bookshelf;

            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeResource(context.getResources(), defaultReaderImageResId, options);

            defaultReaderImageWidth = options.outWidth;
            defaultReaderImageHeight = options.outHeight;
        }
    }

    @Override
    public int getPageTopBarHeight() {
        return topBarHeight;
    }

    @Override
    public int getPageBottomBarHeight() {
        return bottomBarHeight;
    }

    @Override
    public int getPageBgColor() {
        return bgColor;
    }

    @Override
    public int getPadding() {
        return padding;
    }

    @Override
    public int getLineSpace() {
        return lineSpace;
    }

    @Override
    public int getParagraphSpace() {
        return paragraphSpace;
    }

    @Override
    public int getImageMargin() {
        return imageMargin;
    }

    @Override
    public int getPageHeight() {
        return pageHight;
    }

    @Override
    public int getPageWidth() {
        return pageWidth;
    }

    @Override
    public float getScreenDensity() {
        return screenDensity;
    }

    @Override
    public int getTextSize() {
        return textSize;
    }

    @Override
    public int getTextColor() {
        return textColor;
    }

    @Override
    public int getLineColor() {
        return lineColor;
    }

    @Override
    public Typeface getTextTypeface() {
        return textTypeface;
    }

    @Override
    public int getTitleTextSize() {
        return titleTextSize;
    }

    @Override
    public int getTitleColor() {
        return titleTextColor;
    }

    @Override
    public int getTailTextSize() {
        return tailTextSize;
    }

    @Override
    public int getTailColor() {
        return tailTextColor;
    }

    @Override
    public String getTailTitle() {
        return tailTitle;
    }

    @Override
    public void setTailTitle(String tailTitle) {
        this.tailTitle = tailTitle;
    }

    @Override
    public Typeface getTitleTypeface() {
        return titleTypeface;
    }

    @Override
    public int getPageTopBottomExtraTextColor() {
        return pageTopBottomExtraTextColor;
    }

    @Override
    public int getPageHeaderTextSize() {
        return headerTextSize;
    }

    @Override
    public int getPageFooterTextSize() {
        return footerTextSize;
    }

    @Override
    public int getAnimateTime() {
        return animateTime;
    }

    @Override
    public int getVerticalChapterMargin() {
        return verticalChapterMargin;
    }

    @Override
    public int getTitleBetweenContentGapSpace() {
        return titleBetweenContentGapSpace;
    }

    @Override
    public boolean isCatalogPageShowBookInfoBlock() {
        return isCatalogPageShowBookInfoBlock;
    }

    @Override
    public boolean isShowChapterTransitPage() {
        return isShowChapterTransitPage;
    }

    @Override
    public boolean isShowBookEndRecommendPage() {
        return isShowBookEndRecommendPage;
    }

    public boolean isShowFloatCollectLayer() {
        return isShowFloatCollectLayer;
    }

    @Override
    public boolean isShowCollectItemMenu() {
        return isShowCollectItemMenu;
    }

    @Override
    public boolean isShowContentImage() {
        return isShowReaderImage;
    }

    @Override
    public int getDefaultImageResId() {
        return defaultReaderImageResId;
    }

    @Override
    public int getDefaultImageWidth() {
        return defaultReaderImageWidth;
    }

    @Override
    public int getDefaultImageHight() {
        return defaultReaderImageHeight;
    }

}
