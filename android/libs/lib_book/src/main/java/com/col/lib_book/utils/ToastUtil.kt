package com.col.lib_book.utils

import android.app.Activity
import android.os.Handler
import android.os.Looper
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.coordinatorlayout.widget.CoordinatorLayout
import com.col.lib_book.R
import com.col.lib_book.ReadManager.isDebug
import com.col.lib_book.base.BaseActivity
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.google.android.material.snackbar.BaseTransientBottomBar.BaseCallback
import com.google.android.material.snackbar.Snackbar
import splitties.init.appCtx

/**
 * Created by Freedom on 17/1/4.
 */
object ToastUtil {
    private var make: Snackbar? = null


    @JvmStatic
    fun toast(msg:String){
        if (Looper.myLooper() != Looper.getMainLooper()) {
            Handler(Looper.getMainLooper()).post {
                showToast(msg)
            }
        } else {
            showToast(msg)
        }
    }

    @Synchronized
    private fun showSnackBar(activity: Activity, text: String) {
        try {
            val root = activity.findViewById<View>(android.R.id.content)
            if (activity is BaseActivity) {
                val fragments = activity.supportFragmentManager.fragments
                if (fragments.size > 0) {
                    for (fragment in fragments) {
                        if (fragment is BottomSheetDialogFragment) {
                            showSystemToast(text)
                            return
                        }
                    }
                }
            }
            if (root.findViewById<View?>(R.id.snackbar_text) == null) {
                if (make == null || make!!.context !== activity) {
                    make = getBar(root, text)
                } else {
                    make!!.setText(text)
                }
            } else {
                if (make == null || make!!.context !== activity) {
                    make = getBar(root, text)
                } else {
                    make!!.setText(text)
                }
            }
            make!!.show()
        } catch (e: Exception) {
            showSystemToast(text)
        }
    }

    private fun getBar(root: View, text: String): Snackbar {
        val make = Snackbar.make(root, text, Snackbar.LENGTH_SHORT)
        val view = make.view
        when (val layoutParams1 = view.layoutParams) {
            is FrameLayout.LayoutParams -> {
                layoutParams1.width = ViewGroup.LayoutParams.WRAP_CONTENT
                layoutParams1.gravity = Gravity.CENTER
                view.layoutParams = layoutParams1
            }
            is ConstraintLayout.LayoutParams -> {
                layoutParams1.rightToRight = ConstraintLayout.LayoutParams.PARENT_ID
                layoutParams1.leftToLeft = ConstraintLayout.LayoutParams.PARENT_ID
                layoutParams1.bottomToBottom = ConstraintLayout.LayoutParams.PARENT_ID
                layoutParams1.topToTop = ConstraintLayout.LayoutParams.PARENT_ID
                layoutParams1.width = ViewGroup.LayoutParams.WRAP_CONTENT
                view.layoutParams = layoutParams1
            }
            is CoordinatorLayout.LayoutParams -> {
                layoutParams1.gravity = Gravity.CENTER
                layoutParams1.width = ViewGroup.LayoutParams.WRAP_CONTENT
                view.layoutParams = layoutParams1
            }
        }
        val tv = view.findViewById<TextView>(R.id.snackbar_text)
        view.setBackgroundResource(R.drawable.col_bg_toast)
        if (isDebug) {
            tv.maxLines = 20
        } else {
            tv.maxLines = 10
        }
        val layoutParams = tv.layoutParams as LinearLayout.LayoutParams
        layoutParams.width = ViewGroup.LayoutParams.WRAP_CONTENT
        layoutParams.gravity = Gravity.CENTER
        tv.textAlignment = View.TEXT_ALIGNMENT_INHERIT
        tv.layoutParams = layoutParams
        tv.gravity = Gravity.CENTER
        tv.setTextColor(tv.context.resources.getColor(R.color.col_white))
        make.addCallback(object : BaseCallback<Snackbar?>() {
            override fun onDismissed(transientBottomBar: Snackbar?, event: Int) {
                super.onDismissed(transientBottomBar, event)
                ToastUtil.make = null
            }
        })
        return make
    }

    fun showToast(text: String) {
        val topActivity = ActivityStackManager.getInstance().topActivity
        if (topActivity != null) {
            showCenter(topActivity.javaClass.name, text)
        } else {
            showSystemToast(text)
        }
    }

    fun showCenter(className: String?, text: String) {
        var activityByClass: Activity? = null
        try {
            activityByClass =
                ActivityStackManager.getInstance().getActivityByClass(Class.forName(className))
        } catch (e: Exception) {
            e.printStackTrace()
        }
        if (activityByClass != null) {
            showSnackBar(activityByClass, text)
        } else {
            showSystemToast(text)
        }
    }

    @JvmOverloads
    fun showSystemToast(text: String?, gravity: Int = Gravity.CENTER, x: Int = 0, y: Int = 0) {
        val toast = Toast(appCtx)
        val inflate =
            View.inflate(appCtx, R.layout.col_custom_toast, null)
        val tv = inflate.findViewById<View>(R.id.tv_toast) as TextView
        tv.text = text
        toast.duration = Toast.LENGTH_SHORT
        toast.view = inflate
        toast.setGravity(gravity, x, y)
        toast.show()
    }
}