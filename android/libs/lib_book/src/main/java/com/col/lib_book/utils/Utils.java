package com.col.lib_book.utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.PackageManager;
import android.provider.Settings;
import android.text.TextUtils;

import java.lang.reflect.Method;
import java.util.UUID;

public class Utils {

    /**
     * 检测权限
     *
     * @param context Context
     * @param permission 权限名称
     * @return true:已允许该权限; false:没有允许该权限
     */
    public static boolean checkHasPermission(Context context, String permission) {
        try {
            Class<?> contextCompat = null;
            try {
                contextCompat = Class.forName("android.support.v4.content.ContextCompat");
            } catch (Exception e) {
                //ignored
            }

            if (contextCompat == null) {
                try {
                    contextCompat = Class.forName("androidx.core.content.ContextCompat");
                } catch (Exception e) {
                    //ignored
                }
            }

            if (contextCompat == null) {
                return true;
            }

            Method checkSelfPermissionMethod = contextCompat.getMethod("checkSelfPermission", Context.class, String.class);
            int result = (int) checkSelfPermissionMethod.invoke(null, new Object[]{context, permission});
            if (result != PackageManager.PERMISSION_GRANTED) {
                return false;
            }

            return true;
        } catch (Exception e) {
            return true;
        }
    }


    /**
     * 获取运营商
     *
     * @return
     */
    public static String getProvider(Context context) {
        return "未知";
    }

    /**
     * 此方法谨慎修改
     * 插件配置 disableAndroidID 会修改此方法
     * 获取 Android ID
     *
     * @param mContext Context
     * @return androidID
     */
    @SuppressLint("HardwareIds")
    public static String getAndroidID(Context mContext) {
        String androidID = "";
        try {
            androidID = Settings.Secure.getString(mContext.getContentResolver(), Settings.Secure.ANDROID_ID);
        } catch (Exception e) {
        }
        if (TextUtils.isEmpty(androidID)){
            androidID = UUID.randomUUID().toString();
        }
        return androidID;
    }

//    if (opt.equals("中国移动")){
//        opt = "cmcc";
//    }else if (opt.equals("中国联通")){
//        opt = "cucc";
//    }else if (opt.equals("中国电信")){
//        opt = "ctcc";
//    }else {
//        opt = "none";
//    }


}
