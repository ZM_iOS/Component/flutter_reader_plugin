package com.col.lib_book.utils.voice;

/**
 * Created by huangzhengneng on 2020/9/28.
 */

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.os.IBinder;

import com.col.lib_book.utils.LogUtils;

import org.greenrobot.eventbus.EventBus;

public class ChatListenService extends Service {

    private static final String TAG = "ChatListenService";

    private boolean isChangeToPause = false;
    private AudioManager ams = null;

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        initAudio();
        super.onCreate();
    }

    private void initAudio() {
        ams = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        if (ams != null) {
            int ans = ams.requestAudioFocus(mAudioFocusListener, AudioManager.STREAM_MUSIC,
                    AudioManager.AUDIOFOCUS_GAIN);
            LogUtils.d(TAG, "requestAudioFocus：" + ans);
        }
    }

    /**
     * 操作	                                                    状态	                                            处理方式
     * 录制语音消息/播放语音消息	                            [AudioManager]#AUDIOFOCUS_LOSS_TRANSIENT	        暂停播放
     * 被动接收QQ/微信电话	                                    [AudioManager]#AUDIOFOCUS_LOSS_TRANSIENT	        暂停播放
     * 主动拨打QQ/微信电话 & 使用第三方的播放器听歌（酷狗、QQ音乐等）	[AudioManager]#AUDIOFOCUS_LOSS	                    暂停播放
     */
    private final AudioManager.OnAudioFocusChangeListener mAudioFocusListener = focusChange -> {
        LogUtils.d(TAG, "focusChange----------" + focusChange);

        if (focusChange == 1) { // 获得AudioFocus
            if (isChangeToPause) {
                LogUtils.d(TAG, "playResume()" + focusChange);
            }
        } else { // 失去AudioFocus
            isChangeToPause = true;
            EventBus.getDefault().post(new PauseVoiceReaderEvent());
            LogUtils.d(TAG, "playPause()" + focusChange);

            // 实测收到一次后就不会再收到了，需要注销再监听
            abandon();
            initAudio();
        }
    };

    /**
     * 服务销毁的时候调用的方法
     */
    @Override
    public void onDestroy() {
        super.onDestroy();
        abandon();

        LogUtils.d(TAG, "onDestroy()");
    }

    private void abandon() {
        if (ams != null) {
            ams.abandonAudioFocus(mAudioFocusListener);
        }
    }

}
