package com.col.lib_book.utils.voice;

import com.chineseall.reader.lib.reader.config.TangYuanSharedPrefUtils;
import com.col.lib_book.ReadManager;
import com.col.lib_book.utils.FileUtils;
import com.col.lib_book.utils.LogUtils;
import com.col.lib_book.widget.VoiceControllerView;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Freedom on 2018/1/16.
 */

public class TtsUtil {

    public static String textFilename;
    public static String modelFilename;

    public static final String DEFAULT_SPEAKER = "0";
    public static final String VOICE_FEMALE = "F";

    public static final String VOICE_MALE = "M";

    public static Map<String, String> getParams(){
        Map<String, String> params = new HashMap<String, String>();
        // 以下参数均为选填
        boolean isVoiceModeOnline = TangYuanSharedPrefUtils.getInstance().getBoolean(VoiceControllerView.SP_VOICE_MODE_ONLINE, true);
//        params.put(SpeechSynthesizer.PARAM_SPEAKER, SharedPreferencesUtil.getInstance().getString(SpeechSynthesizer.PARAM_SPEAKER + !isVoiceModeOnline,DEFAULT_SPEAKER)); // 设置在线发声音人： 0 普通女声（默认） 1 普通男声 2 特别男声 3 情感男声<度逍遥> 4 情感儿童声<度丫丫>
//        params.put(SpeechSynthesizer.PARAM_VOLUME, SharedPreferencesUtil.getInstance().getString(SpeechSynthesizer.PARAM_VOLUME,"5")); // 设置合成的音量，0-9 ，默认 5
//        params.put(SpeechSynthesizer.PARAM_SPEED, SharedPreferencesUtil.getInstance().getString(SpeechSynthesizer.PARAM_SPEED,"5"));// 设置合成的语速，0-9 ，默认 5
//        params.put(SpeechSynthesizer.PARAM_PITCH, SharedPreferencesUtil.getInstance().getString(SpeechSynthesizer.PARAM_PITCH,"5"));// 设置合成的语调，0-9 ，默认 5
////        if (SharedPreferencesUtil.getInstance().getBoolean(Constant.SP_VOICE_MODE_ONLINE,true)){
////            params.put(SpeechSynthesizer.PARAM_MIX_MODE, SpeechSynthesizer.MIX_MODE_HIGH_SPEED_NETWORK);
////        }else {
//            params.put(SpeechSynthesizer.PARAM_MIX_MODE, SpeechSynthesizer.MIX_MODE_DEFAULT);
////        }
        // MIX_MODE_DEFAULT 默认 ，wifi状态下使用在线，非wifi离线。在线状态下，请求超时6s自动转离线
        // MIX_MODE_HIGH_SPEED_SYNTHESIZE_WIFI wifi状态下使用在线，非wifi离线。在线状态下， 请求超时1.2s自动转离线
        // MIX_MODE_HIGH_SPEED_NETWORK ， 3G 4G wifi状态下使用在线，其它状态离线。在线状态下，请求超时1.2s自动转离线
        // MIX_MODE_HIGH_SPEED_SYNTHESIZE, 2G 3G 4G wifi状态下使用在线，其它状态离线。在线状态下，请求超时1.2s自动转离线
        return params;
    }



//    public static void setTtsSpeed(SpeechSynthesizer speechSynthesizer, String value){
//        speechSynthesizer.setParam(SpeechSynthesizer.PARAM_SPEED,value);
//        SharedPreferencesUtil.getInstance().putString(SpeechSynthesizer.PARAM_SPEED,value);
//    }
//
//    public static void setTtsVolume(SpeechSynthesizer speechSynthesizer, String value){
//        speechSynthesizer.setParam(SpeechSynthesizer.PARAM_VOLUME,value);
//        SharedPreferencesUtil.getInstance().putString(SpeechSynthesizer.PARAM_VOLUME,value);
//    }
//
//    public static void setTtsPitch(SpeechSynthesizer speechSynthesizer, String value){
//        speechSynthesizer.setParam(SpeechSynthesizer.PARAM_PITCH,value);
//        SharedPreferencesUtil.getInstance().putString(SpeechSynthesizer.PARAM_PITCH,value);
//    }
//
//    public static void setTtsMixMode(SpeechSynthesizer speechSynthesizer,boolean isOnlineMode){
//        if (isOnlineMode){
//            speechSynthesizer.setParam(SpeechSynthesizer.PARAM_MIX_MODE,SpeechSynthesizer.MIX_MODE_HIGH_SPEED_NETWORK);
//        }else {
//            speechSynthesizer.setParam(SpeechSynthesizer.PARAM_MIX_MODE,SpeechSynthesizer.MIX_MODE_DEFAULT);
//        }
//        SharedPreferencesUtil.getInstance().putBoolean(VoiceControllerView.SP_VOICE_MODE_ONLINE,isOnlineMode);
//    }
//
//    public static void setTtsSpeaker(SpeechSynthesizer speechSynthesizer, String value, boolean isOffline){
//
//        SharedPreferencesUtil.getInstance().putString(SpeechSynthesizer.PARAM_SPEAKER+isOffline,value);
//        if (!isOffline) { // 设置在线语音类型时，把离线也改一下
//            SharedPreferencesUtil.getInstance().putString(SpeechSynthesizer.PARAM_SPEAKER+false,value);
//        }
////        if (isOffline) {
//            setOfflineVoiceType(value);
//            speechSynthesizer.setParam(SpeechSynthesizer.PARAM_TTS_TEXT_MODEL_FILE, textFilename); // 文本模型文件路径 (离线引擎使用)
//            speechSynthesizer.setParam(SpeechSynthesizer.PARAM_TTS_SPEECH_MODEL_FILE, modelFilename);
//            speechSynthesizer.loadModel(modelFilename, textFilename);
////        }
//        speechSynthesizer.setParam(SpeechSynthesizer.PARAM_SPEAKER,value);
//
//    }



    // 0 普通女声（默认） 1 普通男声 2 特别男声 3 情感男声<度逍遥> 4 情感儿童声<度丫丫>
    public static void setOfflineVoiceType(String voiceType)  {
        String text = "bd_etts_text.dat";
        String model = "";

        switch (voiceType){
            case "0":
                model = "bd_etts_common_speech_f7_mand_eng_high_am-mix_v3.0.0_20170512.dat";//离线女生
                break;
            case "1":
                model = "bd_etts_common_speech_m15_mand_eng_high_am-mix_v3.0.0_20170505.dat";//离线男生
                break;
            case "2": // 在线模式选择特别男声时，离线设置成情感男声
//                break;
            case "3":
                model = "bd_etts_common_speech_yyjw_mand_eng_high_am-mix_v3.0.0_20170512.dat";//度逍遥
                break;
            case "4":
                model = "bd_etts_common_speech_as_mand_eng_high_am_v3.0.0_20170516.dat";//度丫丫
                break;
        }

        textFilename = FileUtils.getTtsPath(text);
        try {
            FileUtils.copyFromAssets(ReadManager.INSTANCE.getGetApplication().getAssets(),"voice/"+text,textFilename,false);
        } catch (IOException e) {
            e.printStackTrace();
        }

        modelFilename = FileUtils.getTtsPath(model);
        try {
            FileUtils.copyFromAssets(ReadManager.INSTANCE.getGetApplication().getAssets(),"voice/"+model,modelFilename,false);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
