package com.col.lib_book.utils.voice;

import com.chineseall.reader.lib.reader.entities.Chapter;
import com.chineseall.reader.lib.reader.entities.Paragraph;

/**
 * Created by huangzhengneng on 2020/4/24.
 */
public interface VoiceCallBack {

    void turnPage(boolean next);

    void gotoNextChapter();

    Paragraph getCurrentParagraph();

    void setCurrentParagraph(Paragraph paragraph);

    int getCurrentParagraphIndex();

    void setCurrentParagraphIndex(int index);

    Chapter getCurrentChapter();

    Chapter getNextChapter();

    void invalidateReaderView();

    void exit();

    void initResult(boolean success);
}
