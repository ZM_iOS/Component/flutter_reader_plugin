package com.col.lib_book.utils.voice;

import static com.chineseall.reader.lib.reader.config.IReadConfig.STATUS_LOADED;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;

import com.chineseall.reader.lib.reader.entities.Chapter;
import com.chineseall.reader.lib.reader.entities.LineBlock;
import com.chineseall.reader.lib.reader.entities.PageData;
import com.chineseall.reader.lib.reader.entities.Paragraph;
import com.chineseall.reader.lib.reader.utils.StringUtil;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * 语音阅读相关
 * <p>
 * Created by huangzhengneng on 2020/4/24.
 */
public class VoiceManager {

    private static final String TAG = "VoiceManager";

    private String mAppId = "";
    private String mAppKey = "";
    private String mAppSecret = "";

    private static volatile VoiceManager sInstance;

    private boolean isTtsInitialized = false;
//    private SpeechSynthesizer mSpeechSynthesizer;
    private Context mContext;
    private VoiceCallBack mVoiceCallBack;

    private Handler mHandler = new Handler(Looper.getMainLooper());

    private int mCurrIndex = 0;
    private int mSentenceSize = 0;

    private int readCharCount = 0;

    /**
     * 切割成多个list的段落，如一个段落切割成句子之后，list的size过大，合成会失败
     */
//    private Queue<List<SpeechSynthesizeBag>> mSplitList = new LinkedList<>();

    private VoiceManager(Context context) {
        mContext = context.getApplicationContext();
    }

    public static VoiceManager getInstance(Context context) {
        if (sInstance == null) {
            synchronized (VoiceManager.class) {
                if (sInstance == null) {
                    sInstance = new VoiceManager(context);
                }
            }
        }
        return sInstance;
    }

//    private SpeechSynthesizerListener mSpeechSynthesizerListener = new SpeechSynthesizerListener() {
//
//        private int lastCharCount = 0;
//
//        @Override
//        public void onSynthesizeStart(String s) {
//            //合成开始
////            LogUtils.d(TAG, "onSynthesizeStart: " + s);
//        }
//
//        @Override
//        public void onSynthesizeDataArrived(String s, byte[] bytes, int i) {
//            //合成过程中的数据回调接口
////            LogUtils.d(TAG, "onSynthesizeDataArrived: " + s + ", bytes: " + bytes.length + ", i: " + i);
//        }
//
//        @Override
//        public void onSynthesizeFinish(String s) {
//            //合成结束
////            LogUtils.d(TAG, "onSynthesizeFinish: " + s);
//        }
//
//        @Override
//        public void onSpeechStart(String s) {
//            //播放开始
////            LogUtils.d(TAG, "onSpeechStart: " + s);
//        }
//
//        /**
//         *
//         * @param s
//         * @param i 表示当前读的字符长度，如果是英文单词，可能大于1
//         */
//        @Override
//        public void onSpeechProgressChanged(String s, int i) {
////            LogUtils.d(TAG, "onSpeechProgressChanged: " + s + ", i: " + i);
//            // 播放过程中的回调
//            if (lastCharCount <= i) {
//                readCharCount = readCharCount + (i - lastCharCount);
//            } else {
//                readCharCount += i;
//            }
////            LogUtils.d(TAG, "onSpeechProgressChanged: " + s + ", i: " + i + ", lastCharCount: " + lastCharCount + ", readCharCount: " + readCharCount);
//            lastCharCount = i;
//            if (mVoiceCallBack != null) {
//                if (i > 0 && needTurnPage()) {
//                    mHandler.post(() -> mVoiceCallBack.turnPage(true));
//                }
//            }
//        }
//
//        private boolean needTurnPage() {
//            Chapter currentChapter = mVoiceCallBack.getCurrentChapter();
//            int currentPageIndex = currentChapter.getCurrentPageIndex();
//            if (currentPageIndex < currentChapter.getPageDataList().size()) {
//                PageData pageData = currentChapter.getPageDataList().get(currentPageIndex);
//                // 当前页最后一段在当前页的最后一句读完了
//                return mVoiceCallBack.getCurrentParagraphIndex() > pageData.getParagraphs().size() - 1;
//            }
//            return false;
//        }
//
//        @Override
//        public void onSpeechFinish(String s) {
////            LogUtils.d(TAG, "onSpeechFinish: " + s);
//            if (mVoiceCallBack == null) return;
//            if (mCurrIndex < mSentenceSize - 1) {
//                mCurrIndex++;
//                return;
//            }
//            mCurrIndex = 0;
//            if (!mSplitList.isEmpty()) {
//                batchSpeak(mSplitList.poll());
//                return;
//            }
//            try {
//                readCharCount = 0;
//                exitPlayVoice();
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        }
//
//        @Override
//        public void onError(String s, SpeechError speechError) {
//            //合成和播放过程中出错时的回调
////            LogUtils.d(TAG, "onError: " + s + ":" + speechError.code + ":" + speechError.description);
//        }
//    };

    public void start(VoiceCallBack callBack) {
        checkConfig();
        mVoiceCallBack = callBack;
        if (init()) return;
        startSpeak();
    }

    public void setAppConfig(String appId, String appKey, String appSecret) {
        this.mAppId = appId;
        this.mAppKey = appKey;
        this.mAppSecret = appSecret;

        checkConfig();
    }

    private void checkConfig() {
        if (StringUtil.isBlank(mAppId) || StringUtil.isBlank(mAppKey) || StringUtil.isBlank(mAppSecret)) {
            throw new IllegalArgumentException("语音初始化参数为空！请检查是否设置了APP_ID、APP_KEY、APP_SECRET等参数!");
        }
    }

    public void setReadMode(String voice, boolean isOffline, boolean isOnlineMode) {
//        if (mVoiceCallBack == null || mSpeechSynthesizer == null) return;
//        mSpeechSynthesizer.stop();
//        TtsUtil.setTtsSpeaker(mSpeechSynthesizer, voice, isOffline);
//        TtsUtil.setTtsMixMode(mSpeechSynthesizer, isOnlineMode);
//        if (mVoiceCallBack.getCurrentParagraph() != null) {
//            resetData();
////            LogUtils.d("setReadMode");
//            speak(mVoiceCallBack.getCurrentParagraph().getContentByLines());
//        }
    }

    public void setPlaySpeed(String speed) {
//        if (mVoiceCallBack == null || mSpeechSynthesizer == null) return;
//        mSpeechSynthesizer.stop();
//        TtsUtil.setTtsSpeed(mSpeechSynthesizer, speed);
//        if (mVoiceCallBack.getCurrentParagraph() != null) {
//            resetData();
////            LogUtils.d("setPlaySpeed");
//            speak(mVoiceCallBack.getCurrentParagraph().getContentByLines());
//        }
    }

    private void resetData() {
        // 重新读当前段落，需要清除待读列表，句子size，当前段落，当前已读字符数
//        mSplitList.clear();
//        mCurrIndex = 0;
//        mSentenceSize = 0;
//        readCharCount = 0;
//        mHandler.removeCallbacksAndMessages(null);
    }

    public boolean isInitialized() {
//        return isTtsInitialized && mSpeechSynthesizer != null;
        return false;
    }

    /**
     * 退出语音朗读
     */
    public void exitVoiceRead(boolean showToast) {
        if (mVoiceCallBack == null) return;
//        if (mSpeechSynthesizer != null) {
//            Chapter currentChapter = mVoiceCallBack.getCurrentChapter();
//            int currentPageIndex = currentChapter.getCurrentPageIndex();
//            List<PageData> pageDataList = currentChapter.getPageDataList();
//            if (currentPageIndex < pageDataList.size()) {
//                PageData pageData = pageDataList.get(currentPageIndex);
//                pageData.resetPlayingStatus(mVoiceCallBack.getCurrentParagraphIndex());
//                if (currentPageIndex > 0) {
//                    PageData lastPageData = pageDataList.get(currentPageIndex - 1);
//                    lastPageData.resetPlayingStatus(mVoiceCallBack.getCurrentParagraphIndex());
//                }
//            }
//            mSpeechSynthesizer.stop();
//            resetData();
//            mVoiceCallBack.invalidateReaderView();
//            if (showToast) {
//                toast("已退出语音朗读");
//            }
//        }
        mVoiceCallBack.exit();
    }

    public void resume() {
//        if (mSpeechSynthesizer != null) {
//            mSpeechSynthesizer.resume();
//        }
    }

    public void pause() {
//        if (mSpeechSynthesizer != null) {
//            mSpeechSynthesizer.pause();
//        }
    }

    public void release() {
//        if (mSpeechSynthesizer != null) {
//            mSpeechSynthesizer.stop();
//            mSpeechSynthesizer.release();
//            mSpeechSynthesizer = null;
//        }
        isTtsInitialized = false;
        resetData();
        mContext.stopService(new Intent(mContext, ChatListenService.class));
    }

    /**
     * //百度语音测试key
     * String AppId = "8535996";
     * String AppKey = "MxPpf3nF5QX0pnd******cB";
     * String AppSecret = "7226e84664474aa09********b2aa434";
     *
     * @return
     */
    private boolean init() {
//        if (!isTtsInitialized || mSpeechSynthesizer == null) {
//            mSpeechSynthesizer = SpeechSynthesizer.getInstance();
//            mSpeechSynthesizer.setContext(mContext);
//            mSpeechSynthesizer.setSpeechSynthesizerListener(mSpeechSynthesizerListener); //listener是SpeechSynthesizerListener 的实现类，需要实现您自己的业务逻辑。SDK合成后会对这个类的方法进行回调。
//            mSpeechSynthesizer.setAppId(mAppId/*这里只是为了让Demo运行使用的APPID,请替换成自己的id。*/);
//            mSpeechSynthesizer.setApiKey(mAppKey, mAppSecret/*这里只是为了让Demo正常运行使用APIKey,请替换成自己的APIKey*/);
//
//            TtsUtil.setOfflineVoiceType(SharedPreferencesUtil.getInstance().getString(SpeechSynthesizer.PARAM_SPEAKER + true, TtsUtil.DEFAULT_SPEAKER));
//
//            mSpeechSynthesizer.setParam(SpeechSynthesizer.PARAM_TTS_TEXT_MODEL_FILE, TtsUtil.textFilename); // 文本模型文件路径 (离线引擎使用)
//            mSpeechSynthesizer.setParam(SpeechSynthesizer.PARAM_TTS_SPEECH_MODEL_FILE, TtsUtil.modelFilename);  // 声学模型文件路径 (离线引擎使用)// 该参数设置为TtsMode.MIX生效。即纯在线模式不生效。
//            AuthInfo authInfo = mSpeechSynthesizer.auth(TtsMode.MIX); // 离在线混合
//            if (!authInfo.isSuccess()) {
//                mHandler.post(() -> {
//                    toast("离线语音验证失败\n请检查网络后重试");
//                    exitVoiceRead(false);
//                    if (mVoiceCallBack != null) {
//                        mVoiceCallBack.initResult(false);
//                    }
//                });
//                return true;
//            }
//            if (mSpeechSynthesizer == null) {
//                return true;
//            }
//            setSpeechParams(TtsUtil.getParams());
//            int result = mSpeechSynthesizer.initTts(TtsMode.MIX);// 初始化离在线混合模式，如果只需要在线合成功能，使用 TtsMode.ONLINE
//            if (result != 0) {
//                // 初始化失败，错误码文档：https://cloud.baidu.com/doc/SPEECH/s/Ck4nwu2vx
//                mHandler.post(() -> {
//                    toast("百度语音初始化失败\n请检查网络后重试");
//                    exitVoiceRead(false);
//                    if (mVoiceCallBack != null) {
//                        mVoiceCallBack.initResult(false);
//                    }
//                });
//                return true;
//            }
//            isTtsInitialized = true;
//            if (mVoiceCallBack != null) {
//                mVoiceCallBack.initResult(true);
//            }
//        }
        return false;
    }

    private void startSpeak() {
        mContext.startService(new Intent(mContext, ChatListenService.class));
        Chapter currentChapter = mVoiceCallBack.getCurrentChapter();
        int currentPageIndex = currentChapter.getCurrentPageIndex();
        PageData pageData = currentChapter.getPageDataList().get(currentPageIndex);
        mVoiceCallBack.setCurrentParagraphIndex(0);
        if (pageData.getParagraphs().size() == 0) {//最后一页 页面数据长度为0的极端情况
            // 一个段落内容超过2页的时候，也会走这个逻辑
            Chapter nextChapter = mVoiceCallBack.getNextChapter();
            if (nextChapter == null || nextChapter.getStatus() != STATUS_LOADED) {
                mHandler.post(() -> exitVoiceRead(true));
                return;
            }
            mVoiceCallBack.setCurrentParagraphIndex(0);
            PageData nextPageData = nextChapter.getPageDataList().get(0);
            mVoiceCallBack.setCurrentParagraph(nextPageData.getParagraphs().get(mVoiceCallBack.getCurrentParagraphIndex()));
            speak(mVoiceCallBack.getCurrentParagraph().getContentByLines());

            for (LineBlock lineBlock : mVoiceCallBack.getCurrentParagraph().getBlocks()) {
                lineBlock.setPlaying(true);
            }
            mVoiceCallBack.invalidateReaderView();
            mHandler.post(() -> mVoiceCallBack.gotoNextChapter());
        } else {
            mVoiceCallBack.setCurrentParagraph(pageData.getParagraphs().get(mVoiceCallBack.getCurrentParagraphIndex()));
            String content = mVoiceCallBack.getCurrentParagraph().getContentByLines();
            if (content != null) {
                speak(content);
                for (LineBlock lineBlock : mVoiceCallBack.getCurrentParagraph().getBlocks()) {
                    lineBlock.setPlaying(true);
                }
                mVoiceCallBack.invalidateReaderView();
            } else {
                Chapter nextChapter = mVoiceCallBack.getNextChapter();
                if (nextChapter == null || nextChapter.getStatus() != STATUS_LOADED) {
                    mHandler.post(() -> exitVoiceRead(true));
                    return;
                }
                mVoiceCallBack.setCurrentParagraphIndex(0);
                PageData nextPageData = nextChapter.getPageDataList().get(0);
                mVoiceCallBack.setCurrentParagraph(nextPageData.getParagraphs().get(mVoiceCallBack.getCurrentParagraphIndex()));
                speak(mVoiceCallBack.getCurrentParagraph().getContentByLines());

                for (LineBlock lineBlock : mVoiceCallBack.getCurrentParagraph().getBlocks()) {
                    lineBlock.setPlaying(true);
                }
                mVoiceCallBack.invalidateReaderView();
                mHandler.post(() -> mVoiceCallBack.gotoNextChapter());
            }
        }
    }

    private void setSpeechParams(Map<String, String> params) {
        if (params != null) {
            for (Map.Entry<String, String> e : params.entrySet()) {
//                mSpeechSynthesizer.setParam(e.getKey(), e.getValue());
            }
        }
    }

    /**
     * 已知：mHandler.post(turnPage()),如果下一段很快就读完又走这个回调了，获取到的PageIndex可能还是上一页
     */
    private void exitPlayVoice() {
        Chapter currentChapter = mVoiceCallBack.getCurrentChapter();
        int currentPageIndex = currentChapter.getCurrentPageIndex();
        // 普通状态,读完一段,读下一段
        PageData pageData = currentChapter.getPageDataList().get(currentPageIndex);
        mVoiceCallBack.setCurrentParagraphIndex(mVoiceCallBack.getCurrentParagraphIndex() + 1);
        if (mVoiceCallBack.getCurrentParagraphIndex() < pageData.getParagraphs().size()) {
            // 不需要翻页的普通状态2 段落都在页内
            pageData.resetPlayingStatus(mVoiceCallBack.getCurrentParagraphIndex() - 1);
            Paragraph paragraph = pageData.readNextParagraph(mVoiceCallBack.getCurrentParagraphIndex());
            if (paragraph != null) {
                mVoiceCallBack.setCurrentParagraph(paragraph);
                read();
            } else {
                boolean canReadNextChapter = readNextChapter();
                if (canReadNextChapter) {
                    mHandler.post(() -> mVoiceCallBack.gotoNextChapter());
                }
            }
        } else {
            // 当前页段落已读完,翻页读下一页
            boolean hasNoDataNextPage = false;
            if (currentPageIndex + 1 < currentChapter.getPageDataList().size()) {
                // 判断当前章节有没有下一页
                PageData nextPageData = currentChapter.getPageDataList().get(currentPageIndex + 1);
                pageData.resetPlayingStatus(mVoiceCallBack.getCurrentParagraphIndex() - 1);
                mVoiceCallBack.setCurrentParagraphIndex(0);
                if (mVoiceCallBack.getCurrentParagraphIndex() < nextPageData.getParagraphs().size()) {
                    Paragraph paragraph = nextPageData.readNextParagraph(mVoiceCallBack.getCurrentParagraphIndex());
                    if (paragraph != null) {
                        mVoiceCallBack.setCurrentParagraph(paragraph);
                        read();
                    } else {
                        boolean canReadNextChapter = readNextChapter();
                        if (canReadNextChapter) {
                            mHandler.post(() -> mVoiceCallBack.gotoNextChapter());
                            return;
                        }
                    }
                } else {
                    // 下一页没有任何段落,直接翻页
                    hasNoDataNextPage = true;
                }
            } else {
                // 当前章节已读完  开始读下一章节
                pageData.resetPlayingStatus(mVoiceCallBack.getCurrentParagraphIndex() - 1);
                readNextChapter();
            }
            boolean finalHasNoDataNextPage = hasNoDataNextPage;
            mHandler.post(() -> {
                mVoiceCallBack.turnPage(true);
                if (finalHasNoDataNextPage) {
                    exitPlayVoice();
                }
            });
        }
    }

    private void read() {
        speak(mVoiceCallBack.getCurrentParagraph().getContentByLines());
        mVoiceCallBack.invalidateReaderView();
    }

    private void speak(String content) {
        if (!TextUtils.isEmpty(content)) {
            List<String> texts;
            if (content.contains("，")) {
                texts = Arrays.asList(content.split("，"));
            } else {
                texts = new ArrayList<>();
                while (content.length() >= 100) {
                    texts.add(content.substring(0, 100));
                    content = content.substring(100);
                }
                if (!StringUtil.isEmpty(content)) {
                    texts.add(content);
                }
            }
//            List<SpeechSynthesizeBag> bags = new ArrayList<>();
//            for (int i = 0; i < texts.size(); ++i) {
//                bags.add(getSpeechSynthesizeBag(texts.get(i), String.valueOf(i)));
//            }
//            while (bags.size() > 25) {
//                mSplitList.add(bags.subList(0, 25));
//                bags = bags.subList(25, bags.size());
//                if (bags.size() <= 25) {
//                    mSplitList.add(bags);
//                }
//            }
//            if (mSplitList.isEmpty()) {
//                batchSpeak(bags);
//            } else {
//                batchSpeak(mSplitList.poll());
//            }
        } else {
            content = ".";
            mCurrIndex = 0;
            mSentenceSize = 1;
//            mSpeechSynthesizer.speak(content);
        }
    }

//    private void batchSpeak(List<SpeechSynthesizeBag> bags) {
//        mCurrIndex = 0;
//        mSentenceSize = bags.size();
//        mSpeechSynthesizer.batchSpeak(bags);
//    }

//    private SpeechSynthesizeBag getSpeechSynthesizeBag(String text, String utteranceId) {
//        SpeechSynthesizeBag speechSynthesizeBag = new SpeechSynthesizeBag();
//        // 需要合成的文本text的长度不能超过1024个GBK字节。
//        speechSynthesizeBag.setText(StringUtil.isEmpty(text) ? "." : text);
//        speechSynthesizeBag.setUtteranceId(utteranceId);
//        return speechSynthesizeBag;
//    }

    /**
     * 读下一章数据
     */
    private boolean readNextChapter() {
        Chapter nextChapter = mVoiceCallBack.getNextChapter();
        if (nextChapter == null || nextChapter.getStatus() != STATUS_LOADED) {
            if (mVoiceCallBack.getCurrentParagraphIndex() > 0) {
                mVoiceCallBack.setCurrentParagraphIndex(mVoiceCallBack.getCurrentParagraphIndex() - 1);
            }
            mHandler.post(() -> exitVoiceRead(true));
            return false;
        }
        mVoiceCallBack.setCurrentParagraphIndex(0);
        PageData nextPageData = nextChapter.getPageDataList().get(0);
        Paragraph paragraph = nextPageData.readNextParagraph(mVoiceCallBack.getCurrentParagraphIndex());
        if (paragraph != null) {
            mVoiceCallBack.setCurrentParagraph(paragraph);
//            LogUtils.d("readNextChapter...");
            read();
            return true;
        }
        return false;
    }
}
