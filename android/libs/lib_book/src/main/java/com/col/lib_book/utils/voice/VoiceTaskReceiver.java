package com.col.lib_book.utils.voice;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import org.greenrobot.eventbus.EventBus;

/**
 * Created by Freedom on 2018/7/12.
 */

public class VoiceTaskReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        EventBus.getDefault().post(new CancelVoiceTaskEvent());
    }
}
