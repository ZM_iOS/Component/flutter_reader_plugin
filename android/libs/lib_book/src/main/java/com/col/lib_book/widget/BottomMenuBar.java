package com.col.lib_book.widget;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.chineseall.reader.lib.reader.config.IReadConfig;
import com.chineseall.reader.lib.reader.config.TangYuanSharedPrefUtils;
import com.col.lib_book.R;

import skin.support.content.res.SkinCompatResources;
import skin.support.widget.SkinCompatHelper;
import skin.support.widget.SkinCompatTextView;

/**
 * Created by huangzhengneng on 2019-12-19.
 */
public class BottomMenuBar extends LinearLayout {

    private CallBack mCallBack;
    private SeekBar mChapterProgress;
    private TextView tv_read_progress_name;
    private SkinCompatTextView tv_night;

    public BottomMenuBar(Context context) {
        this(context, null);
    }

    public BottomMenuBar(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public BottomMenuBar(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        LayoutInflater.from(getContext()).inflate(R.layout.col_layout_reader_level_1_bar, this, true);
        initViews();
    }

    private void initViews() {
        TextView read_bottom_directory = findViewById(R.id.read_bottom_directory);
        read_bottom_directory.setOnClickListener(v -> {
            if (mCallBack != null) {
                mCallBack.directory();
            }
        });
        tv_night = findViewById(R.id.tv_night);
        tv_night.setOnClickListener(v -> {
            boolean isNightMode = TangYuanSharedPrefUtils.getInstance().getSceneMode(IReadConfig.SKIN_THEME_DAYTIME) != IReadConfig.SKIN_THEME_NIGHT;
            setNightModeInternal(isNightMode);
            if (mCallBack != null) {
                mCallBack.nightMode(isNightMode);
            }
        });
        TextView read_chapter_setting = findViewById(R.id.read_chapter_setting);
        read_chapter_setting.setOnClickListener(v -> {
            if (mCallBack != null) {
                mCallBack.setting();
            }
        });

        TextView tv_last_chapter = findViewById(R.id.tv_last_chapter);
        tv_last_chapter.setOnClickListener(v -> {
            if (mCallBack != null) {
                mCallBack.chapterChanged(false, false);
            }
        });
        TextView tv_next_chapter = findViewById(R.id.tv_next_chapter);
        tv_next_chapter.setOnClickListener(v -> {
            if (mCallBack != null) {
                mCallBack.chapterChanged(false, true);
            }
        });
        mChapterProgress = findViewById(R.id.seekbar_read_progress);
        mChapterProgress.setOnSeekBarChangeListener(new onBookProgressChangeListener());
        if (android.os.Build.VERSION.SDK_INT >=21) {
            mChapterProgress.setProgressDrawable(getSkinDrawable(R.drawable.col_seek_bar_scene_daytime_style));
            mChapterProgress.setThumb(getSkinDrawable(R.drawable.col_img_read_seek_bar_thumb_daytime));
        }

        tv_read_progress_name = findViewById(R.id.tv_read_progress_name);

        findViewById(R.id.tv_read_auto).setOnClickListener(v -> {
            if (mCallBack != null) {
                mCallBack.openVoice();
            }
        });

        boolean isNightMode = TangYuanSharedPrefUtils.getInstance().getSceneMode(IReadConfig.SKIN_THEME_DAYTIME) == IReadConfig.SKIN_THEME_NIGHT;
        setNightModeInternal(isNightMode);
    }

    public void setNightMode(boolean isNight) {
        setNightModeInternal(isNight);
        if (mChapterProgress!=null){
            if (android.os.Build.VERSION.SDK_INT>=21) {
                mChapterProgress.setProgressDrawable(getSkinDrawable(R.drawable.col_seek_bar_scene_daytime_style));
                mChapterProgress.setThumb(getSkinDrawable(R.drawable.col_img_read_seek_bar_thumb_daytime));
            }
        }
        if (mCallBack != null) {
            mCallBack.nightMode(isNight);
        }
    }

    private void setNightModeInternal(boolean isNight) {
        if (isNight) {
            tv_night.setText("白天");
            if (android.os.Build.VERSION.SDK_INT>=21) {
                tv_night.setCompoundDrawablesWithIntrinsicBounds(null, getSkinDrawable(R.drawable.col_btn_book_ri_selector), null, null);
            }
        } else {
            tv_night.setText("夜间");
            if (android.os.Build.VERSION.SDK_INT>=21) {
                tv_night.setCompoundDrawablesWithIntrinsicBounds(null, getSkinDrawable(R.drawable.col_btn_book_night_selector), null, null);
            }
        }
    }

    public SeekBar getProgressSeekBar() {
        return mChapterProgress;
    }


    public void setCallBack(CallBack mCallBack) {
        this.mCallBack = mCallBack;
    }

    public void refreshStatus(int max, int progress) {
        mChapterProgress.setMax(max);
        mChapterProgress.setProgress(progress);
    }

    public void setReaderProgress(String content, boolean visible) {
        if (visible) {
            if (tv_read_progress_name != null) {
                if (tv_read_progress_name.getVisibility() == View.GONE) {
                    tv_read_progress_name.setVisibility(View.VISIBLE);
                }
                tv_read_progress_name.setText(content);
                tv_read_progress_name.removeCallbacks(progressRunnable);
                tv_read_progress_name.postDelayed(progressRunnable, 3000);
            }
        } else {
            tv_read_progress_name.setVisibility(GONE);
        }
    }

    private final Runnable progressRunnable = () -> {
        if (tv_read_progress_name != null) {
            tv_read_progress_name.setVisibility(View.GONE);
        }
    };

    class onBookProgressChangeListener implements SeekBar.OnSeekBarChangeListener {

        @Override
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            if (mCallBack != null) {
                mCallBack.chapterChangedBySeek(false, progress, false, fromUser);
            }
        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {

        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {
            seekBar.setEnabled(false);
            seekBar.postDelayed(() -> seekBar.setEnabled(true), 1000);
            if (mCallBack != null) {
                mCallBack.chapterChangedBySeek(false, seekBar.getProgress(), true, true);
            }
        }
    }

    private Drawable getSkinDrawable(int resId) {
        Drawable drawable = null;
        int drawableResourceId = SkinCompatHelper.checkResourceId(resId);
        if (drawableResourceId != 0) {
            drawable = SkinCompatResources.getDrawable(getContext().getApplicationContext(), drawableResourceId);
        }
        return drawable;
    }


    public interface CallBack {

        void directory();

        void nightMode(boolean night);

        void setting();

        /**
         * 章节改变行为
         *
         * @param online true表示在线书籍， false本地书籍
         * @param next   true表示下一章，false上一章
         */
        void chapterChanged(boolean online, boolean next);

        void chapterChangedBySeek(boolean online, int percent, boolean stop, boolean fromUser);

        void openVoice();
    }
}
