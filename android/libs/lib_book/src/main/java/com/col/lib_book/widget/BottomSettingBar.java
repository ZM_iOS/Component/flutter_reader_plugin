package com.col.lib_book.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.SeekBar;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.chineseall.reader.lib.reader.config.IReadConfig;
import com.chineseall.reader.lib.reader.config.ReaderConfigWrapper;
import com.chineseall.reader.lib.reader.config.TangYuanSharedPrefUtils;
import com.chineseall.reader.lib.reader.utils.SharedPrefUtil;
import com.col.lib_book.R;
import com.col.lib_book.ReadManager;
import com.col.lib_book.callbacks.OnReadNightModeChangeListener;
import com.col.lib_book.utils.ColorUtil;
import com.col.lib_book.utils.ConstantKt;
import com.col.lib_book.widget.skin.SkinCheckBox;

/**
 * Created by huangzhengneng on 2019-12-19.
 */
public class BottomSettingBar extends LinearLayout {

    private CallBack mCallBack;
    private TextView tv_read_menu_font_size;
    private SkinCheckBox cb_bright_follow_system;
    private SeekBar seek_read_menu_brightness_bar;
    private TextView btn_read_menu_reduce_font_size;
    private TextView btn_read_menu_enlarge_font_size;
    private RadioGroup radiogroup_read_menu_pager_anim;
    private RadioGroup radiogroup_read_menu_font;
    private RadioButton rb_default_font, rb_hyqh_font;
    private View fontSizeRoot;
    private RadioGroup radiogroup_read_menu_theme;
    private int mSkinStatus;

    public BottomSettingBar(Context context) {
        this(context, null);
    }

    public BottomSettingBar(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public BottomSettingBar(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {

        LayoutInflater.from(getContext()).inflate(R.layout.col_layout_reader_bottom_setting_bar, this, true);

        initViews();

    }

    private void initViews() {

        cb_bright_follow_system = findViewById(R.id.cb_bright_follow_system);
        cb_bright_follow_system.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (mCallBack != null) {
                mCallBack.followSystemBrightness(isChecked);
            }
        });
        fontSizeRoot = findViewById(R.id.ll_font_size);

        cb_bright_follow_system.requestFocus();
        seek_read_menu_brightness_bar = findViewById(R.id.seek_read_menu_brightness_bar);
        seek_read_menu_brightness_bar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (mCallBack != null) {
                    mCallBack.brightnessChanged(progress, fromUser, cb_bright_follow_system.isChecked());
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });


        radiogroup_read_menu_theme = findViewById(R.id.radiogroup_read_menu_theme);
        radiogroup_read_menu_theme.setOnCheckedChangeListener(new onThemeCheckedChangeListener());

        btn_read_menu_reduce_font_size = findViewById(R.id.btn_read_menu_reduce_font_size);
        btn_read_menu_reduce_font_size.setOnClickListener(v -> {
            if (mCallBack != null) {
                mCallBack.fontSizeChanged(true);
            }
            tv_read_menu_font_size.setText(String.valueOf(TangYuanSharedPrefUtils.getInstance().getFontSize(ReaderConfigWrapper.TEXT_SIZE_DEFAULT)));
        });
        tv_read_menu_font_size = findViewById(R.id.tv_read_menu_font_size);
        btn_read_menu_enlarge_font_size = findViewById(R.id.btn_read_menu_enlarge_font_size);
        btn_read_menu_enlarge_font_size.setOnClickListener(v -> {
            if (mCallBack != null) {
                mCallBack.fontSizeChanged(false);
            }
            tv_read_menu_font_size.setText(String.valueOf(TangYuanSharedPrefUtils.getInstance().getFontSize(ReaderConfigWrapper.TEXT_SIZE_DEFAULT)));
        });

        radiogroup_read_menu_pager_anim = findViewById(R.id.radiogroup_read_menu_pager_anim);
        resetAnimListener();

//        radiogroup_read_menu_font = findViewById(R.id.radiogroup_read_menu_font);
//        radiogroup_read_menu_font.check(FONT_HYQH.equals(TangYuanSharedPrefUtils.getInstance().getFontType()) ? R.id.rb_hyqh_font : R.id.rb_default_font);
//        resetAnimColor(radiogroup_read_menu_font, R.color.col_color_5DA9FA, R.color.col_reader_menu_txt_normal);
//        rb_default_font = findViewById(R.id.rb_default_font);
//        rb_hyqh_font = findViewById(R.id.rb_hyqh_font);
//        radiogroup_read_menu_font.setOnCheckedChangeListener((group, checkedId) -> {
//            String font = "sans";
//            if (checkedId == R.id.rb_hyqh_font) {
//                font = FONT_HYQH;
//            }
//            TangYuanSharedPrefUtils.getInstance().setFontType(font);
//            if (mCallBack != null) {
//                mCallBack.fontTypeChanged(font);
//            }
//            resetAnimColor(radiogroup_read_menu_font, R.color.col_color_5DA9FA, R.color.col_reader_menu_txt_normal);
//            rb_default_font.setEnabled(false);
//            rb_hyqh_font.setEnabled(false);
//            radiogroup_read_menu_font.postDelayed(() -> {
//                rb_default_font.setEnabled(true);
//                rb_hyqh_font.setEnabled(true);
//            }, 1000);
//        });
    }

    private void resetAnimListener() {
        resetAnimColor(radiogroup_read_menu_pager_anim, R.color.col_skin_color_main, R.color.col_reader_menu_txt_normal);
        radiogroup_read_menu_pager_anim.setOnCheckedChangeListener((group, checkedId) -> {
            int i2 = 34;
            int i3 = 2;
            int readerPagerAnim = TangYuanSharedPrefUtils.getInstance().getReaderPagerAnim(34);
            if (checkedId == R.id.rb_fz) {
                i3 = 2;
                i2 = 34;
            } else if (checkedId == R.id.rb_fg) {
                i3 = 2;
                i2 = 35;
            } else if (checkedId == R.id.rb_noanim) {
                i2 = 33;
                i3 = 2;
            } else if (checkedId == R.id.rb_sx) {
                i2 = readerPagerAnim;
                i3 = 1;  //2 水平 1 垂直
            }
            if (mCallBack != null) {
                mCallBack.onAnimChanged(i3, i2);
            }
            resetAnimColor(radiogroup_read_menu_pager_anim, R.color.col_skin_color_main, R.color.col_reader_menu_txt_normal);
        });
    }

    private void resetAnimColor(RadioGroup radioGroup, int p, int p2) {
        int count = radioGroup.getChildCount();
        for (int i = 0; i < count; ++i) {
            RadioButton radioButton = (RadioButton) radioGroup.getChildAt(i);
            radioButton.setTextColor(ColorUtil.getSkinColor(radioButton.isChecked() ? p : p2));
            if (android.os.Build.VERSION.SDK_INT>=21) {
                radioButton.setBackground(ColorUtil.getSkinDrawable(radioButton.isChecked() ? R.drawable.col_bg_reader_radio_selected : R.drawable.col_bg_reader_radio_unselected));
            }
        }
    }

    public void resetColor() {
//        resetAnimColor(radiogroup_read_menu_font, R.color.col_color_5DA9FA, R.color.col_reader_menu_txt_normal);
        resetAnimColor(radiogroup_read_menu_pager_anim, R.color.col_skin_color_main, R.color.col_reader_menu_txt_normal);
        if (android.os.Build.VERSION.SDK_INT>=21) {
            fontSizeRoot.setBackground(ColorUtil.getSkinDrawable(R.drawable.col_bg_reader_radio_font_selected));
        }

        int readerPagerAnim = TangYuanSharedPrefUtils.getInstance().getSceneMode(IReadConfig.SKIN_THEME_DAYTIME);

        boolean isNightMode = false;
        int themeId;
        if (IReadConfig.SKIN_THEME_DAYTIME == readerPagerAnim) {
            themeId = R.id.radio_read_menu_skin_theme_gray;
        } else if (IReadConfig.SKIN_THEME_YELLOW == readerPagerAnim) {
            themeId = R.id.radio_read_menu_skin_theme_yellow;
        } else if (IReadConfig.SKIN_THEME_GREEN == readerPagerAnim) {
            themeId = R.id.radio_read_menu_skin_theme_green;
        } else if (IReadConfig.SKIN_THEME_NIGHT == readerPagerAnim) {
            themeId = R.id.radio_read_menu_skin_theme_night;
            isNightMode = true;
        } else if (IReadConfig.SKIN_THEME_PINK == readerPagerAnim) {
            themeId = R.id.radio_read_menu_skin_theme_pink;
        } else if (IReadConfig.SKIN_THEME_BLUE == readerPagerAnim) {
            themeId = R.id.radio_read_menu_skin_theme_blue;
        } else if (IReadConfig.SKIN_THEME_SBLUE == readerPagerAnim) {
            themeId = R.id.radio_read_menu_skin_theme_sblue;
        } else {
            themeId = R.id.radio_read_menu_skin_theme_gray;
        }
        setTheme(mSkinStatus, themeId);
    }

    public void setAnim(int id) {
        radiogroup_read_menu_pager_anim.setOnCheckedChangeListener(null);
        radiogroup_read_menu_pager_anim.check(id);
        resetAnimListener();
    }

    public void setFontSize(int size) {
        tv_read_menu_font_size.setText(String.valueOf(size));
    }

    public void setCallBack(CallBack mCallBack) {
        this.mCallBack = mCallBack;
    }

    public void setBrightness(int brightness) {
        seek_read_menu_brightness_bar.setProgress(brightness);
    }

    public void setFollowSystem(boolean followSystem) {
        cb_bright_follow_system.setChecked(followSystem);
    }

    public void setReduceFontSize(boolean enable) {
        btn_read_menu_reduce_font_size.setEnabled(enable);
    }

    public void setEnlargeFontSize(boolean enable) {
        btn_read_menu_enlarge_font_size.setEnabled(enable);
    }

    public int getAnimCheckID() {
        return radiogroup_read_menu_pager_anim.getCheckedRadioButtonId();
    }

    public interface CallBack {
        void themeChanged(String skinName, int id);

        void followSystemBrightness(boolean checked);

        void brightnessChanged(int progress, boolean fromUser, boolean followSystem);

        void fontSizeChanged(boolean reduce);

        void fontTypeChanged(String font);

        void onAnimChanged(int orientation, int mode);
    }


    public void setTheme(int skinStatus, int themeID) {
        this.mSkinStatus = skinStatus;
        RadioButton themeBt = radiogroup_read_menu_theme.findViewById(themeID);
        if (themeBt == null) {
            themeBt = radiogroup_read_menu_theme.findViewById(R.id.radio_read_menu_skin_theme_gray);
        }
        if (themeBt != null) {
            themeBt.setChecked(true);
        }
    }

    public void setThemeEnable(boolean enable) {
        int childCount = radiogroup_read_menu_theme.getChildCount();
        for (int i = 0; i < childCount; i++) {
            radiogroup_read_menu_theme.getChildAt(i).setEnabled(enable);
        }
        if (enable){
            resetAnimColor(radiogroup_read_menu_pager_anim, R.color.col_skin_color_main, R.color.col_reader_menu_txt_normal);
        }
    }

    private class onThemeCheckedChangeListener implements RadioGroup.OnCheckedChangeListener {

        onThemeCheckedChangeListener() {
        }

        public void onCheckedChanged(RadioGroup radioGroup, int i) {
            if (mSkinStatus != 0) {
                return;
            }
            boolean isNightMode = false;
            int i2;
            if (i != R.id.radio_read_menu_skin_theme_night) {
                TangYuanSharedPrefUtils.getInstance().putInt(ConstantKt.READER_THEME_DAY_TIME, i);
            }
            String skinName = null;
            if (i == R.id.radio_read_menu_skin_theme_gray) {
                i2 = IReadConfig.SKIN_THEME_DAYTIME;
            } else if (i == R.id.radio_read_menu_skin_theme_green) {
                i2 = IReadConfig.SKIN_THEME_GREEN;
                skinName = "green.skin";
            } else if (i == R.id.radio_read_menu_skin_theme_pink) {
                i2 = IReadConfig.SKIN_THEME_PINK;
                skinName = "pink.skin";
            } else if (i == R.id.radio_read_menu_skin_theme_blue) {
                i2 = IReadConfig.SKIN_THEME_BLUE;
                skinName = "blue.skin";
            } else if (i == R.id.radio_read_menu_skin_theme_sblue) {
                i2 = IReadConfig.SKIN_THEME_SBLUE;
                skinName = "sblue.skin";
            } else if (i == R.id.radio_read_menu_skin_theme_night) {
                isNightMode = true;
                i2 = IReadConfig.SKIN_THEME_NIGHT;
                skinName = "night.skin";
            } else if (i == R.id.radio_read_menu_skin_theme_yellow) {
                i2 = IReadConfig.SKIN_THEME_YELLOW;
                skinName = "yellow.skin";
            } else {
                i2 = IReadConfig.SKIN_THEME_DAYTIME;
            }
            OnReadNightModeChangeListener onReadNightModeChangeListener = ReadManager.INSTANCE.getOnReadNightModeChangeListener();
            if (onReadNightModeChangeListener != null) {
                onReadNightModeChangeListener.onNightModeChanged(isNightMode);
            }
            if (mCallBack != null) {
                mCallBack.themeChanged(skinName == null ? "" : skinName, i2);
            }
        }
    }
}
