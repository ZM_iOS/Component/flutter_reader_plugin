package com.col.lib_book.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.RelativeLayout;

import com.col.lib_book.callbacks.OnTouchMoveListener;
import com.col.lib_book.utils.LogUtils;

/**
 * <pre>
 *     author : xuxinghai
 *     e-mail : xuxh_6@163.com
 *     time   : 2021/05/13
 *     desc   :
 *     version: 1.0
 * </pre>
 */
public class CustomRelativeLayout extends RelativeLayout {
    public CustomRelativeLayout(Context context) {
        super(context);
    }

    public CustomRelativeLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CustomRelativeLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    private boolean flag = false;

    private float touchX;
    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            LogUtils.d("TAG=onTouchEvent=ACTION_DOWN");
            touchX = event.getX();
            flag = false;
            if (onTouchMoveListener!=null){
                onTouchMoveListener.onTouch(event);
            }
        } else if (event.getAction() == MotionEvent.ACTION_MOVE) {
            float moveX = event.getX() - touchX;
            LogUtils.d("TAG=onTouchEvent=ACTION_MOVE=moveX="+moveX);
            if (Math.abs(moveX)>30){
                flag = true;
                if (onTouchMoveListener!=null){
                    onTouchMoveListener.onTouch(event);
                }
            }
        } else if (event.getAction() == MotionEvent.ACTION_UP
                || event.getAction() == MotionEvent.ACTION_CANCEL) {
            if (onTouchMoveListener!=null && flag){
                onTouchMoveListener.onTouch(event);
            }
            LogUtils.d("TAG=onTouchEvent=ACTION_UP");
        }
        return super.dispatchTouchEvent(event);
    }

    private OnTouchMoveListener onTouchMoveListener;

    public void setOnTouchMoveListener(OnTouchMoveListener onTouchMoveListener) {
        this.onTouchMoveListener = onTouchMoveListener;
    }

}
