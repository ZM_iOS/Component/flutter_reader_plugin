package com.col.lib_book.widget

import android.animation.Animator
import android.animation.ObjectAnimator
import android.view.View
import android.widget.FrameLayout
import com.chineseall.reader.lib.reader.callbacks.OnBottomMenuListener
import com.chineseall.reader.lib.reader.entities.Chapter

/**
 * Created by huangzhengneng on 2020/11/12.
 */
class ReadPageMenuManager constructor(
    private var flTopMenuContainer: FrameLayout,
    private var flBottomMenuContainer: FrameLayout,
    private var mTopMenuBar: TopMenuBar,
    private var mBottomMenuBar: BottomMenuBar,
    private var mBottomSettingBar: BottomSettingBar
) {

    private var mBottomHideAnim: ObjectAnimator? = null
    private var mTopHideAnim: ObjectAnimator? = null

    var isMenuOnShow = false

    fun switchMenus(show: Boolean, currentChapter: Chapter?, maxSize: Int,onBottomMenuListener : OnBottomMenuListener) {
        this.isMenuOnShow = show
        if (show) {
            cancelMenuAnim()
            showTopMenuBar()
            showBottomMenuBar(currentChapter, maxSize)
        } else {
            if (flTopMenuContainer.visibility != View.GONE
                && isTopBarNotHiding()
            ) {
                hideTopBar()
            }
            if (flBottomMenuContainer.visibility != View.GONE
                && isBottomBarNotHiding()
            ) {
                hideBottomBar()
                onBottomMenuListener.onHideBottomBar()
            }
        }
    }

    fun showSettingBar(fontSize: Int) {
        flBottomMenuContainer.removeAllViews()
        flBottomMenuContainer.addView(mBottomSettingBar)
        ObjectAnimator.ofFloat(
            flBottomMenuContainer,
            "translationY",
            flBottomMenuContainer.height.toFloat(),
            0f
        ).setDuration(300).start()
        mBottomSettingBar.setFontSize(fontSize)
        mBottomSettingBar.resetColor()
        hideTopBar()
    }

    fun cancelMenuAnim() {
        if (!isBottomBarNotHiding()) {
            mBottomHideAnim?.cancel()
        }
        if (!isTopBarNotHiding()) {
            mTopHideAnim?.cancel()
        }
    }

    private fun showBottomMenuBar(currentChapter: Chapter?, maxSize: Int) {
        if (this.flBottomMenuContainer.visibility == View.VISIBLE) {
            this.flBottomMenuContainer.visibility = View.GONE
        } else {
            flBottomMenuContainer.removeAllViews()
            flBottomMenuContainer.addView(mBottomMenuBar)
            if (currentChapter != null) {
                mBottomMenuBar.refreshStatus(maxSize, currentChapter.index)
            }
            this.flBottomMenuContainer.visibility = View.VISIBLE
            ObjectAnimator.ofFloat(
                flBottomMenuContainer,
                "translationY",
                flBottomMenuContainer.height.toFloat(),
                0f
            ).setDuration(300).start()
        }
    }

    private fun showTopMenuBar() {
        if (this.flTopMenuContainer.visibility == View.VISIBLE) {
            this.flTopMenuContainer.visibility = View.GONE
        } else {
            this.flTopMenuContainer.visibility = View.VISIBLE
            flTopMenuContainer.removeAllViews()
            flTopMenuContainer.addView(mTopMenuBar)
            ObjectAnimator.ofFloat(
                flTopMenuContainer,
                "translationY",
                -flTopMenuContainer.height.toFloat(),
                0f
            ).setDuration(300).start()
        }
    }

    private fun isTopBarNotHiding(): Boolean {
        return mTopHideAnim == null || (mTopHideAnim?.isRunning == false)
    }

    private fun isBottomBarNotHiding(): Boolean {
        return mBottomHideAnim == null || mBottomHideAnim?.isRunning == false
    }

    private fun hideBottomBar() {
        mBottomHideAnim = ObjectAnimator.ofFloat(
            flBottomMenuContainer,
            "translationY",
            0f,
            flBottomMenuContainer.height.toFloat()
        )
        mBottomHideAnim?.addListener(object : Animator.AnimatorListener {
            override fun onAnimationStart(animation: Animator) {}
            override fun onAnimationEnd(animation: Animator) {
                flBottomMenuContainer.visibility = View.GONE
            }

            override fun onAnimationCancel(animation: Animator) {}
            override fun onAnimationRepeat(animation: Animator) {}
        })
        mBottomHideAnim?.setDuration(300)?.start()
    }

    private fun hideTopBar() {
        mTopHideAnim = ObjectAnimator.ofFloat(
            flTopMenuContainer,
            "translationY",
            0f,
            -flTopMenuContainer.height.toFloat()
        )
        mTopHideAnim?.addListener(object : Animator.AnimatorListener {
            override fun onAnimationStart(animation: Animator) {}
            override fun onAnimationEnd(animation: Animator) {
                flTopMenuContainer.visibility = View.GONE
            }

            override fun onAnimationCancel(animation: Animator) {}
            override fun onAnimationRepeat(animation: Animator) {}
        })
        mTopHideAnim?.setDuration(300)?.start()
    }
}