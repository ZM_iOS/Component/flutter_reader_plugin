package com.col.lib_book.widget;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.SeekBar;

/**
 * @descriable 可以通过STATUS设置 Seekbar禁止点击禁止滑动等等
 *
 */
@SuppressLint("AppCompatCustomView")
public class SlideSeekBar extends SeekBar {
    private static final int PADDING_DEFAULT = 50;

    public static enum STATUS {
        STATUS_ENABLE, STATUS_CLICK, STATUS_SLIDE, STATUS_UNABLE
    }

    private STATUS mStaus = STATUS.STATUS_ENABLE;

    private Rect mThumbRect;

    private int mX, mY;

    private SlideSeekBar(Context context) {
        super(context);
    }

    public SlideSeekBar(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    private SlideSeekBar(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        int thumbLeft = getThumb().getBounds().left;
        int thumbRight = getThumb().getBounds().right;
        int thumbWidth = getThumb().getBounds().width();
        int eventX = (int) event.getX();
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            if (eventX <= thumbLeft - thumbWidth || eventX >= thumbRight + thumbWidth) {
                return false;
            }
        }
        return super.dispatchTouchEvent(event);
    }
    //    @Override
//    public boolean onTouchEvent(MotionEvent event) {
//
//        if (MotionEvent.ACTION_DOWN == event.getAction()) {
//            mThumbRect = getThumb().getBounds();
//        }
//        mX = (int) event.getX();
//        mY = (int) event.getY();
//
//        if (mStaus == STATUS.STATUS_CLICK && !checkBound()) {
//            return true;
//        }
//        if (mStaus == STATUS.STATUS_SLIDE && checkBound()) {
//            return true;
//        }
//        if (mStaus == STATUS.STATUS_UNABLE) {
//            return true;
//        }
//        return super.onTouchEvent(event);
//    }

    /**
     * 判断当前触摸点 是否在滑块之类-mPaddingSize是为了提高体验，因为有些滑块太小
     *
     * @return
     */
    private boolean checkBound() {

        /** 填充大小 */
        int mPaddingSize = PADDING_DEFAULT;
        return mX < mThumbRect.left - mPaddingSize
                || mX > mThumbRect.right + mPaddingSize
                || mY < mThumbRect.top - mPaddingSize
                || mY > mThumbRect.bottom + mPaddingSize;
    }

    public void setStatus(STATUS status) {
        this.mStaus = status;
    }

}

