package com.col.lib_book.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.col.lib_book.R;

/**
 * Created by huangzhengneng on 2020/4/20.
 */
public class TopMenuBar extends FrameLayout {

    private TextView tv_title;
    private ClickListener mClickListener;
    private TextView tv_add_shelf;

    public TopMenuBar(@NonNull Context context) {
        this(context, null);
    }

    public TopMenuBar(@NonNull Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public TopMenuBar(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initViews();
    }

    private void initViews() {
        LayoutInflater.from(getContext()).inflate(R.layout.col_layout_reader_top_menu_bar, this, true);
        tv_add_shelf = findViewById(R.id.tv_add_shelf);
        tv_add_shelf.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mClickListener != null) {
                    mClickListener.addBookToShelf();
                }
            }
        });
        findViewById(R.id.tv_back).setOnClickListener(v -> {
            if (mClickListener != null) {
                mClickListener.back();
            }
        });
        tv_title = findViewById(R.id.tv_title);

        findViewById(R.id.iv_more).setOnClickListener(v -> {
            if (mClickListener != null) {
                mClickListener.more();
            }
        });
    }

    public void setAddBookShlefButtonVisible(boolean isVisible){
        if (tv_add_shelf!=null){
            tv_add_shelf.setVisibility(isVisible?VISIBLE:GONE);
        }
    }

    public void setTitle(String title) {
//        if (tv_title != null) {
//            tv_title.setText(title);
//        }
    }

    public void setClickListener(ClickListener mClickListener) {
        this.mClickListener = mClickListener;
    }

    public interface ClickListener {
        void back();

        void addBookToShelf();

        void more();
    }
}
