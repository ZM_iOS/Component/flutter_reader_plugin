package com.col.lib_book.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.col.lib_book.R;

import com.col.lib_book.utils.ColorUtil;
import com.col.lib_book.utils.voice.VoiceManager;
import com.warkiz.widget.IndicatorSeekBar;


/**
 * Created by huangzhengneng on 2020/4/24.
 */
public class VoiceControllerView extends LinearLayout {

    public static final String SP_VOICE_MODE_ONLINE = "SP_VOICE_MODE_ONLINE";
    public static final String AUTO_VOICE_LAST_CHECK_RADIO_ID = "AUTO_VOICE_LAST_CHECK_RADIO_ID";

    private RadioGroup rg_reader_timer;
    private RadioGroup radiogroup_read_menu_voice;

    private CallBack callBack;

    public VoiceControllerView(Context context) {
        this(context, null);
    }

    public VoiceControllerView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public VoiceControllerView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initViews();
    }

    private void initViews() {
        LayoutInflater.from(getContext()).inflate(R.layout.col_view_read_voiceread_bottom_menu, this, true);
        setOnClickListener(v -> {
            // do nothing
        });

        TextView tv_reader_speed = findViewById(R.id.tv_reader_speed);
        radiogroup_read_menu_voice = findViewById(R.id.radiogroup_read_menu_voice);
        rg_reader_timer = findViewById(R.id.rg_reader_timer);
        IndicatorSeekBar seekbar_read_speed = findViewById(R.id.seekbar_read_speed);

//        String speakVoice = SharedPreferencesUtil.getInstance().getString(SpeechSynthesizer.PARAM_SPEAKER + true, TtsUtil.DEFAULT_SPEAKER);
//        radiogroup_read_menu_voice.check(getResources().getIdentifier("rb_reader_voice" + (speakVoice) + "_offline",
//                "id",
//                getContext().getPackageName()));
//        {
//            String speed = SharedPreferencesUtil.getInstance().getString(SpeechSynthesizer.PARAM_SPEED, "5");
//            String speedStr;
//            switch (speed) {
//                case "3":
//                    speedStr = "语速：慢";
//                    break;
//                case "4":
//                    speedStr = "语速：较慢";
//                    break;
//                case "6":
//                    speedStr = "语速：较快";
//                    break;
//                case "7":
//                    speedStr = "语速：快";
//                    break;
//                default:
//                    speedStr = "语速：正常";
//                    break;
//            }
//            tv_reader_speed.setText(speedStr);
//            seekbar_read_speed.setProgress(Float.parseFloat(speed));
//        }
        seekbar_read_speed.clearFocus();
        seekbar_read_speed.setOnSeekChangeListener(new IndicatorSeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(IndicatorSeekBar seekBar, int progress, float progressFloat, boolean fromUserTouch) {
                String speedStr = "";
                switch (progress) {
                    case 3:
                        speedStr = "语速：慢";
                        break;
                    case 4:
                        speedStr = "语速：较慢";
                        break;
                    case 5:
                        speedStr = "语速：正常";
                        break;
                    case 6:
                        speedStr = "语速：较快";
                        break;
                    case 7:
                        speedStr = "语速：快";
                        break;
                }
                tv_reader_speed.setText(speedStr);
            }

            @Override
            public void onSectionChanged(IndicatorSeekBar seekBar, int thumbPosOnTick, String textBelowTick, boolean fromUserTouch) {

            }

            @Override
            public void onStartTrackingTouch(IndicatorSeekBar seekBar, int thumbPosOnTick) {

            }

            @Override
            public void onStopTrackingTouch(IndicatorSeekBar seekBar) {
                VoiceManager.getInstance(getContext()).setPlaySpeed(seekBar.getProgress() + "");
            }
        });


        rg_reader_timer.setOnCheckedChangeListener((group, checkedId) -> {
            int time = 0;
            if (checkedId == R.id.rb_reader_timer_off) {
                time = -1;
            } else if (checkedId == R.id.rb_reader_timer_0) {
                time = 900 * 1000;
            } else if (checkedId == R.id.rb_reader_timer_1) {
                time = 1800 * 1000;
            } else if (checkedId == R.id.rb_reader_timer_2) {
                time = 3600 * 1000;
            }
            resetColor();
            if (callBack != null) {
                callBack.setReadTime(time);
            }
        });

        //离线语音选择
        radiogroup_read_menu_voice.setOnCheckedChangeListener((group, checkedId) -> {
            String voice;
            // 0 普通女声（默认） 1 普通男声 2 特别男声 3 情感男声<度逍遥> 4 情感儿童声<度丫丫>
            if (checkedId == R.id.rb_reader_voice0_offline) {
                voice = "0";
            } else if (checkedId == R.id.rb_reader_voice1_offline) {
                voice = "1";
            } else if (checkedId == R.id.rb_reader_voice3_offline) {
                voice = "3";
            } else if (checkedId == R.id.rb_reader_voice4_offline) {
                voice = "4";
            } else {
                return;
            }
            resetColor();
            VoiceManager.getInstance(getContext()).setReadMode(voice, true, false);
        });

        View btnStopRead = findViewById(R.id.btn_stop_read);
        btnStopRead.setOnClickListener(v -> VoiceManager.getInstance(getContext()).exitVoiceRead(true));
    }

    public void resetColor() {
        resetAnimColor(radiogroup_read_menu_voice, R.color.col_color_5DA9FA, R.color.col_reader_menu_txt_normal);
        resetAnimColor(rg_reader_timer, R.color.col_color_5DA9FA, R.color.col_reader_menu_txt_normal);
    }

    private void resetAnimColor(RadioGroup radioGroup, int p, int p2) {
        int count = radioGroup.getChildCount();
        for (int i = 0; i < count; ++i) {
            RadioButton radioButton = (RadioButton) radioGroup.getChildAt(i);
            radioButton.setTextColor(ColorUtil.getSkinColor(radioButton.isChecked() ? p : p2));
        }
    }

    public void setCallBack(CallBack callBack) {
        this.callBack = callBack;
    }

    public void checkTimer(int id) {
        rg_reader_timer.check(id);
    }

    public interface CallBack {
        void setReadTime(int time);
    }
}
