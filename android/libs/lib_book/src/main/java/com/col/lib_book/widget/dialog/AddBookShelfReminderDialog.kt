package com.col.lib_book.widget.dialog

import android.content.Context
import android.os.Bundle
import android.view.Gravity
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import com.col.lib_book.R
import com.col.lib_book.utils.DensityUtil

/**
 * Created by huangzhengneng on 11/20/20.
 */
class AddBookShelfReminderDialog constructor(context: Context) : AlertDialog(
    context,
    R.style.ColDialog
) {

    var callBack: ReminderCallBack? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.col_layout_add_book_shelf_reminder)

        findViewById<TextView>(R.id.tv_no)?.setOnClickListener {
            callBack?.cancel()
        }
        findViewById<TextView>(R.id.tv_ok)?.setOnClickListener {
            callBack?.ok()
        }


        val lp = window?.attributes
        lp?.width = DensityUtil.dip2px(context, 269f)
        window?.setGravity(Gravity.CENTER)
        window?.attributes = lp
    }

}

interface ReminderCallBack {
    fun ok() {
    }

    fun cancel();
}