package com.col.lib_book.widget.dialog;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.col.lib_book.R;
import com.col.lib_book.utils.StringUtil;


/**
 * Create custom Dialog windows for your application Custom dialogs rely on
 * custom layouts wich allow you to create and use your own look & feel.
 * <p>
 * Under GPL v3 : http://www.gnu.org/licenses/gpl-3.0.html
 *
 * @author antoine vianey
 */
public class CustomDialog extends Dialog {

    public CustomDialog(Context context, int theme) {
        super(context, theme);
    }

    public CustomDialog(Context context) {
        super(context);
    }

    /**
     * Helper class for creating a custom dialog
     */
    public static class Builder {

        private Context context;
        private String title;
        private CharSequence message;
        private String tipText;
        private String positiveButtonText;
        private String negativeButtonText;
        private View contentView;

        private OnClickListener positiveButtonClickListener,
                negativeButtonClickListener;

        private CompoundButton.OnCheckedChangeListener mOnCheckedChangeListener;

        public Builder(Context context) {
            this.context = context;
        }

        /**
         * Set the Dialog message from String
         *
         * @param message content
         */
        public Builder setMessage(CharSequence message) {
            this.message = message;
            return this;
        }

        /**
         * Set the Dialog message from resource
         *
         * @param message message id
         */
        public Builder setMessage(int message) {
            this.message = (String) context.getText(message);
            return this;
        }

        /**
         * Set the Dialog title from resource
         *
         * @param title dialog title
         */
        public Builder setTitle(int title) {
            this.title = (String) context.getText(title);
            return this;
        }

        public void setTipText(String tipText) {
            this.tipText = tipText;
        }

        public void setOnCheckedChangeListener(CompoundButton.OnCheckedChangeListener mOnCheckedChangeListener) {
            this.mOnCheckedChangeListener = mOnCheckedChangeListener;
        }

        /**
         * Set the Dialog title from String
         *
         * @param title dialog title id
         */
        public Builder setTitle(String title) {
            this.title = title;
            return this;
        }

        /**
         * Set a custom content view for the Dialog. If a message is set, the
         * contentView is not added to the Dialog...
         *
         * @param v content view
         */
        public Builder setContentView(View v) {
            this.contentView = v;
            return this;
        }

        /**
         * Set the positive button resource and it's listener
         *
         * @param positiveButtonText position Button
         * @param listener           listener
         */
        public Builder setPositiveButton(int positiveButtonText,
                                         OnClickListener listener) {
            this.positiveButtonText = (String) context
                    .getText(positiveButtonText);
            this.positiveButtonClickListener = listener;
            return this;
        }

        /**
         * Set the positive button text and it's listener
         *
         * @param positiveButtonText positive button
         * @param listener           listener
         */
        public Builder setPositiveButton(String positiveButtonText,
                                         OnClickListener listener) {
            this.positiveButtonText = positiveButtonText;
            this.positiveButtonClickListener = listener;
            return this;
        }

        /**
         * Set the negative button resource and it's listener
         *
         * @param negativeButtonText negativeButton Text id
         * @param listener           listener
         */
        public Builder setNegativeButton(int negativeButtonText,
                                         OnClickListener listener) {
            this.negativeButtonText = (String) context
                    .getText(negativeButtonText);
            this.negativeButtonClickListener = listener;
            return this;
        }

        /**
         * Set the negative button text and it's listener
         *
         * @param negativeButtonText negativeButton Text
         * @param listener           listener
         */
        public Builder setNegativeButton(String negativeButtonText,
                                         OnClickListener listener) {
            this.negativeButtonText = negativeButtonText;
            this.negativeButtonClickListener = listener;
            return this;
        }

        /**
         * Create the custom dialog
         */
        public CustomDialog create() {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            // instantiate the dialog with the custom Theme
            final CustomDialog dialog = new CustomDialog(context, R.style.ColDialog);
            dialog.setCancelable(false);
            View layout = inflater.inflate(R.layout.col_dialog_custom, null);
            dialog.addContentView(layout, new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
            TextView mTvTitle = layout.findViewById(R.id.tv_dialog_title);
            View mTvLine = layout.findViewById(R.id.tv_line);
            TextView mBtnYes = layout.findViewById(R.id.btn_dialog_confirm);
            TextView mBtnCancel = layout.findViewById(R.id.btn_dialog_cancel);
            LinearLayout mLlContentView = layout.findViewById(R.id.content);
            View line = layout.findViewById(R.id.kongbai);

            // set the dialog title
            if (!TextUtils.isEmpty(title)) {
                mTvTitle.setText(title);
            } else {
                mTvTitle.setVisibility(View.GONE);
                mTvLine.setVisibility(View.GONE);
            }


            // set the confirm button
            if (!TextUtils.isEmpty(positiveButtonText)) {
                mBtnYes.setText(positiveButtonText);
                if (positiveButtonClickListener != null) {
                    mBtnYes.setOnClickListener(v -> {
                        positiveButtonClickListener.onClick(dialog, DialogInterface.BUTTON_POSITIVE);
                        if (dialog.isShowing()) {
                            dialog.dismiss();
                        }
                    });
                }
            } else {
                // if no confirm button just set the visibility to GONE
                mBtnYes.setVisibility(View.GONE);
                line.setVisibility(View.GONE);
            }
            // set the cancel button
            if (!TextUtils.isEmpty(negativeButtonText)) {
                mBtnCancel.setText(negativeButtonText);
                if (negativeButtonClickListener != null) {
                    mBtnCancel.setOnClickListener(v -> {
                        negativeButtonClickListener.onClick(dialog, DialogInterface.BUTTON_NEGATIVE);
                        if (dialog.isShowing()) {
                            dialog.dismiss();
                        }
                    });
                }
            } else {
                // if no confirm button just set the visibility to GONE
                mBtnCancel.setVisibility(View.GONE);
                line.setVisibility(View.GONE);
            }
            // set the content message
            TextView tvMessage = layout.findViewById(R.id.tv_dialog_content);
            LinearLayout ll_tip = layout.findViewById(R.id.ll_tip);

            CheckBox cb_tip = layout.findViewById(R.id.cb_tip);

            if (!TextUtils.isEmpty(tipText)) {
                cb_tip.setVisibility(View.VISIBLE);
                cb_tip.setOnCheckedChangeListener((buttonView, isChecked) -> {
                    if (mOnCheckedChangeListener != null) {
                        mOnCheckedChangeListener.onCheckedChanged(buttonView, isChecked);
                    }
                });
            }
            if (!TextUtils.isEmpty(message)) {
                tvMessage.setVisibility(View.VISIBLE);
                if (message instanceof Spanned) {
                    tvMessage.setText(StringUtil.getClickableHtml(message));
                    tvMessage.setMovementMethod(LinkMovementMethod.getInstance());
                } else {
                    tvMessage.setText(message);
                }
            } else {
                tvMessage.setVisibility(View.GONE);
                mTvLine.setVisibility(View.GONE);
                if (TextUtils.isEmpty(tipText)) {
                    ll_tip.setVisibility(View.GONE);
                }
            }
            if (contentView != null) {
                // if no message set
                // add the contentView to the dialog body
                mLlContentView.setVisibility(View.VISIBLE);
                mTvLine.setVisibility(View.VISIBLE);
                mLlContentView.removeAllViews();
                mLlContentView.addView(contentView, new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
            } else {
                mLlContentView.setVisibility(View.GONE);
            }
            dialog.setContentView(layout);
            return dialog;
        }
    }
}