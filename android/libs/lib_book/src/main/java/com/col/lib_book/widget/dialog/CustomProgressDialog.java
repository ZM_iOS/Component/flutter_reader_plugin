package com.col.lib_book.widget.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.col.lib_book.R;


public class CustomProgressDialog extends Dialog {

    private TextView mProgress;

    public CustomProgressDialog(Context context) {
        this(context, R.style.Colloading_dialog);
    }

    public CustomProgressDialog(Context context, int themeResId) {
        super(context, themeResId);
    }

    public CustomProgressDialog instance(Activity activity) {
        ViewGroup v = (ViewGroup) View.inflate(activity, R.layout.col_common_progress_view, null);
        mProgress = (TextView) v.findViewById(R.id.tv_progress);

//        ProgressBar pb = (ProgressBar) v.findViewById(R.id.common_progress);
//        if (pb!=null){
//
//            RotateDrawable rd = (RotateDrawable) pb.getIndeterminateDrawable();
//            GradientDrawable gd = (GradientDrawable) rd.getDrawable();
//            gd.setColors(new int[]{Color.WHITE, pb.getContext().getResources().getColor(R.color.main)});
//            gd.setShape(GradientDrawable.RING);
//        }
//        v.setColor(ContextCompat.getColor(activity, R.color.reader_menu_bg_color));
//        CustomDialog dialog = new CustomDialog(activity, R.style.loading_dialog);
        setContentView(v,
                new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.MATCH_PARENT)
        );
        return this;
    }

    public void setProgress(String progress) {
        mProgress.setVisibility(View.VISIBLE);
        mProgress.setText(progress);
    }
}