package com.col.lib_book.widget.dialog;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.col.lib_book.R;
import com.col.lib_book.utils.StringUtil;

/**
 * <pre>
 *     author : xuxinghai
 *     e-mail : xuxh_6@163.com
 *     time   : 2021/09/14
 *     desc   :
 *     version: 1.0
 * </pre>
 */
public class ThanksDialog extends Dialog {
    public ThanksDialog(Context context, int theme) {
        super(context, theme);
    }

    public ThanksDialog(Context context) {
        super(context);
    }

    /**
     * Helper class for creating a custom dialog
     */
    public static class Builder {

        private Context context;
        private String title;
        private CharSequence message;
        private String positiveButtonText;
        private String negativeButtonText;

        private RelativeLayout adViewContainer;

        private OnClickListener positiveButtonClickListener,
                negativeButtonClickListener;
        private TextView mTvTitle;
        private TextView mBtnYes;
        private TextView mBtnCancel;
        private LinearLayout ll_bottom;
        private View line1;
        private View line2;
        private TextView tvMessage;
        private ImageView iv_close;
        private LinearLayout ll_dialog;


        public Builder(Context context) {
            this.context = context;
        }

        /**
         * Set the Dialog message from String
         *
         * @param message content
         */
        public Builder setMessage(CharSequence message) {
            this.message = message;
            return this;
        }

        /**
         * Set the Dialog message from resource
         *
         * @param message message id
         */
        public Builder setMessage(int message) {
            this.message = (String) context.getText(message);
            return this;
        }

        /**
         * Set the Dialog title from resource
         *
         * @param title dialog title
         */
        public Builder setTitle(int title) {
            this.title = (String) context.getText(title);
            return this;
        }

        /**
         * Set the Dialog title from String
         *
         * @param title dialog title id
         */
        public Builder setTitle(String title) {
            this.title = title;
            return this;
        }



        /**
         * Set the positive button resource and it's listener
         *
         * @param positiveButtonText position Button
         * @param listener           listener
         */
        public Builder setPositiveButton(int positiveButtonText,
                                                        OnClickListener listener) {
            this.positiveButtonText = (String) context
                    .getText(positiveButtonText);
            this.positiveButtonClickListener = listener;
            return this;
        }

        /**
         * Set the positive button text and it's listener
         *
         * @param positiveButtonText positive button
         * @param listener           listener
         */
        public Builder setPositiveButton(String positiveButtonText,
                                                        OnClickListener listener) {
            this.positiveButtonText = positiveButtonText;
            this.positiveButtonClickListener = listener;
            return this;
        }

        public void setPositiveButtonText(String positiveButtonText) {
            if (mBtnYes != null) {
                mBtnYes.setText(positiveButtonText);
            }
        }

        /**
         * Set the negative button resource and it's listener
         *
         * @param negativeButtonText negativeButton Text id
         * @param listener           listener
         */
        public Builder setNegativeButton(int negativeButtonText,
                                                        OnClickListener listener) {
            this.negativeButtonText = (String) context
                    .getText(negativeButtonText);
            this.negativeButtonClickListener = listener;
            return this;
        }

        /**
         * Set the negative button text and it's listener
         *
         * @param negativeButtonText negativeButton Text
         * @param listener           listener
         */
        public Builder setNegativeButton(String negativeButtonText,
                                                        OnClickListener listener) {
            this.negativeButtonText = negativeButtonText;
            this.negativeButtonClickListener = listener;
            return this;
        }

        public ViewGroup getAdViewContainer() {
            return adViewContainer;
        }

        public CustomDialog createThanks() {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            // instantiate the dialog with the custom Theme
            final CustomDialog dialog = new CustomDialog(context, R.style.ColDialog);
            dialog.setCancelable(false);
            View layout = inflater.inflate(R.layout.col_layout_reader_ad_thanks, null);
            dialog.addContentView(layout, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

            mTvTitle = layout.findViewById(R.id.tv_title);
            mBtnYes = layout.findViewById(R.id.tv_ok);
            mBtnCancel = layout.findViewById(R.id.tv_no);
            ll_bottom = layout.findViewById(R.id.ll_bottom);

            line1 = layout.findViewById(R.id.divide_line1);
            line2 = layout.findViewById(R.id.divide_line2);

            adViewContainer = layout.findViewById(R.id.fl_ad_container);

            // set the dialog title
            if (!TextUtils.isEmpty(title)) {
                mTvTitle.setText(title);
            } else {
                mTvTitle.setVisibility(View.GONE);
            }

            // set the confirm button
            if (!TextUtils.isEmpty(positiveButtonText)) {
                mBtnYes.setText(positiveButtonText);
                if (positiveButtonClickListener != null) {
                    mBtnYes.setOnClickListener(v -> {
                        positiveButtonClickListener.onClick(dialog, DialogInterface.BUTTON_POSITIVE);
//                        if (dialog.isShowing()) {
//                            dialog.dismiss();
//                        }
                    });
                }
            } else {
                mBtnYes.setVisibility(View.GONE);
                line2.setVisibility(View.GONE);
            }
            // set the cancel button
            if (!TextUtils.isEmpty(negativeButtonText)) {
                mBtnCancel.setText(negativeButtonText);
                if (negativeButtonClickListener != null) {
                    mBtnCancel.setOnClickListener(v -> {
                        negativeButtonClickListener.onClick(dialog, DialogInterface.BUTTON_NEGATIVE);
                        if (dialog.isShowing()) {
                            dialog.dismiss();
                        }
                    });
                }
            } else {
                // if no confirm button just set the visibility to GONE
                mBtnCancel.setVisibility(View.GONE);
                line2.setVisibility(View.GONE);

                if (TextUtils.isEmpty(positiveButtonText)) {
                    ll_bottom.setVisibility(View.GONE);
                    line1.setVisibility(View.GONE);
                }
            }

            // set the content message
            tvMessage = layout.findViewById(R.id.tv_msg);

            if (!TextUtils.isEmpty(message)) {
                tvMessage.setVisibility(View.VISIBLE);
                if (message instanceof Spanned) {
                    tvMessage.setText(StringUtil.getClickableHtml(message));
                    tvMessage.setMovementMethod(LinkMovementMethod.getInstance());
                } else {
                    tvMessage.setText(message);
                }
            } else {
                tvMessage.setVisibility(View.GONE);
            }

            iv_close = layout.findViewById(R.id.iv_close);

            iv_close.setVisibility(View.VISIBLE);
            iv_close.setOnClickListener(v -> {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
            });

            dialog.setContentView(layout);
            return dialog;
        }
    }
}
