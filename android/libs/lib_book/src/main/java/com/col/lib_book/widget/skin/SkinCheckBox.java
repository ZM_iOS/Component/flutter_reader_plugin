package com.col.lib_book.widget.skin;

import android.content.Context;
import android.util.AttributeSet;

import com.col.lib_book.R;

import skin.support.widget.SkinCompatBackgroundHelper;
import skin.support.widget.SkinCompatCheckBox;
import skin.support.widget.SkinCompatTextHelper;

public class SkinCheckBox extends SkinCompatCheckBox {
    private SkinCompatTextHelper mTextHelper;
    private SkinCompatBackgroundHelper mBackgroundTintHelper;
    public SkinCheckBox(Context context) {
        super(context);
    }

    public SkinCheckBox(Context context, AttributeSet attrs) {
        this(context, attrs, R.attr.checkboxStyle);
    }

    public SkinCheckBox(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mBackgroundTintHelper = new SkinCompatBackgroundHelper(this);
        mBackgroundTintHelper.loadFromAttributes(attrs, defStyleAttr);
        mTextHelper = SkinCompatTextHelper.create(this);
        mTextHelper.loadFromAttributes(attrs, defStyleAttr);
    }

    @Override
    public void applySkin() {
        super.applySkin();
        if (mBackgroundTintHelper != null) {
            mBackgroundTintHelper.applySkin();
        }
        if (mTextHelper != null) {
            mTextHelper.applySkin();
        }
    }
}
