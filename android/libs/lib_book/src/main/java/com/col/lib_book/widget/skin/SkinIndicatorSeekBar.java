package com.col.lib_book.widget.skin;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.util.AttributeSet;

import com.col.lib_book.R;
import com.warkiz.widget.IndicatorSeekBar;
import skin.support.content.res.SkinCompatResources;
import skin.support.widget.SkinCompatHelper;

public class SkinIndicatorSeekBar extends IndicatorSeekBar {


    private int mThumbColorResourceId;

    public SkinIndicatorSeekBar(Context context) {
        super(context);
    }


    public SkinIndicatorSeekBar(Context context, AttributeSet attrs) {
        this(context, attrs,0);
    }

    public SkinIndicatorSeekBar(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        TypedArray ta = context.obtainStyledAttributes(attrs, R.styleable.IndicatorSeekBar);
        mThumbColorResourceId = ta.getResourceId(R.styleable.IndicatorSeekBar_isb_thumb_color, 0);
        ta.recycle();
    }



    @Override
    protected synchronized void onDraw(Canvas canvas) {
        int checkedColor = SkinCompatHelper.checkResourceId(mThumbColorResourceId);
        if (checkedColor!=0) {
            int color = SkinCompatResources.getColor(getContext(), checkedColor);
            Builder builder = getBuilder();
            builder.setThumbColor(color);
            builder.apply();
        }
        super.onDraw(canvas);

    }
}
