package com.chineseall.reader.lib.reader.ad;

import android.view.View;

import java.util.LinkedHashMap;

public class ReaderAdManager {
    private static volatile ReaderAdManager sInstance;
    LinkedHashMap<Integer, View> adList;

    public static ReaderAdManager getInstance() {
        if (sInstance == null) {
            synchronized (ReaderAdManager.class) {
                if (sInstance == null) {
                    sInstance = new ReaderAdManager();
                }
            }
        }
        return sInstance;
    }

    public ReaderAdManager() {

    }

    public void addTTAd(View view) {
        if (adList == null) {
            adList = new LinkedHashMap<>();
        }
        view.setTag(adList.size());
        adList.put(adList.size(), view);
    }

    public View getTTAd(int adKey) {
        return (View) adList.get(adKey);
    }

    int randomIndex = 0;

    public View randomGetTTAd() {
        if (adList != null && randomIndex < adList.size()) {
            View view = adList.get(randomIndex);
            randomIndex++;
            return view;
        } else {
            randomIndex = 0;
            return null;
        }
    }

    public int getAdSize() {
        return adList == null ? 0 : adList.size();
    }

    public void clearAdList(){
        if(adList!=null && adList.size()>0){
            adList.clear();
        }
    }
}
