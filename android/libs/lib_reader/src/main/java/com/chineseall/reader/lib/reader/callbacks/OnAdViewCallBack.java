package com.chineseall.reader.lib.reader.callbacks;

import android.view.View;

/**
 * Created by huangzhengneng on 11/23/20.
 */
public interface OnAdViewCallBack {

    void get(OnAdViewReceiver receiver);

    void adClick(View adView);

    void adShow(View adView);
}
