package com.chineseall.reader.lib.reader.callbacks;

import android.view.View;

/**
 * Created by huangzhengneng on 11/24/20.
 */
public interface OnAdViewReceiver {

    void onReceive(boolean success, View view);
    void onAdClosed();

}
