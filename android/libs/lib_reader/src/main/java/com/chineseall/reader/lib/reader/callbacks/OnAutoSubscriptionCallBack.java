package com.chineseall.reader.lib.reader.callbacks;

/**
 * Created by huangzhengneng on 2020/4/15.
 */
public interface OnAutoSubscriptionCallBack {

    /**
     * 指定的书是否开启了自动订阅
     * @param bookId
     * @return true开启
     */
    boolean autoSub(long bookId);
}
