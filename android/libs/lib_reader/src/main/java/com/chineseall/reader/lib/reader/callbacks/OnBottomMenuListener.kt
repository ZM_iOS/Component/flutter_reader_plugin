package com.chineseall.reader.lib.reader.callbacks

/**
 * <pre>
 *     author : xuxinghai
 *     e-mail : xuxh_6@163.com
 *     time   : 2021/04/14
 *     desc   :
 *     version: 1.0
 * </pre>
 */
interface OnBottomMenuListener {
    fun onHideBottomBar()
}