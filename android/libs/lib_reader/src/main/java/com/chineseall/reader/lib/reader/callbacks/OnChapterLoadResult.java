package com.chineseall.reader.lib.reader.callbacks;

import com.chineseall.reader.lib.reader.view.ReaderView;

/**
 * Created by huangzhengneng on 2020/4/16.
 */
public interface OnChapterLoadResult {

    void chapterTitle(ReaderView readerView, String chapterTitle);

}
