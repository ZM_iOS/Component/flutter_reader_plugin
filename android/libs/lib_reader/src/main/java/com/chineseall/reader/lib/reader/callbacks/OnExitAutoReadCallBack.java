package com.chineseall.reader.lib.reader.callbacks;

import com.chineseall.reader.lib.reader.view.ReaderView;

/**
 * Created by huangzhengneng on 2020/4/15.
 */
public interface OnExitAutoReadCallBack {

    boolean isExitAutoMode(ReaderView readerView);

    void setExitAutoMode(ReaderView readerView, boolean exitAutoMode);

}
