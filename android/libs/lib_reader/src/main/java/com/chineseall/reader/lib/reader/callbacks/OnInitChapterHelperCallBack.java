package com.chineseall.reader.lib.reader.callbacks;

import com.chineseall.reader.lib.reader.entities.Article;
import com.chineseall.reader.lib.reader.utils.IChapterHelper;

/**
 * Created by huangzhengneng on 2020/4/15.
 */
public interface OnInitChapterHelperCallBack {

    IChapterHelper init(Article article);

}
