package com.chineseall.reader.lib.reader.callbacks;

import android.view.View;

/**
 * Created by xuxinghai on 8/6/21.
 */
public interface OnListAdViewReceiver {

    void onReceive(View adView,String adKey);
    void onAdClosed(String adKey);

}
