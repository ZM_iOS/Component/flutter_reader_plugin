package com.chineseall.reader.lib.reader.callbacks;

import android.content.Context;

import com.chineseall.reader.lib.reader.entities.Chapter;
import com.chineseall.reader.lib.reader.view.ILockView;
import com.chineseall.reader.lib.reader.view.ReaderView;

/**
 * Created by huangzhengneng on 2020/4/16.
 */
public interface OnLockViewCallBack {

    void draw(ReaderView readerView, ILockView lockView, Chapter chapter);

    ILockView initInstance(Context context);

}
