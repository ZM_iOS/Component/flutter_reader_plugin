package com.chineseall.reader.lib.reader.callbacks;

/**
 * Created by huangzhengneng on 12/16/20.
 */
public interface OnLogCallBack {

    void log(String tag, String msg);

}
