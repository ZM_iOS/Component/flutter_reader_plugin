package com.chineseall.reader.lib.reader.callbacks;

import android.graphics.PointF;

import com.chineseall.reader.lib.reader.entities.Paragraph;

public interface OnLongPressCallBack {
    void onPress(PointF pointF, Paragraph paragraph);
}
