package com.chineseall.reader.lib.reader.callbacks;

import com.chineseall.reader.lib.reader.entities.Paragraph;

/**
 * Created by huangzhengneng on 2020/7/9.
 */
public interface OnParaCommentClickCallBack {

    void click(Paragraph paragraph);

}
