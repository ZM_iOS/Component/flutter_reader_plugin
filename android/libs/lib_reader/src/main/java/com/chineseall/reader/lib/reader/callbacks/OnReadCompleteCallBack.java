package com.chineseall.reader.lib.reader.callbacks;

/**
 * Created by huangzhengneng on 2020/4/16.
 */
public interface OnReadCompleteCallBack {
    void complete(String bookId);
}
