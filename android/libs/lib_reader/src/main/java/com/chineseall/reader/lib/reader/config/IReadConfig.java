package com.chineseall.reader.lib.reader.config;

import android.graphics.Typeface;

import com.chineseall.reader.lib.reader.core.ReaderClient;
import com.chineseall.reader.lib.reader.utils.DensityUtil;

public interface IReadConfig extends IConfig {
    int CURRENT_PAGE = 0;
    int FIRST_PAGE = -99;
    int LAST_PAGE = 99;
    int LINE_GAP_LARGE = 24;
    int LINE_GAP_MIDDLE = 18;
    int LINE_GAP_SMALL = 12;
    int LINE_GAP_TINY = 6;
    int NEXT_PAGE = 1;
    int PAGER_ANIM_BOOK = 34;
    int PAGER_ANIM_COVER = 35;
    int PAGER_ANIM_NONE = 33;
    int PAGE_ORIENTATION_HORIZONTAL = 2;
    int PAGE_ORIENTATION_VERTICAL = 1;
    int PREVIOUS_PAGE = -1;
    int SKIN_THEME_SBLUE = 69630;
    int SKIN_THEME_BLUE = 69631;
    int SKIN_THEME_PINK = 69632;
    int SKIN_THEME_DAYTIME = 69633;
    int SKIN_THEME_GREEN = 69636;
    int SKIN_THEME_NIGHT = 69634;
    int SKIN_THEME_YELLOW = 69635;
    int STATUS_FAILED = 4;
    int STATUS_INIT = 1;
    int STATUS_LOADED = 2;
    int STATUS_LOADING = 3;
    int STATUS_LOCK = 5;
    int TEXT_SIZE_18 = 18;
    int TEXT_SIZE_20 = 20;
    int TEXT_SIZE_22 = 22;
    int TEXT_SIZE_24 = 24;
    int TEXT_SIZE_26 = 26;
    int TEXT_SIZE_28 = 28;
    int TEXT_SIZE_30 = 30;
    int TEXT_SIZE_DEFAULT = 22;
    int TEXT_SIZE_MAX = 40;
    int TEXT_SIZE_MIN = 12;

    int AD_VIEW_HEIGHT = DensityUtil.dip2px(ReaderClient.getContext(), 284f);

    int getAnimateTime();

    int getDefaultImageHight();

    int getDefaultImageResId();

    int getDefaultImageWidth();

    int getImageMargin();

    int getLineSpace();

    int getPadding();

    int getPageBgColor();

    int getPageBottomBarHeight();

    int getPageFooterTextSize();

    int getPageHeaderTextSize();

    int getPageHeight();

    int getPageTopBarHeight();

    int getPageTopBottomExtraTextColor();

    int getPageWidth();

    int getParagraphSpace();

    float getScreenDensity();

    int getTailColor();

    int getTailTextSize();

    String getTailTitle();

    int getTextColor();

    int getLineColor();

    int getTextSize();

    Typeface getTextTypeface();

    int getTitleBetweenContentGapSpace();

    int getTitleColor();

    int getTitleTextSize();

    Typeface getTitleTypeface();

    int getVerticalChapterMargin();

    boolean isCatalogPageShowBookInfoBlock();

    boolean isShowBookEndRecommendPage();

    boolean isShowChapterTransitPage();

    boolean isShowCollectItemMenu();

    boolean isShowContentImage();

    boolean isShowFloatCollectLayer();

    void setTailTitle(String str);
}
