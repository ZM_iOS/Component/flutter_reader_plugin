package com.chineseall.reader.lib.reader.config;

import android.graphics.Typeface;

public class ReaderConfigWrapper implements IReadConfig {

    private static ReaderConfigWrapper instance;
    private IReadConfig configImpl;

    public static ReaderConfigWrapper getInstance() {
        return instance;
    }

    public static void init(IReadConfig iReadConfig) {
        if (instance == null) {
            instance = new ReaderConfigWrapper();
        }
        instance.configImpl = iReadConfig;
    }

    public int getAnimateTime() {
        return this.configImpl.getAnimateTime();
    }

    public int getDefaultImageHight() {
        return this.configImpl.getDefaultImageHight();
    }

    public int getDefaultImageResId() {
        return this.configImpl.getDefaultImageResId();
    }

    public int getDefaultImageWidth() {
        return this.configImpl.getDefaultImageWidth();
    }

    public int getImageMargin() {
        return this.configImpl.getImageMargin();
    }

    public int getLineSpace() {
        return this.configImpl.getLineSpace();
    }

    public int getPadding() {
        return this.configImpl.getPadding();
    }

    public int getPageBgColor() {
        return this.configImpl.getPageBgColor();
    }

    public int getPageBottomBarHeight() {
        return this.configImpl.getPageBottomBarHeight();
    }

    public int getPageFooterTextSize() {
        return this.configImpl.getPageFooterTextSize();
    }

    public int getPageHeaderTextSize() {
        return this.configImpl.getPageHeaderTextSize();
    }

    public int getPageHeight() {
        return this.configImpl.getPageHeight();
    }

    public int getPageTopBarHeight() {
        return this.configImpl.getPageTopBarHeight();
    }

    public int getPageTopBottomExtraTextColor() {
        return this.configImpl.getPageTopBottomExtraTextColor();
    }

    public int getPageWidth() {
        return this.configImpl.getPageWidth();
    }

    public int getParagraphSpace() {
        return this.configImpl.getParagraphSpace();
    }

    public float getScreenDensity() {
        return this.configImpl.getScreenDensity();
    }

    public int getTailColor() {
        return this.configImpl.getTailColor();
    }

    public int getTailTextSize() {
        return this.configImpl.getTailTextSize();
    }

    public String getTailTitle() {
        return this.configImpl.getTailTitle();
    }

    public int getTextColor() {
        return this.configImpl.getTextColor();
    }

    @Override
    public int getLineColor() {
        return configImpl.getLineColor();
    }

    public int getTextSize() {
        return this.configImpl.getTextSize();
    }

    public Typeface getTextTypeface() {
        return null;
    }

    public int getTitleBetweenContentGapSpace() {
        return this.configImpl.getTitleBetweenContentGapSpace();
    }

    public int getTitleColor() {
        return this.configImpl.getTitleColor();
    }

    public int getTitleTextSize() {
        return this.configImpl.getTitleTextSize();
    }

    public Typeface getTitleTypeface() {
        return this.configImpl.getTextTypeface();
    }

    public int getVerticalChapterMargin() {
        return this.configImpl.getVerticalChapterMargin();
    }

    public boolean isCatalogPageShowBookInfoBlock() {
        return this.configImpl.isCatalogPageShowBookInfoBlock();
    }

    public boolean isShowBookEndRecommendPage() {
        return this.configImpl.isShowBookEndRecommendPage();
    }

    public boolean isShowChapterTransitPage() {
        return this.configImpl.isShowChapterTransitPage();
    }

    public boolean isShowCollectItemMenu() {
        return this.configImpl.isShowCollectItemMenu();
    }

    public boolean isShowContentImage() {
        return this.configImpl.isShowContentImage();
    }

    public boolean isShowFloatCollectLayer() {
        return this.configImpl.isShowFloatCollectLayer();
    }

    public void resetConfig(IReadConfig iReadConfig) {
        if (instance == null) {
            init(iReadConfig);
        } else {
            instance.configImpl = iReadConfig;
        }
    }

    public void setTailTitle(String str) {
        this.configImpl.setTailTitle(str);
    }
}
