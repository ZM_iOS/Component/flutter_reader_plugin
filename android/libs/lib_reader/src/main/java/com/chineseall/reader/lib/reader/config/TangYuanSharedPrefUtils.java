package com.chineseall.reader.lib.reader.config;

import android.content.Context;
import android.text.TextUtils;

import com.chineseall.reader.lib.reader.utils.SharedPrefUtil;

import java.util.ArrayList;
import java.util.Map;

public class TangYuanSharedPrefUtils extends SharedPrefUtil {
    private static String SHARE_PREF_FILE;
    private static TangYuanSharedPrefUtils instance;

    static {
        SHARE_PREF_FILE = "TyAppPrefs";
    }

    public static TangYuanSharedPrefUtils getInstance() {
        if (instance == null) {
            //Logger.e("TangYuanSharedPrefUtils", "You Should Initicalized TangYuanSharedPrefUtils Before Use it !!!", new Object[0]);
        }
        return instance;
    }

    public static void initSharePref(Context context) {
        if (instance == null) {
            instance = new TangYuanSharedPrefUtils();
            instance.sharedPreferences = context.getSharedPreferences(SHARE_PREF_FILE, 0);
        }
    }

    public void addReadedWriteBookNotice(int i) {
        ArrayList readedWriteBookNoticeList = getReadedWriteBookNoticeList();
        if (readedWriteBookNoticeList == null) {
            readedWriteBookNoticeList = new ArrayList();
        }
        if (!readedWriteBookNoticeList.contains(Integer.getInteger("" + i))) {
            readedWriteBookNoticeList.add(Integer.valueOf(i));
        }
        saveObject("write_book_notice", readedWriteBookNoticeList);
    }

    public void cacheLastThreadEditContent(String str) {
        putString("last_thread_edit_content", str);
    }

    public void clearThreadEditContentCache() {
        clearValue("last_thread_edit_content");
    }

    public String getAccountRiskTypUrl() {
        return getString("config_account_risk_typ", "");
    }

    public String getAdvert() {
        return getString("advert");
    }

    public Map<Integer, String> getAppReactiveSetting() {
        return (Map<Integer, String>) getMap("app_reactive_time_periods");
    }

    public String getAppUpdateFileLastModifiedTime() {
        return getString("app_update_file_last_modified");
    }

    public String getAppUpdateLastDownloadUrl() {
        return getString("app_update_last_url");
    }

    public long getAppUpdateLastPosition() {
        return getLong("app_update_last_position", 0);
    }

    public String getBankDistrictVersion() {
        return getString("bank_district_version", "");
    }

  

    public String getBookPublishText14() {
        return getString("book_publish_text_14");
    }

    public String getBookPublishText28() {
        return getString("book_publish_text_28");
    }

    public String getBookPublishText7() {
        return getString("book_publish_text_7");
    }

    public long getBookTagLastModifyTime(long j) {
        return getLong("write_book_" + j + "_last_modify_time", 0);
    }

    public int getBrightness(int i) {
        return getInt("app_bright", i);
    }

    public int getChapterSortGuideFlag() {
        return getInt("chapterSort_guideflag", 0);
    }

    public String getConfigSystemActivity() {
        return getString("config_system_activity", "");
    }

    public String getCurrentReadingBookId() {
        return getString("current_reading_bookid");
    }

    public long getDiagnoseLastValidTime() {
        return getLong("diagnose_last_valid_time");
    }

    public int getEssaycontestId() {
        return getInt("essaycontest_id");
    }

    public String getEssaycontestTitle() {
        return getString("essaycontest_title");
    }

    public String getFamousWisdomJson() {
        return getString("famouswisdom");
    }

    public boolean getFastPunctuationBarShow() {
        return getBoolean("write_editor_fast_punctuation_bar", true);
    }

    public String getFirstHotWord() {
        String string = getString("first_hot_word");
        return TextUtils.isEmpty(string) ? "\u8f93\u5165\u4f5c\u54c1\u3001\u6807\u7b7e\u6216\u7528\u6237\u540d" : string;
    }

    public String getFirstWriteGuide() {
        return getString("is_first_writeguide");
    }

    public int getFontSize(int i) {
        return getInt("get_font_size", i);
    }

    

  

    public int getIntValueByKey(String str, int i) {
        return getInt(str, i);
    }

    public String getLastThreadEditContent() {
        return getString("last_thread_edit_content");
    }

    public String getLastValidMagicCode() {
        return getString("diagnose_last_valid_code");
    }

   

    public String getLogin() {
        return getString("login", "");
    }

    public int getPageLineGap(int i) {
        return getInt("read_line_gap", i);
    }

    public int getPageMode(int i) {
        return getInt("read_pagemode", i);
    }

    public int getPagerAnim(int i) {
        return getInt("read_pager_anim", i);
    }

    public String getPhoneNumber() {
        return getString("phonenumber");
    }

    public int getPpkinNum(String str) {
        return getInt(str, 0);
    }

    public String getPushEnv() {
        return getString("getui_push_env", "local");
    }

    public String getPushSettingSwitchJson() {
        return getString("push_setting_switch");
    }

    public int getReadBookProgress(String str, String str2) {
        return getInt("read_progress_" + str + "_" + str2, 0);
    }

    public int getReadGuideFlag() {
        return getInt("reader_guideflag", 0);
    }

    public String getReadProgress(String str) {
        return getString("readbookid_" + str, "");
    }

    public ArrayList<Integer> getReadedWriteBookNoticeList() {
        ArrayList<Integer> arrayList = (ArrayList) getObject("write_book_notice");
        return arrayList == null ? new ArrayList() : arrayList;
    }

    public int getReaderPageOrientation(int i) {
        return getInt("Reader_Page_Orientation", i);
    }

    public int getReaderPagerAnim(int i) {
        return getInt("Reader_Pager_Anim", i);
//		//case BadgeView.POSITION_TOP_LEFT /*1*/:
//		//case IReadConfig.PAGER_ANIM_NONE /*33*/:
//		//case IReadConfig.PAGER_ANIM_BOOK /*34*/:
//		//case IReadConfig.PAGER_ANIM_COVER /*35*/:
    }

    public boolean isReadModeOn(){
        return getBoolean("isReadModeOn",false);
    }

    public void setReadModeOn(boolean flag){
        putBoolean("isReadModeOn",flag);
    }

    public String getRewardPopsToken() {
        return getString("reward_props_token");
    }

    public int getSceneMode(int i) {
        return getInt("readscenemode", i);
    }

    public String getSex() {
        return getString("sex_preference");
    }

    public String getShareMessage() {
        return getString("sharemessage", "");
    }

    public String getShareTemplate(int i) {
        return getString("sharetemplate_" + i, "");
    }

    public int getShowImageType(int i) {
        return getInt("show_image_type", i);
    }

    public int getSleepTime() {
        return getInt("sleeptime", 300000);
    }

    public int getSoftInputMethodHeight(String str) {
        return getInt(str, 540);
    }

    public long getStatisticEditWordDate() {
        return getLong("statistic_edit_word_date", 0);
    }

   

    public String getSystemConfig() {
        return getString("systemconfig");
    }

    public int getTodayEditWordcount() {
        return getInt("today_edit_wordcount", 0);
    }

   

    public boolean getUserWebLoginStatus(String str) {
        return getBoolean(str + "_is_web_login", false);
    }

    

    public String getVersionCode() {
        return getString("app_version_code");
    }

    public boolean getWriteDraftEditAuthorSayShowed() {
        return getBoolean("write_draft_edit_author_say", false);
    }

    public boolean getWriteDraftEditGuideShowed() {
        return getBoolean("write_draft_edit_guide", false);
    }

    public int getWriteEditorFontSize(int i) {
        return getInt("write_editor_font_size", i);
    }

    
    public String getWriteEditorSkinCodeBeforeNight(String str) {
        return getString("write_editor_skin_code_before_night", str);
    }

    public String getWriteFuli() {
        return getString("writefuli");
    }

    public String getWriteStatistics() {
        String string;
        synchronized (this) {
            string = getString("write_statistics");
        }
        return string;
    }

    public String getWriteguideJson() {
        return getString("writeguide");
    }

    public String get_book_category(long j) {
        return getString("book_category_" + j);
    }

    public String get_book_cover(long j) {
        return getString("book_cover_" + j);
    }

    public int get_favCount(String str) {
        return getInt("user_favecount_id" + str, 0);
    }

    public boolean get_load(String str) {
        return getBoolean("save_load_uid" + str, false);
    }

    

    public String gettangyuanvip(String str) {
        return getString(str, "");
    }

    public boolean isAgreeWriteProtocal() {
        return getBoolean("agree_write_protocal", false);
    }

    public boolean isAppAloneBrightness() {
        return getBoolean("is_app_alone_bright", false);
    }

    public boolean isBibiShareDownloaded() {
        return getBoolean("is_bibi_share_downloaded", false);
    }

    public boolean isBookInfoReplenishChecked(long j) {
        return getBoolean("write_book_" + j + "_info_replenish_checked", false);
    }

    public boolean isBookSynced(long j) {
        return getBoolean("sync_bookid_" + j, false);
    }

    public boolean isDownloadChapterOnlyInWifi() {
        return getBoolean("is_download_only_in_wifi", false);
    }

    public boolean isFirstRun() {
        return getBoolean("is_first_run", true);
    }

    public boolean isNeedShowRewardMask() {
        return getBoolean("is_need_show_reward_mask", true);
    }

    public boolean isNeedShowSortChapterTip() {
        return getBoolean("is_need_show_sort_chapter_tip", true);
    }

    public boolean isNeedShowTipInWorksInfoActivity() {
        return getBoolean("is_need_show_tip_in_works_info", true);
    }

    public boolean isOnlyWifiSync() {
        return getBoolean("onlywifsync", false);
    }

    public boolean isRecordRunningLogs() {
        return getBoolean("is_record_running_logs", false);
    }

    public boolean isShowCover() {
        return getBoolean("is_show_cover", true);
    }

    public boolean isShowEssaycontestTip() {
        return getBoolean("is_show_essaycontest_tip", false);
    }

    public boolean isShowedTabWriteGuide(String str) {
        return getBoolean("tab_" + str, false);
    }

    public boolean isShowedWriteGuide(String str) {
        return getBoolean(str, false);
    }

    public boolean isThreeGuideImgShow() {
        return getBoolean("three_guide_tip_show", false);
    }

    public boolean isUseVolumeKeyPager() {
        return getBoolean("is_volume_key_pager", true);
    }

    public boolean is_chapters_first(long j) {
        return getBoolean("is_chapters_first_" + j, true);
    }

    public boolean is_first_brightness() {
        return getBoolean("is_first_brightness", true);
    }

    public int isfirstShowFav(String str) {
        return getInt("first_showfav_" + str, 0);
    }

    public int isfirstShowItem(String str) {
        return getInt("first_showitem_" + str, 0);
    }

    public int offlineloadflag() {
        return getInt("offlineloadflag", 0);
    }

    public void putBrightness(int i) {
        putInt("app_bright", i);
    }

    public void putPageLineGap(int i) {
        putInt("read_line_gap", i);
    }

    public void putPageMode(int i) {
        putInt("read_pagemode", i);
    }

    public void putSceneMode(int i) {
        putInt("readscenemode", i);
    }
    
    public void setFontType(String typeface){
    	putString("read_font_typeface",typeface);
    }
    
    public String getFontType(){
    	return getString("read_font_typeface", "");
    }

    public void resetAccount() {
        resetUserInfo();
        resetWeibo();
        resetQQ();
    }

    public void resetQQ() {
        putString("qq_v1", "");
    }

    public void resetUserInfo() {
        putString("account_v1", "");
    }

    public void resetWeibo() {
        putString("weibo_v1", "");
    }

    public void saveAccountRiskTypUrl(String str) {
        putString("config_account_risk_typ", str);
    }

    public void saveAppReactiveSetting(Map<Integer, String> map) {
        saveMap("app_reactive_time_periods", map);
    }

    public void saveAppUpdatePosition(long j) {
        putLong("app_update_last_position", j);
    }

    public void saveBiggieAuthors(String str) {
        putString("savedaka_", str);
    }

    public void saveBookTagLastModifyTime(long j, long j2) {
        putLong("write_book_" + j + "_last_modify_time", j2);
    }

    public void saveBookinfoFav(boolean z) {
        putBoolean("f_bookinfo_fav", z);
    }

    public void saveDiagnoseLastValidTime(long j) {
        putLong("diagnose_last_valid_time", j);
    }

    public void saveHotAuthorAstroRank(String str) {
        putString("saveAustroList_", str);
    }

    public void saveHotAuthorFansRank(String str) {
        putString("_FunsList", str);
    }

    public void saveHotAuthorPageJson(String str) {
        putString("hotauthor", str);
    }

    public void saveHotAuthorWeeklyRank(String str) {
        putString("saveweekAuthorList_", str);
    }

    public void saveLastValidMagicCode(String str) {
        putString("diagnose_last_valid_code", str);
    }

   

    public void savePagerAnim(int i) {
        putInt("read_pager_anim", i);
    }

    public void savePhoneNumber(String str) {
        putString("phonenumber", str);
    }

    public void savePushSettingSwitchJson(String str) {
        putString("push_setting_switch", str);
    }

    public void saveReadBookProgress(String str, String str2, int i) {
        putInt("read_progress_" + str + "_" + str2, i);
    }
    
    public void saveLastReaderChapterId(String key,String value){
    	putString("last_chapterid_"+key, value);
    }
    
    public String getLastReaderChapterId(String bookid){
    	 return getString("last_chapterid_"+bookid,"");
    }

    public void saveReadProgress(String str, String str2, int i) {
        putString("readbookid_" + str, str2 + "|" + i);
    }

    public void saveReaderPageOrientation(int i) {
        putInt("Reader_Page_Orientation", i);
    }

    public void saveReaderPagerAnim(int i) {
        putInt("Reader_Pager_Anim", i);
    }

    public void saveRewardPopsToken(String str) {
        putString("reward_props_token", str);
    }

    public void saveShareTemplate(int i, String str) {
        putString("sharetemplate_" + i, str);
    }

    public void saveSoftInputMethodHeight(String str, int i) {
        putInt(str, i);
    }

    public void saveSuperList(String str) {
        putString("savesuperlist_", str);
    }

    public void saveSuperRoom(String str) {
        putString("saveSuperRoom_", str);
    }

    public void saveSuperRoomList(String str) {
        putString("saveSuperRoomList_", str);
    }

  

    public void saveUserWebLoginStatus(String str, boolean z) {
        putBoolean(str + "_is_web_login", z);
    }

    public void saveVerifyAuthors(String str) {
        putString("saveauthorlist_", str);
    }

    public void saveWriteGuideImg(boolean z) {
        putBoolean("write_error_guide", z);
    }

    public void saveWriteStatistics(String str) {
        synchronized (this) {
            putString("write_statistics", str);
        }
    }

    public void save_book_category(long j, String str) {
        putString("book_category_" + j, str);
    }

    public void save_book_cover(long j, String str) {
        putString("book_cover_" + j, str);
    }

    public void save_favCount(String str, int i) {
        putInt("user_favecount_id" + str, i);
    }

    public void save_load(String str, boolean z) {
        putBoolean("save_load_uid" + str, z);
    }

    public void save_qrcode_guide(boolean z) {
        putBoolean("qrcode_guide", z);
    }

    public void savef_AccountView(boolean z) {
        putBoolean("f_accountview", z);
    }

    public void savefirstShowFav(String str) {
        putInt("first_showfav_" + str, 1);
    }

    public void savefirstShowItem(String str) {
        putInt("first_showitem_" + str, 1);
    }

    public void savetangyuanvip(String str, String str2) {
        putString(str, str2);
    }

    public void setAdvert(String str) {
        putString("advert", str);
    }

    public void setAgreeWriteProtocal(boolean z) {
        putBoolean("agree_write_protocal", z);
    }

    public void setAppAloneBrightness(boolean z) {
        putBoolean("is_app_alone_bright", z);
    }

    public void setBankDistrictVersion(String str) {
        putString("bank_district_version", str);
    }

    public void setBibiShareDownloaded(boolean z) {
        putBoolean("is_bibi_share_downloaded", z);
    }

    public void setBookInfoReplenishChecked(long j) {
        putBoolean("write_book_" + j + "_info_replenish_checked", true);
    }

    public void setBookPublishText14(String str) {
        putString("book_publish_text_14", str);
    }

    public void setBookPublishText28(String str) {
        putString("book_publish_text_28", str);
    }

    public void setBookPublishText7(String str) {
        putString("book_publish_text_7", str);
    }

    public void setBookSynced(long j) {
        putBoolean("sync_bookid_" + j, true);
    }

    public void setChapterSortGuideFlag(int i) {
        putInt("chapterSort_guideflag", i);
    }

    public void setConfigSystemActivity(String str) {
        putString("config_system_activity", str);
    }

    public void setCurrentReadingBookId(String str) {
        putString("current_reading_bookid", str);
    }

    public void setDownloadChapterOnlyInWifi(boolean z) {
        putBoolean("is_download_only_in_wifi", z);
    }

    public void setEssaycontestId(int i) {
        putInt("essaycontest_id", i);
    }

    public void setEssaycontestTitle(String str) {
        putString("essaycontest_title", str);
    }

    public void setFamousWisdomJson(String str) {
        putString("famouswisdom", str);
    }

    public void setFastPunctuationBarShow(boolean z) {
        putBoolean("write_editor_fast_punctuation_bar", z);
    }

    public void setFirstHotWord(String str) {
        putString("first_hot_word", str);
    }

    public void setFirstWriteGuide(String str) {
        putString("is_first_writeguide", str);
    }

    public void setFontSize(int i) {
        putInt("get_font_size", i);
    }

    public void setIsFirstRun(boolean z) {
        putBoolean("is_first_run", z);
    }

    public void setLogin(String str) {
        putString("login", str);
    }

    public void setNeedShowRewardMask(boolean z) {
        putBoolean("is_need_show_reward_mask", z);
    }

    public boolean setNeedShowSortChapterTip(boolean z) {
        return putBoolean("is_need_show_sort_chapter_tip", z);
    }

    public void setNeedShowTipInWorksInfoActivity(boolean z) {
        putBoolean("is_need_show_tip_in_works_info", z);
    }

    public void setPpkinNum(String str, int i) {
        putInt(str, i);
    }

    public void setPushEnv(String str) {
        putString("getui_push_env", str);
    }

    public void setReadGuideFlag(int i) {
        putInt("reader_guideflag", i);
    }

    public void setRecordRunningLogs(boolean z) {
        putBoolean("is_record_running_logs", z);
    }

    public void setSex(String str) {
        putString("sex_preference", str);
    }

    public void setShareMessage(String str) {
        putString("sharemessage", str);
    }

    public void setShowEssaycontestTip(boolean z) {
        putBoolean("is_show_essaycontest_tip", z);
    }

    public void setShowImageType(int i) {
        putInt("show_image_type", i);
    }

    public void setShowedTabWriteGuide(String str) {
        putBoolean("tab_" + str, true);
    }

    public void setShowedWriteGuide(String str) {
        putBoolean(str, true);
    }

    public void setStatisticEditWordDate(long j) {
        putLong("statistic_edit_word_date", j);
    }

   

    public void setSystemConfig(String str) {
        putString("systemconfig", str);
    }

    public void setThreeGuideImgShow(boolean z) {
        putBoolean("three_guide_tip_show", z);
    }

    public void setTodayEditWordcount(int i) {
        putInt("today_edit_wordcount", i);
    }

    public void setUserVolumeKeyPager(boolean z) {
        putBoolean("is_volume_key_pager", z);
    }

    public void setVersionCode(String str) {
        putString("app_version_code", str);
    }

    public void setWifiSync(boolean z) {
        putBoolean("onlywifsync", z);
    }

    public void setWriteDraftEditAuthorSayShowed() {
        putBoolean("write_draft_edit_author_say", true);
    }

    public void setWriteDraftEditGuideShowed() {
        putBoolean("write_draft_edit_guide", true);
    }

    public void setWriteEditorFontSize(int i) {
        putInt("write_editor_font_size", i);
    }

    

    public void setWriteEditorSkinCodeBeforeNight(String str) {
        putString("write_editor_skin_code_before_night", str);
    }

    public void setWriteFuli(String str) {
        putString("writefuli", str);
    }

    public void setWriteGuideJson(String str) {
        putString("writeguide", str);
    }

    public void set_chapters_first(long j) {
        putBoolean("is_chapters_first_" + j, false);
    }

    public void set_first_brightness(boolean z) {
        putBoolean("is_first_brightness", z);
    }

    public void setofflineloadflag(int i) {
        putInt("offlineloadflag", i);
    }

    public boolean showBookinfoFav() {
        return getBoolean("f_bookinfo_fav", false);
    }

    public boolean showWriteGuideImg() {
        return getBoolean("write_error_guide", true);
    }

    public boolean show_qrcode_guide() {
        return getBoolean("qrcode_guide", true);
    }

    public boolean showf_AccountView() {
        return getBoolean("f_accountview", false);
    }
    public boolean isProtectedEyeMode() {
        return getBoolean("is_protected_eye", false);
    }

    public void setProtectEyeMode(boolean b) {
        putBoolean("is_protected_eye", b);
    }

    public boolean isSingleTouchMode() {
        return getBoolean("is_single_touch_mode", false);
    }

    public void setSingleTouchMode(boolean singleTouchMode) {
        putBoolean("is_single_touch_mode", singleTouchMode);
    }

    public boolean hasShownReaderTips() {
        return getBoolean("has_shown_reader_tips_1", false);
    }

    public boolean hasShownParaCommentTips() {
        return getBoolean("has_shown_para_comment_tips", false);
    }

    public boolean hasShownReaderMenuTips() {
        return getBoolean("has_shown_reader_tips_2", false);
    }

    public void setShownParaCommentTips(boolean shown) {
        putBoolean("has_shown_para_comment_tips", shown);
    }

    public void setShownReaderTips(boolean shown) {
        putBoolean("has_shown_reader_tips_1", shown);
    }

    public void setShownReaderMenuTips(boolean shown) {
        putBoolean("has_shown_reader_tips_2", shown);
    }

    public void saveChapterEndViewHeight(int height) {
        putInt("READER_VIEW_END_VIEW_HEIGHT", height);
    }

    public int getChapterEndViewHeight() {
        return getInt("READER_VIEW_END_VIEW_HEIGHT", 0);
    }

    public boolean showParaCommentCount() {
        return getBoolean("show_para_comment_count", true);
    }

    public void setParaCommentCountVisible(boolean visible) {
        putBoolean("show_para_comment_count", visible);
    }
}
