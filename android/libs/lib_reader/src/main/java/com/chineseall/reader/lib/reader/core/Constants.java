package com.chineseall.reader.lib.reader.core;

/**
 * Created by huangzhengneng on 2020/4/15.
 */
public class Constants {

    /*
     * 字体相关 ------ start
     */
    public static String FONT_PATH = "";
    public static final String FONT_KAITI = "kaiti.ttf";
    public static final String FONT_HEITI = "heiti.ttf";
    public static final String FONT_ALIBABA = "alibabapht.otf";//阿里巴巴普惠体
    public static final String FONT_SONGTI = "songti.ttf";
    public static final String FONT_YUANTI = "yuanti.ttf";
    public static final String FONT_FANGZHENG = "fzlth.ttf";
    public static final String FONT_FZYS = "fzys.ttf";
    public static final String FONT_SANS = "sans";
    public static final String FONT_SERIF = "serif";
    public static final String FONT_MONOSPACE = "monospace";
    public static final String FONT_NORMAL = "normal";
    public static final String FONT_FZQK = "fzqk.ttf";//方正清刻
    public static final String FONT_HYZS = "hyzs.ttf";//汉仪中宋
    public static final String FONT_HYQH = "hyqh.ttf";//汉仪旗黑

    /**
     * 阅读页单页广告间隔数
     */
    public static int CHAPTERAD_INTERVALNUM = 300000;
    /*
     * 字体相关 ------ end
     */

    public static float OFFSET_STARTX = 0;

    /*
     * 请求阅读内容广告尺寸
     * 大图和小图
     */
    public static final int AD_SIZE_SMALL = 0;
    public static final int AD_SIZE_BIG = 1;

    public static final String AUTO_SCROLL = "auto_scroll";
    public static final String AUTO_SCROLL_SPEED = "auto_scroll_speed";
}
