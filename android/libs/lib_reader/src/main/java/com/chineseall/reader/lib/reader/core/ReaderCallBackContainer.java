package com.chineseall.reader.lib.reader.core;

import android.content.Context;
import android.view.View;

import com.chineseall.reader.lib.reader.callbacks.OnAdViewCallBack;
import com.chineseall.reader.lib.reader.callbacks.OnAdViewReceiver;
import com.chineseall.reader.lib.reader.callbacks.OnAppPvCallBack;
import com.chineseall.reader.lib.reader.callbacks.OnAutoSubscriptionCallBack;
import com.chineseall.reader.lib.reader.callbacks.OnChapterLoadResult;
import com.chineseall.reader.lib.reader.callbacks.OnExitAutoReadCallBack;
import com.chineseall.reader.lib.reader.callbacks.OnLockViewCallBack;
import com.chineseall.reader.lib.reader.callbacks.OnLogCallBack;
import com.chineseall.reader.lib.reader.callbacks.OnLongPressCallBack;
import com.chineseall.reader.lib.reader.callbacks.OnParaCommentClickCallBack;
import com.chineseall.reader.lib.reader.callbacks.OnReadCompleteCallBack;
import com.chineseall.reader.lib.reader.callbacks.OnVerticalViewCallBack;
import com.chineseall.reader.lib.reader.entities.Chapter;
import com.chineseall.reader.lib.reader.view.ILockView;
import com.chineseall.reader.lib.reader.view.ReaderView;

/**
 * Created by huangzhengneng on 2020/4/15.
 */
public class ReaderCallBackContainer {

    private OnAutoSubscriptionCallBack autoSubscriptionCallBack;
    private OnAutoSubscriptionCallBack mDefaultAutoSubscriptionCallBack = bookId -> false;
    private OnAppPvCallBack mOnAppPvCallBack;
    private OnAppPvCallBack mDefaultOnAppPvCallBack = () -> { };

    private OnLongPressCallBack mOnLongPressCallBack;
    private OnLongPressCallBack mDefaultOnLongPressCallBack = ((pointF, paragraph) -> {

    });

    private OnExitAutoReadCallBack mOnExitAutoReadCallBack;
    private OnExitAutoReadCallBack mDefaultOnExitAutoReadCallBack = new OnExitAutoReadCallBack() {
        @Override
        public boolean isExitAutoMode(ReaderView readerView) {
            return false;
        }

        @Override
        public void setExitAutoMode(ReaderView readerView, boolean exitAutoMode) {

        }
    };

    private OnChapterLoadResult mOnChapterLoadResult;
    private OnChapterLoadResult mDefaultOnChapterLoadResult = (readerView, chapterTitle) -> {

    };
    private OnReadCompleteCallBack mOnReadCompleteCallBack;
    private OnReadCompleteCallBack mDefaultOnReadCompleteCallBack = (bookId) -> { };

    private OnVerticalViewCallBack mOnVerticalViewCallBack;
    private OnVerticalViewCallBack mDefaultOnVerticalViewCallBack = new OnVerticalViewCallBack() {
        @Override
        public void exitAutoScrollMode() {

        }

        @Override
        public boolean isAutoScrollMenuShow(ReaderView readerView) {
            return false;
        }
    };

    private OnLockViewCallBack mOnLockViewDrawCallBack;
    private OnLockViewCallBack mDefaultOnLockViewDrawCallBack = new OnLockViewCallBack() {
        @Override
        public void draw(ReaderView readerView, ILockView lockView, Chapter chapter) {

        }

        @Override
        public ILockView initInstance(Context context) {
            return null;
        }
    };

    private OnParaCommentClickCallBack mOnParaCommentClickCallBack;
    private OnParaCommentClickCallBack mDefaultOnParaCommentClickCallBack = paragraph -> {

    };

    private OnAdViewCallBack mOnAdViewCallBack;
    private OnAdViewCallBack mDefaultOnAdViewCallBack = new OnAdViewCallBack() {
        @Override
        public void get(OnAdViewReceiver receiver) {

        }

        @Override
        public void adClick(View adView) {

        }

        @Override
        public void adShow(View adView) {

        }
    };

    private OnLogCallBack mOnLogCallBack;
    private OnLogCallBack mDefaultLogCallBack = (tag, msg) -> {

    };

    ReaderCallBackContainer() {
    }

    public void setOnLogCallBack(OnLogCallBack mOnLogCallBack) {
        this.mOnLogCallBack = mOnLogCallBack;
    }

    public OnLogCallBack getOnLogCallBack() {
        return mOnLogCallBack == null ? mDefaultLogCallBack : mOnLogCallBack;
    }

    public void setOnAdViewCallBack(OnAdViewCallBack mOnAdViewCallBack) {
        this.mOnAdViewCallBack = mOnAdViewCallBack;
    }

    public OnAdViewCallBack getOnAdViewCallBack() {
        return mOnAdViewCallBack;
    }

    void setOnParaCommentClickCallBack(OnParaCommentClickCallBack onParaCommentClickCallBack) {
        this.mOnParaCommentClickCallBack = onParaCommentClickCallBack;
    }

    void setAutoSubscriptionCallBack(OnAutoSubscriptionCallBack autoSubscriptionCallBack) {
        this.autoSubscriptionCallBack = autoSubscriptionCallBack;
    }

    void setOnAppPvCallBack(OnAppPvCallBack mOnAppPvCallBack) {
        this.mOnAppPvCallBack = mOnAppPvCallBack;
    }

    void setOnExitAutoReadCallBack(OnExitAutoReadCallBack mOnExitAutoReadCallBack) {
        this.mOnExitAutoReadCallBack = mOnExitAutoReadCallBack;
    }

    void setOnChapterLoadResult(OnChapterLoadResult result) {
        mOnChapterLoadResult = result;
    }

    void setOnReadCompleteCallBack(OnReadCompleteCallBack callBack) {
        mOnReadCompleteCallBack = callBack;
    }

    void setOnVerticalViewCallBack(OnVerticalViewCallBack callBack) {
        mOnVerticalViewCallBack = callBack;
    }

    void setOnLockViewDrawCallBack(OnLockViewCallBack mOnLockViewDrawCallBack) {
        this.mOnLockViewDrawCallBack = mOnLockViewDrawCallBack;
    }

    void setOnLongPressCallBack(OnLongPressCallBack callBack) {
        this.mOnLongPressCallBack = callBack;
    }

    public OnAutoSubscriptionCallBack getOnAutoSubscriptionCallBack() {
        return autoSubscriptionCallBack == null ? mDefaultAutoSubscriptionCallBack : autoSubscriptionCallBack;
    }

    public OnAppPvCallBack getOnAppPvCallBack() {
        return mOnAppPvCallBack == null ? mDefaultOnAppPvCallBack : mOnAppPvCallBack;
    }

    public OnExitAutoReadCallBack getOnExitAutoReadCallBack() {
        return mOnExitAutoReadCallBack == null ? mDefaultOnExitAutoReadCallBack : mOnExitAutoReadCallBack;
    }

    public OnChapterLoadResult getOnChapterLoadResult() {
        return mOnChapterLoadResult == null ? mDefaultOnChapterLoadResult : mOnChapterLoadResult;
    }

    public OnReadCompleteCallBack getOnReadCompleteCallBack() {
        return mOnReadCompleteCallBack == null ? mDefaultOnReadCompleteCallBack : mOnReadCompleteCallBack;
    }

    public OnVerticalViewCallBack getOnVerticalViewCallBack() {
        return mOnVerticalViewCallBack == null ? mDefaultOnVerticalViewCallBack : mOnVerticalViewCallBack;
    }

    public OnLockViewCallBack getOnLockViewDrawCallBack() {
        return mOnLockViewDrawCallBack == null ? mDefaultOnLockViewDrawCallBack : mOnLockViewDrawCallBack;
    }

    public OnLongPressCallBack getOnLongPressCallBack() {
        return mOnLongPressCallBack == null ? mDefaultOnLongPressCallBack : mOnLongPressCallBack;
    }

    public OnParaCommentClickCallBack getOnParaCommentClickCallBack() {
        return mOnParaCommentClickCallBack == null ? mDefaultOnParaCommentClickCallBack : mOnParaCommentClickCallBack;
    }
}
