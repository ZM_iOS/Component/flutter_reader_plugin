package com.chineseall.reader.lib.reader.core;

import android.annotation.SuppressLint;
import android.content.Context;

import com.chineseall.reader.lib.reader.callbacks.OnAdViewCallBack;
import com.chineseall.reader.lib.reader.callbacks.OnAppPvCallBack;
import com.chineseall.reader.lib.reader.callbacks.OnAutoSubscriptionCallBack;
import com.chineseall.reader.lib.reader.callbacks.OnChapterLoadResult;
import com.chineseall.reader.lib.reader.callbacks.OnExitAutoReadCallBack;
import com.chineseall.reader.lib.reader.callbacks.OnLockViewCallBack;
import com.chineseall.reader.lib.reader.callbacks.OnLogCallBack;
import com.chineseall.reader.lib.reader.callbacks.OnLongPressCallBack;
import com.chineseall.reader.lib.reader.callbacks.OnParaCommentClickCallBack;
import com.chineseall.reader.lib.reader.callbacks.OnReadCompleteCallBack;
import com.chineseall.reader.lib.reader.callbacks.OnVerticalViewCallBack;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by huangzhengneng on 2020/4/15.
 */
public class ReaderClient {

    @SuppressLint("StaticFieldLeak")
    private static Context mContext;
    private static final ReaderCallBackContainer mCallBackContainer = new ReaderCallBackContainer();
    private static ReaderClient mInstance;

    public Map<Integer, Integer> map = new HashMap<>();

    private ReaderClient(Context context,
                         String fontPath) {
        mContext = context.getApplicationContext();
        Constants.FONT_PATH = fontPath;
    }

    public static Context getContext() {
        if (mContext == null) {
            throw new IllegalStateException("reader manager not init...");
        }
        return mContext;
    }

    public static ReaderClient getInstance() {
        if (mInstance == null) {
            throw new IllegalStateException("reader manager not init...");
        }
        return mInstance;
    }

    public ReaderCallBackContainer getCallBackContainer() {
        return mCallBackContainer;
    }

    public static class Builder {

        private final Context context;
        private String fontPath;

        public Builder(Context context) {
            this.context = context;
        }

        public Builder fontPath(String fontPath) {
            this.fontPath = fontPath;
            return this;
        }

        public Builder autoSub(OnAutoSubscriptionCallBack callBack) {
            mCallBackContainer.setAutoSubscriptionCallBack(callBack);
            return this;
        }

        public Builder appPv(OnAppPvCallBack callBack) {
            mCallBackContainer.setOnAppPvCallBack(callBack);
            return this;
        }

        public Builder exitAutoRead(OnExitAutoReadCallBack callBack) {
            mCallBackContainer.setOnExitAutoReadCallBack(callBack);
            return this;
        }

        public Builder chapterLoad(OnChapterLoadResult result) {
            mCallBackContainer.setOnChapterLoadResult(result);
            return this;
        }

        public Builder readComplete(OnReadCompleteCallBack callBack) {
            mCallBackContainer.setOnReadCompleteCallBack(callBack);
            return this;
        }

        public Builder verticalCallBacks(OnVerticalViewCallBack callBack) {
            mCallBackContainer.setOnVerticalViewCallBack(callBack);
            return this;
        }

        public Builder drawLockView(OnLockViewCallBack callBack) {
            mCallBackContainer.setOnLockViewDrawCallBack(callBack);
            return this;
        }

        public Builder longPress(OnLongPressCallBack callBack) {
            mCallBackContainer.setOnLongPressCallBack(callBack);
            return this;
        }

        public Builder clickParaComment(OnParaCommentClickCallBack callBack) {
            mCallBackContainer.setOnParaCommentClickCallBack(callBack);
            return this;
        }

        public Builder adViewProvider(OnAdViewCallBack callBack) {
            mCallBackContainer.setOnAdViewCallBack(callBack);
            return this;
        }

        public Builder log(OnLogCallBack callBack) {
            mCallBackContainer.setOnLogCallBack(callBack);
            return this;
        }

        public ReaderClient build() {
            if (context == null) {
                throw new IllegalArgumentException("reader init context is null");
            }
            mInstance = new ReaderClient(context, fontPath);
            return mInstance;
        }
    }
}
