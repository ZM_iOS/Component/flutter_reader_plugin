//
// Decompiled by Procyon v0.5.30
// 

package com.chineseall.reader.lib.reader.entities;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;

import com.chineseall.reader.lib.reader.config.ReaderConfigWrapper;
import com.chineseall.reader.lib.reader.config.TangYuanSharedPrefUtils;
import com.chineseall.reader.lib.reader.core.ReaderClient;
import com.chineseall.reader.lib.reader.utils.IChapterHelper;
import com.chineseall.reader.lib.reader.utils.LogUtils;
import com.chineseall.reader.lib.reader.utils.PaintHelper;
import com.chineseall.reader.lib.reader.view.ReaderView;
import com.chineseall.reader.lib.reader.view.horizontal.PaperView;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class Article implements Serializable {
    private static final long serialVersionUID = 4094918153185295335L;
    /**
     * 当前文章的长度，可变
     */
    private int articleHeight;
    private int bookid;
    public boolean canDraw;
    private IChapterHelper chapterHelper;
    private Map<String, Integer> chapterIdMap;
    /**
     * 这里的章节就是整本书的完整章节，即使是2000多章
     */
    private List<Chapter> chapters;
    /**
     * 作品章节信息加载状态
     */
    private int chaptersInfoStatus;
    /**
     * 视野里面能看到的第一个章节内容
     */
    private Chapter currentChapter;
    /**
     * 默认显示的章节
     */
    private String defid;
    private ReaderView readerview;

    public Chapter getRecordChapter() {
        return recordChapter;
    }

    /**
     * 阅读统计中最后上报的章节
     */
    private Chapter recordChapter;
    /**
     * 章节总页数
     */
    private int totalPage;
    /**
     * 页面的高度
     */
    private int viewHeight;
    private int viewWidth;
    private int yOffset;
    private boolean needLoadBookFromNet;

    private boolean isClickPreviousChapter;  //是否点击的前一章节

    public boolean isNeedLoadBookFromNet() {
        return needLoadBookFromNet;
    }

    public int getBookid() {
        return bookid;
    }

    public void setNeedLoadBookFromNet(boolean needLoadBookFromNet) {
        this.needLoadBookFromNet = needLoadBookFromNet;
    }

    public Article(final ReaderView readerview, final int bookid, final ArrayList<Chapter> chapters) {
        this.chapters = new ArrayList<Chapter>();
        this.totalPage = 0;
        this.viewHeight = 0;
        this.viewWidth = 0;
        this.articleHeight = 0;
        this.chaptersInfoStatus = 1;
        this.yOffset = 0;
        this.readerview = null;
        this.recordChapter = null;
        this.chapterIdMap = new HashMap<String, Integer>();
        this.readerview = readerview;
        setCurrentChapter(null);
        this.bookid = bookid;
        this.chapters = chapters;
        if (chapters != null) {
            int size = chapters.size();
            for (int i = 0; i < size; ++i) {
                this.chapterIdMap.put(chapters.get(i).getChapterId(), i);
            }
        }
//        this.viewWidth = 1080; this.viewHeight = 1920 - 130;


        this.viewHeight = readerview.getHeight();

        this.viewWidth = readerview.getWidth();

    }

    /**
     * 需在load方法之前调用
     *
     * @param helper
     */
    public void setChapterHelper(IChapterHelper helper) {
        this.chapterHelper = helper;
    }

    private void drawBattery(Canvas canvas, int bx1, int by1, int height, int width, int mPower) {
        int battery_left = bx1;
        int battery_top = by1;
        int battery_width = width;
        int battery_height = height;

        int battery_head_width = 3;
        int battery_head_height = 3;

        float density = ReaderConfigWrapper.getInstance().getScreenDensity();

        battery_head_width = (int) (3 * density);
        battery_head_height = (int) (3 * density);

        int battery_inside_margin = 3;

        //先画外框
        Paint paint = new Paint(PaintHelper.getInstance().getHeaderPaint());
        paint.setStyle(Paint.Style.STROKE);

        Rect rect = new Rect(battery_left, battery_top, battery_left + battery_width, battery_top + battery_height);
        canvas.drawRect(rect, paint);

        float power_percent = mPower / 100.0f;

        Paint paint2 = new Paint(paint);
        paint2.setStyle(Paint.Style.FILL);
        //画电量
        if (power_percent != 0) {
            int p_left = battery_left + battery_inside_margin;
            int p_top = battery_top + battery_inside_margin;
            int p_right = p_left - battery_inside_margin + (int) ((battery_width - battery_inside_margin) * power_percent);
            int p_bottom = p_top + battery_height - battery_inside_margin * 2;
            Rect rect2 = new Rect(p_left, p_top, p_right, p_bottom);
            canvas.drawRect(rect2, paint2);
        }

        //画电池头
        int h_left = battery_left + battery_width;
        int h_top = battery_top + battery_height / 2 - battery_head_height / 2;
        int h_right = h_left + battery_head_width;
        int h_bottom = h_top + battery_head_height;
        Rect rect3 = new Rect(h_left, h_top, h_right, h_bottom);
        canvas.drawRect(rect3, paint2);
    }

    private String getChapterTitle(int max) {
        int index = 0;
        for (int i = 0; index == 0 && i < chapters.size(); i++) {
            if (chapters.get(i).getChapterId().equals(currentChapter.getChapterId())) {
                index = i + 1;
                break;
            }
        }

        Paint headerPaint = PaintHelper.getInstance().getHeaderPaint();
//        String last = "(" + index + "/" + chapters.size() + "章)";
//        max -= headerPaint.measureText(last);
        String str = "" + (currentChapter.getChapterName());
        boolean reduce = false;
        do {
            int sw = (int) headerPaint.measureText(str);
            if (sw > max) {
                str = str.substring(0, str.length() - 1);
                reduce = true;
            } else {
                break;
            }
        } while (true);

        if (reduce) {
            str = str.substring(0, str.length() - 1);
            str = str + "...";
        }

        return str;
    }

    private int getCurrentIndexInChapters() {
        if (null == currentChapter) {
            return -1;
        }

        int size = chapters.size();
        for (int i = size - 1; i > -1; i--) {
            Chapter c = chapters.get(i);
            if (c.getChapterId().equals(currentChapter.getChapterId())) {
                return i;
            }
        }

        return -1;
    }

    /**
     * @return
     */
    private boolean needPreLoad() {
        //解决自动订阅时，预加载导致提前扣用户前一章和后一章的钱
        boolean isVip = currentChapter.isVip();
        boolean needPreload = this.currentChapter != null
                && (this.currentChapter.getStatus() == ReaderConfigWrapper.STATUS_LOADED
                || this.currentChapter.getStatus() == ReaderConfigWrapper.STATUS_LOCK);

        boolean auto_subscribe = ReaderClient.getInstance().getCallBackContainer().getOnAutoSubscriptionCallBack().autoSub(bookid);
        if (isVip && auto_subscribe) {
            return false;
        }
        return needPreload;
    }

    private boolean canPrepareLoadNext() {
        //解决自动订阅时，预加载导致提前扣用户前一章和后一章的钱
        int size = chapters.size();
        int index = getCurrentIndexInChapters();
        if (index < size - 1) {
            Chapter nextChapter = chapters.get(index + 1);
            boolean isVip = nextChapter.isVip();
//            boolean needPreload = nextChapter != null && (nextChapter.getStatus() == ReaderConfig.STATUS_LOADED || nextChapter.getStatus() == ReaderConfig.STATUS_LOCK);
            if (isVip) {
                boolean auto_subscribe = ReaderClient.getInstance().getCallBackContainer().getOnAutoSubscriptionCallBack().autoSub(bookid);
                return !auto_subscribe;
            } else {
                return true;
            }
        } else {
            return false;
        }

    }

    private boolean canPrepareLoadPrevious() {
        //解决自动订阅时，预加载导致提前扣用户前一章和后一章的钱

        int index = getCurrentIndexInChapters();
        if (index > 0) {
            Chapter previousChapter = chapters.get(index - 1);
            boolean isVip = previousChapter.isVip();
//            boolean needPreload = previousChapter != null && (previousChapter.getStatus() == ReaderConfig.STATUS_LOADED || previousChapter.getStatus() == ReaderConfig.STATUS_LOCK);

            if (isVip) {
                boolean auto_subscribe = ReaderClient.getInstance().getCallBackContainer().getOnAutoSubscriptionCallBack().autoSub(bookid);
                return !auto_subscribe;
            } else {
                return true;
            }
        } else {
            return false;
        }


    }

    /**
     * 绘制页眉页脚
     *
     * @param canvas
     * @param scrollY
     */
    private void paintHeaderAndFooter(Canvas canvas, int scrollY) {
        Paint paint = PaintHelper.getInstance().getHeaderPaint();

        float density = ReaderConfigWrapper.getInstance().getScreenDensity();
        int pageHeaderHeight = ReaderConfigWrapper.getInstance().getPageTopBarHeight();
        Paint.FontMetricsInt fontMetrics = paint.getFontMetricsInt();
        int baseline = (pageHeaderHeight - fontMetrics.top - fontMetrics.bottom) / 2;

        int tx1, tx2, ty1, ty2; //页眉
        int topBarHeight = pageHeaderHeight * 3 / 4;
        {
            tx1 = ReaderConfigWrapper.getInstance().getPadding();
            tx2 = getPageWidth() - ReaderConfigWrapper.getInstance().getPadding();
            ty1 = topBarHeight + scrollY;
            ty2 = (int) (ty1 + density);

            //			canvas.drawRect(tx1, ty1, tx2, ty2, paint);
        }

        recordReadStatistics();
        if (null != currentChapter && null != readerview) {
            //			Const.headerPaint.setColor(color);
            {
                //章节名()
                int fontHeight = (int) paint.getTextSize();
                int padding = (topBarHeight - fontHeight) / 2;

                int batteryHeight = pageHeaderHeight / 5;
                int startY = getViewHeight() - pageHeaderHeight;

                {
                    //电池
                    int width = (int) (15 * density);
                    drawBattery(canvas, tx2 - width, baseline + scrollY + startY - batteryHeight, batteryHeight, width, readerview.getBatteryLevel());

                    //时间
                    width += (int) (6 * density);
                    SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
                    String str = sdf.format(new Date());
                    int sw = (int) paint.measureText(str);
                    canvas.drawText(str, tx2 - width - sw, baseline + scrollY + startY, paint);

                    //章节名()
                    int maxw = tx2 - tx1;
                    int starty = getCurrentOffsetY() - currentChapter.getChapterTop();
                    float startPage = (starty * 1.0f / viewHeight);
                    if (startPage <= 0.2) {
                        str = "" + currentChapter.getBookName();
                    } else {
                        str = "" + getChapterTitle(maxw);
                    }
                    canvas.drawText(str, tx1, baseline + scrollY, paint);
                }

                {

                    //中间
                    int totalPages = getCurrChapterTotalPages();
                    int startPage = getCurrPageInChapter(totalPages, ReaderConfigWrapper.CURRENT_PAGE);
                    String str = startPage + "/" + totalPages;
                    currentChapter.setCurrentPageIndex(startPage - 1);
                    int width = (int) paint.measureText(str);
                    //int startx = (bx2 - bx1 - width) / 2;
                    int startx = (ReaderConfigWrapper.getInstance().getPageWidth() - width) / 2;
                    canvas.drawText(str, tx1, baseline + scrollY + startY, paint);
                }

            }
        }

    }

    /**
     * 绘制页眉页脚 --- 水平
     *
     * @param canvas
     * @param pageDir
     */
    synchronized private void paintHeaderAndFooterPage(Canvas canvas, int pageDir) {

        float density = ReaderConfigWrapper.getInstance().getScreenDensity();

        int tx1, tx2, ty1, ty2; //页眉
        Paint headerPaint = PaintHelper.getInstance().getHeaderPaint();
        int topBarHeight = ReaderConfigWrapper.getInstance().getPageTopBarHeight();
        Paint.FontMetricsInt fontMetrics = headerPaint.getFontMetricsInt();
        int baseline = (topBarHeight - fontMetrics.top - fontMetrics.bottom) / 2;
        {
            tx1 = ReaderConfigWrapper.getInstance().getPadding();
            tx2 = getPageWidth() - ReaderConfigWrapper.getInstance().getPadding();
            ty1 = topBarHeight;
            ty2 = (int) (ty1 + density);

            //			canvas.drawRect(tx1, ty1, tx2, ty2, headerPaint);
        }

        int bx1, bx2, by1, by2; //页脚
        Paint footerPaint = PaintHelper.getInstance().getFooterPaint();
        int bottomBarHeight = ReaderConfigWrapper.getInstance().getPageBottomBarHeight();
        {
            bx1 = ReaderConfigWrapper.getInstance().getPadding();
            bx2 = getPageWidth() - ReaderConfigWrapper.getInstance().getPadding();
            by1 = viewHeight - bottomBarHeight;
            by2 = (int) (by1 + density);

            //			canvas.drawRect(bx1, by1, bx2, by2, footerPaint);
        }

        if (null != currentChapter && null != readerview) {
            {
                // header章节名称，第一页显示书名
                int totalPages = getCurrChapterTotalPages();
                int startPage = getCurrPageInChapter(totalPages, pageDir);

                // 章节名
                int maxw = tx2 - tx1;
                String str;
                if (startPage == 1) {
                    str = "" + currentChapter.getBookName();
                } else {
                    str = "" + getChapterTitle(maxw);
                }

                canvas.drawText(str, tx1, baseline, headerPaint);
            }

            if (getCurrentChapter().getStatus() != ReaderConfigWrapper.STATUS_LOCK) {
                int fontHeight = (int) footerPaint.getTextSize();
                int padding = (bottomBarHeight - fontHeight) / 2;
                int startY = getViewHeight() - padding;
                // footer 页码-时间-电量
                {
                    // 页码
                    int totalPages = getCurrChapterTotalPages();
                    int startPage = getCurrPageInChapter(totalPages, pageDir);
                    currentChapter.setCurrentPageIndex(startPage - 1);
//                    if (startPage == totalPages) {
//                        return;
//                    }
                    String str = startPage + "/" + totalPages;
                    int width = (int) footerPaint.measureText(str);
                    //int startx = (bx2 - bx1 - width) / 2;
                    int startx = (ReaderConfigWrapper.getInstance().getPageWidth() - width) / 2;
                    canvas.drawText(str, tx1, startY, footerPaint);
                }

                {
                    // 电池
                    int batteryHeight = topBarHeight / 5;
                    int width = (int) (15 * density);
                    drawBattery(canvas, tx2 - width, startY - batteryHeight, batteryHeight, width, readerview.getBatteryLevel());

                    // 时间
                    width += (int) (6 * density);
                    SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
                    String str = sdf.format(new Date());
                    int sw = (int) headerPaint.measureText(str);
                    canvas.drawText(str, tx2 - width - sw, startY, headerPaint);
                }
            }
        }
    }

    public int getCurrPageInChapter() {
        return getCurrPageInChapter(getCurrChapterTotalPages(), ReaderConfigWrapper.CURRENT_PAGE);
    }

    public int getCurrPageInChapter(int totalPages, int pageDir) {
        if (currentChapter != null) {
            int starty = getCurrentOffsetY() - currentChapter.getChapterTop() + pageDir * viewHeight;
            int startPage = (starty / viewHeight) + 1;
            if (startPage < 1) {
                startPage = 1;
            } else if (startPage > totalPages) {
                startPage = totalPages;
            }
            return startPage;
        }
        return 1;
    }

    public int getCurrChapterTotalPages() {
        if (currentChapter != null) {
            int totalPages = currentChapter.getChapterHeight() / viewHeight;
            return Math.max(totalPages, 1);
        }
        return 1;
    }

    private void recordReadStatistics() {
        if (this.recordChapter == null || !this.recordChapter.getChapterId().equals(this.currentChapter.getChapterId())) {
            this.recordChapter = this.currentChapter;
            LogUtils.d("test changed: recordReadStatistics");
            ReaderClient.getInstance().getCallBackContainer().getOnAppPvCallBack().appPv();
        }
    }

    /**
     * 章节高度发生变化(章节高度发生变化的原因可能是load、或者是unload)
     *
     * @param chapter
     */
    synchronized public void chapterHeightChanged(Chapter chapter, int oldHeight) {
        //Log.e("Article:chapterHeightChanged", "章节高度改变：" + chapter + "\n当前章节: " + currentChapter);
        if (chapter.getChapterHeight() == oldHeight) { //高度不变，什么也不管
            return;
        }

        articleHeight = 0;
        int size = null == chapters ? 0 : chapters.size();

        int pageHeaderHeight = ReaderConfigWrapper.getInstance().getPageTopBarHeight();
        int pageFooterHeight = ReaderConfigWrapper.getInstance().getPageBottomBarHeight();

        if (isVerticalScrollMode()) { //单页和多页完全不同了
            articleHeight += pageHeaderHeight;
            for (int i = 0; i < size; i++) {
                Chapter c = chapters.get(i);
                c.setChapterTop(articleHeight);
                articleHeight += c.getChapterHeight() + ReaderConfigWrapper.getInstance().getVerticalChapterMargin();
            }
            articleHeight += pageFooterHeight;
        } else {
            for (int i = 0; i < size; i++) {
                Chapter c = chapters.get(i);
                c.setChapterTop(articleHeight);
                articleHeight += c.getChapterHeight();
            }
        }

        int disHeight = 0;
        int offsety = getCurrentOffsetY();
        //if (null != currentChapter && chapterHelper.compare(chapter, currentChapter) < 0) {
        if (offsety > chapter.getChapterTop()) { //如果进度条比当前进度条还远，说明加载的是之前的章节
            disHeight = chapter.getChapterHeight() - oldHeight;
        }

        offsety += disHeight;
        readerview.articleHeightChanged(articleHeight); //文章的高度发生的变化
        if (disHeight > 0) { //仅高度发生变化时才需要重置
            //Log.e("Article:chapterHeightChanged", "原位置：" + getCurrentOffsetY() + ", 新位置：" + offsety);
            readerview.setOffsetY(offsety);
        }
        //readerview.setOffsetY(offsety);
    }

    public void freeMemory() {
        if (this.chapters != null) {
            final Iterator<Chapter> iterator = this.chapters.iterator();
            while (iterator.hasNext()) {
                iterator.next().unload(false);
            }
        }
        this.chapters = null;
        this.chapterHelper = null;
    }

    public int getArticleHeight() {
        return this.articleHeight;
    }

    public IChapterHelper getChapterHelper() {
        return this.chapterHelper;
    }

    public Context getContext() {
        return this.readerview.getContext();
    }

    public Chapter getCurrentChapter() {
        return this.currentChapter;
    }

    public int getCurrentOffsetY() {
        if (isVerticalScrollMode()) {
            return readerview.getScrollY();
        }
        return yOffset;
    }

    public int getId() {
        return this.bookid;
    }

    public int getPageCount() {
        return this.totalPage;
    }

    public int getPageWidth() {
        return this.readerview.getWidth();
    }

    /**
     * 获得当前正在阅读的章节的进度
     *
     * @return
     */
    public int getProgress() {
        if (null != currentChapter) {
            synchronized (currentChapter) {
                int offset = getCurrentOffsetY() - currentChapter.getChapterTop();
                if (offset < 0) {
//                    Logger.i("Article:getProgress", "(offset < 0) --> " + getCurrentOffsetY());
                    return 0;
                }
                int progress = -1;

                if (isVerticalScrollMode()) {
                    if (ReaderClient.getInstance().getCallBackContainer().getOnExitAutoReadCallBack().isExitAutoMode(readerview)) {
                        int i = getViewHeight() / 2;
                        //解决第一页开启自动阅读后，退出自动阅读，进度不准的问题
                        offset = Math.max(offset - i, 0);
                        ReaderClient.getInstance().getCallBackContainer().getOnExitAutoReadCallBack().setExitAutoMode(readerview, false);
                    }
//                    Activity activity = readerview.getActivity();
//                    if (activity instanceof ReaderMainActivity){
//                        boolean exitAutoMode = ((ReaderMainActivity) activity).isExitAutoMode();
//                        if (exitAutoMode){
//                            int i = getViewHeight() / 2;
//                            if (offset - i > 0){
//                                offset = offset - i ;
//                            }else {
//                                offset = 0;//解决第一页开启自动阅读后，退出自动阅读，进度不准的问题
//                            }
//                            ((ReaderMainActivity) activity).setExitAutoMode(false);
//                        }
//                    }

                }
                if (currentChapter.getChapterHeight() != 0) {
                    progress = (offset * 100) / currentChapter.getChapterHeight();
                }
//                Logger.i("Article:getProgress", "保存进度：" + currentChapter + ", offset: " + offset + "(" + getCurrentOffsetY() + ")" + ", " + progress + "%");
                defid = currentChapter.getChapterId();

                return progress;
            }
        } else { //可能是正在显示尾页
            return 0;
        }

    }

    public ReaderView getView() {
        return this.readerview;
    }

    public int getViewHeight() {
        return this.viewHeight;
    }

    public boolean isFirstPage() {
        return this.yOffset <= 0;
    }

    public boolean isLastPage() {
        return this.yOffset + this.viewHeight >= this.articleHeight;
    }

    public boolean isVerticalScrollMode() {
        int pageOrientation = TangYuanSharedPrefUtils.getInstance().getReaderPageOrientation(ReaderConfigWrapper.PAGE_ORIENTATION_HORIZONTAL);
        return pageOrientation == ReaderConfigWrapper.PAGE_ORIENTATION_VERTICAL;
    }

    public void jumpToChapter(String chapterId) {
        if (null == chapters || chapters.size() < 1) {
            return;
        }

        Chapter find = null;
        for (int i = 0; null == find && null != chapters && i < chapters.size(); i++) {
            Chapter c = chapters.get(i);
            if (c.getChapterId().equals(chapterId)) {
                find = c;
                break;
            }
        }

        if (null != find && null != readerview && null != chapterHelper) {
//            Logger.i("Article:jumpToChapter", "跳转：" + find);
            getView().setOffsetY(find.getChapterTop());
//            currentChapter = find;
            setCurrentChapter(find);
            chapterHelper.loadChapter(find, null, 0);
        }
    }

    private void setCurrentChapter(Chapter chapter) {
        boolean hasChapterChanged = chapter != null && !chapter.equals(currentChapter);
        currentChapter = chapter;
        if (hasChapterChanged) {
            readerview.chapterChanged();
        }
    }

    public void jumpToOffset(final int yOffset) {
        this.yOffset = yOffset;
    }


    /**
     * 加载时只是简单的从数据库中加载章节的ID和标题信息，其它的一概不加载
     */
    synchronized public void load(final String chapterId, final int percent) {
        if (chaptersInfoStatus == ReaderConfigWrapper.STATUS_INIT) {
            chaptersInfoStatus = ReaderConfigWrapper.STATUS_LOADING;
        }
        int size = null == chapters ? 0 : chapters.size();
        articleHeight = 0;

        int pageHeaderHeight = ReaderConfigWrapper.getInstance().getPageTopBarHeight();
        int pageFooterHeight = ReaderConfigWrapper.getInstance().getPageBottomBarHeight();

        if (isVerticalScrollMode()) { //单页和多页完全不同了, 2016.3.1
            // 页内容高度
            int contentHeight = viewHeight - pageHeaderHeight - pageFooterHeight;

            articleHeight += pageHeaderHeight;
            for (int i = 0; i < size; i++) {
                Chapter c = chapters.get(i);
                c.setArticle(Article.this, chapterHelper);
                c.setChapterTop(articleHeight);
                c.setChapterHeight(contentHeight); //未加载任何内容时，默认每个章节一页
                articleHeight += c.getChapterHeight() + ReaderConfigWrapper.getInstance().getVerticalChapterMargin();
            }
            articleHeight += pageFooterHeight;
        } else {
            for (int i = 0; i < size; i++) {
                Chapter c = chapters.get(i);
                c.setArticle(Article.this, chapterHelper);
                c.setChapterTop(articleHeight);
                c.setChapterHeight(viewHeight); //未加载任何内容时，默认每个章节一页
                articleHeight += c.getChapterHeight();
            }
        }
        readerview.articleHeightChanged(articleHeight);
        chaptersInfoStatus = ReaderConfigWrapper.STATUS_LOADED;
        loadDefaultChapter(chapterId, percent);
    }

    /**
     * @param defid
     * @param percent =0的时候，表示回到章节首
     */
    protected void loadDefaultChapter(String defid, int percent) {
        if (null == chapters || chapters.size() < 1) {
            return;
        }
        int p = -1;
        Chapter find = null; //默认显示第一章节
        for (int i = 0; null != chapters && i < chapters.size(); i++) {
            Chapter c = chapters.get(i);
            if (c.getChapterId().equals(defid)) {
                find = c;
                p = percent;
                break;
            } else {
                //c.loadChapter();
            }
        }

        if (null == find) { //新书
            find = chapters.get(0);
            p = 0;
        }

        if (null != find && null != readerview && null != chapterHelper) {
            // Logger.i("Article:loadDefaultChapter", "打开：" + find + ", 进度：" + p);
            /*if (p != 0) {
                chapterHelper.loadChapter(find, null, p);
			} else {
				readerview.setOffsetY(find.getChapterTop());
			}*/
//            currentChapter = find;
            setCurrentChapter(find);
            ReaderClient.getInstance().getCallBackContainer().getOnChapterLoadResult().chapterTitle(readerview, currentChapter.getChapterName());
//            if (readerview.getActivity() instanceof ReaderMainActivity){
//                ((ReaderMainActivity) readerview.getActivity()).setTopBarChapterName(currentChapter.getChapterName());
//            }
            recordReadStatistics();
            chapterHelper.loadChapter(find, null, p);
        }
    }

    public void loadNextChapter() {
//    	   Logger.i("loadNextChapter()");

        if (chaptersInfoStatus == ReaderConfigWrapper.STATUS_INIT
                || chaptersInfoStatus == ReaderConfigWrapper.STATUS_LOADING
                || null == currentChapter) {
            return;
        }
        recordChapter = null;
        totalPage = 0;
        articleHeight = 0;
        chaptersInfoStatus = ReaderConfigWrapper.STATUS_INIT;
        yOffset = 0;
        setCanDraw(false);
        if (chapters != null) {
            for (Chapter chapter : chapters) {
                chapter.unload(false);
            }
        }
        readerview.setScrollY(0);
        Chapter nextChapter = getNextChapter();
        if (nextChapter != null) {
            load(nextChapter.getChapterId(), 0);
        }
    }

    /**
     * 加载下一章节，不管是否可见
     *
     * @param currentChapter
     * @return false表示不需要预加载
     */
    public boolean loadNextChapter(Chapter currentChapter) {
        if (!canPrepareLoadNext()) {
            return false;
        }
//        Chapter nextChapter = getNextChapter();
//        if(user_auto_subscript && nextChapter!= null && nextChapter.getSubscript_flag() == 1 && nextChapter.getUser_subscript_flag() == 0){
//            return false;
//        }
        int size = chapters.size();
        int index = getCurrentIndexInChapters();
        if (index < size - 1) {
            Chapter c = chapters.get(index + 1);
            if (c.getStatus() == ReaderConfigWrapper.STATUS_INIT) {
                //Log.e("Article:loadNextChapter()", "加载下一章节, 当前: " + currentChapter);
                chapterHelper.loadChapter(c, currentChapter, -1);
                return true;
            } else {
                return false;
            }
        }

        return false;
    }

    public void loadPreviousChapter() {

        if (chaptersInfoStatus == ReaderConfigWrapper.STATUS_INIT
                || chaptersInfoStatus == ReaderConfigWrapper.STATUS_LOADING
                || null == currentChapter) {
            return;
        }
        recordChapter = null;
        totalPage = 0;
        articleHeight = 0;
        chaptersInfoStatus = ReaderConfigWrapper.STATUS_INIT;
        yOffset = 0;
        setCanDraw(false);
        if (chapters != null) {
            for (Chapter chapter : chapters) {
                chapter.unload(false);
            }
        }
        readerview.setScrollY(0);
        Chapter previousChapter = getPreviousChapter();
        if (previousChapter != null) {
            load(previousChapter.getChapterId(), 0);
        }
    }

    /**
     * 加载上一章节，不管是否可见
     *
     * @param currentChapter
     * @return false表示不需要预加载
     */
    public boolean loadPreviousChapter(Chapter currentChapter) {
        if (!canPrepareLoadPrevious()) {
            return false;
        }
//        Chapter previoutChapter = getPreviousChapter();
//        if(user_auto_subscript && previoutChapter!= null && previoutChapter.getSubscript_flag() == 1 && previoutChapter.getUser_subscript_flag() == 0){
//            return false;
//        }
        int index = getCurrentIndexInChapters();
        if (index > 0) {
            Chapter c = chapters.get(index - 1);
            if (c.getStatus() == ReaderConfigWrapper.STATUS_INIT) {
                //Log.e("Article:loadPreviousChapter()", "加载上一章节, 当前: " + currentChapter);
                chapterHelper.loadChapter(c, currentChapter, -1);
                return true;
            } else {
                return false;
            }
        }

        return false;
    }

    public boolean onClick(int x, int y) {
        if (canDraw && null != chapters) {
            if (isVerticalScrollMode()) {

                boolean clickPreviousChapter = isClickPreviousChapter();
                if (clickPreviousChapter) {
                    Chapter previousChapter = getPreviousChapter();
                    if (previousChapter != null) {
                        if (previousChapter.onClick(x, y, currentChapter)) {
                            return true;
                        }
                    }
                } else {
                    if (currentChapter.onClick(x, y, currentChapter)) {
                        return true;
                    }
                }
            } else {
                int size = chapters.size();
                for (int i = 0; i < size; i++) {
                    Chapter chapter = chapters.get(i);
                    if (chapter.onClick(x, y, currentChapter)) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    synchronized public void onSizeChanged(int w, int h) {
        //Log.e("Article:onSizeChanged", "屏幕尺寸改变(" + w + ", " + h + ")");
        boolean relayout = viewHeight != h; //只有宽度改变才要求重排
        int ow = this.viewWidth;
        int oh = this.viewHeight;
        this.viewWidth = w;
        this.viewHeight = h;
        if (relayout) {
            if (null != readerview
                    && readerview instanceof PaperView) {
                ((PaperView) readerview).initCanvas(w, h);
                readerview.reload(true, 0);
            }

            //Log.e("Article:onSizeChanged", "屏幕尺寸改变(" + ow + ", " + oh + ") --> (" + w + ", " + h + ")");
            //reload();
        }
    }

    public void pageTruned(int dir) {
        yOffset += dir * viewHeight;
        yOffset = Math.max(yOffset, 0);
    }

    /**
     * 垂直
     *
     * @param canvas
     * @param scrollY
     */
    public void paint(Canvas canvas, int scrollY) {
        try {

            canvas.drawColor(ReaderConfigWrapper.getInstance().getPageBgColor());

            Paint paint = PaintHelper.getInstance().getContentPaint();

            if (null == chapters || chapters.size() == 0) { //没有章节，假设挂了
//                canvas.drawText("没有章节信息^_^", 50, 100, paint);
                return;
            }
            int pageHeaderHeight = ReaderConfigWrapper.getInstance().getPageTopBarHeight();
            int bottomBarHeight = ReaderConfigWrapper.getInstance().getPageBottomBarHeight();

            canvas.save();
            canvas.clipRect(0, scrollY + pageHeaderHeight, getPageWidth(), scrollY + getViewHeight() - bottomBarHeight);

            int size = chapters.size();
            int[] bgcolor = {0xFFFFFFCC, 0xFFFFCCFF, 0xFFCCFFFF};
            Chapter firstChapter = null;
            for (int i = 0; i < size; i++) {
                if (i == 0) {
                    firstChapter = chapters.get(i);
                }
                Chapter c = chapters.get(i);
                if (c.getChapterTop() + c.getChapterHeight() <= scrollY) { //上方不可见

                } else if (c.getChapterTop() >= scrollY + getViewHeight() - bottomBarHeight) { //下方不可见

                } else {
                    if (currentChapter.getChapterId().equals(firstChapter.getChapterId()) || i != 0 || canDraw) {
                        c.paint(canvas, scrollY, bgcolor[i % bgcolor.length]);
                        setCanDraw(true);
//                        currentChapter = c;
                        setCurrentChapter(c);
                    } else {
                        currentChapter.paint(canvas, currentChapter.getChapterTop(), bgcolor[i % bgcolor.length], true);
                    }
                }
            }
            canvas.restore();
            paintHeaderAndFooter(canvas, scrollY);

            if (!loadPreviousChapter(currentChapter)) {
                loadNextChapter(currentChapter);
            }
        } catch (Exception ex) {

        }
    }

    /**
     * 左右
     *
     * @param canvas
     * @param pageDir 绘制当前页值为0，下一页值为1，上一页值为-1
     * @param first
     */
    public void paintPage(Canvas canvas, int pageDir, boolean first) {
        try {
            //Log.e("Article:paintPage", (first ? "上层页" : "底层页") + ", 方向 = " + pageDir + ", " + (yOffset + pageDir * getViewHeight()));
            // Logger.i("HINT", "Article ~ paintPage ~ canvas is Null ? ---->" + (canvas == null));
            // Logger.i("HINT", "Article ~ paintPage ~ ReaderConfig is Null ? ---->" + (ReaderConfig.getInstance() == null));

            if (canvas == null) {
                return;
            }

            canvas.drawColor(ReaderConfigWrapper.getInstance().getPageBgColor());

            int scrolly = yOffset + pageDir * getViewHeight();

            Paint paint = PaintHelper.getInstance().getContentPaint();

            float density = ReaderConfigWrapper.getInstance().getScreenDensity();

            if (null == chapters || chapters.size() == 0) { //没有章节，假设挂了
//                canvas.drawText("没有章节信息^_^", 50, 20 * density, paint);
                return;
            }

            if (chaptersInfoStatus == ReaderConfigWrapper.STATUS_INIT) {
                canvas.drawText("正在加载……", 50, 20 * density, paint);
                return;
            } else if (chaptersInfoStatus == ReaderConfigWrapper.STATUS_LOADING) {
                canvas.drawText("正在加载……", 50, 20 * density, paint);
                return;
            } else if (chaptersInfoStatus == ReaderConfigWrapper.STATUS_FAILED) {
                canvas.drawText("加载失败", 50, 20 * density, paint);
                return;
            }


            int size = chapters.size();
            int[] bgcolor = {0xFFFFFFCC, 0xFFFFCCFF, 0xFFCCFFFF};
            Chapter firstChapter = null;
            for (int i = 0; i < size; i++) {
                if (i == 0) {
                    firstChapter = chapters.get(i);
                }
                Chapter c = chapters.get(i); //对于章节来说，位置偏移总是0
                if (c.getChapterTop() + c.getChapterHeight() <= scrolly) { //上方不可见

                } else if (c.getChapterTop() >= scrolly + getViewHeight()) { //下方不可见

                } else {
                    if (currentChapter.getChapterId().equals(firstChapter.getChapterId()) || i != 0 || canDraw) {
                        c.paintPage(canvas, scrolly, bgcolor[i % bgcolor.length], pageDir);
//                        currentChapter = c;
                        setCurrentChapter(c);
                        setCanDraw(true);
                        recordReadStatistics();
                    } else {
                        currentChapter.paintPage(canvas, scrolly, bgcolor[i % bgcolor.length], true, pageDir);
                    }
                    break;
                }
            }
            paintHeaderAndFooterPage(canvas, pageDir);
            if (!loadPreviousChapter(currentChapter)) {
                loadNextChapter(currentChapter);
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void reload() {
        //null == currentChapter表示刚打开，还未来得及刷新，因此可以不重新加载 by zhangjian, 2016.4.18
        if (chaptersInfoStatus == ReaderConfigWrapper.STATUS_INIT
                || chaptersInfoStatus == ReaderConfigWrapper.STATUS_LOADING
                || null == currentChapter) {
            return;
        }


        //Log.e("Article:reload", "重新加载, " + currentChapter);
        final int p = getProgress();
//        Logger.i("Article:reload", "重新加载, 章节ID：" + defid + ", " + p + "%");

        recordChapter = null;
        totalPage = 0;
        articleHeight = 0;
        chaptersInfoStatus = ReaderConfigWrapper.STATUS_INIT;
        yOffset = 0;
        setCanDraw(false);
        if (chapters != null) {
            for (Chapter chapter : chapters) {
                chapter.unload(false);
            }
        }
        readerview.setScrollY(0);
        load(currentChapter.getChapterId(), p);
    }

    public void reloadChapters(int bookid, ArrayList<Chapter> newchapters) {
        if (this.bookid == bookid && chapters != null) {
            synchronized (chapters) {
                int size = null == newchapters ? 0 : newchapters.size();
                articleHeight = 0;

                int pageHeaderHeight = ReaderConfigWrapper.getInstance().getPageTopBarHeight();
                int pageFooterHeight = ReaderConfigWrapper.getInstance().getPageBottomBarHeight();

                if (isVerticalScrollMode()) { //单页和多页完全不同了, 2016.3.1
                    // 页内容高度
                    int contentHeight = viewHeight - pageHeaderHeight - pageFooterHeight;

                    articleHeight += pageHeaderHeight;
                    for (int i = 0; i < size; i++) {
                        Chapter c = newchapters.get(i);
                        if (chapterIdMap.containsKey(c.getChapterId())) {
                            Integer oldindex = chapterIdMap.get(c.getChapterId());
                            c = chapters.get(oldindex.intValue());
                            c.setChapterTop(articleHeight);
                        } else {
                            c.setArticle(Article.this, chapterHelper);
                            c.setChapterTop(articleHeight);
                            c.setChapterHeight(contentHeight); //未加载任何内容时，默认每个章节一页
                        }
                        newchapters.set(i, c);
                        articleHeight += c.getChapterHeight() + ReaderConfigWrapper.getInstance().getVerticalChapterMargin();
                    }
                    articleHeight += pageFooterHeight;
                } else {
                    for (int i = 0; i < size; i++) {
                        Chapter c = newchapters.get(i);
                        if (chapterIdMap.containsKey(c.getChapterId())) {
                            Integer oldindex = chapterIdMap.get(c.getChapterId());
                            c = chapters.get(oldindex.intValue());
                            c.setChapterTop(articleHeight);
                        } else {
                            c.setArticle(Article.this, chapterHelper);
                            c.setChapterTop(articleHeight);
                            c.setChapterHeight(viewHeight); //未加载任何内容时，默认每个章节一页
                        }
                        newchapters.set(i, c);
                        articleHeight += c.getChapterHeight();
                    }
                }
                chapters = newchapters;
                readerview.articleHeightChanged(articleHeight);

            }
        }
    }

    public void requestRepaint(Chapter chapter, String hint) {
        if (chapter == currentChapter) {
//            Logger.i("Article:requestRepaint", "可视章节引发的重绘: " + chapter + ", 当前位置: " + getCurrentOffsetY());
            readerview.requestRepaint(hint);
        }
    }

    public void requestRepaint(String hint) {
        readerview.requestRepaint(hint);
    }


    //获取当前章节的上一章节
    public Chapter getPreviousChapter() {
        if (currentChapter != null && chapters != null) {
            int currentChapterIndex = chapterIdMap.get(currentChapter.getChapterId()) == null ? 0 : chapterIdMap.get(currentChapter.getChapterId());
            int previoutChapterIndex = currentChapterIndex - 1;
            if (previoutChapterIndex >= 0 && previoutChapterIndex < chapters.size()) {
                return chapters.get(previoutChapterIndex);
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    //获取当前章节的下一章节
    public Chapter getNextChapter() {
        if (currentChapter != null && chapters != null) {
            int currentChapterIndex = chapterIdMap.get(currentChapter.getChapterId()) == null ? 0 : chapterIdMap.get(currentChapter.getChapterId());
            int nextChapterIndex = currentChapterIndex + 1;
            if (nextChapterIndex < chapters.size()) {
                return chapters.get(nextChapterIndex);
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    public boolean isClickPreviousChapter() {
        return isClickPreviousChapter;
    }

    public void setClickPreviousChapter(boolean clickPreviousChapter) {
        isClickPreviousChapter = clickPreviousChapter;
    }

    public void setCanDraw(boolean canDraw) {
        this.canDraw = canDraw;
//        printStackTraces();
    }

    private void printStackTraces() {
        Map<Thread, StackTraceElement[]> map = Thread.getAllStackTraces();
        StackTraceElement[] stackTraceElements = map.get(Thread.currentThread());
        StringBuilder stringBuilder = new StringBuilder();
        for (StackTraceElement element : stackTraceElements) {
            stringBuilder.append(element.toString()).append("\n");
        }
        LogUtils.d("" + stringBuilder.toString());
    }
}
