package com.chineseall.reader.lib.reader.entities;

import android.app.Activity;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.PointF;
import android.graphics.RectF;
import android.os.Looper;
import android.util.SparseArray;
import android.view.View.MeasureSpec;
import android.view.ViewGroup;

import com.chineseall.reader.lib.reader.config.IReadConfig;
import com.chineseall.reader.lib.reader.config.ReaderConfigWrapper;
import com.chineseall.reader.lib.reader.config.TangYuanSharedPrefUtils;
import com.chineseall.reader.lib.reader.core.Constants;
import com.chineseall.reader.lib.reader.core.ReaderClient;
import com.chineseall.reader.lib.reader.utils.BreakLinesUtil;
import com.chineseall.reader.lib.reader.utils.DensityUtil;
import com.chineseall.reader.lib.reader.utils.IChapterHelper;
import com.chineseall.reader.lib.reader.utils.PaintHelper;
import com.chineseall.reader.lib.reader.utils.StringUtil;
import com.chineseall.reader.lib.reader.view.ILockView;
import com.chineseall.reader.lib.reader.view.vertical.YViewBiz;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static com.chineseall.reader.lib.reader.config.IReadConfig.AD_VIEW_HEIGHT;

public class Chapter extends ReadChapter implements Serializable {
    private static final long serialVersionUID = -2570752691484007943L;
    private int MaxLinesWhenLock;
    private Article article;
    //    private List<LineBlock> blocks;
    private List<Paragraph> mParagraphs;
    private List<PageData> mPageDataList = new ArrayList<PageData>();
    private Paragraph mSelectedParagraph = null;
    private int currentPageIndex;
    private int chapterHeight;
    private IChapterHelper chapterHelper;
    private int chapterTop;
    public RectF reloadRectF;
    private int lineNumber;
    private long offset;
    private boolean is_jingpin;
    private String bookName;
    private String novelSecondType;
    private String novelFirstType;
    private String is_vip;
    private boolean chapterPayType;
    private boolean bookVip;
    private int chaptPrice;
    private int code;
    private int vedioCount;
    private int vedioSumCount;
    private int adPositionY;
    private SparseArray<Integer> adViewKeyList = new SparseArray<>();

    public SparseArray<Integer> getAdViewKeyList() {
        return adViewKeyList;
    }

    public void setAdViewKeyList(SparseArray<Integer> adViewKeyList) {
        this.adViewKeyList = adViewKeyList;
    }

    public void clearAdViewKeyList() {
        adViewKeyList.clear();
    }

    public List<Paragraph> getParagraphs() {
        return mParagraphs;
    }

    public int getVedioSumCount() {
        return vedioSumCount;
    }

    public void setVedioSumCount(int vedioSumCount) {
        this.vedioSumCount = vedioSumCount;
    }

    public int getVedioCount() {
        return vedioCount;
    }

    public void setVedioCount(int vedioCount) {
        this.vedioCount = vedioCount;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    private int index;

    public int type;
    public int ori_price;
    public int cur_price;
    public int old_month_price;
    public int new_month_price;

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getOri_price() {
        return ori_price;
    }

    public void setOri_price(int ori_price) {
        this.ori_price = ori_price;
    }

    public int getCur_price() {
        return cur_price;
    }

    public void setCur_price(int cur_price) {
        this.cur_price = cur_price;
    }

    public int getOld_month_price() {
        return old_month_price;
    }

    public void setOld_month_price(int old_month_price) {
        this.old_month_price = old_month_price;
    }

    public int getNew_month_price() {
        return new_month_price;
    }

    public void setNew_month_price(int new_month_price) {
        this.new_month_price = new_month_price;
    }

    public int getCurrentPageIndex() {
        return currentPageIndex;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    private int adSize;

    public boolean is_jingpin() {
        return is_jingpin;
    }

    public void setIs_jingpin(boolean is_jingpin) {
        this.is_jingpin = is_jingpin;
    }

    public long getOffset() {
        return offset;
    }

    public void setOffset(long offset) {
        this.offset = offset;
    }

    public int getLineNumber() {
        return lineNumber;
    }

    public void setLineNumber(int lineNumber) {
        this.lineNumber = lineNumber;
    }

    private int status;

    public boolean isLocal() {
        return isLocal;
    }

    public void setLocal(boolean local) {
        isLocal = local;
    }

    private boolean isLocal;
    private String filePath;

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    private boolean isVip;

    public long getFreeEndTime() {
        return freeEndTime;
    }

    public void setFreeEndTime(long freeEndTime) {
        this.freeEndTime = freeEndTime;
    }

    private long freeEndTime;


    public boolean isVip() {
        if (ori_price > 0) {
            return isVip || (freeEndTime >= 0 ? freeEndTime < System.currentTimeMillis() : freeEndTime != -1);
        } else {
            return isVip;
        }
    }

    public void setVip(boolean vip) {
        isVip = vip;
    }

    public Chapter() {
        this.status = ReaderConfigWrapper.STATUS_INIT;
        this.mParagraphs = null;
        this.MaxLinesWhenLock = 15;
        this.chapterTop = 0;
        this.chapterHeight = 0;
    }

    private void drawLockView(Canvas canvas) {
        try {
            setLockViewCanDraw(true);
            ILockView lockView = this.article.getView().getLockView();
            Activity activity = this.article.getView().getActivity();

            ReaderClient.getInstance().getCallBackContainer().getOnLockViewDrawCallBack().draw(article.getView(), lockView, this);
//            if (activity instanceof ReaderMainActivity) {
            // TODO: 2020/4/15  asd
//                ReaderMainActivity readerMainActivity = (ReaderMainActivity) activity;
//                BookDirectoryList bookDirectoryList = readerMainActivity.getBookDirectoryList();
//                lockView.setData(bookDirectoryList, this);
//            }

            if (lockView instanceof ViewGroup) {
                ((ViewGroup) lockView).measure(MeasureSpec.makeMeasureSpec(this.article.getView().getWidth(), MeasureSpec.EXACTLY), MeasureSpec.makeMeasureSpec(this.article.getView().getHeight(), MeasureSpec.EXACTLY));
                ((ViewGroup) lockView).layout(this.article.getView().getLeft(), this.article.getView().getTop(), this.article.getView().getRight(), this.article.getView().getBottom());
            }
            if (canvas != null) {
                canvas.save();
                if (this.article.getView() instanceof YViewBiz) {
                    canvas.translate(0.0f, (float) this.chapterTop);
                }
                if (lockView instanceof ViewGroup) {
                    ((ViewGroup) lockView).draw(canvas);
                }
                canvas.restore();
            }
        } catch (Exception e) {
//            Logger.e(e);
        }
    }

    private void drawMessage(Canvas canvas, String text, boolean border) {
        drawMessage(canvas, text, border, false);
    }

    private void drawMessage(Canvas canvas, String text, boolean border, boolean force) {
        Paint paint = new Paint(PaintHelper.getInstance().getContentPaint());
        paint.setTextSize((float) ReaderConfigWrapper.getInstance().getTextSize());
        float fontheight = paint.getTextSize();
        float y = (((float) (force ? 0 : this.chapterTop)) + ((((float) this.chapterHeight) - fontheight) / 2.0f)) + fontheight;
        if (!this.chapterHelper.isVerticalScrollMode()) {
            y = ((((float) this.chapterHeight) - fontheight) / 2.0f) + fontheight;
        }
        float w = paint.measureText(text);

        float x = (((float) this.article.getPageWidth()) - w) / 2.0f;
        float density = ReaderConfigWrapper.getInstance().getScreenDensity();
        if (border) {
            float p1 = 30.0f * density;
            float p2 = 15.0f * density;
            paint.setColor(ReaderConfigWrapper.getInstance().getTextColor());
            paint.setStyle(Style.STROKE);
            this.reloadRectF = new RectF(x - p1, (y - p2) - fontheight, (w + x) + p1, y + p2);
            canvas.drawRoundRect(this.reloadRectF, 5.0f * density, density * 5.0f, paint);
            paint.setStyle(Style.FILL_AND_STROKE);
        }
        int ceil = (int) paint.measureText("啊");
        List<String> breakLines = BreakLinesUtil.breakLines2(paint, text, this.article.getPageWidth() - (ReaderConfigWrapper.getInstance().getPadding() * 2), false);
        int size = breakLines.size();
        for (int i = 0; i < size; i++) {

            String tmp = breakLines.get(i);
            w = paint.measureText(tmp);

            x = (((float) this.article.getPageWidth()) - w) / 2.0f;
            canvas.drawText(tmp, x, y + i * (ceil + 10), paint);

        }
    }

    // 状态 1初始化   2加载完成  3正在加载  4 加载失败  5加锁章节
    private String statusStr(int status) {
        switch (status) {
            case ReaderConfigWrapper.STATUS_INIT:
                return "init";
            case ReaderConfigWrapper.STATUS_LOADED:
                return "加载完成";
            case ReaderConfigWrapper.STATUS_LOADING:
                return "正在加载";
            case ReaderConfigWrapper.STATUS_LOCK:
                return "文章上锁";
            case ReaderConfigWrapper.STATUS_FAILED:
            default:
                return "加载失败";
        }
    }

    public int getChapterHeight() {
        return this.chapterHeight;
    }

    public int getChapterTop() {
        return this.chapterTop;
    }

    public int getStatus() {
        return this.status;// 状态 1初始化   2加载完成  3正在加载  4 加载失败  5加锁章节
    }


    public void setBookName(String str) {
        this.bookName = str;
    }

    public void setCurrentPageIndex(int currentPageIndex) {
        this.currentPageIndex = currentPageIndex;
    }

    public List<PageData> getPageDataList() {
        return mPageDataList;
    }

    /**
     * 以多页的方式排版
     *
     * @param paragraphs
     * @param pageHeight
     * @param firstChapterInSight
     * @param progress            顺便跳转到位置 0表示不用处理
     */
    synchronized public int morePagesLayout(List<Paragraph> paragraphs, int pageWidth, int pageHeight, Chapter firstChapterInSight, int progress, int status) {
        /** 页头高度、内容高度、页脚高度 */
        int headerHeight, contentHeight, footerHeight, bottomHeight;
        headerHeight = ReaderConfigWrapper.getInstance().getPageTopBarHeight();
        footerHeight = ReaderConfigWrapper.getInstance().getPageBottomBarHeight();
        contentHeight = pageHeight - headerHeight - footerHeight;
        bottomHeight = pageHeight - headerHeight;

        mPageDataList.clear();

        PageData pageData = new PageData();

        int oldHeight = getChapterHeight();
        int x = (int) (ReaderConfigWrapper.getInstance().getPadding() + Constants.OFFSET_STARTX);
        int pageStartY = 0;
        int pageOffsetY = ReaderConfigWrapper.getInstance().getPageTopBarHeight();
        int index = 0;

        int lastAdPageIndex = -1;
        boolean insertAdInThisPage = false;

        adPositionY = (DensityUtil.getScreenHeight(ReaderClient.getContext()) - AD_VIEW_HEIGHT) / 2;

        for (Paragraph paragraph : paragraphs) {
            Paragraph paraInCurrPageData = new Paragraph(paragraph);
            paraInCurrPageData.setPageIndex(mPageDataList.size());
            pageData.getParagraphs().add(paraInCurrPageData);
            List<LineBlock> blocks = paragraph.getBlocks();
            int adLineIndexInParagraph;

            int blocksSize = blocks.size();

            // 记录这一段里需要插入广告Line的line位置和y值, 段落跨页修复
            Map<Integer, Integer> adMap = new HashMap<>();

            for (int j = 0; j < blocksSize; j++) {
                LineBlock b = blocks.get(j);
                if (b.getHeight() > 0 && pageOffsetY + b.getHeight() > bottomHeight && index != 1) { // 当前页无法放下，需要换页放(第一张大图无论如何都强行放下)
                    if (j == 0) { // 意味着上一段落刚好在当前页画完  目前这个地方有点问题 多加了一个段落
                        pageData.getParagraphs().remove(paraInCurrPageData); // 需要移除当前段落
                    }

                    mPageDataList.add(pageData);//说明完成一页的数据

                    pageStartY += pageHeight;
                    pageOffsetY = ReaderConfigWrapper.getInstance().getPageTopBarHeight();
                    pageData = new PageData();
                    paraInCurrPageData = new Paragraph(paragraph);
                    // 判断当前页是否可以插入广告：每3页出现一个广告
                    insertAdInThisPage = (mPageDataList.size() + 1) % Constants.CHAPTERAD_INTERVALNUM == 0;

                    if (lastAdPageIndex <= mPageDataList.size() - Constants.CHAPTERAD_INTERVALNUM && insertAdInThisPage) {
                        adLineIndexInParagraph = j + 1;
                        pageOffsetY += pageHeight + ReaderConfigWrapper.getInstance().getLineSpace() / 2;

                        // 记录当前这个位置的广告信息：lineIndex, y值
                        adMap.put(adLineIndexInParagraph, pageStartY);
                        // 记录插入广告的页码
                        lastAdPageIndex = mPageDataList.size();
//                        adTopHeightList.put(lastAdPageIndex, adPositionY);
                        j--;
                    } else {
                        paraInCurrPageData.getBlocks().add(b);
                    }
                    paraInCurrPageData.setPageIndex(mPageDataList.size());
                    if (!(j == blocksSize - 1 && StringUtil.isEmpty(b.getStr()))) {
                        // 段落最后一行是空行，如果刚好被排到下一页，PageData的第一个Paragraph对象内容为空
                        pageData.getParagraphs().add(paraInCurrPageData);
                    }
                } else {
                    paraInCurrPageData.getBlocks().add(b);
                }

                if (b.getType() == LineBlock.IMAGE) {
                    int offx = x + (pageWidth - b.getWidth()) / 2;
                    b.setX(offx);
                    b.setY(pageStartY + pageOffsetY);
                    if (pageOffsetY + b.getHeight() > bottomHeight) { //图片有可能超长
                        b.setHeight(bottomHeight - pageOffsetY);
                    }

                    pageOffsetY += b.getHeight() + ReaderConfigWrapper.getInstance().getImageMargin();
                }
                if (b.getType() == LineBlock.TAIL_CHAPTER_REWARD) {
                    b.setX(x);
                    b.setY(pageStartY + pageHeight - TangYuanSharedPrefUtils.getInstance().getChapterEndViewHeight());
                    if (b.getHeight() <= 0 && pageData.getParagraphs().size() == 0) {//首行是个空字符串，不绘制

                    } else {
                        pageOffsetY += b.getHeight() + ReaderConfigWrapper.getInstance().getLineSpace();
                    }
                } else {
                    b.setX(x);
                    b.setY(pageStartY + pageOffsetY);
                    if (b.getHeight() <= 0 && pageData.getParagraphs().size() == 0) {//首行是个空字符串，不绘制

                    } else {
                        pageOffsetY += b.getHeight() + ReaderConfigWrapper.getInstance().getLineSpace();
                    }
                }
                index++;

                if (status == ReaderConfigWrapper.STATUS_LOCK && index > 15) {
                    break;
                }
            }

            // 检查当前段落里是否有需要插入的广告
            if (!adMap.isEmpty()) {
                Set<Integer> set = adMap.keySet();
                for (Integer key : set) {
                    LineBlock adLine = new LineBlock(article.getView(), this, LineBlock.AD, null);
                    adLine.setHeight(pageHeight);
                    adLine.setY(adMap.get(key));
                    adLine.setWidth(pageWidth);
                    adLine.setStr("");
                    float[] pos = new float[2];
                    adLine.setPos(pos);
                    adLine.setPageIndex(lastAdPageIndex);
                    paragraph.getBlocks().add(key, adLine);
                    index++;
                }
            }
            if (status == ReaderConfigWrapper.STATUS_LOCK && index > 15) {
                break;
            }
        }

        mPageDataList.add(pageData);// 最后一页的数据

        // 如果上一个含广告的页码在倒数第三页及之前，加入章节末广告
        if (Constants.CHAPTERAD_INTERVALNUM < mPageDataList.size() && lastAdPageIndex < mPageDataList.size() - 1) {
            LineBlock adLine = new LineBlock(article.getView(), this, LineBlock.AD, null);
            adLine.setHeight(pageHeight);
            adLine.setWidth(pageWidth);
            adLine.setStr("");
            float[] pos = new float[2];
            adLine.setPos(pos);
            adLine.setPageIndex(mPageDataList.size() - 1);

            if (adLine.getHeight() > 0 && pageOffsetY + adLine.getHeight() > bottomHeight && index != 1) { // 当前页无法放下，需要换页放(第一张大图无论如何都强行放下)
                pageStartY += pageHeight;
                pageOffsetY = ReaderConfigWrapper.getInstance().getPageTopBarHeight();
                pageData = new PageData();

                Paragraph p = new Paragraph();
                p.getBlocks().add(adLine);
                pageData.getParagraphs().add(p);
                mPageDataList.add(pageData);
                adLine.setPageIndex(mPageDataList.size() - 1);
            }
            paragraphs.get(paragraphs.size() - 1).getBlocks().add(adLine);
            adLine.setY(pageStartY);
            int pageIndex = mPageDataList.size() - 1;
//            adTopHeightList.put(pageIndex < 0 ? 0 : pageIndex, adPositionY);

            pageOffsetY += pageHeight + ReaderConfigWrapper.getInstance().getLineSpace();

            lastAdPageIndex = mPageDataList.size() - 1;

            index++;
        }

        pageStartY += pageOffsetY;
        int pages = pageStartY / pageHeight; //计算出需要多少页
        if (pageStartY % pageHeight > (ReaderConfigWrapper.getInstance().getPageTopBarHeight() + ReaderConfigWrapper.getInstance().getLineSpace())) {//修复最后一页有可能仅仅是初始化高度加上空行的高度
            pages++;
        }

        if (pageStartY % contentHeight != 0) {
            //有空隙 可以显示广告
            double radis = (pageHeight - pageOffsetY) / (float) pageHeight;
            if (radis > 0.25) {
                this.adSize = Constants.AD_SIZE_SMALL;
            }
        }

        pageStartY = pages * pageHeight;
        setChapterHeight(pageStartY);

        this.mParagraphs = paragraphs;
        this.status = status;
        article.chapterHeightChanged(this, oldHeight);
        if (progress > 0) {
            progress = progress * pageStartY / 100;
            for (int i = 0; i < pages; i++) {
                if (i * pageHeight >= progress) {
                    return i * pageHeight;
                }
            }
            if (pages >= 2) {
                return (pages - 2) * pageHeight;
            }
        }
        return 0;
    }

    public boolean onClick(int x, int y, Chapter firstChapterInSight) {
        if (y < chapterTop || y > chapterTop + chapterHeight) { //事件不在范围内
            return false;
        }

        if (status != ReaderConfigWrapper.STATUS_LOADED && status != ReaderConfigWrapper.STATUS_LOCK) { //需要加载
            if (reloadRectF != null && reloadRectF.contains(x, y - (chapterHelper.isVerticalScrollMode() ? 0 : article.getCurrentOffsetY()))) {
                chapterHelper.loadChapter(this, firstChapterInSight, 0);
                return true;
            }
        } else {
            List<LineBlock> blocks = new ArrayList<LineBlock>();
            if (TangYuanSharedPrefUtils.getInstance().getReaderPageOrientation(2) == 1) {
                for (Paragraph paragraph : mParagraphs) {
                    blocks.addAll(paragraph.getBlocks());
                }
            } else {
                for (Paragraph paragraph : mParagraphs) {
                    blocks.addAll(paragraph.getBlocks());
                }
            }

            int size = blocks.size();
            for (int i = 0; i < size; i++) {
                LineBlock b = blocks.get(i);
                if (b.onClick(x, y - chapterTop)) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * 以整页的方式排版
     *
     * @param paragraphs
     * @param pageWidth
     * @param firstChapterInSight
     * @param progress            顺便跳转到位置 0表示不用处理
     */
    public int onePageLayout(List<Paragraph> paragraphs, int pageWidth, int pageHeight, Chapter firstChapterInSight, int progress, int status) {
        int pageHeaderHeight = ReaderConfigWrapper.getInstance().getPageTopBarHeight();
        int pageFooterHeight = ReaderConfigWrapper.getInstance().getPageBottomBarHeight();

        List<LineBlock> blocks = new ArrayList<LineBlock>();
        for (Paragraph paragraph : paragraphs) {
            blocks.addAll(paragraph.getBlocks());
        }

        pageHeight = pageHeight - pageHeaderHeight;
        int oldHeight = getChapterHeight();
        int x = ReaderConfigWrapper.getInstance().getPadding();
        int y = ReaderConfigWrapper.getInstance().getVerticalChapterMargin();
        int index = 0;
        int size = blocks.size();
        do {
            LineBlock b = blocks.get(index);
            if (b.getType() == LineBlock.IMAGE) {
                int offx = x + (pageWidth - b.getWidth()) / 2;
                b.setX(offx);
                b.setY(y);
                y += b.getHeight() + ReaderConfigWrapper.getInstance().getImageMargin();
            } else {
                b.setX(x);
                b.setY(y);
                y += b.getHeight() + ReaderConfigWrapper.getInstance().getLineSpace();
            }
            index++;
        }
        while (index < (status == ReaderConfigWrapper.STATUS_LOCK ? Math.min(MaxLinesWhenLock, size) : size));
        y = y < pageHeight ? pageHeight : y;
        setChapterHeight(y);
        this.mParagraphs = paragraphs;
        this.status = status;
        article.chapterHeightChanged(this, oldHeight);
        if (progress > 0) {
            progress = progress * y / 100;
            for (int i = 0; i < size; i++) {
                LineBlock b = blocks.get(i);
                if (b.getY() >= progress) {
                    return b.getY();
                }
            }
        }
        return 0;
    }

    /**
     * 垂直模式
     *
     * @param canvas
     * @param scrolly
     * @param bgcolor
     */
    public void paint(Canvas canvas, int scrolly, int bgcolor) {
        paint(canvas, scrolly, bgcolor, false);
    }


    private void setLockViewCanDraw(boolean flag) {
        article.getView().getLockView().setCanDraw(flag);
    }

    /**
     * 垂直模式
     * <p>
     * 状态 1 正在加载   2加载完成  3正在加载  4 加载失败  5加锁章节
     *
     * @param canvas
     * @param scrolly
     * @param bgcolor
     * @param force
     */
    public void paint(Canvas canvas, int scrolly, int bgcolor, boolean force) {
        if (!force) {
            if (chapterTop + chapterHeight < scrolly) { //上方不可见
                return;
            } else if (chapterTop > scrolly + article.getViewHeight()) { //下方不可见
                return;
            }
        }
        int pageHeight = this.article.getViewHeight();
        LineBlock lineBlock;
        switch (this.status) {
            case ReaderConfigWrapper.STATUS_INIT:
                drawMessage(canvas, "正在加载 " + getChapterName() + " …", false, force);
                this.chapterHelper.loadChapter(this, null, -1);
                return;
            case ReaderConfigWrapper.STATUS_LOADED: {
                setLockViewCanDraw(false);
                List<LineBlock> blocks = new ArrayList<LineBlock>();
                for (Paragraph paragraph : mParagraphs) {
                    blocks.addAll(paragraph.getBlocks());
                }
                int size = null == blocks ? 0 : blocks.size();
                for (int i = 0; i < size; i++) {
                    lineBlock = blocks.get(i);
                    lineBlock.paint(canvas, scrolly, chapterTop, pageHeight);
                }
            }
            return;
            case ReaderConfigWrapper.STATUS_LOADING:
                drawMessage(canvas, "正在加载 " + getChapterName() + " …", false, force);
                return;
            case ReaderConfigWrapper.STATUS_FAILED:
                drawMessage(canvas, "加载失败，重新加载", true, force);
                return;
            case ReaderConfigWrapper.STATUS_LOCK: {
                int canDrawTextHeight = pageHeight - this.article.getView().getLockView().getLockHeight();
                List<LineBlock> blocks = new ArrayList<LineBlock>();
                for (Paragraph paragraph : mParagraphs) {
                    blocks.addAll(paragraph.getBlocks());
                }
                int size1 = blocks.size();
                for (int i = 0; i < size1; i++) {
                    lineBlock = blocks.get(i);
                    if (!(lineBlock.getY() + lineBlock.getHeight() >= canDrawTextHeight || lineBlock.getY() == 0 || canDrawTextHeight == pageHeight)) {
                        lineBlock.paint(canvas, scrolly, this.chapterTop, pageHeight);
                    }
                }
                drawLockView(canvas);
            }
            return;

            default:
                return;
        }
    }

    /**
     * 左右模式
     *
     * @param canvas
     * @param scrolly
     * @param bgcolor
     * @param pageDir
     */
    public void paintPage(Canvas canvas, int scrolly, int bgcolor, int pageDir) {
        paintPage(canvas, scrolly, bgcolor, false, pageDir);
    }

    /**
     * 左右模式
     *
     * @param canvas
     * @param scrolly
     * @param bgcolor
     * @param force
     * @param pageDir
     */
    public void paintPage(Canvas canvas, int scrolly, int bgcolor, boolean force, int pageDir) {
        if (this.status == ReaderConfigWrapper.STATUS_LOADED && Looper.myLooper() != Looper.getMainLooper()) {
            //非主线程
            return;
        }
        if (!force) {
            if (chapterTop + chapterHeight < scrolly) { //上方不可见
                return;
            } else if (chapterTop > scrolly + article.getViewHeight()) { //下方不可见
                return;
            }
        }
        int pageHeight = this.article.getViewHeight();
        int i;
        LineBlock lineBlock;
        switch (this.status) {
            case ReaderConfigWrapper.STATUS_INIT:
                drawMessage(canvas, "正在加载 " + getChapterName() + " …", false);
                this.chapterHelper.loadChapter(this, null, -1);
                return;
            case ReaderConfigWrapper.STATUS_LOADED: {
                setLockViewCanDraw(false);
                List<LineBlock> blocks = new ArrayList<LineBlock>();
                for (Paragraph paragraph : mParagraphs) {
                    blocks.addAll(paragraph.getBlocks());
                }
                int size = blocks.size();
                i = 0;

                boolean prev = pageDir == IReadConfig.PREVIOUS_PAGE;

                while (i < size) {

                    LineBlock currLB = blocks.get(i);

                    currLB.paintPage(canvas, scrolly, this.chapterTop, pageHeight);

                    int sy = prev ? -pageHeight * 1 : pageHeight * 1;

                    if (currLB.type == LineBlock.AD) {
                        boolean prevAdLineArea = (currLB.y > scrolly + sy - chapterTop - pageHeight)
                                && (currLB.y < scrolly - chapterTop);
                        boolean nextAdLineArea = (currLB.y + chapterTop < scrolly + sy + pageHeight)
                                && (currLB.y + chapterTop > scrolly);
                        if (prev ? prevAdLineArea : nextAdLineArea) {
                            currLB.requestAd((scrolly + sy - chapterTop) / pageHeight);
                        }
                    }
                    i++;
                }
            }
            return;
            case ReaderConfigWrapper.STATUS_LOADING:
                drawMessage(canvas, "正在加载 " + getChapterName() + " …", false);
                return;
            case ReaderConfigWrapper.STATUS_FAILED:
                drawMessage(canvas, "加载失败，重新加载", true);
                return;
            case ReaderConfigWrapper.STATUS_LOCK: {
                List<LineBlock> blocks = new ArrayList<LineBlock>();
                for (Paragraph paragraph : mParagraphs) {
                    blocks.addAll(paragraph.getBlocks());
                }
                int canDrawTextHeight = pageHeight - this.article.getView().getLockView().getLockHeight();
                for (i = 0; i < blocks.size(); i++) {
                    lineBlock = (LineBlock) blocks.get(i);
                    if (!(lineBlock.getY() + lineBlock.getHeight() >= canDrawTextHeight || lineBlock.getY() == 0 || canDrawTextHeight == pageHeight)) {
                        lineBlock.paintPage(canvas, scrolly, this.chapterTop, pageHeight);
                    }
                }
                drawLockView(canvas);
            }
            return;
            default:
        }
    }

    public void repaint(String str) {
        if (this.article != null) {
            this.article.requestRepaint(this, str);
        }
    }

    public void setArticle(Article article, IChapterHelper chapterHelper) {
        this.article = article;
        this.chapterHelper = chapterHelper;
    }

    public void setChapterHeight(int i) {
        this.chapterHeight = i;
    }

    public void setChapterTop(int i) {
        this.chapterTop = i;
    }

    public void setStatus(int status) {
        if (this.status != status) {
            this.status = status;
            repaint("chapter  --状态改变[" + statusStr(status) + "]：" + this);
        }
    }

    public String toString() {
        return "id: " + getChapterId() + ", " + getChapterName() + "[位置：" + this.chapterTop + ", " + (this.chapterTop + this.chapterHeight) + ", \u9ad8\u5ea6:" + this.chapterHeight + "]";
    }

    public void unload(boolean nofity) {
        if (this.article == null) {
            return;
        }
        if (mParagraphs != null) {
            for (Paragraph paragraph : mParagraphs) {
                paragraph.reset();
            }
        }
        int viewHeight = this.getChapterHeight();

        this.status = ReaderConfigWrapper.STATUS_INIT;
        this.chapterHeight = 0;
        this.chapterTop = 0;
        this.mParagraphs = null;
        if (nofity) {
            this.article.chapterHeightChanged(this, viewHeight);
        }

    }

    //1级分类
    public void setNovelFirstType(String novelFirstType) {
        this.novelFirstType = novelFirstType;
    }

    //2级分类
    public void setNovelSecondType(String novelSecondType) {
        this.novelSecondType = novelSecondType;
    }

    //小说付费类型
    public String getNovelPayType() {
        if (is_jingpin) {//如果是精品 直接返回精品
            return "精品";
        } else {
            if (bookVip) {
                return "包月";
            } else {
                return "免费";
            }
        }
    }


    public String getBookName() {
        return this.bookName;
    }

    public String getNovelFirstType() {
        return this.novelFirstType;
    }

    public String getNovelSecondType() {
        return this.novelSecondType;
    }


    public void setChapterPayType(boolean chapterPayType) {
        this.chapterPayType = chapterPayType;
    }

    public boolean getChapterPayType() {
        return chapterPayType;
    }


    public int getChapterPrice() {
        return chaptPrice;
    }


    public void setBookVip(boolean bookVip) {
        this.bookVip = bookVip;
    }

    public boolean getBookVip() {
        return bookVip;
    }

    public void setChaptPrice(int chaptPrice) {
        this.chaptPrice = chaptPrice;
    }

    public int getAdSize() {
        return adSize;
    }

    public void setAdSize(int adSize) {
        this.adSize = adSize;
    }

    /**
     * 设置长按选中
     *
     * @param pointF
     */
    public void setLongPressSelect(PointF pointF) {
        if (mParagraphs != null) {
            for (Paragraph paragraph : mParagraphs) {
                boolean select = paragraph.contains(pointF, article.getCurrentOffsetY() - chapterTop);
                paragraph.setSelect(select && paragraph.getParagraphId() > 0);
                if (paragraph.isSelect()) {
                    mSelectedParagraph = paragraph;
                }
            }
        }
    }

    public void resetSelect() {
        mSelectedParagraph = null;
        if (mParagraphs != null) {
            for (Paragraph paragraph : mParagraphs) {
                paragraph.setSelect(false);
            }
        }
    }

    public Paragraph getSelectedParagraph() {
        return mSelectedParagraph;
    }

    public boolean containsPoint(PointF pointF) {
        if (mParagraphs != null) {
            Paragraph firstPara = mParagraphs.get(0);
            Paragraph latestPara = mParagraphs.get(mParagraphs.size() - 1);
            if (firstPara != null && latestPara != null) {
                LineBlock firstLine = firstPara.getBlocks().isEmpty() ? null : firstPara.getBlocks().get(0);
                LineBlock latestLine = latestPara.getBlocks().isEmpty() ? null : latestPara.getBlocks().get(latestPara.getBlocks().size() - 1);
                if (firstLine != null && latestLine != null) {
                    return pointF.y > firstLine.y && pointF.y < latestLine.y;
                }
            }
        }
        return false;
    }
}
