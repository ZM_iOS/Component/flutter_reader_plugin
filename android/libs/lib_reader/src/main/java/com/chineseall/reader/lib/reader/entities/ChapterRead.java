package com.chineseall.reader.lib.reader.entities;

import java.io.Serializable;

public class ChapterRead implements Serializable {

    private static final long serialVersionUID = 5222930732047557519L;

    private String chapterId;
    private String bookId;
    private String chapterName;
    private Long wordCount;
    private int imageCount;
    private long timeStamp_value;
    private String timeStamp_local;
    private double orderValue = 0.0f;
    private String htmlUrl;
    private String attchmentnames;
    private int localRead;
    // true==1 false=0
    private int localUpdate;
    /**  章节是否可被守护 */
    private boolean guard_flag;
    /** 当前用户是否守护该章节 */
    private boolean user_guard_flag;

    private int subscript_flag;
    private int user_subscript_flag;

    private int chapter_coins;	//订阅金币

    private int promot_chapter_coins;	//优惠后的金币

    public long getPublishTime() {
        return publishTime;
    }

    public void setPublishTime(long publishTime) {
        this.publishTime = publishTime;
    }

    private long publishTime;

    private long preId;
    private long nextId;
    private long price;

    public boolean isShielded() {
        return isShielded;
    }

    public void setShielded(boolean shielded) {
        isShielded = shielded;
    }

    private boolean isShielded;//是否被屏蔽

    public long getPrice() {
        return price;
    }

    public void setPrice(long price) {
        this.price = price;
    }

    public long getPreId() {
        return preId;
    }

    public void setPreId(long preId) {
        this.preId = preId;
    }

    public long getNextId() {
        return nextId;
    }

    public void setNextId(long nextId) {
        this.nextId = nextId;
    }

    public int getLocalRead() {
        return localRead;
    }

    public void setLocalRead(int localRead) {
        this.localRead = localRead;
    }

    public int getLocalUpdate() {
        return localUpdate;
    }

    public void setLocalUpdate(int localUpdate) {
        this.localUpdate = localUpdate;
    }

    public void setTimeStamplocal(String tl) {
        this.timeStamp_local = tl;
    }

    public long getTimeStamp_value() {
        return timeStamp_value;
    }

    public void setTimeStamp_value(long timeStamp_value) {
        this.timeStamp_value = timeStamp_value;
    }

    public void setAttchmentnames(String attchmentnames) {
        this.attchmentnames = attchmentnames;
    }

    public String getAttchmentnames() {
        return attchmentnames;
    }

    public String getTimeStamplocal() {
        return timeStamp_local;
    }

    public String getChapterName() {
        return this.chapterName;
    }

    public void setChapterName(String chapterName) {
        this.chapterName = chapterName;
    }

    public void setBookId(String bookId) {
        this.bookId = bookId;
    }

    public String getBookId() {
        return bookId;
    }

    public void setChapterId(String chapterId) {
        this.chapterId = chapterId;
    }

    public String getChapterId() {
        return chapterId;
    }

    public double getOrderValue() {
        return orderValue;
    }

    public void setOrderValue(double orderId) {
        this.orderValue = orderId;
    }

    public int getImageCount() {
        return imageCount;
    }

    public void setImageCount(int imageCount) {
        this.imageCount = imageCount;
    }

    public Long getWordCount() {
        return wordCount;
    }

    public void setWordCount(Long wordCount) {
        this.wordCount = wordCount;
    }

    public String getHtmlUrl() {
        return htmlUrl;
    }

    public void setHtmlUrl(String htmlUrl) {
        this.htmlUrl = htmlUrl;
    }

    public boolean isGuard_flag() {
        return guard_flag;
    }

    public void setGuard_flag(boolean guard_flag) {
        this.guard_flag = guard_flag;
    }

    public boolean isUser_guard_flag() {
        return user_guard_flag;
    }

    public void setUser_guard_flag(boolean user_guard_flag) {
        this.user_guard_flag = user_guard_flag;
    }

    public int getSubscript_flag() {
        return subscript_flag;
    }

    public void setSubscript_flag(int subscript_flag) {
        this.subscript_flag = subscript_flag;
    }

    public int getUser_subscript_flag() {
        return user_subscript_flag;
    }

    public void setUser_subscript_flag(int user_subscript_flag) {
        this.user_subscript_flag = user_subscript_flag;
    }

    public int getChapter_coins() {
        return chapter_coins;
    }

    public void setChapter_coins(int chapter_coins) {
        this.chapter_coins = chapter_coins;
    }

    public int getPromot_chapter_coins() {
        return promot_chapter_coins;
    }

    public void setPromot_chapter_coins(int promot_chapter_coins) {
        this.promot_chapter_coins = promot_chapter_coins;
    }
}
