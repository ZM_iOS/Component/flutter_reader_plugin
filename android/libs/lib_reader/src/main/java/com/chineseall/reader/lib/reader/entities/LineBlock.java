package com.chineseall.reader.lib.reader.entities;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.os.Looper;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.widget.RelativeLayout;

import androidx.core.content.ContextCompat;

import com.chineseall.reader.lib.reader.R;
import com.chineseall.reader.lib.reader.ad.ReaderAdManager;
import com.chineseall.reader.lib.reader.callbacks.OnAdViewReceiver;
import com.chineseall.reader.lib.reader.config.ReaderConfigWrapper;
import com.chineseall.reader.lib.reader.core.ReaderClient;
import com.chineseall.reader.lib.reader.utils.LogUtils;
import com.chineseall.reader.lib.reader.utils.PaintHelper;
import com.chineseall.reader.lib.reader.utils.StringUtil;
import com.chineseall.reader.lib.reader.view.Path;
import com.chineseall.reader.lib.reader.view.ReaderView;

import java.io.Serializable;


public class LineBlock implements Serializable {
    public static final int AD = 5;
    public static final int IMAGE = 1;
    public static final int TAIL = 4;
    public static final int TAIL_CHAPTER_COMMENTS = 8;
    public static final int TAIL_CHAPTER_REWARD = 10;
    public static final int TAIL_AUTHOR_SAY = 9;
    public static final int TAIL_TITLE = 6;
    public static final int TAIL_AUTHOR = 7;
    public static final int TAIL_WRITE_COMMENT = 11;
    public static final int TEXT = 2;
    public static final int TITLE = 3;
    private static final long serialVersionUID = 3335167099080488817L;
    protected Chapter chapter;
    protected RElement element;
    protected ReaderView readerview;
    protected Paragraph paragraph;
    protected String str;
    protected int type;
    protected int pageIndex;
    protected int height;
    protected int width;
    protected int x;
    protected int y;
    protected float pos[];
    protected int mRightMax;
    protected boolean selected;

    private boolean loadingAd = false;
    private ViewGroup mAdViewRoot;
    private RelativeLayout mAdViewParent;

    public View mAdView;

    public LineBlock(ReaderView readerView, Chapter chapter, int type, RElement rElement) {
        this.readerview = readerView;
        this.chapter = chapter;
        this.type = type;
        this.element = rElement;
        int width = readerView.getWidth();
        int padding = ReaderConfigWrapper.getInstance().getPadding();
        mRightMax = width - padding;
    }

    public int getPageIndex() {
        return pageIndex;
    }

    public void setPageIndex(int pageIndex) {
        this.pageIndex = pageIndex;
    }

    public int getHeight() {
        return this.height;
    }

    public String getStr() {
        return this.str;
    }

    public int getType() {
        return this.type;
    }

    public int getWidth() {
        return this.width;
    }

    public int getX() {
        return this.x;
    }

    public int getY() {
        return this.y;
    }

    public void setParagraph(Paragraph paragraph) {
        this.paragraph = paragraph;
    }

    public boolean onClick(int x, int y) {
        return false;
    }

    /**
     * 垂直模式
     *
     * @param canvas
     * @param scrolly    滚动位置
     * @param offsety    偏移高度
     * @param pageHeight 可见区域高度
     */
    public synchronized void paint(Canvas canvas, int scrolly, int offsety, int pageHeight) {
        if ((this.y + this.height) + offsety >= scrolly) {
            if (this.y <= (pageHeight + scrolly) - offsety) {
                if (this.type == TEXT) {
                    float tmpy = ((this.y + this.height) + offsety);
                    if (selected && !TextUtils.isEmpty(str)) {
                        drawSelectionArea(canvas, tmpy);
                    }

                    int length = pos.length;
                    for (int j = 0; j < length; j++) {
                        int tmpIndex = j + 1;
                        if (tmpIndex % 2 == 0) {
                            pos[j] = tmpy;
                        }
                    }
                    drawPosText(canvas);
                } else if (this.type == TITLE) {
                    canvas.drawText(this.str, (float) this.x, (float) ((this.y + this.height) + offsety), PaintHelper.getInstance().getTitlePaint());
                } else if (this.type == TAIL) {
                    canvas.drawText(this.str, (float) this.x, (float) ((this.y + this.height) + offsety), PaintHelper.getInstance().getContentTailPaint());
                }
            }
        }
    }


    protected float getFontHeight() {
        Paint contentPaint = PaintHelper.getInstance().getContentPaint();
        return contentPaint.measureText("啊") + contentPaint.descent();
    }

    protected void drawSelectionArea(Canvas canvas, float tmpy) {
        Paint contentPaintHasUnderLine = PaintHelper.getInstance().getContentPaintHasUnderLine();
        Paint contentPaint = PaintHelper.getInstance().getContentPaint();
        float space = 0;
        int strLength = str.length();
        if (strLength > 2 && str.substring(0, 2).equals("\u3000\u3000")) {
            space = contentPaint.measureText("啊啊");
        }

        String lastStr = str.substring(strLength - 1);

        float liney = tmpy + contentPaint.descent();
        int length = strLength;
        float v1 = contentPaint.measureText(str, length - 1, length);
        if (StringUtil.isNeedHandler(lastStr.toCharArray()[0])) {
            v1 = v1 / 2;
        }
        int length1 = pos.length;
        float right = pos[length1 - 2] + v1;
        if (right > this.mRightMax) {
            right = this.mRightMax;
        }
        canvas.drawRect((float) this.x + space, liney - getFontHeight(), right, liney, contentPaintHasUnderLine);
    }

    /**
     * 左右模式
     *
     * @param canvas
     * @param scrolly    滚动位置
     * @param offsety    偏移高度
     * @param pageHeight 可见区域高度
     */
    public void paintPage(final Canvas canvas, final int scrolly, final int offsety, int pageHeight) {
        if ((this.y + this.height) + offsety < scrolly || this.y > (pageHeight + scrolly) - offsety) {
            return;
        }
        if (this.type == TEXT) {
            float tmpy = (float) (((this.y + this.height) - scrolly) + offsety);
            if (selected) {
                if (!TextUtils.isEmpty(str)) {
                    drawSelectionArea(canvas, tmpy);
                }
            }
            int length = pos.length;
            for (int j = 0; j < length; j++) {
                int tmpIndex = j + 1;
                if (tmpIndex % 2 == 0) {
                    pos[j] = tmpy;
                }
            }
            drawPosText(canvas);
        } else if (this.type == TITLE) {
            canvas.drawText(this.str, (float) this.x, (float) (((this.y + this.height) - scrolly) + offsety), PaintHelper.getInstance().getTitlePaint());
        } else if (this.type == TAIL_TITLE) {
            float tmpy = (((this.y + this.height) - scrolly) + offsety);
            Path path = new Path();
            float[] rids = {10.0f, 10.0f, 10.0f, 10.0f, 0.0f, 0.0f, 0.0f, 0.0f,};
            int padding = ReaderConfigWrapper.getInstance().getPadding();
            int pageWidth = readerview.getArticle().getPageWidth();
            Paint contentPaint = PaintHelper.getInstance().getContentTailTitlePaint();
            float fontHeight = contentPaint.measureText("啊") + contentPaint.descent();
            float liney = tmpy + contentPaint.descent();
            int lineSpace = ReaderConfigWrapper.getInstance().getLineSpace();
            path.addRoundRect(new RectF(this.x, liney - fontHeight, pageWidth - padding, liney + lineSpace), rids, Path.Direction.CW);
            canvas.drawPath(path, PaintHelper.getInstance().getContentPaintHasUnderLine());
            canvas.drawText(this.str, (float) this.x, tmpy, contentPaint);
        } else if (this.type == TAIL) {
            float tmpy = (((this.y + this.height) - scrolly) + offsety);
            Path path = new Path();
            float[] rids = {0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f,};
            int padding = ReaderConfigWrapper.getInstance().getPadding();
            int pageWidth = readerview.getArticle().getPageWidth();
            Paint contentPaint = PaintHelper.getInstance().getContentTailPaint();
            float fontHeight = contentPaint.measureText("啊") + contentPaint.descent();
            float liney = tmpy + contentPaint.descent();
            int lineSpace = ReaderConfigWrapper.getInstance().getLineSpace();

            path.addRoundRect(new RectF(this.x, liney - fontHeight, pageWidth - padding, liney + lineSpace), rids, Path.Direction.CW);
            canvas.drawPath(path, PaintHelper.getInstance().getContentPaintHasUnderLine());
            canvas.drawText(this.str, (float) this.x, tmpy, contentPaint);
        } else if (this.type == AD) {
            if (this.y + offsety < scrolly) {
                //上下模式下不需要绘制
                return;
            }
            try {
                if (closeAdPage != pageIndex){
                    closeAdPage = -1;
                    requestAd(pageIndex);
                    if (mAdView != null) {
                        chapter.getAdViewKeyList().put(pageIndex, (Integer) mAdView.getTag());
                        final ViewGroup[] parent = new ViewGroup[1];
                        if (Looper.myLooper() != Looper.getMainLooper()) {
                            readerview.post(() -> {
                                parent[0] = (ViewGroup) View.inflate(readerview.getContext(), R.layout.col_layout_ad, null);
                                if (mAdView.getParent() != null) {
                                    ((ViewGroup) mAdView.getParent()).removeView(mAdView);
                                }

                                parent[0].addView(mAdView);
                            });
                        } else {
                            parent[0] = (ViewGroup) View.inflate(readerview.getContext(), R.layout.col_layout_ad, null);
                            if (mAdView.getParent() != null) {
                                ((ViewGroup) mAdView.getParent()).removeView(mAdView);
                            }

                            parent[0].addView(mAdView);
                        }

                        int pageWidth = readerview.getArticle().getPageWidth();
                        int widthSpec = View.MeasureSpec.makeMeasureSpec(pageWidth, View.MeasureSpec.EXACTLY);
                        int heightSpec = View.MeasureSpec.makeMeasureSpec(height, View.MeasureSpec.EXACTLY);

                        parent[0].measure(widthSpec, heightSpec);
                        parent[0].layout(0, 0, pageWidth, parent[0].getMeasuredHeight());
                        canvas.save();
                        canvas.translate(0, (float) (((this.y) - scrolly) + offsety));
                        parent[0].draw(canvas);
                        canvas.restore();
                        if (readerview.getOnRectClickCallback()!=null){
                            readerview.getOnRectClickCallback().adVisible(5);
                        }
                    }
                }else if(closeAdPage != -1){
                    final ViewGroup[] parent = new ViewGroup[1];
                    if (Looper.myLooper() != Looper.getMainLooper()) {
                        readerview.post(() -> {
                            parent[0] = (ViewGroup) View.inflate(readerview.getContext(), R.layout.col_layout_ad_empty, null);
                        });
                    } else {
                        parent[0] = (ViewGroup) View.inflate(readerview.getContext(), R.layout.col_layout_ad_empty, null);
                    }
                    parent[0].setBackgroundColor(ContextCompat.getColor(readerview.getContext(),ReaderConfigWrapper.getInstance().getPageBgColor()));
                    int pageWidth = readerview.getArticle().getPageWidth();
                    int widthSpec = View.MeasureSpec.makeMeasureSpec(pageWidth, View.MeasureSpec.EXACTLY);
                    int heightSpec = View.MeasureSpec.makeMeasureSpec(height, View.MeasureSpec.EXACTLY);

                    parent[0].measure(widthSpec, heightSpec);
                    parent[0].layout(0, 0, pageWidth, parent[0].getMeasuredHeight());
                    canvas.save();
                    canvas.translate(0, (float) (((this.y) - scrolly) + offsety));
                    parent[0].draw(canvas);
                    canvas.restore();
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    int closeAdPage = -1;

    public void requestAd(int page) {

        Log.d("TAG=requestAd", "page=" + page);
        if (mAdView == null && !loadingAd) {
            loadingAd = true;
            ReaderClient.getInstance().getCallBackContainer().getOnAdViewCallBack().get(new OnAdViewReceiver(){

                @Override
                public void onReceive(boolean success, View view) {
                    loadingAd = false;
                    Log.d("TAG=requestAd", "getOnAdViewCallBack=" + chapter.getChapterName() + ", " + page + "," + mAdView + ", ");
                    ReaderAdManager.getInstance().addTTAd(view);
                    mAdView = view;
                    readerview.post(() -> readerview.requestRepaint(""));
                }

                @Override
                public void onAdClosed() {
                    loadingAd = false;
                    Log.d("TAG=requestAd", "onAdClosed=" + chapter.getChapterName() + ", " + page + "," + mAdView + ", ");
                    chapter.getAdViewKeyList().remove(page);
                    mAdView = null;
                    if (readerview.getOnRectClickCallback()!=null){
                        readerview.getOnRectClickCallback().adGone(5);
                    }
                    closeAdPage = page;
                    readerview.post(() -> {
                        readerview.setNorepaint(false);
                        readerview.requestRepaint("");
                    });
                }
            });
        }

    }

    private final WebChromeClient webChromeClient = new WebChromeClient() {
        @Override
        public void onProgressChanged(WebView view, int newProgress) {
            super.onProgressChanged(view, newProgress);
            LogUtils.d("toutiaoad webChromeClient: " + newProgress);
            if (newProgress >= 100) {
                readerview.post(() -> readerview.requestRepaint(""));
            }
        }
    };

    private WebView findWebView(View view) {
        if (view instanceof ViewGroup) {
            if (view instanceof WebView) {
                return (WebView) view;
            } else {
                for (int i = 0; i < ((ViewGroup) view).getChildCount(); ++i) {
                    WebView ans = findWebView(((ViewGroup) view).getChildAt(i));
                    if (ans instanceof WebView) {
                        return ans;
                    }
                }
            }
        }
        return null;
    }

    private void drawPosText(Canvas canvas) {
        Paint contentPaint = PaintHelper.getInstance().getContentPaint();
        if (str != null && str.length() > 0) {
            char[] chars = str.toCharArray();
            int charsLength = chars.length;
            for (int i = 0; i < charsLength; i++) {
                if (canvas != null && contentPaint != null) {
                    canvas.drawText(chars, i, 1, pos[i * 2], pos[i * 2 + 1], contentPaint);
                } else {
//                    Log.e("LineBlock", "canvas or chars is null");
                }
            }
        }
    }

    public void setPlaying(boolean playing) {
        selected = playing;
    }

    public void setHeight(int i) {
        this.height = i;
    }

    public void setStr(String str) {
        this.str = str;
    }

    public void setType(int i) {
        this.type = i;
    }

    public void setWidth(int i) {
        this.width = i;
    }


    public void setX(int i) {
        this.x = i;
    }

    public void setY(int i) {
        this.y = i;
    }

    public void setPos(float[] pos) {
        this.pos = pos;
    }

    public String toString() {
        StringBuffer stringBuffer = new StringBuffer();
        if (this.type == IMAGE) {
            stringBuffer.append("\u56fe\u7247\uff1a[" + this.y + ", " + (this.y + this.height) + "]: " + this.str);
        } else if (this.type == TEXT) {
            stringBuffer.append("\u6587\u5b57\uff1a[" + this.y + ", " + (this.y + this.height) + "]");
        } else if (this.type == TITLE) {
            stringBuffer.append("\u6807\u9898\uff1a[" + this.y + ", " + (this.y + this.height) + "]");
        } else if (this.type == TAIL) {
            stringBuffer.append("\u5c3e\u90e8\uff1a[" + this.y + ", " + (this.y + this.height) + "]");
        }
        return stringBuffer.toString();
    }
}
