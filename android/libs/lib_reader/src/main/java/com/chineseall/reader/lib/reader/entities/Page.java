package com.chineseall.reader.lib.reader.entities;

public class Page {
    public int index = 0;
    public int left = 0, top = 0, right = 0, bottom = 0;
    public Object data = null;
    public static int PAGEHEIGHT = 0;
    public static int PAGEWIDTH = 0;

    public Page duplicate() {
        Page p = new Page();
        p.index = index;
        p.top = top;
        p.bottom = bottom;
        return p;
    }
}
