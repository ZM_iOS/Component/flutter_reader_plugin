package com.chineseall.reader.lib.reader.entities;

import com.chineseall.reader.lib.reader.utils.StringUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Freedom on 2017/12/26.
 */

public class PageData {
    private List<Paragraph> mParagraphs = new ArrayList<Paragraph>();
    private List<LineBlock> mLineBlocks = new ArrayList<>();

    public List<Paragraph> getParagraphs() {
        return mParagraphs;
    }

    public List<LineBlock> getLineBlocks() {
        return mLineBlocks;
    }

    public void clean() {
        mParagraphs.clear();
    }

    /**
     * 重置上一个段落画的粗线条
     *
     * @param paragraphIndex
     */
    public void resetPlayingStatus(int paragraphIndex) {
        List<Paragraph> paragraphs = getParagraphs();
        if (paragraphIndex < paragraphs.size()) {
            Paragraph lastParagraph = paragraphs.get(paragraphIndex);
            for (LineBlock lineBlock : lastParagraph.getBlocks()) {
                lineBlock.setPlaying(false);
            }
        }
    }

    public Paragraph readNextParagraph(int paragraphIndex) {
        List<Paragraph> paragraphs = getParagraphs();
        Paragraph currentReadParagraph = null;
        if (paragraphIndex < paragraphs.size()) {
            Paragraph paragraph = paragraphs.get(paragraphIndex);
            String content = paragraph.getContentByLines();
            if (!StringUtil.isEmpty(content)) {
                currentReadParagraph = paragraph;
                for (LineBlock lineBlock : currentReadParagraph.getBlocks()) {
                    lineBlock.setPlaying(true);
                }
            }
        }
        return currentReadParagraph;
    }
}
