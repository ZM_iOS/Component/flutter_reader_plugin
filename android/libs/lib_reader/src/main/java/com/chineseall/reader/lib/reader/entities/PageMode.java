package com.chineseall.reader.lib.reader.entities;

public enum PageMode {
    Page,
    Recommend,
    LOADING,
    LASTPAGE
}
