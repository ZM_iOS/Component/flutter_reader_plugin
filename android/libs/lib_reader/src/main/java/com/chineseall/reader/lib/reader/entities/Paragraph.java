package com.chineseall.reader.lib.reader.entities;

import android.graphics.PointF;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Freedom on 2017/12/26.
 */

public class Paragraph implements Serializable {
    private List<LineBlock> mBlocks = new ArrayList<>();
    private int pageIndex;
    private String content;
    private boolean select = false;// 是否选中
    private int commentCount;
    private long paragraphId;
    private boolean isContentType = false;
    private Chapter chapter;

    public Paragraph() {

    }

    public Paragraph(Paragraph origin) {
        this.pageIndex = origin.pageIndex;
        this.select = origin.select;
        this.isContentType = origin.isContentType;
    }

    public void setContentType(boolean contentType) {
        isContentType = contentType;
    }

    public boolean isContentType() {
        return isContentType;
    }

    public Chapter getChapter() {
        return chapter;
    }

    public void setChapter(Chapter chapter) {
        this.chapter = chapter;
    }

    public void setParagraphId(long paragraphId) {
        this.paragraphId = paragraphId;
    }

    public long getParagraphId() {
        return paragraphId;
    }

    public void setCommentCount(int commentCount) {
        this.commentCount = commentCount;
    }

    public int getCommentCount() {
        return commentCount;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public void clean() {
        mBlocks.clear();
    }

    public List<LineBlock> getBlocks() {
        return mBlocks;
    }

    public void setBlocks(List<LineBlock> blocks) {
        mBlocks = blocks;
    }

    public int getPageIndex() {
        return pageIndex;
    }

    public void setPageIndex(int pageIndex) {
        this.pageIndex = pageIndex;
    }

    public String getContent() {
        if (content == null) {
            return "";
        }
        return content;
    }

    public String getContentByLines() {
        StringBuilder sb = new StringBuilder();
        for (LineBlock lineBlock : mBlocks) {
            if (lineBlock.type == LineBlock.TEXT
                    || lineBlock.type == LineBlock.TITLE) {
                sb.append(lineBlock.getStr());
            }
        }
        return sb.toString();
    }

    public void setSelect(boolean select) {
        this.select = select;
        for (LineBlock lineBlock : mBlocks) {
            if (lineBlock.type == LineBlock.TEXT || lineBlock.type == LineBlock.TITLE) {
                lineBlock.setPlaying(select);
            }
        }
    }

    public boolean isSelect() {
        return select;
    }

    public boolean contains(PointF pointF, int yOffset) {
        boolean ans = false;
        if (!mBlocks.isEmpty()) {
            int y = (int) (pointF.y + yOffset);
            LineBlock first = mBlocks.get(0);
            LineBlock last = mBlocks.get(mBlocks.size() - 1);
            if (y >= first.y && y <= last.y + last.height) {
                ans = true;
            }
        }
        return ans;
    }

    public void reset() {
        mBlocks.clear();
        content = null;
        pageIndex = 0;
    }

}
