package com.chineseall.reader.lib.reader.entities;

import android.graphics.Canvas;

import com.chineseall.reader.lib.reader.view.ReaderView;

import java.util.List;

import static com.chineseall.reader.lib.reader.config.IReadConfig.AD_VIEW_HEIGHT;

public class RAD extends RElement {
    /**
     * 当宽度为xx时，其高度为多少
     */
    private static int last_w = -1;
    /**
     * 记录高度，避免重复计算
     */
    private static int last_h = -1;


    @Override
    public Page measureSize(Page page) {
        return null;
    }

    @Override
    public void draw(Canvas canvas, int scrollY) {

    }

    @Override
    public void layout(ReaderView view, Chapter chapter, List<Paragraph> paragraphs, int maxWidth) {
        LineBlock block = null;
        Paragraph paragraph = new Paragraph();

        {
            LineBlock lastLine = new LineBlock(view, chapter, LineBlock.AD, this);
            lastLine.setHeight(AD_VIEW_HEIGHT);
            lastLine.setWidth(maxWidth);
            lastLine.setStr("");
            float pos[] = new float[2];
            lastLine.setPos(pos);
            paragraph.getBlocks().add(lastLine);
        }



        paragraphs.add(paragraph);
    }
}