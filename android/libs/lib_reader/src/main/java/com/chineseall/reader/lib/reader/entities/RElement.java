package com.chineseall.reader.lib.reader.entities;

import android.graphics.Canvas;

import com.chineseall.reader.lib.reader.view.ReaderView;

import java.util.List;
import java.util.Vector;

public abstract class RElement {
    /** 所有对象自己控制自己的页数 */
    protected Vector<Page> pages = new Vector<Page>();

    public RElement() {
    }

    public Vector<Page> getPages() {
        return pages;
    }

    public abstract Page measureSize(Page page);

    public abstract void draw(Canvas canvas, int scrollY);

    public abstract void layout(ReaderView view, Chapter chapter, List<Paragraph> paragraphs, final int MAXWIDTH);
}
