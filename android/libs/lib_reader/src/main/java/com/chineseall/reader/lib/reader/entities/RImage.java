package com.chineseall.reader.lib.reader.entities;

import android.graphics.Canvas;
import android.graphics.Paint;

import com.chineseall.reader.lib.reader.config.ReaderConfigWrapper;
import com.chineseall.reader.lib.reader.view.ReaderView;

import java.util.List;

public class RImage extends RElement {
    protected String url = "";
    protected String name;
    protected String note;
    protected int width, height;

    public RImage(String url, int width, int height, String name, String note, int MAXWIDTH) {
        setImage(url, width, height, name, note, MAXWIDTH);
    }

    public void setImage(String url, final int width, final int height, String name, String note, int MAXWIDTH) {
        float density = ReaderConfigWrapper.getInstance().getScreenDensity();

        this.url = url;
        this.name = name;
        this.note = note;
        this.width = (int) (width * density);
        this.height = (int) (height * density);
        if (this.width > MAXWIDTH) {
            float scale = MAXWIDTH * 1.0F / this.width;
            this.height = (int) (this.height * scale);
            this.width = MAXWIDTH;
        }
    }

    public String getImageUrl(){
        return url;
    }

    public String getImageName(){
        return name;
    }

    public String getImageNote(){
        return note;
    }

    @Override
    public Page measureSize(Page page) {
        Page self = page.duplicate();

        int imageMargin = ReaderConfigWrapper.getInstance().getImageMargin();

        if (page.top + height < Page.PAGEHEIGHT) { //能放下
            if (page.top + height + imageMargin < Page.PAGEHEIGHT) { //边距也满足条件
                self.top += imageMargin;
                self.bottom = self.top + height;

                page.top = page.bottom = self.bottom;
            } else { //边距条件不能完全满足，适当处理
                int rest = Page.PAGEHEIGHT - (page.top + height);
                rest >>= 1;
                self.top += rest;
                self.bottom = self.top + height;

                page.top = page.bottom = self.bottom;
            }
        } else { //不能放下，则另起一页
            page.index++;
            self = page.duplicate();

            self.top = 0;
            self.bottom = height;
            int margin = 0;
            if (height < Page.PAGEHEIGHT) { //只要图片小于屏幕高度，则留上边距
                margin = (Page.PAGEHEIGHT - height) / 2;
                margin = imageMargin < margin ? imageMargin : margin;
                self.top += margin;
                self.bottom += margin;
            }

            page.top = page.bottom = height + margin;
        }
        self.left = (Page.PAGEWIDTH - width) >> 1;
        pages.add(self);

        return page;
    }

    @Override
    public void draw(final Canvas canvas, int pagenumber) {
        final Page p = pages.get(0);
        if (p.index == pagenumber) {
            final int top = p.top;
//			Const.paint.setColor(0xFF000000);
            Paint paint = new Paint();
            canvas.drawRect(p.left, top, p.left + width, top + height, paint);
            //canvas.drawText("" + url + ", " + width + " x " + heigth, 20, Page.PAGEHEIGHT * p.index + p.top + 50, paint);

        }
    }

    @Override
    public String toString() {
        int page = 0;
        if (pages.size() > 0) {
            page = pages.get(0).index;
        }
        return "第 " + page + " 页\n\t" + url;
    }

    @Override
    public void layout(ReaderView view, Chapter chapter, List<Paragraph> paragraphs, int MAXWIDTH) {
        LineBlock block = new LineBlock(view, chapter, LineBlock.IMAGE, this);
        block.setWidth(width);
        block.setHeight(height);
        block.setStr(url);
//        blocks.add(block);
        Paragraph paragraph = new Paragraph();
        paragraph.getBlocks().add(block);
        paragraphs.add(paragraph);
    }
}
