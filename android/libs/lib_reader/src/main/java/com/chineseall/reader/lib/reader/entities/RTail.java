package com.chineseall.reader.lib.reader.entities;

import android.graphics.Paint;
import android.graphics.Paint.FontMetrics;
import android.text.TextUtils;

import com.chineseall.reader.lib.reader.view.ReaderView;

import java.util.List;

import static com.chineseall.reader.lib.reader.utils.BreakLinesUtil.breakLines;

public class RTail extends RText {
    public RTail(Paint paint, String text) {
        super(paint, text);
    }



    @Override
    public void layout(ReaderView view, Chapter chapter, List<Paragraph> paragraphs, int maxWidth) {
        if (TextUtils.isEmpty(text)) {
            return;
        }
        FontMetrics fm = paint.getFontMetrics();
        int fontHeight = (int) Math.ceil(fm.descent - fm.ascent);
        LineBlock block = null;
        //加半个空行
        block=new LineBlock(view,chapter,LineBlock.TAIL,this);
        block.setStr("");
        block.setHeight(fontHeight/2);
        Paragraph paragraph = new Paragraph();
        paragraph.getBlocks().add(block);

//        blocks.add(block);

        //加上一排分割线
//        String str="---------------------------------------------------------";
//        List<String> deng = breakLines(paint, str, MAXWIDTH, false);
//        block=new LineBlock(view,chapter,LineBlock.TAIL,this);
//        block.setStr(deng.get(0));
//        block.setHeight(fontHeight);
//        paragraph.getBlocks().add(block);
//        blocks.add(block);
        //加上作者有话说这行字
//        block=new LineBlock(view,chapter,LineBlock.TAIL,this);
//        block.setStr(ReaderConfig.getInstance().getTailTitle());
//        block.setHeight(fontHeight);
////        blocks.add(block);
//        paragraph.getBlocks().add(block);

        String[] strs=text.split("\n");
        for (int i = 0; i < strs.length; i++) {
            if(strs[i].trim().equals("")){
                block = new LineBlock(view, chapter, LineBlock.TAIL, this);
                block.setStr("");
                block.setHeight(fontHeight);
//                blocks.add(block);
                paragraph.getBlocks().add(block);
            }else{
                List<String> lines = breakLines(paint, strs[i], maxWidth, false);
                for (int j = 0; j < lines.size(); j++) {
                    block = new LineBlock(view, chapter, LineBlock.TAIL, this);
                    block.setStr(lines.get(j));
                    block.setHeight(fontHeight);
                    paragraph.getBlocks().add(block);
//                    blocks.add(block);
                }
            }
        }
        paragraphs.add(paragraph);

    }
}
