package com.chineseall.reader.lib.reader.entities;

import android.graphics.Paint;
import android.graphics.Paint.FontMetrics;
import android.text.TextUtils;

import com.chineseall.reader.lib.reader.view.ReaderView;

import java.util.List;

public class RTailTitle extends RText {
    public RTailTitle(Paint paint, String text) {
        super(paint, text);
    }

    @Override
    public void layout(ReaderView view, Chapter chapter, List<Paragraph> paragraphs, int maxWidth) {
        if (TextUtils.isEmpty(text)) {
            return;
        }
        FontMetrics fm = paint.getFontMetrics();
        int fontHeight = (int) Math.ceil(fm.descent - fm.ascent);
        LineBlock block = null;
        //加半个空行

        Paragraph paragraph = new Paragraph();

        block=new LineBlock(view,chapter,LineBlock.TAIL_TITLE,this);
        block.setStr(text);
        block.setHeight(fontHeight);
        paragraph.getBlocks().add(block);
        paragraphs.add(paragraph);

    }
}
