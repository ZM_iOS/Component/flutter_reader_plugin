package com.chineseall.reader.lib.reader.entities;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.FontMetrics;
import android.text.TextUtils;

import com.chineseall.reader.lib.reader.config.ReaderConfigWrapper;
import com.chineseall.reader.lib.reader.config.TangYuanSharedPrefUtils;
import com.chineseall.reader.lib.reader.utils.BreakLinesUtil;
import com.chineseall.reader.lib.reader.utils.StringUtil;
import com.chineseall.reader.lib.reader.view.ReaderView;

import java.util.List;
import java.util.Vector;

import static com.chineseall.reader.lib.reader.utils.BreakLinesUtil.breakParagraph;

public class RText extends RElement {
    protected Paint paint;
    protected String text = "";

    public RText(Paint paint, String text) {
        setText(paint, text);
    }

    public void setText(Paint paint, String text) {
        this.paint = paint;
        this.text = text;
    }

    public Paint getPaint() {
        return paint;
    }

    public String getText() {
        return text;
    }

    @Override
    public Page measureSize(Page page) {
        TxtLine[] lines = WordParser.parse(0, text, Page.PAGEWIDTH, paint);
        if (null == lines || lines.length == 0) { //不显示
            return page;
        }

        FontMetrics fm = paint.getFontMetrics();
        int fontHeight = (int) Math.ceil(fm.descent - fm.ascent);
        Vector<TxtLine> ls = new Vector<TxtLine>();
        Page p;

        int lineSpace = ReaderConfigWrapper.getInstance().getLineSpace();
        int paragraphSpace = ReaderConfigWrapper.getInstance().getParagraphSpace();

        //首先判断当前页是否能放下
        if (page.top + fontHeight > Page.PAGEHEIGHT) { //不能放下
            page.index++;
            page.top = page.bottom = 0;

            p = page.duplicate();
        } else { //能放下，则判断加上段间距后是否能放下至少一行
            if (page.top + fontHeight + paragraphSpace > Page.PAGEHEIGHT) { //最多只能放一行
                p = page.duplicate();
                p.top = Page.PAGEHEIGHT - fontHeight;

                page.index++;
                page.top = page.bottom = 0;
                pages.add(p);
                p = page.duplicate();
            } else {
                p = page.duplicate();
                p.top = page.top + (page.top == 0 ? 0 : paragraphSpace); //如果不是第一行，则需要加上段间距
                p.bottom = p.top;
            }
        }

        int index = 0;
        int rest = Page.PAGEHEIGHT - p.top; //可用空间
        ls = new Vector<TxtLine>();
        while (index < lines.length) {
            if (rest > fontHeight) {
                ls.add(lines[index]);
                rest -= fontHeight + lineSpace;
                p.bottom += fontHeight + lineSpace;
            } else { //当前页已经无法放下
                p.data = ls;
                pages.add(p);

                ls = new Vector<TxtLine>();
                ls.add(lines[index]);
                p = p.duplicate();
                p.index++;
                p.data = null;
                p.top = 0;
                p.bottom = fontHeight + lineSpace;
                rest = Page.PAGEHEIGHT - (fontHeight + lineSpace);
            }
            index++;
        }

        p.data = ls;
        pages.add(p);

        page = p.duplicate();
        page.data = null;
        page.top = p.bottom;

        return page;
    }

    @Override
    public void draw(Canvas canvas, int pagenumber) {
        FontMetrics fm = paint.getFontMetrics();
        int fontHeight = (int) Math.ceil(fm.descent - fm.ascent);

        int ps = pages.size();
        for (int i = 0; i < ps; i++) {
            Page p = pages.get(i);
            if (p.index == pagenumber) {
                int top = p.top;
                if (null != p.data && (p.data instanceof Vector)) {
                    Vector<TxtLine> lines = (Vector<TxtLine>) p.data;
                    for (int j = 0; j < lines.size(); j++) {
                        canvas.drawText(lines.get(j).str, 0, top + fontHeight, paint);
                        top += fontHeight + ReaderConfigWrapper.getInstance().getLineSpace();
                    }
                }
            }
        }
    }

    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < pages.size(); i++) {
            Page p = pages.get(i);
            Vector<TxtLine> lines = (Vector<TxtLine>) p.data;

            sb.append("第 " + p.index + " 页\n").append(merge(lines));
        }
        return sb.toString();
    }

    private String merge(Vector<TxtLine> lines) {
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < lines.size(); i++) {
            sb.append("\t").append(lines.get(i)).append("\n");
        }
        return sb.toString();
    }


    /**
     * 由于英文整体测量和拆字测量的结果不一致 故统一采用拆字测量
     * @param lineContent
     * @return
     */
    private float measureText(String lineContent){


        float stringWidth = 0;
        char[] chars = lineContent.toCharArray();

        for (int i = 0 , size = chars.length; i < size; i++) {
            stringWidth += paint.measureText(String.valueOf(chars[i]));
        }
        return stringWidth;
    }

    public void layout(ReaderView readerView, Chapter chapter, List<Paragraph> paragraphs, int maxWidth) {
        if (!TextUtils.isEmpty(this.text)) {
//            FontMetrics fontMetrics = this.paint.getFontMetrics();
//            int ceil = (int) Math.ceil((double) (fontMetrics.descent - fontMetrics.ascent));
            int charWidth = (int) paint.measureText("啊");

            // 分段
            List<String> strings = breakParagraph(text);

            int contentParaSize = strings.size();

            // 解决本地书最后一段是空字符串导致,最后一页空白页,并且导致朗读无法翻页
            if (contentParaSize > 0 && contentParaSize < strings.size() - 1 && StringUtil.isBlank(strings.get(contentParaSize - 1))) {
                contentParaSize = contentParaSize - 1;
            }

            String fontType = TangYuanSharedPrefUtils.getInstance().getFontType();
            boolean space = false;
            if (fontType.contains("alibaba")) {
                // 阿里巴巴普惠体全角空格只占半个字的空间，导致显示错位 前两个空格特殊处理
                space = true;
            }

//            int lineSpace = ReaderConfigWrapper.getInstance().getLineSpace();
            int lineSpace = charWidth * 14 / 100;

            for (int i = 0; i < contentParaSize; i++) {
                Paragraph paragraph = new Paragraph();

                String paraContent = strings.get(i);
                if (StringUtil.isBlank(paraContent)) {
                    continue;
                }
                paragraph.setContent(paraContent);
                // 分行
                List<String> breakLines = BreakLinesUtil.breakLines(this.paint, paraContent, (float) maxWidth, false);

                int lineCount = breakLines.size();
                for (int j = 0; j < lineCount; j++) {

                    LineBlock lineBlock = getLineBlock(readerView, chapter, j == lineCount - 1);
                    lineBlock.setParagraph(paragraph);
                    lineBlock.setHeight(charWidth);

                    String lineContent = breakLines.get(j);
                    char[] chars = lineContent.toCharArray();

                    lineBlock.setStr(lineContent);
                    int length = lineContent.length();
                    char lastChar = chars[length - 1];
                    // 最后一个字符符号处理
                    boolean punctuation = StringUtil.isNeedHandler(lastChar);

                    float[] pos = new float[length * 2];
                    // 字符串实际宽度
                    float originContentWidth = measureText(lineContent);
                    if (j == 0 && space) {
                        // 阿里巴巴字体  第一行宽度计算有问题  会多出一个字
                        originContentWidth = originContentWidth + charWidth;
                    }
                    // 剩余空间
                    float remainingWidth = maxWidth - originContentWidth;
                    if (punctuation) { // 65311 ? 8221 ”   8216 ‘ 8220 “
                        float v4 = paint.measureText(String.valueOf(lastChar));
                        if (lastChar == 65311 || lastChar == 8216) {
                            remainingWidth = remainingWidth + v4 / 4; // 增加1/4符号的剩余空间
                        } else {
                            remainingWidth = remainingWidth + v4 / 2; // 增加半个符号的剩余空间
                        }
                    }
                    float charSpace; // 字间距
                    if (j == 0) { // 段落第一行
                        charSpace = remainingWidth / (length - 1 - 2);
                    } else {
                        charSpace = remainingWidth / (length - 1);
                    }

                    float startX = ReaderConfigWrapper.getInstance().getPadding();
                    float oneFontWidth = paint.measureText("啊");

                    if (j == lineCount - 1) { // 最后一行字间距不处理
                        charSpace = 0;
                        lineBlock.setWidth((int) originContentWidth);
                    } else {
                        if (j == 0) { // 第一行宽度需要减去空格缩进
                            lineBlock.setWidth((int) (maxWidth - startX * 2 - oneFontWidth * 2));
                        } else {
                            lineBlock.setWidth((int) (maxWidth - startX * 2));
                        }
                    }
                    int startIndex = 0;

                    if (j == 0) { // 首行设置缩进的两个字符x值
                        startIndex = 2;
                        pos[0] = startX;
                        pos[2] = startX + oneFontWidth;
                        startX = pos[2] + oneFontWidth;
                    }
                    for (int k = startIndex; k < length; k++) {
                        int tmpIndex = k * 2;
                        pos[tmpIndex] = startX;
                        startX = startX + paint.measureText(String.valueOf(chars[k])) + charSpace;
                    }
                    lineBlock.setPos(pos);
                    paragraph.getBlocks().add(lineBlock);
                }
                // 最后一个段落的最后一行不加空字符串,否则会多画一页空白页
                if (i < contentParaSize - 1) {
                    LineBlock lastLine = new LineBlock(readerView, chapter, LineBlock.TEXT, this);
                    float pos[] = new float[2];
                    lastLine.setPos(pos);
                    lastLine.setHeight(lineSpace);//控制此行 可以控制段间距
                    lastLine.setStr("");
                    paragraph.getBlocks().add(lastLine);
                }
                paragraph.setContentType(true);
                paragraphs.add(paragraph);
            }


        }
    }

    protected LineBlock getLineBlock(ReaderView readerView, Chapter chapter, boolean isLatestLine) {
        return new LineBlock(readerView, chapter, LineBlock.TEXT, this);
    }

    private void removeLastBlank(List<LineBlock> arrayList) {
        LineBlock lineBlock = arrayList.get(arrayList.size() - 1);
        if (StringUtil.isBlank(lineBlock.getStr())) {
            arrayList.remove(arrayList.size() - 1);
            removeLastBlank(arrayList);

        }
    }


    protected boolean isQuote(char c) {
        return c == ',' || c == '.' || c == '?' || c == '!' || c == '，' || c == '。' || c == '？' || c == '！' || c == '…';
    }

}
