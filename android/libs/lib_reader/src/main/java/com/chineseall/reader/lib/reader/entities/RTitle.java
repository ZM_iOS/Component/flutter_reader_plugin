package com.chineseall.reader.lib.reader.entities;

import android.graphics.Paint;
import android.text.TextUtils;

import com.chineseall.reader.lib.reader.config.ReaderConfigWrapper;
import com.chineseall.reader.lib.reader.view.ReaderView;

import java.util.List;

import static com.chineseall.reader.lib.reader.utils.BreakLinesUtil.breakLines;

public class RTitle extends RText {
    public RTitle(Paint paint, String text) {
        super(paint, text);
    }

    @Override
    public void layout(ReaderView view, Chapter chapter, List<Paragraph> paragraphs, int maxWidth) {
        if (TextUtils.isEmpty(text)) {
            return;
        }
        Paint.FontMetrics fm = paint.getFontMetrics();
        int fontHeight = (int) Math.ceil(fm.descent - fm.ascent);
        LineBlock block = null;
        Paragraph paragraph = new Paragraph();

        List<String> lines = breakLines(paint, text, maxWidth, false);
        {
            LineBlock lastLine = new LineBlock(view, chapter, LineBlock.TITLE, this);
            lastLine.setHeight(0);
            lastLine.setStr("");
            float pos[] = new float[2];
            lastLine.setPos(pos);
            paragraph.getBlocks().add(lastLine);
        }
        for (int i = 0; i < lines.size(); i++) {
            block = new LineBlock(view, chapter, LineBlock.TITLE, this);
            String str = lines.get(i);
            block.setStr(str);
            char[] chars = str.toCharArray();
            block.setHeight(fontHeight);
            int length = str.length();
            float pos[] = new float[length * 2];
//            float v = paint.measureText(str);
//            float v1 = MAXWIDTH - v;
            float startX = ReaderConfigWrapper.getInstance().getPadding();
            for (int j = 0; j < length; j++) {
                int tmpIndex = j * 2;
                if (tmpIndex % 2 == 0) {
                    pos[tmpIndex] = startX;
                    startX = paint.measureText(String.valueOf(chars[j]));
                }
            }
            block.setPos(pos);
            paragraph.getBlocks().add(block);
        }
        {
            LineBlock lastLine = new LineBlock(view, chapter, LineBlock.TITLE, this);
            lastLine.setHeight(0);
            lastLine.setStr("");
            float pos[] = new float[2];
            lastLine.setPos(pos);
            paragraph.getBlocks().add(lastLine);
        }
        {
            LineBlock lastLine = new LineBlock(view, chapter, LineBlock.TITLE, this);
            lastLine.setHeight(0);
            lastLine.setStr("");
            float pos[] = new float[2];
            lastLine.setPos(pos);
            paragraph.getBlocks().add(lastLine);
        }

        paragraph.setContent(text);
        paragraphs.add(paragraph);
    }
}
