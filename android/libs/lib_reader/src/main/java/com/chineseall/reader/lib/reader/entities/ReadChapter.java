package com.chineseall.reader.lib.reader.entities;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class ReadChapter extends ChapterRead implements Parcelable{

    private static final long serialVersionUID = 1L;

    public static final int LOADTYPE_LOADED = 0X10010;	//65552
    public static final int LOADTYPE_FAIL = 0x10020;	//65568
    public static final int LOADTYPE_ING = 0x10030;		//65584
    public static final int LOADTYPE_UNLOAD = 0X10040;	//65600
    private List<String> attchmentFileNameList = new ArrayList<String>();
    private String previewFilePath = null;
    public int LoadType = -1;
    ArrayList<TyCharSequence> lines = new ArrayList<>();
    private boolean showstatus;
    public PageMode type = PageMode.Page; // 仅在显示时使用

    public ReadChapter(){}

    public boolean isShowstatus() {
        return showstatus;
    }

    public void setShowstatus(boolean showstatus) {
        this.showstatus = showstatus;
    }

    public List<String> getAttchmentFileNameList() {
        return attchmentFileNameList;
    }

    public void setAttchmentFileNameList(List<String> attchmentFileNameList) {
        this.attchmentFileNameList = attchmentFileNameList;
    }

    public String getPreviewFilePath() {
        return previewFilePath;
    }

    public void setPreviewFilePath(String previewFilePath) {
        this.previewFilePath = previewFilePath;
    }

    public File getPreviewFile() {
        if (previewFilePath == null)
            return null;
        File file = new File(previewFilePath);
        return file;
    }

    public boolean previewFileIsExist() {
        if (previewFilePath == null)
            return false;
        return new File(previewFilePath).exists();
    }

    public ArrayList<TyCharSequence> getChapterLines() {
        return lines;
    }

    public void putChapterLines(ArrayList<TyCharSequence> content) {
        lines.clear();
        lines.addAll(content);
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof ReadChapter) {
            if (o == this) {
                return true;
            }
            return getChapterId().equals(((ReadChapter) o).getChapterId());
        }
        return false;
    }


    public ReadChapter(Parcel in) {
        setChapterId(in.readString());
        setBookId(in.readString());
        setChapterName(in.readString());
        setWordCount(in.readLong());
        setImageCount(in.readInt());
        setTimeStamp_value(in.readLong());
        setTimeStamplocal(in.readString());
        setOrderValue(in.readDouble());
        setHtmlUrl(in.readString());
        setAttchmentnames(in.readString());
        setLocalRead(in.readInt());
        setSubscript_flag(in.readInt());
        setUser_subscript_flag(in.readInt());
        setChapter_coins(in.readInt());
        setPromot_chapter_coins(in.readInt());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(getChapterId());
        dest.writeString(getBookId());
        dest.writeString(getChapterName());
        dest.writeLong(getWordCount());
        dest.writeInt(getImageCount());
        dest.writeLong(getTimeStamp_value());
        dest.writeString(getTimeStamplocal());
        dest.writeDouble(getOrderValue());
        dest.writeString(getHtmlUrl());
        dest.writeString(getAttchmentnames());
        dest.writeInt(getLocalRead());
        dest.writeInt(getSubscript_flag());
        dest.writeInt(getUser_subscript_flag());
        dest.writeInt(getChapter_coins());
        dest.writeInt(getPromot_chapter_coins());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ReadChapter> CREATOR = new Creator<ReadChapter>(){
        @Override
        public ReadChapter createFromParcel(Parcel source) {
            return new ReadChapter(source);
        }

        @Override
        public ReadChapter[] newArray(int size) {
            return new ReadChapter[size];
        }
    };


}
