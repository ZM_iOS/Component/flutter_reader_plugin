package com.chineseall.reader.lib.reader.entities;

////////////////////////////////////////
//
// 保存每一行的信息
// offest： 此行在文件中的偏移位置
// lenght： 此行的长度
// text : 此行的文本内容
//
// //////////////////////////////////////
public class TxtLine {
    public int offset = 0;
    public int length = 0;
    public String str = null;

    public TxtLine() {
        this.offset = 0;
        this.length = 0;
    }

    public TxtLine(int offset, int lenght, String str) {
        this.offset = offset;
        this.length = lenght;
        this.str = str;
    }

    @Override
    public String toString() {
        return "" + str;
    }
}
