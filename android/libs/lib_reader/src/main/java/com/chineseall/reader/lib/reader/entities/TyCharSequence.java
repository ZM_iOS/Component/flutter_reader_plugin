package com.chineseall.reader.lib.reader.entities;

import android.graphics.Rect;
import android.os.Parcelable;

public interface TyCharSequence extends Parcelable {

    public enum TYPE {
        TEXT,
        IMAGE
    }

    Rect getBound();

    char[] getCharArray();

    void setBound(Rect rect);

    String toString();
}
