package com.chineseall.reader.lib.reader.entities;

import android.graphics.Paint;

import java.util.Vector;

public class WordParser {
    private WordParser() {
    }

    private static int getSuiteChars(StringBuffer sb, float width, Paint paint) {
        int index = 0;
        while (index < sb.length()) {
            width -= paint.measureText(sb, index, index + 1);
            if (width < 0) {
                return index;
            }
            index++;
        }

        return index;
    }

    /**
     * 对字符串进行分割
     * @param offset int
     * @param src String utf-8格式的字符串
     * @param viewWidth int
     * @return String[]
     */
    public static TxtLine[] parse(int offset, String src, int viewWidth, Paint paint) {
        if (null == src || "".equals(src)) {
            return null;
        }
        Vector<TxtLine> txtLine = new Vector<TxtLine>();
        char[] chs = src.toCharArray();
        int index = 0;
        float width = 0;
        StringBuffer sb = new StringBuffer();
        StringBuffer english = new StringBuffer();
        int pos = offset;
        int lineLen = 0;
        float chwidth = paint.measureText("\u5f20");

        while (true) {
            if (index >= chs.length) {
                break;
            }
            char ch = chs[index];
            index++;

            if (ch >= 'A' && ch <= 'z') { //英文字母
                english.append(ch);
            } else if (ch == 0x0D) {
                if (english.length() > 0) {
                    float w = paint.measureText(english.toString());
                    while (w > viewWidth) { //本身这个单词就无法在一行中显示完整，切割
                        int first = getSuiteChars(english, (viewWidth - width), paint); //当前行可以显示的字符个数
                        for (int i = 0; i < first; i++) {
                            sb.append(english.charAt(i));
                        }
                        //vector.addElement(sb.toString());

                        lineLen += first;
                        txtLine.addElement(new TxtLine(pos, lineLen, sb.toString()));
                        pos += lineLen;
                        lineLen = 0;

                        width = 0;
                        sb.delete(0, sb.length());
                        english.delete(0, first);
                        w = paint.measureText(english.toString());
                    }

                    //把回车\r\n添加到后面
                    english.append(ch);
                    if (index < chs.length && chs[index] == 0x0A) {
                        english.append(chs[index]);
                        index++;
                    }
                    //end

                    if (width + w > viewWidth) { //此行无法显示完整
                        //vector.addElement(sb.toString());
                        //vector.addElement(english.toString());

                        txtLine.addElement(new TxtLine(pos, lineLen, sb.toString()));
                        pos += lineLen;
                        lineLen = english.length();
                        txtLine.addElement(new TxtLine(pos, lineLen, english.toString()));
                        pos += lineLen;
                    } else {
                        sb.append(english.toString());
                        //vector.addElement(sb.toString());

                        lineLen += english.length();
                        txtLine.addElement(new TxtLine(pos, lineLen, sb.toString()));
                        pos += lineLen;
                    }
                    sb.delete(0, sb.length());
                    english.delete(0, english.length());
                    width = 0;
                    lineLen = 0;
                } else {
                    lineLen++;
                }
                continue;
            } else if (ch == 0x0A) { //需要换行
                lineLen++;
                float w = paint.measureText(english.toString());
                while (w > viewWidth) { //本身这个单词就无法在一行中显示完整，切割
                    int first = getSuiteChars(english, (viewWidth - width), paint); //当前行可以显示的字符个数
                    for (int i = 0; i < first; i++) {
                        sb.append(english.charAt(i));
                    }
                    //vector.addElement(sb.toString());

                    lineLen += first;
                    txtLine.addElement(new TxtLine(pos, lineLen, sb.toString()));
                    pos += lineLen;
                    lineLen = 0;

                    width = 0;
                    sb = new StringBuffer();
                    english.delete(0, first);
                    w = paint.measureText(english.toString());
                }
                if (width + w > viewWidth) { //此行无法显示完整
                    //vector.addElement(sb.toString());
                    //vector.addElement(english.toString());

                    txtLine.addElement(new TxtLine(pos, lineLen, sb.toString()));
                    pos += lineLen;
                    lineLen = english.length();
                    txtLine.addElement(new TxtLine(pos, lineLen, english.toString()));
                    pos += lineLen;
                } else {
                    sb.append(english.toString());
                    //vector.addElement(sb.toString());

                    lineLen += english.length();
                    txtLine.addElement(new TxtLine(pos, lineLen, sb.toString()));
                    pos += lineLen;
                }
                sb.delete(0, sb.length());
                english.delete(0, english.length());
                width = 0;
                lineLen = 0;
            } else { //其它字符
                if (english.length() > 0) { //检查english缓冲是否为空
                    float w = paint.measureText(english.toString());
                    while (w > viewWidth) { //本身这个单词就无法在一行中显示完整，切割
                        int first = getSuiteChars(english, (viewWidth - width), paint); //当前行可以显示的字符个数
                        for (int i = 0; i < first; i++) {
                            sb.append(english.charAt(i));
                        }
                        //vector.addElement(sb.toString());

                        lineLen += first;
                        txtLine.addElement(new TxtLine(pos, lineLen, sb.toString()));
                        pos += lineLen;
                        lineLen = 0;

                        width = 0;
                        sb.delete(0, sb.length());
                        english.delete(0, first);
                        w = paint.measureText(english.toString());
                    }
                    if (width + w > viewWidth) { //此行无法显示完整
                        //vector.addElement(sb.toString());
                        txtLine.addElement(new TxtLine(pos, lineLen, sb.toString()));
                        pos += lineLen;
                        lineLen = english.length();
                        sb = new StringBuffer(english.toString());
                        //english.delete(0, english.length());
                        width = w;
                    } else {
                        sb.append(english.toString());
                        width += w;

                        lineLen += english.length();
                    }
                    english.delete(0, english.length());
                }

                //检查当前字符是否为汉字
                float charwidth = chwidth;
                if (ch < 127) {
                    charwidth = paint.measureText("" + ch);
                }

                int cbl = 1;

                if (ch <= 0x7f) {
                    cbl = 1;
                } else if (ch <= 0x7ff) {
                    cbl = 2;
                } else if (ch <= 0xffff) {
                    cbl = 3;
                } else if (ch <= 0x1FFFFF) {
                    cbl = 4;
                } else if (ch <= 0x3FFFFFF) {
                    cbl = 5;
                } else {
                    cbl = 6;
                }

                if (width + charwidth > viewWidth) { //当前行无法显示完整
                    //vector.addElement(sb.toString());
                    txtLine.addElement(new TxtLine(pos, lineLen, sb.toString()));
                    pos += lineLen;
                    sb.delete(0, sb.length());
                    sb.append(ch);
                    lineLen = cbl;
                    width = charwidth;
                } else {
                    sb.append(ch);
                    lineLen += cbl;
                    width += charwidth;
                }
            }
        }

        if (english.length() > 0) {
            float w = paint.measureText(english.toString());
            while (w > viewWidth) { //本身这个单词就无法在一行中显示完整，切割
                int first = getSuiteChars(english, (viewWidth - width), paint); //当前行可以显示的字符个数
                for (int i = 0; i < first; i++) {
                    sb.append(english.charAt(i));
                }
                //vector.addElement(sb.toString());

                lineLen += first;
                txtLine.addElement(new TxtLine(pos, lineLen, sb.toString()));
                pos += lineLen;
                lineLen = 0;

                width = 0;
                sb = new StringBuffer();
                english.delete(0, first);
                w = paint.measureText(english.toString());
            }
            if (width + w > viewWidth) { //此行无法显示完整
                txtLine.addElement(new TxtLine(pos, lineLen, sb.toString()));
                pos += lineLen;
                lineLen = english.length();
                txtLine.addElement(new TxtLine(pos, lineLen, english.toString()));
            } else {
                sb.append(english.toString());
                //vector.addElement(sb.toString());

                lineLen += english.length();
                txtLine.addElement(new TxtLine(pos, lineLen, sb.toString()));
            }
        } else {
            txtLine.addElement(new TxtLine(pos, lineLen, sb.toString()));
        }
        TxtLine[] str = new TxtLine[txtLine.size()];
        for (int i = 0; i < str.length; i++) {
            str[i] = (TxtLine) txtLine.elementAt(i);
            //str[i].bytebuffer = SysDotFont.str2bytes(str[i].str);
        }

        txtLine = null;
        return str;
    }

    public final static TxtLine[] parse(Paint paint, String str, int maxwidth) {
        if (null == str || "".equals(str)) {
            return new TxtLine[] { new TxtLine(0, 0, "") };
        }
        Vector<TxtLine> txtLine = new Vector<TxtLine>();
        char[] chs = str.toCharArray();
        StringBuffer sb = new StringBuffer();
        float w = 0;
        for (int i = 0; i < chs.length; i++) {
            char ch = chs[i];
            float chw = paint.measureText(chs, i, i + 1);
            if (ch == '\n' || w + chw > maxwidth) {
                TxtLine line = new TxtLine();
                line.str = sb.toString();
                sb.delete(0, sb.length());
                txtLine.addElement(line);

                sb.append(ch);
                w = chw;
            } else if (ch == '\r') {

            } else {
                sb.append(ch);
                w += chw;
            }
        }

        if (sb.length() > 0) {
            TxtLine line = new TxtLine();
            line.str = sb.toString();
            txtLine.addElement(line);
        }

        TxtLine[] lines = new TxtLine[txtLine.size()];
        for (int i = 0; i < lines.length; i++) {
            lines[i] = (TxtLine) txtLine.elementAt(i);
        }

        txtLine = null;
        return lines;
    }
}
