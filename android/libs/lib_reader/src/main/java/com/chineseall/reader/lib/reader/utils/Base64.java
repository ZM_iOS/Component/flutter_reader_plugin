package com.chineseall.reader.lib.reader.utils;

import java.io.UnsupportedEncodingException;

public class Base64 {

   // $FF: synthetic field
   static final boolean $assertionsDisabled = false;
   public static final int CRLF = 4;
   public static final int DEFAULT = 0;
   public static final int NO_CLOSE = 16;
   public static final int NO_PADDING = 1;
   public static final int NO_WRAP = 2;
   public static final int URL_SAFE = 8;


   static {
      boolean var0;
      if(!Base64.class.desiredAssertionStatus()) {
         var0 = true;
      } else {
         var0 = false;
      }

//      $assertionsDisabled = var0;
   }

   public static byte[] decode(String var0, int var1) {
      return decode(var0.getBytes(), var1);
   }

   public static byte[] decode(byte[] var0, int var1) {
      return decode(var0, 0, var0.length, var1);
   }

   public static byte[] decode(byte[] var0, int var1, int var2, int var3) {
      Base64.Decoder var4 = new Base64.Decoder(var3, new byte[var2 * 3 / 4]);
      if(!var4.process(var0, var1, var2, true)) {
         throw new IllegalArgumentException("bad base-64");
      } else {
         byte[] var5;
         if(var4.op == var4.output.length) {
            var5 = var4.output;
         } else {
            var5 = new byte[var4.op];
            System.arraycopy(var4.output, 0, var5, 0, var4.op);
         }

         return var5;
      }
   }

   public static byte[] encode(byte[] var0, int var1) {
      return encode(var0, 0, var0.length, var1);
   }

   public static byte[] encode(byte[] var0, int var1, int var2, int var3) {
      Base64.Encoder var4 = new Base64.Encoder(var3, (byte[])null);
      int var5 = 4 * (var2 / 3);
      if(var4.do_padding) {
         if(var2 % 3 > 0) {
            var5 += 4;
         }
      } else {
         switch(var2 % 3) {
         case 0:
         default:
            break;
         case 1:
            var5 += 2;
            break;
         case 2:
            var5 += 3;
         }
      }

      if(var4.do_newline && var2 > 0) {
         int var7 = 1 + (var2 - 1) / 57;
         byte var8;
         if(var4.do_cr) {
            var8 = 2;
         } else {
            var8 = 1;
         }

         var5 += var8 * var7;
      }

      var4.output = new byte[var5];
      var4.process(var0, var1, var2, true);
      if(!$assertionsDisabled && var4.op != var5) {
         throw new AssertionError();
      } else {
         return var4.output;
      }
   }

   public static String encodeToString(byte[] var0, int var1) {
      try {
         String var2 = new String(encode(var0, var1), "US-ASCII");
         return var2;
      } catch (UnsupportedEncodingException var4) {
         throw new AssertionError(var4);
      }
   }

   public static String encodeToString(byte[] var0, int var1, int var2, int var3) {
      try {
         String var4 = new String(encode(var0, var1, var2, var3), "US-ASCII");
         return var4;
      } catch (UnsupportedEncodingException var6) {
         throw new AssertionError(var6);
      }
   }

   static class Encoder extends Base64.Coder {

      // $FF: synthetic field
      static  boolean $assertionsDisabled = false;
      private static final byte[] ENCODE;
      private static final byte[] ENCODE_WEBSAFE;
      public static final int LINE_GROUPS = 19;
      private final byte[] alphabet;
      private int count;
      public final boolean do_cr;
      public final boolean do_newline;
      public final boolean do_padding;
      private final byte[] tail;
      int tailLen;


      static {
         boolean var0;
         if(!Base64.class.desiredAssertionStatus()) {
            var0 = true;
         } else {
            var0 = false;
         }

         $assertionsDisabled = var0;
         byte[] var1 = new byte[]{(byte)65, (byte)66, (byte)67, (byte)68, (byte)69, (byte)70, (byte)71, (byte)72, (byte)73, (byte)74, (byte)75, (byte)76, (byte)77, (byte)78, (byte)79, (byte)80, (byte)81, (byte)82, (byte)83, (byte)84, (byte)85, (byte)86, (byte)87, (byte)88, (byte)89, (byte)90, (byte)97, (byte)98, (byte)99, (byte)100, (byte)101, (byte)102, (byte)103, (byte)104, (byte)105, (byte)106, (byte)107, (byte)108, (byte)109, (byte)110, (byte)111, (byte)112, (byte)113, (byte)114, (byte)115, (byte)116, (byte)117, (byte)118, (byte)119, (byte)120, (byte)121, (byte)122, (byte)48, (byte)49, (byte)50, (byte)51, (byte)52, (byte)53, (byte)54, (byte)55, (byte)56, (byte)57, (byte)43, (byte)47};
         ENCODE = var1;
         byte[] var2 = new byte[]{(byte)65, (byte)66, (byte)67, (byte)68, (byte)69, (byte)70, (byte)71, (byte)72, (byte)73, (byte)74, (byte)75, (byte)76, (byte)77, (byte)78, (byte)79, (byte)80, (byte)81, (byte)82, (byte)83, (byte)84, (byte)85, (byte)86, (byte)87, (byte)88, (byte)89, (byte)90, (byte)97, (byte)98, (byte)99, (byte)100, (byte)101, (byte)102, (byte)103, (byte)104, (byte)105, (byte)106, (byte)107, (byte)108, (byte)109, (byte)110, (byte)111, (byte)112, (byte)113, (byte)114, (byte)115, (byte)116, (byte)117, (byte)118, (byte)119, (byte)120, (byte)121, (byte)122, (byte)48, (byte)49, (byte)50, (byte)51, (byte)52, (byte)53, (byte)54, (byte)55, (byte)56, (byte)57, (byte)45, (byte)95};
         ENCODE_WEBSAFE = var2;
      }

      public Encoder(int var1, byte[] var2) {
         boolean var3 = true;
         this.output = var2;
         boolean var4;
         if((var1 & 1) == 0) {
            var4 = var3;
         } else {
            var4 = false;
         }

         this.do_padding = var4;
         boolean var5;
         if((var1 & 2) == 0) {
            var5 = var3;
         } else {
            var5 = false;
         }

         this.do_newline = var5;
         if((var1 & 4) == 0) {
            var3 = false;
         }

         this.do_cr = var3;
         byte[] var6;
         if((var1 & 8) == 0) {
            var6 = ENCODE;
         } else {
            var6 = ENCODE_WEBSAFE;
         }

         this.alphabet = var6;
         this.tail = new byte[2];
         this.tailLen = 0;
         byte var7;
         if(this.do_newline) {
            var7 = 19;
         } else {
            var7 = -1;
         }

         this.count = var7;
      }

      public int maxOutputSize(int var1) {
         return 10 + var1 * 8 / 5;
      }

      public boolean process(byte[] var1, int var2, int var3, boolean var4) {
         byte[] var5 = this.alphabet;
         byte[] var6 = this.output;
         byte var7 = 0;
         int var8 = this.count;
         int var9 = var2;
         int var10 = var3 + var2;
         int var11 = -1;
         switch(this.tailLen) {
         case 0:
         default:
            break;
         case 1:
            if(var2 + 2 <= var10) {
               int var57 = (255 & this.tail[0]) << 16;
               int var58 = var2 + 1;
               int var59 = var57 | (255 & var1[var2]) << 8;
               var9 = var58 + 1;
               var11 = var59 | 255 & var1[var58];
               this.tailLen = 0;
            }
            break;
         case 2:
            if(var2 + 1 <= var10) {
               int var12 = (255 & this.tail[0]) << 16 | (255 & this.tail[1]) << 8;
               int var13 = var2 + 1;
               var11 = var12 | 255 & var1[var2];
               this.tailLen = 0;
               var9 = var13;
            }
         }

         int var14;
         int var15;
         int var60;
         if(var11 != -1) {
            int var53 = 0 + 1;
            var6[var7] = var5[63 & var11 >> 18];
            int var54 = var53 + 1;
            var6[var53] = var5[63 & var11 >> 12];
            int var55 = var54 + 1;
            var6[var54] = var5[63 & var11 >> 6];
            var60 = var55 + 1;
            var6[var55] = var5[var11 & 63];
            --var8;
            if(var8 == 0) {
               if(this.do_cr) {
                  int var56 = var60 + 1;
                  var6[var60] = 13;
                  var60 = var56;
               }

               var15 = var60 + 1;
               var6[var60] = 10;
               var8 = 19;
               var14 = var9;
            } else {
               var14 = var9;
               var15 = var60;
            }
         } else {
            var14 = var9;
            var15 = var7;
         }

         while(true) {
            while(var14 + 3 <= var10) {
               int var51 = (255 & var1[var14]) << 16 | (255 & var1[var14 + 1]) << 8 | 255 & var1[var14 + 2];
               var6[var15] = var5[63 & var51 >> 18];
               var6[var15 + 1] = var5[63 & var51 >> 12];
               var6[var15 + 2] = var5[63 & var51 >> 6];
               var6[var15 + 3] = var5[var51 & 63];
               var9 = var14 + 3;
               var60 = var15 + 4;
               --var8;
               if(var8 != 0) {
                  var14 = var9;
                  var15 = var60;
               } else {
                  if(this.do_cr) {
                     int var52 = var60 + 1;
                     var6[var60] = 13;
                     var60 = var52;
                  }

                  var15 = var60 + 1;
                  var6[var60] = 10;
                  var8 = 19;
                  var14 = var9;
               }
            }

            int var17;
            if(var4) {
               int var25;
               label110: {
                  int var35;
                  if(var14 - this.tailLen == var10 - 1) {
                     int var42 = 0;
                     byte var43;
                     if(this.tailLen > 0) {
                        byte[] var49 = this.tail;
                        int var50 = 0 + 1;
                        var43 = var49[0];
                        var42 = var50;
                        var25 = var14;
                     } else {
                        var25 = var14 + 1;
                        var43 = var1[var14];
                     }

                     int var44 = (var43 & 255) << 4;
                     this.tailLen -= var42;
                     int var45 = var15 + 1;
                     var6[var15] = var5[63 & var44 >> 6];
                     int var46 = var45 + 1;
                     var6[var45] = var5[var44 & 63];
                     if(this.do_padding) {
                        int var48 = var46 + 1;
                        var6[var46] = 61;
                        var46 = var48 + 1;
                        var6[var48] = 61;
                     }

                     var17 = var46;
                     if(!this.do_newline) {
                        break label110;
                     }

                     if(this.do_cr) {
                        int var47 = var46 + 1;
                        var6[var46] = 13;
                        var17 = var47;
                     }

                     var35 = var17 + 1;
                     var6[var17] = 10;
                  } else {
                     if(var14 - this.tailLen != var10 - 2) {
                        if(this.do_newline && var15 > 0 && var8 != 19) {
                           int var26;
                           if(this.do_cr) {
                              var26 = var15 + 1;
                              var6[var15] = 13;
                           } else {
                              var26 = var15;
                           }

                           var15 = var26 + 1;
                           var6[var26] = 10;
                        }

                        var25 = var14;
                        var17 = var15;
                        break label110;
                     }

                     int var27 = 0;
                     byte var28;
                     if(this.tailLen > 1) {
                        byte[] var40 = this.tail;
                        int var41 = 0 + 1;
                        var28 = var40[0];
                        var27 = var41;
                        var25 = var14;
                     } else {
                        var25 = var14 + 1;
                        var28 = var1[var14];
                     }

                     int var29 = (var28 & 255) << 10;
                     byte var31;
                     if(this.tailLen > 0) {
                        byte[] var38 = this.tail;
                        int var39 = var27 + 1;
                        var31 = var38[var27];
                        var27 = var39;
                     } else {
                        int var30 = var25 + 1;
                        var31 = var1[var25];
                        var25 = var30;
                     }

                     int var32 = var29 | (var31 & 255) << 2;
                     this.tailLen -= var27;
                     int var33 = var15 + 1;
                     var6[var15] = var5[63 & var32 >> 12];
                     int var34 = var33 + 1;
                     var6[var33] = var5[63 & var32 >> 6];
                     var17 = var34 + 1;
                     var6[var34] = var5[var32 & 63];
                     if(this.do_padding) {
                        int var37 = var17 + 1;
                        var6[var17] = 61;
                        var17 = var37;
                     }

                     if(!this.do_newline) {
                        break label110;
                     }

                     if(this.do_cr) {
                        int var36 = var17 + 1;
                        var6[var17] = 13;
                        var17 = var36;
                     }

                     var35 = var17 + 1;
                     var6[var17] = 10;
                  }

                  var17 = var35;
               }

               if(!$assertionsDisabled && this.tailLen != 0) {
                  throw new AssertionError();
               }

               if(!$assertionsDisabled && var25 != var10) {
                  throw new AssertionError();
               }
            } else if(var14 == var10 - 1) {
               byte[] var22 = this.tail;
               int var23 = this.tailLen;
               this.tailLen = var23 + 1;
               var22[var23] = var1[var14];
               var17 = var15;
            } else {
               if(var14 == var10 - 2) {
                  byte[] var18 = this.tail;
                  int var19 = this.tailLen;
                  this.tailLen = var19 + 1;
                  var18[var19] = var1[var14];
                  byte[] var20 = this.tail;
                  int var21 = this.tailLen;
                  this.tailLen = var21 + 1;
                  var20[var21] = var1[var14 + 1];
               }

               var17 = var15;
            }

            this.op = var17;
            this.count = var8;
            return true;
         }
      }
   }

   abstract static class Coder {

      public int op;
      public byte[] output;


      public abstract int maxOutputSize(int var1);

      public abstract boolean process(byte[] var1, int var2, int var3, boolean var4);
   }

   static class Decoder extends Base64.Coder {

      private static final int[] DECODE;
      private static final int[] DECODE_WEBSAFE;
      private static final int EQUALS = -2;
      private static final int SKIP = -1;
      private final int[] alphabet;
      private int state;
      private int value;


      static {
         int[] var0 = new int[]{-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 62, -1, -1, -1, 63, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, -1, -1, -1, -2, -1, -1, -1, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, -1, -1, -1, -1, -1, -1, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1};
         DECODE = var0;
         int[] var1 = new int[]{-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 62, -1, -1, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, -1, -1, -1, -2, -1, -1, -1, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, -1, -1, -1, -1, 63, -1, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1};
         DECODE_WEBSAFE = var1;
      }

      public Decoder(int var1, byte[] var2) {
         this.output = var2;
         int[] var3;
         if((var1 & 8) == 0) {
            var3 = DECODE;
         } else {
            var3 = DECODE_WEBSAFE;
         }

         this.alphabet = var3;
         this.state = 0;
         this.value = 0;
      }

      public int maxOutputSize(int var1) {
         return 10 + var1 * 3 / 4;
      }

      public boolean process(byte[] var1, int var2, int var3, boolean var4) {
         boolean var13;
         if(this.state == 6) {
            var13 = false;
         } else {
            int var5 = var2;
            int var6 = var3 + var2;
            int var7 = this.state;
            int var8 = this.value;
            int var9 = 0;
            byte[] var10 = this.output;
            int[] var11 = this.alphabet;

            while(true) {
               int var12;
               if(var5 < var6) {
                  label93: {
                     if(var7 == 0) {
                        while(var5 + 4 <= var6) {
                           var8 = var11[255 & var1[var5]] << 18 | var11[255 & var1[var5 + 1]] << 12 | var11[255 & var1[var5 + 2]] << 6 | var11[255 & var1[var5 + 3]];
                           if(var8 < 0) {
                              break;
                           }

                           var10[var9 + 2] = (byte)var8;
                           var10[var9 + 1] = (byte)(var8 >> 8);
                           var10[var9] = (byte)(var8 >> 16);
                           var9 += 3;
                           var5 += 4;
                        }

                        if(var5 >= var6) {
                           var12 = var9;
                           break label93;
                        }
                     }

                     int var17 = var5 + 1;
                     int var18 = var11[255 & var1[var5]];
                     switch(var7) {
                     case 0:
                        if(var18 >= 0) {
                           var8 = var18;
                           ++var7;
                        } else if(var18 != -1) {
                           this.state = 6;
                           var13 = false;
                           return var13;
                        }
                        break;
                     case 1:
                        if(var18 >= 0) {
                           var8 = var18 | var8 << 6;
                           ++var7;
                        } else if(var18 != -1) {
                           this.state = 6;
                           var13 = false;
                           return var13;
                        }
                        break;
                     case 2:
                        if(var18 >= 0) {
                           var8 = var18 | var8 << 6;
                           ++var7;
                        } else if(var18 == -2) {
                           int var19 = var9 + 1;
                           var10[var9] = (byte)(var8 >> 4);
                           var7 = 4;
                           var9 = var19;
                        } else if(var18 != -1) {
                           this.state = 6;
                           var13 = false;
                           return var13;
                        }
                        break;
                     case 3:
                        if(var18 >= 0) {
                           var8 = var18 | var8 << 6;
                           var10[var9 + 2] = (byte)var8;
                           var10[var9 + 1] = (byte)(var8 >> 8);
                           var10[var9] = (byte)(var8 >> 16);
                           var9 += 3;
                           var7 = 0;
                        } else if(var18 == -2) {
                           var10[var9 + 1] = (byte)(var8 >> 2);
                           var10[var9] = (byte)(var8 >> 10);
                           var9 += 2;
                           var7 = 5;
                        } else if(var18 != -1) {
                           this.state = 6;
                           var13 = false;
                           return var13;
                        }
                        break;
                     case 4:
                        if(var18 == -2) {
                           ++var7;
                        } else if(var18 != -1) {
                           this.state = 6;
                           var13 = false;
                           return var13;
                        }
                        break;
                     case 5:
                        if(var18 != -1) {
                           this.state = 6;
                           var13 = false;
                           return var13;
                        }
                     }

                     var5 = var17;
                     continue;
                  }
               } else {
                  var12 = var9;
               }

               if(!var4) {
                  this.state = var7;
                  this.value = var8;
                  this.op = var12;
                  var13 = true;
               } else {
                  int var16;
                  switch(var7) {
                  case 0:
                     var16 = var12;
                     break;
                  case 1:
                     this.state = 6;
                     var13 = false;
                     return var13;
                  case 2:
                     var16 = var12 + 1;
                     var10[var12] = (byte)(var8 >> 4);
                     break;
                  case 3:
                     int var14 = var12 + 1;
                     var10[var12] = (byte)(var8 >> 10);
                     int var15 = var14 + 1;
                     var10[var14] = (byte)(var8 >> 2);
                     var16 = var15;
                     break;
                  case 4:
                     this.state = 6;
                     var13 = false;
                     return var13;
                  default:
                     var16 = var12;
                  }

                  this.state = var7;
                  this.op = var16;
                  var13 = true;
               }
               break;
            }
         }

         return var13;
      }
   }
}
