package com.chineseall.reader.lib.reader.utils;

import android.graphics.Paint;

import com.chineseall.reader.lib.reader.config.TangYuanSharedPrefUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class BreakLinesUtil {

    public static List<String> breakParagraph(String text) {
        List<String> paragraphs = new ArrayList<String>();
        if (StringUtil.isBlank(text)) {
            return paragraphs;
        }

        Pattern pattern = Pattern.compile("\n(\u3000| )*|\r\n(\u3000| )*");// 2013-07-19 正则表达式修正
        Matcher matcher = pattern.matcher(text);
        text = matcher.replaceAll("\n\u3000\u3000");
        int indexOf = text.lastIndexOf("\u3000\u3000");
        if (indexOf != -1 && indexOf == text.length() - 2) {
            text = text.substring(0, text.length() - 3);
        }

        String[] split = text.split("\n");
        if (split[0].length() < 2) {//解决某些书第一行没有字符，且只有1个字符的情况
            split[0] = "\u3000\u3000" + split[0];
        } else if (!split[0].startsWith("\u3000\u3000")) {//某些书第一行不加空格，影响排版
            split[0] = "\u3000\u3000" + split[0].trim();//第一行可能只有一个空格
        }
        List<String> strings = Arrays.asList(split);
        return strings;

    }


    /**
     * @param paint
     * @param text
     * @param lineWidth
     * @param isAllowPunctuationAtLineHead 是否允许标点符号在行头
     * @return
     */
    public static List<String> breakLines(Paint paint, String text, float lineWidth, boolean isAllowPunctuationAtLineHead) {
        List<String> lines = new ArrayList<>();

        if (StringUtil.isBlank(text)) {
            return lines;
        }

//        Pattern pattern = Pattern.compile("\n(\u3000| )*|\r\n(\u3000| )*");// 2013-07-19 正则表达式修正
//        Matcher matcher = pattern.matcher(text);
//        text = matcher.replaceAll("\n\u3000\u3000");
//        int indexOf = text.lastIndexOf("\u3000\u3000");
//        if (indexOf!=-1 &&  indexOf== text.length()-2) {
//            text = text.substring(0, text.length() - 3);
//        }

        String fontType = TangYuanSharedPrefUtils.getInstance().getFontType();
        boolean space = false;
        float charWidth = 0;
        if (fontType.contains("alibaba")) { // 阿里巴巴普惠体全角空格只占半个字的空间，导致显示错位 前两个空格特殊处理
            charWidth = paint.measureText("啊");
            space = true;
        }
        char[] chars = text.toCharArray();
        float lastCharWidth; // 上一个字符的宽度
        float lineMeasuredWidth = 0f;
        StringBuilder line = new StringBuilder();
        for (int i = 0; i < chars.length; i++) {
            // 采取先切，后检查宽度的办法，提高速率
            line.append(chars[i]);
            if (space && (i == 0 || i == 1)) {
                lastCharWidth = charWidth;
            } else {
                lastCharWidth = paint.measureText(String.valueOf(chars[i]));
            }
            lineMeasuredWidth += lastCharWidth;

            // 如果超出给定的行宽
            if (lineMeasuredWidth > lineWidth) {
                int lineLastCharIndex = line.length() - 1;

                // 取出末尾超出行宽的字符
                char extraChar = line.charAt(lineLastCharIndex);

                if (!isAllowPunctuationAtLineHead) { // 不允许标点符号在行首
                    boolean isPreviousCharPunctuation = StringUtil.isPunctuation(extraChar);
                    lineMeasuredWidth = checkNextLineChar(paint, lines, line, lineLastCharIndex, isPreviousCharPunctuation);
                } else {
                    // 删除末尾超出行宽的字符
                    lineMeasuredWidth = doExtraChar(lines, lastCharWidth, line, lineLastCharIndex, extraChar);
                }
            }
        }
        //最后一行
        lines.add(line.toString());

        return lines;
    }


    private static float checkNextLineChar(Paint paint, List<String> lines, StringBuilder line, int lineLastCharIndex, boolean lastCharIsPunctuation) {
        float lineMeasuredWidth = 0;
        int maxSize = 4;
        int count = 0;
        if (lastCharIsPunctuation) {
            for (int j = lineLastCharIndex - 1; j > 0; j--) {
                char previousChar = line.charAt(j);
                boolean isPreviousCharPunctuation = StringUtil.isPunctuation(previousChar);
                count++;
                if (isPreviousCharPunctuation) {//倒数第二个是符号，
                    lastCharIsPunctuation = true;
                    if (count == maxSize) {//往回查5次 还没合适的，就直接把最后一个多余字符移入下一行
                        String substring = line.substring(lineLastCharIndex);
                        lineMeasuredWidth = doNextLineChar(paint, lines, line, lineLastCharIndex, substring);
                        break;
                    }
                    continue;
                } else {

                }
                //倒数第二个不是符号，//删除末尾超出行宽的字符

                if (!isPreviousCharPunctuation) {
                    String substring = line.substring(j);
                    lineMeasuredWidth = doNextLineChar(paint, lines, line, lineLastCharIndex, substring);
                    break;
                }

                lastCharIsPunctuation = false;


            }
        } else {
            String substring = line.substring(lineLastCharIndex);
            lineMeasuredWidth = doNextLineChar(paint, lines, line, lineLastCharIndex, substring);
        }


//        char previousChar = line.charAt(lineLastCharIndex - 1);
//        boolean isPreviousCharPunctuation = StringUtil.isPunctuation(previousChar);
//        if (isPreviousCharPunctuation){//倒数第二个是符号，
//            char previous2Char = line.charAt(lineLastCharIndex - 2);
//            boolean isPrevious2CharPunctuation = StringUtil.isPunctuation(previous2Char);
//            if (isPrevious2CharPunctuation){//倒数第三个是符号
//                char previous3Char = line.charAt(lineLastCharIndex - 3);
//                boolean isPrevious3CharPunctuation = StringUtil.isPunctuation(previous3Char);
//                if (isPrevious3CharPunctuation){//倒数第四个是符号
//
//                }else {
//
//                }
//            }else {
//                lineMeasuredWidth = doNextLineChar(paint, lines, line, lineLastCharIndex, extraChar, previousChar, previous2Char);
//            }
//        }else {//倒数第二个不是符号，//删除末尾超出行宽的字符
//            lineMeasuredWidth = doExtraChar(lines, lastCharWidth, line, lineLastCharIndex, extraChar);
//        }
//
        return lineMeasuredWidth;
    }


    /**
     * 把当前行的最后1个字符放入下一行
     *
     * @param lines
     * @param lastCharWidth
     * @param line
     * @param lineLastCharIndex
     * @param extraChar
     * @return
     */
    private static float doExtraChar(List<String> lines, float lastCharWidth, StringBuilder line, int lineLastCharIndex, char extraChar) {
        float lineMeasuredWidth;//删除末尾超出行宽的字符
        line.deleteCharAt(lineLastCharIndex);
        //将切出的这行，加到列表中
        lines.add(line.toString());

        //清除记录行字符的buffer
        line.delete(0, line.length());
        //将超出行宽的字符，作为新的行的起始字符
        line.append(extraChar);
        //行内已经切过的字符的总的宽度,初始为末尾超出行宽的字符
        lineMeasuredWidth = lastCharWidth;
        return lineMeasuredWidth;
    }

    /**
     * @param paint
     * @param lines
     * @param line
     * @param lineLastCharIndex
     * @return
     */
    private static float doNextLineChar(Paint paint, List<String> lines, StringBuilder line, int lineLastCharIndex, String nextLineChars) {
        float lineMeasuredWidth;
        int length = nextLineChars.length();
        line.delete(lineLastCharIndex - length + 1, line.length());
        lines.add(line.toString());

        line.delete(0, line.length());


        line.append(nextLineChars);

        //重新初始化，行内已经切过的字符的总的宽度
        float nextLineCharsWidth = paint.measureText(line.toString());
        lineMeasuredWidth = nextLineCharsWidth;
        return lineMeasuredWidth;
    }

    /**
     * 把当前行的最后2个字符放入下一行
     *
     * @param paint
     * @param lines
     * @param lastCharWidth
     * @param line
     * @param lineLastCharIndex
     * @param extraChar
     * @param previousChar
     * @return
     */
    private static float doPreviousChar(Paint paint, List<String> lines, float lastCharWidth, StringBuilder line, int lineLastCharIndex, char extraChar, char previousChar) {
        float lineMeasuredWidth;
        line.delete(lineLastCharIndex - 1, line.length());
        lines.add(line.toString());

        //清除记录行字符的buffer
        line.delete(0, line.length());

        line.append(previousChar);
        line.append(extraChar);

        //重新初始化，行内已经切过的字符的总的宽度
        float previousCharWidth = paint.measureText(String.valueOf(previousChar));
        lineMeasuredWidth = previousCharWidth + lastCharWidth;
        return lineMeasuredWidth;
    }

    public static List<String> breakLines2(Paint paint, String text, float lineWidth, boolean isAllowPunctuationAtLineHead) {
        List<String> lines = new ArrayList<String>();

        if (StringUtil.isBlank(text)) {
            return lines;
        }


        char[] chars = text.toCharArray();
        float lastCharWidth = 0f;
        float lineMeasuredWidth = 0f;
        StringBuilder line = new StringBuilder();
        for (int i = 0; i < chars.length; i++) {
            //采取先切，后检查宽度的办法，提高速率
            line.append(chars[i]);
            lastCharWidth = paint.measureText(String.valueOf(chars[i]));
            lineMeasuredWidth += lastCharWidth;


            //如果超出给定的行宽
            if (lineMeasuredWidth > lineWidth) {
                int lineLastCharIndex = line.length() - 1;

                //取出末尾超出行宽的字符
                char extraChar = line.charAt(lineLastCharIndex);

                if (!isAllowPunctuationAtLineHead) {
                    if (StringUtil.isPunctuation(extraChar)) {//行末尾是一个符号
                        //判断下一个字符是否为符号
                        boolean isNextCharPunctuation = false;
                        int nextCharIndex = lineLastCharIndex + 1;
                        if (nextCharIndex < chars.length - 1) {
                            char nextChar = chars[nextCharIndex];
                            isNextCharPunctuation = StringUtil.isPunctuation(nextChar);
                        }

                        if (isNextCharPunctuation) {//行末尾是符号，接着的下一个也是符号，那就将行尾符号连带它前面的字符一起下放到下一行
                            char previousChar = line.charAt(lineLastCharIndex - 1);

                            lineMeasuredWidth = doPreviousChar(paint, lines, lastCharWidth, line, lineLastCharIndex, extraChar, previousChar);

                        } else { //如果下一个不是符号，则看行末尾的符号，超出行宽的距离
                            float extraWidth = (lineMeasuredWidth - lineWidth);
                            if (extraWidth <= lastCharWidth * 0.3) {//如果符号只有最多30%的宽度超出行宽，则符号留在行末尾
                                lines.add(line.toString());
                                line.delete(0, line.length());

                                //重新初始化，行内已经切过的字符的总的宽度
                                lineMeasuredWidth = 0f;

                            } else {//末尾符号超出行宽过多，只能将符号连带符号前面的字符一起下放到下一行
                                char previousChar = line.charAt(lineLastCharIndex - 1);

                                lineMeasuredWidth = doPreviousChar(paint, lines, lastCharWidth, line, lineLastCharIndex, extraChar, previousChar);
                            }
                        }
                    } else {//行末尾是一个字符,直接下放到下一行

                        //删除末尾超出行宽的字符
                        lineMeasuredWidth = doExtraChar(lines, lastCharWidth, line, lineLastCharIndex, extraChar);
                    }
                } else {
                    //删除末尾超出行宽的字符
                    lineMeasuredWidth = doExtraChar(lines, lastCharWidth, line, lineLastCharIndex, extraChar);
                }
            }
        }
        //最后一行
        lines.add(line.toString());

        return lines;
    }
}
