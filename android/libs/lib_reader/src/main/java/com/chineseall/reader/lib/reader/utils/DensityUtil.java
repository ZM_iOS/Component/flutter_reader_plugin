package com.chineseall.reader.lib.reader.utils;

import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.WindowManager;

import com.chineseall.reader.lib.reader.core.ReaderClient;

import java.lang.reflect.Method;

public class DensityUtil {
    /** 
     * 根据手机的分辨率�?dp 的单�?转成�?px(像素) 
     */  
    public static int dip2px(Context context, float dpValue) {
        final float scale = context.getResources().getDisplayMetrics().density;  
        return (int) (dpValue * scale + 0.5f);  
    }

	/**
	 * 根据手机的分辨率�?dp 的单�?转成�?px(像素)
	 */
	public static int dip2px(float dpValue) {
		return dip2px(ReaderClient.getContext(), dpValue);
	}

	/**
     * 根据手机的分辨率�?px(像素) 的单�?转成�?dp 
     */  
    public static int px2dip(Context context, float pxValue) {
        final float scale = context.getResources().getDisplayMetrics().density;  
        return (int) (pxValue / scale + 0.5f);  
    }

	/**
	 * 根据手机的分辨率�?px(像素) 的单�?转成�?dp
	 */
	public static int px2dip(float pxValue) {
		return px2dip(ReaderClient.getContext(), pxValue);
	}

	/**
	 * 获得屏幕宽度
	 *
	 * @param context
	 * @return
	 */
	public static int getScreenWidth(Context context) {
		WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
		DisplayMetrics outMetrics = new DisplayMetrics();
		wm.getDefaultDisplay().getMetrics(outMetrics);
		return outMetrics.widthPixels;
	}

	/**
	 * 获得屏幕高度
	 *
	 * @param context
	 * @return
	 */
	public static int getScreenHeight(Context context) {
		int height;
		if (context instanceof Activity) {
			Activity activity = (Activity) context;
			Display display = activity.getWindowManager().getDefaultDisplay();
			DisplayMetrics realDisplayMetrics = new DisplayMetrics();
			display.getRealMetrics(realDisplayMetrics);
			height = realDisplayMetrics.heightPixels;
		} else {
			height = context.getResources().getDisplayMetrics().heightPixels; //此高度在有导航栏的手机上不准确
		}
		return height;
	}
    
    /*
	 * 适配屏幕文字大小
	 */
	public static int getTextSize(Context cxt) {
		DisplayMetrics displaymetrics = new DisplayMetrics();
		((Activity) cxt).getWindowManager().getDefaultDisplay()
				.getMetrics(displaymetrics);
		int densityDpi = displaymetrics.densityDpi;
		int size = 0;
		switch (densityDpi) {
		case 120:
			size = 12;
			break;
		case 160:
			size = 18;
			break;
		case 240:
			size = 24;
			break;
		case 480:// 适配小米3手机
			size = 48;
			break;
		default:
			size = 31;
			break;
		}

		// size = sp.getInt("textSize", size);
		return size;
	}

	/**
	 * 取得屏幕密度
	 * 
	 * @param cxt
	 * @return
	 */
	public static int getdensityDpi(Context cxt) {
		DisplayMetrics displaymetrics = new DisplayMetrics();
		((Activity) cxt).getWindowManager().getDefaultDisplay()
				.getMetrics(displaymetrics);
		int densityDpi = displaymetrics.densityDpi;
		// size = sp.getInt("textSize", size);
		return densityDpi;
	}
}
