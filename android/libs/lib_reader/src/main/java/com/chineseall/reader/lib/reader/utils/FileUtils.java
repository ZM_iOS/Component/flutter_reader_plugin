package com.chineseall.reader.lib.reader.utils;

import android.content.res.AssetManager;

import com.chineseall.reader.lib.reader.core.Constants;

import java.io.File;
import java.io.IOException;

/**
 * Created by huangzhengneng on 2020/4/15.
 */
public class FileUtils {

    public static boolean isExistsInAssets(AssetManager am, String fileName) {
        try {
            String[] names = am.list("fonts");
            if (names == null) return false;
            for (String name : names) {
                if (name.equals(fileName.trim())) {
                    return true;
                }
            }
        } catch (IOException e) {
//            Logger.e(e);
        }
        return false;
    }

    public static File getFontDir(String font) {
        File file = new File(getFontPath(font));
        if (file.exists()) {
            return file;
        }
        return null;
    }

    public static String getFontPath(String font) {
        return Constants.FONT_PATH + font;
    }
}
