package com.chineseall.reader.lib.reader.utils;

import com.chineseall.reader.lib.reader.entities.Chapter;

/**
 * Created by huangzhengneng on 2020/4/15.
 */
public interface IChapterHelper {

    boolean isVerticalScrollMode();

    void loadChapter(final Chapter self, final Chapter next, final int progress);

}
