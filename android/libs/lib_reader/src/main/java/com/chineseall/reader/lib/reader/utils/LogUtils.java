package com.chineseall.reader.lib.reader.utils;

import com.chineseall.reader.lib.reader.core.ReaderClient;

/**
 * Created by huangzhengneng on 2020/7/2.
 */
public class LogUtils {

    private static final String TAG = "reader lib log default tag >>> ";

    public static void d(String tag, String msg) {
        log(tag, msg);
    }

    public static void d(String msg) {
        log(TAG, msg);
    }

    private static void log(@SuppressWarnings("SameParameterValue") String tag, String msg) {
        ReaderClient.getInstance().getCallBackContainer().getOnLogCallBack().log(tag, msg);
    }
}
