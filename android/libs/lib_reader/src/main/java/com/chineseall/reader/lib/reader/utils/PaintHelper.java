package com.chineseall.reader.lib.reader.utils;

import android.content.res.AssetManager;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.text.TextUtils;

import com.chineseall.reader.lib.reader.config.IReadConfig;
import com.chineseall.reader.lib.reader.config.TangYuanSharedPrefUtils;
import com.chineseall.reader.lib.reader.core.Constants;
import com.chineseall.reader.lib.reader.core.ReaderClient;

import java.io.File;

public class PaintHelper {
    private static PaintHelper instance;
    private Paint basePaint;
    private Paint contentPaint;
    private Paint contentPaintHasUnderLine;
    private Paint footerPaint;
    private Paint headerPaint;
    private IReadConfig readConfig;
    private Paint tailPaint;
    private Paint tailTitlePaint;
    private Paint tailAuthorPaint;
    private Paint titlePaint;
    private Paint mSelectedPaint;
    private Paint paraCommentPaint;

    private PaintHelper() {
    }

    private PaintHelper(IReadConfig iReadConfig) {
        this.readConfig = iReadConfig;
        this.basePaint = new Paint();
        this.basePaint.setAntiAlias(true);
        String fontType = TangYuanSharedPrefUtils.getInstance().getFontType();
//        if (Constants.FONT_FANGZHENG.equals(fontType)) {
//
//        } else {
//            TangYuanSharedPrefUtils.getInstance().setFontType("");
//        }
//        Typeface createFromAsset = Typeface.createFromAsset(ReaderApplication.getApp().getAssets(), Constant.FONT_FANGZHENG);
//        this.basePaint.setTypeface(createFromAsset);
    }

    public static PaintHelper getInstance() {
        return instance;
    }

    public static void init(IReadConfig iReadConfig) {
        if (instance == null) {
            instance = new PaintHelper(iReadConfig);
        }
    }

    public Paint getBasePaint() {
        return this.basePaint;
    }

    public Paint getContentPaint() {
        if (this.contentPaint == null) {
            this.contentPaint = new Paint(this.basePaint);
            this.contentPaint.setColor(this.readConfig.getTextColor());
            setFontTypeFace(contentPaint);
            this.contentPaint.setTextSize((float) this.readConfig.getTextSize());
        }
        return this.contentPaint;
    }

    public Paint getParaCommentPaint() {
        if (paraCommentPaint == null) {
            paraCommentPaint = new Paint(basePaint);
            paraCommentPaint.setColor(this.readConfig.getTextColor());
//            setFontTypeFace(paraCommentPaint);
        }
        return paraCommentPaint;
    }

    public Paint getSelectedPaint() {
        if (mSelectedPaint == null) {
            mSelectedPaint = new Paint(this.basePaint);
            mSelectedPaint.setColor(Color.parseColor("#880000ff"));
        }
        return mSelectedPaint;
    }

    public Paint getContentPaintHasUnderLine() {
        if (this.contentPaintHasUnderLine == null) {
            this.contentPaintHasUnderLine = new Paint(this.basePaint);

            this.contentPaintHasUnderLine.setColor(this.readConfig.getLineColor());
            contentPaintHasUnderLine.setUnderlineText(true);
            setFontTypeFace(contentPaintHasUnderLine);

            this.contentPaintHasUnderLine.setTextSize((float) this.readConfig.getTextSize());
        }
        return this.contentPaintHasUnderLine;
    }

    private void setFontTypeFace(Paint paint) {

        String fontType = TangYuanSharedPrefUtils.getInstance().getFontType();

        if (!TextUtils.isEmpty(fontType) && (fontType.contains("ttf") || fontType.contains("otf"))) {
            AssetManager am = ReaderClient.getContext().getAssets();
            boolean existsInAssets = FileUtils.isExistsInAssets(am, fontType);
            Typeface createFromAsset = null;
            if (existsInAssets) {
                createFromAsset = Typeface.createFromAsset(am, "fonts/" + fontType);
            } else {

                File fontDir = FileUtils.getFontDir(fontType);
                if (fontDir != null) {

                    try {

                        createFromAsset = Typeface.createFromFile(FileUtils.getFontPath(fontType));
                    } catch (Exception e) {
                        fontDir.delete();
//                        Logger.e(e);
                    }
                }
            }

            if (createFromAsset != null) {
                paint.setTypeface(createFromAsset);
            } else {
                setSystemFont(paint, Constants.FONT_SANS);
            }
        } else {
            setSystemFont(paint, fontType);

        }
    }

    private void setSystemFont(Paint paint, String fontType) {
        switch (fontType) {
            case Constants.FONT_NORMAL:
                paint.setTypeface(Typeface.SANS_SERIF);
                break;
            case Constants.FONT_SANS:
                paint.setTypeface(Typeface.SANS_SERIF);
                break;
            case Constants.FONT_SERIF:
                paint.setTypeface(Typeface.SERIF);
                break;
            case Constants.FONT_MONOSPACE:
                paint.setTypeface(Typeface.MONOSPACE);
                break;
        }
    }

    public Paint getContentTailPaint() {
        if (this.tailPaint == null) {
            this.tailPaint = new Paint(getContentPaint());
            this.tailPaint.setTextSize((float) this.readConfig.getTailTextSize());
            this.tailPaint.setColor(this.readConfig.getTailColor());
            this.tailPaint.setFakeBoldText(false);
            setFontTypeFace(tailPaint);
            this.tailPaint.setTextSkewX(0.0f);
        }
        return this.tailPaint;
    }

    public Paint getContentTailTitlePaint() {
        if (this.tailTitlePaint == null) {
            this.tailTitlePaint = new Paint(getContentPaint());
            this.tailTitlePaint.setTextSize(30);
            this.tailTitlePaint.setColor(Color.GRAY);
            this.tailTitlePaint.setFakeBoldText(false);
            setFontTypeFace(tailTitlePaint);
            this.tailTitlePaint.setTextSkewX(0.0f);
        }
        return this.tailTitlePaint;
    }

    public Paint getContentTailAuthorPaint() {
        if (this.tailAuthorPaint == null) {
            this.tailAuthorPaint = new Paint(getContentPaint());
            this.tailAuthorPaint.setTextSize(20);
            this.tailAuthorPaint.setColor(Color.BLACK);
            this.tailAuthorPaint.setFakeBoldText(false);
            setFontTypeFace(tailAuthorPaint);
            this.tailAuthorPaint.setTextSkewX(0.0f);
        }
        return this.tailAuthorPaint;
    }

    public Paint getFooterPaint() {
        if (this.footerPaint == null) {
            this.footerPaint = new Paint(this.basePaint);
            this.footerPaint.setTextSize((float) this.readConfig.getPageFooterTextSize());
            this.footerPaint.setColor(this.readConfig.getPageTopBottomExtraTextColor());
            setFontTypeFace(footerPaint);
        }
        return this.footerPaint;
    }

    public Paint getHeaderPaint() {
        if (this.headerPaint == null) {
            this.headerPaint = new Paint(this.basePaint);
            this.headerPaint.setTextSize((float) this.readConfig.getPageHeaderTextSize());
            this.headerPaint.setColor(this.readConfig.getPageTopBottomExtraTextColor());
            setFontTypeFace(headerPaint);
        }
        return this.headerPaint;
    }

    public Paint getTitlePaint() {
        if (this.titlePaint == null) {
            this.titlePaint = new Paint(this.basePaint);
            this.titlePaint.setColor(this.readConfig.getTitleColor());
            this.titlePaint.setTextSize((float) this.readConfig.getTitleTextSize());
            this.titlePaint.setFakeBoldText(true);
            setFontTypeFace(titlePaint);
        }
        return this.titlePaint;
    }

    public void resetConfig(IReadConfig iReadConfig) {
        this.titlePaint = null;
        this.contentPaint = null;
        this.tailPaint = null;
        this.headerPaint = null;
        this.footerPaint = null;
        this.paraCommentPaint = null;
        this.mSelectedPaint = null;
        contentPaintHasUnderLine = null;
        this.readConfig = iReadConfig;
    }

    public void resetContetnFontSize(int i) {
        getContentPaint().setTextSize((float) i);
    }
}
