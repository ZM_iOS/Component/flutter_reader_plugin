package com.chineseall.reader.lib.reader.utils;

import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.HashMap;
import java.util.Map;

public class SharedPrefUtil {
    protected SharedPreferences sharedPreferences;

    public boolean clearValue(String str) {
        Editor edit = this.sharedPreferences.edit();
        edit.remove(str);
        return edit.commit();
    }

    public boolean getBoolean(String str, boolean z) {
        return this.sharedPreferences.getBoolean(str, z);
    }

    public float getFloat(String str) {
        return this.sharedPreferences.getFloat(str, 0.0f);
    }

    public float getFloat(String str, float f) {
        return this.sharedPreferences.getFloat(str, f);
    }

    public int getInt(String str) {
        return this.sharedPreferences.getInt(str, 0);
    }

    public int getInt(String str, int i) {
        return this.sharedPreferences.getInt(str, i);
    }

    public long getLong(String str) {
        return this.sharedPreferences.getLong(str, 0);
    }

    public long getLong(String str, long j) {
        return this.sharedPreferences.getLong(str, j);
    }

    public Map<?, ?> getMap(String str) {
        try {
            return (Map) new ObjectInputStream(new ByteArrayInputStream(Base64.decode(getString(str, ""), 0))).readObject();
        } catch (Exception e) {
            return new HashMap();
        }
    }

    public Object getObject(String str) {
        try {
            return new ObjectInputStream(new ByteArrayInputStream(Base64.decode(getString(str, ""), 0))).readObject();
        } catch (Exception e) {
            return null;
        }
    }

    public String getString(String str) {
        return this.sharedPreferences.getString(str, "");
    }

    public String getString(String str, String str2) {
        return this.sharedPreferences.getString(str, str2);
    }

    public boolean has(String str) {
        return this.sharedPreferences.contains(str);
    }

    public boolean putBoolean(String str, boolean z) {
        Editor edit = this.sharedPreferences.edit();
        edit.putBoolean(str, z);
        return edit.commit();
    }

    public boolean putFloat(String str, float f) {
        Editor edit = this.sharedPreferences.edit();
        edit.putFloat(str, f);
        return edit.commit();
    }

    public boolean putInt(String str, int i) {
        Editor edit = this.sharedPreferences.edit();
        edit.putInt(str, i);
        return edit.commit();
    }

    public boolean putLong(String str, long j) {
        Editor edit = this.sharedPreferences.edit();
        edit.putLong(str, j);
        return edit.commit();
    }

    public boolean putString(String str, String str2) {
        Editor edit = this.sharedPreferences.edit();
        edit.putString(str, str2);
        return edit.commit();
    }

    public void saveMap(String str, Map<?, ?> map) {
        try {
        	ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            new ObjectOutputStream(byteArrayOutputStream).writeObject(map);
            String str2 = new String(Base64.encode(byteArrayOutputStream.toByteArray(), 0));
            Editor edit = this.sharedPreferences.edit();
            edit.putString(str, str2);
            edit.commit();
        } catch (IOException e) {
//            Logger.e(e);
        }
    }

    public void saveObject(String str, Object obj) {
        try {
        	ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            new ObjectOutputStream(byteArrayOutputStream).writeObject(obj);
            String str2 = new String(Base64.encode(byteArrayOutputStream.toByteArray(), 0));
            Editor edit = this.sharedPreferences.edit();
            edit.putString(str, str2);
            edit.commit();
        } catch (IOException e) {
//            Logger.e(e);
        }
    }
}
