package com.chineseall.reader.lib.reader.utils;

import android.content.Context;

/**
 * Created by huangzhengneng on 2020/7/9.
 */
public class SizeUtils {

    public static int dip2px(Context context, float dpValue) {
        final float scale = context.getResources().getDisplayMetrics().density;
        return (int) (dpValue * scale + 0.5f);
    }

    public static int px2dip(Context context, float pxValue) {
        final float scale = context.getResources().getDisplayMetrics().density;
        return (int) (pxValue / scale + 0.5f);
    }

}
