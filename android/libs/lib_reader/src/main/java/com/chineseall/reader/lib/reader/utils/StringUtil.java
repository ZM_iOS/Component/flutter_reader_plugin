package com.chineseall.reader.lib.reader.utils;

/**
 * Created by huangzhengneng on 2020/4/15.
 */
public class StringUtil {

    /**
     * Whether the String is null, zero-length and does contain only whitespace
     */
    public static boolean isBlank(String text) {
        if (isEmpty(text))
            return true;
        for (int i = 0; i < text.length(); i++) {
            if (!Character.isWhitespace(text.charAt(i)))
                return false;
        }
        return true;
    }

    /**
     * Whether the given string is null or zero-length
     */
    public static boolean isEmpty(String text) {
        return text == null || text.length() == 0;
    }

    public static boolean isPunctuation(char ch){
        if(isCjkPunc(ch)) return true;
        if(isEnPunc(ch)) return true;

        if(0x2018 <= ch && ch <= 0x201F) return true;
        if(ch == 0xFF01 || ch == 0xFF02) return true;
        if(ch == 0xFF07 || ch == 0xFF0C) return true;
        if(ch == 0xFF1A || ch == 0xFF1B) return true;
        if(ch == 0xFF1F || ch == 0xFF61) return true;
        if(ch == 0xFF0E) return true;
        if(ch == 0xFF65) return true;

        return false;
    }
    public static boolean isEnPunc(char ch){
        if (0x21 <= ch && ch <= 0x22) return true;
        if (ch == 0x27 || ch == 0x2C) return true;
        if (ch == 0x2E || ch == 0x3A) return true;
        if (ch == 0x3B || ch == 0x3F) return true;

        return false;
    }
    public static boolean isCjkPunc(char ch){

        if (0x3001 <= ch && ch <= 0x3003) return true;
        if (0x301D <= ch && ch <= 0x301F) return true;

        return isPunctuation1(ch);
    }

    //是否是标点
    public static boolean isPunctuation1(char ch) {
        switch (ch) {
            case '!':
            case '！':
            case '?':
            case '？':
            case '"':
            case '“':    // opening double quote
            case '”':    // closing double quote
            case ':':
            case '：':
            case '\'':
            case '‘':    // opening single quote
            case '’':    // closing single quote
            case ',':
            case '，':
            case '.':
            case '。':
            case ';':
            case '；':
            case '…':
            case '、':
                return true;
        }
        return false;
    }

    //是否需要处理的符号
    public static boolean isNeedHandler(char ch) {
        switch (ch) {
            case '。':
            case '！':
            case '？':
            case '：':
            case '；':
            case '》':
            case '）':
            case '】':
            case '，':
            case '、':
            case '』':
            case '」':
                return true;
        }
        return false;
    }
}
