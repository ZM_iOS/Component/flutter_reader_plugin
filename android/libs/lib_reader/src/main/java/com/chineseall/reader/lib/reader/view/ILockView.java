package com.chineseall.reader.lib.reader.view;

import android.view.MotionEvent;
import android.view.View;
import android.widget.CheckBox;

import com.chineseall.reader.lib.reader.entities.Chapter;

/**
 * Created by huangzhengneng on 2020/4/15.
 */
public interface ILockView {

    void setCanDraw(boolean flag);

    int getLockHeight();

    boolean dispatchTouchEvent(MotionEvent ev);

}
