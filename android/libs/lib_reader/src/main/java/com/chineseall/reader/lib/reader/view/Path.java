package com.chineseall.reader.lib.reader.view;

import android.graphics.Canvas;
import android.graphics.Paint;

import java.util.Vector;

public class Path extends android.graphics.Path {
    private final static boolean debug = false;
    private Vector<float[]> points = new Vector<float[]>();

    @Override
    public void moveTo(float x, float y) {
        super.moveTo(x, y);
        if (debug) {
            points.removeAllElements();
            points.add(new float[] { x, y });
        }
    }

    @Override
    public void lineTo(float x, float y) {
        super.lineTo(x, y);
        if (debug) {
            points.add(new float[] { x, y });
        }
    }

    @Override
    public String toString() {
        if (debug) {
            if (points.size() > 0) {
                StringBuffer sb = new StringBuffer();

                for (int i = 0; i < points.size(); i++) {
                    float[] f = points.elementAt(i);
                    sb.append("[").append(f[0]).append(", ").append(f[1]).append("] ");
                }
                return sb.toString();
            }
        }

        return super.toString();

    }

    public void draw(Canvas canvas, int color) {
        if (debug) {
//            Logger.v("path", toString());
            if (points.size() > 1) {
                Paint p = new Paint();
                p.setColor(color);
                p.setAntiAlias(true);

                for (int i = 0; i < points.size() - 1; i++) {
                    float[] f = points.elementAt(i);
                    float[] f2 = points.elementAt(i + 1);
                    canvas.drawLine(f[0], f[1], f2[0], f2[1], p);
                }

                int i = points.size() - 1;
                float[] f = points.elementAt(i);
                float[] f2 = points.elementAt(0);
                canvas.drawLine(f[0], f[1], f2[0], f2[1], p);
            }
        }
    }
}
