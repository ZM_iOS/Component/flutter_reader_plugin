package com.chineseall.reader.lib.reader.view;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PointF;
import android.graphics.Rect;
import android.os.Build;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;

import androidx.annotation.NonNull;

import com.chineseall.reader.lib.reader.callbacks.OnInitChapterHelperCallBack;
import com.chineseall.reader.lib.reader.config.ReaderConfigWrapper;
import com.chineseall.reader.lib.reader.config.TangYuanSharedPrefUtils;
import com.chineseall.reader.lib.reader.core.ReaderClient;
import com.chineseall.reader.lib.reader.entities.Article;
import com.chineseall.reader.lib.reader.entities.Chapter;
import com.chineseall.reader.lib.reader.entities.LineBlock;
import com.chineseall.reader.lib.reader.entities.Paragraph;
import com.chineseall.reader.lib.reader.utils.LogUtils;
import com.chineseall.reader.lib.reader.utils.PaintHelper;
import com.chineseall.reader.lib.reader.view.horizontal.PaperView;
import com.chineseall.reader.lib.reader.view.vertical.YViewBiz;

import java.util.ArrayList;
import java.util.Vector;

public abstract class ReaderView extends View {

    private Activity activity;
    /**
     * 文章本身存在着一种未知性
     */
    protected Article article;
    protected int batteryLevel;
    protected Bitmap backgroundBitmap;
    /**
     * 翻页效果的
     */
    protected Bitmap backgroundBitmapH;
    protected ILockView lockView;
    protected Rect menu;
    private Matrix matrix;
    protected OnRectClickCallback onRectClickCallback;
    protected boolean preview;
    private ReadChaptersWatcher readWatcher;
    protected Vector<String> readed;
    protected View topView;
    private static final String openningStr = "努力加载中，请稍后";
    private String loading = "";

    private ViewGroup adView_normal;
    private ViewGroup adView_big;
    private Context context;

    protected OnLongClickListener mOnLongClickListener;
    protected boolean hasHandledLongPress = false;
    protected PointF mPressedPoint;
    protected OnChangedListener mOnChangedListener;

    public void setNorepaint(boolean norepaint) {
        this.norepaint = norepaint;
    }

    protected boolean norepaint;

    public boolean isDrawing() {
        return isDrawing;
    }

    public void setOnChangedListener(OnChangedListener mOnChangedListener) {
        this.mOnChangedListener = mOnChangedListener;
    }

    public void chapterChanged() {
        if (mOnChangedListener != null) {
            mOnChangedListener.onChapterChanged();
        }
    }

    public void onPageChanged() {
        if (mOnChangedListener != null) {
            mOnChangedListener.onPageChanged();
        }
    }

    protected boolean isDrawing; //是否正在绘制

    // 阅读页的布局文件里的
    public ViewGroup adViewContainer;

    public interface ReadChaptersWatcher {
        void onReadSomeChapters();
    }

    public interface OnRectClickCallback {
        void hideMenu();

        void menuRectClick();

        void nextPageRectClick(int w);

        void previousPageRectClick(int w);

        boolean isMenuShowing();

        void adGone(int w);//隐藏广告

        void adVisible(int w);//显示广告

    }

    public ReaderView(Context context) {
        super(context);
        this.context = context;
        this.batteryLevel = -1;
        this.menu = new Rect();
        this.readed = new Vector<>();
        this.matrix = new Matrix();
        this.mPressedPoint = new PointF();
    }

    public ReaderView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.batteryLevel = -1;
        this.menu = new Rect();
        this.readed = new Vector<>();
        this.matrix = new Matrix();
        this.context = context;
//        initBackgroundDrawable();
        this.mPressedPoint = new PointF();
    }

    public ReaderView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.context = context;
        this.batteryLevel = -1;
        this.menu = new Rect();
        this.readed = new Vector<>();
        this.matrix = new Matrix();
        this.mPressedPoint = new PointF();
    }

    public abstract void animateStoped();


    private Bitmap bitmap;

    public abstract void articleHeightChanged(int i);

    protected void drawOpening(Canvas canvas) {
        if (Build.VERSION.SDK_INT >= 0) { // 怎么没写原因?
            canvas.drawColor(ReaderConfigWrapper.getInstance().getPageBgColor());
        } else {
            drawBackground(canvas, 0);//8.0系统,这个方法运行耗时较长
        }
        Paint contentPaint = PaintHelper.getInstance().getContentPaint();
        float fontWidth = contentPaint.measureText("打");
        float height = ((float) (getHeight() / 2)) + contentPaint.getTextSize();
        if (loading.length() == 3) {
            loading = ".";
        } else {
            loading = loading + ".";
        }
        String tmp = openningStr + loading;
        canvas.drawText(tmp, (((float) getWidth()) - contentPaint.measureText(openningStr)) / 2.0f, height - fontWidth / 2, contentPaint);
        postInvalidateDelayed(500);
    }

    protected void drawView(View view, Canvas canvas) {
        if (view != null) {
            view.measure(MeasureSpec.makeMeasureSpec(getWidth(), MeasureSpec.EXACTLY), MeasureSpec.makeMeasureSpec(getHeight(), MeasureSpec.EXACTLY));
            view.layout(getLeft(), getTop(), getRight(), getBottom());
            if (canvas != null) {
                view.draw(canvas);
            }
        }
    }

    public void onAttachedToWindow() {
        super.onAttachedToWindow();
//        EventBus.getDefault().register(this);
    }

    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
//        EventBus.getDefault().unregister(this);
    }

    public void freeMemory() {
        if (backgroundBitmap != null) {
            backgroundBitmap.recycle();
            backgroundBitmap = null;
        }

        if (backgroundBitmapH != null) {
            backgroundBitmapH.recycle();
            backgroundBitmapH = null;
        }

        ReaderClient.getInstance().map.clear();
    }

    public Activity getActivity() {
        return this.activity;
    }

    public Article getArticle() {
        return this.article;
    }

    public int getBatteryLevel() {
        return this.batteryLevel;
    }

    public Chapter getCurrentChapter() {
        return this.article != null ? this.article.getCurrentChapter() : null;
    }

    public Chapter getPreviousChapter() {
        return this.article != null ? this.article.getPreviousChapter() : null;
    }

    public Chapter getNextChapter() {
        return this.article != null ? this.article.getNextChapter() : null;
    }

    public String getCurrentChapterID() {
        if (this.article != null) {
            Chapter currentChapter = this.article.getCurrentChapter();
            if (currentChapter != null) {
                return currentChapter.getChapterId();
            }
        }
        return null;
    }

    public ILockView getLockView() {
        if (this.lockView == null) {
            this.lockView = ReaderClient.getInstance().getCallBackContainer().getOnLockViewDrawCallBack().initInstance(getContext());
        }
        return lockView;
    }

    public int getProgress() {
        if (this.article != null) {
            return this.article.getProgress();
        }
        return 0;
    }

    public boolean isPreview() {
        return this.preview;
    }

    public void jumpChapter(String str, int i) {
    }

    /**
     * @param id
     * @param defid 默认阅读的ID
     */
    public void loadArticle(int id, final String defid, ArrayList<Chapter> chapters, int progress, @NonNull OnInitChapterHelperCallBack callBack) {

    }

    public abstract void loadNextChapter();

    public abstract void loadPreChapter();

    protected void onClick(int i, int i2) {
    }

    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
    }

    protected void onMeasure(int i, int i2) {
        super.onMeasure(i, i2);
        int width = MeasureSpec.getSize(i);
        int height = MeasureSpec.getSize(i2);
        this.menu.set(width / 3, 0, width * 2 / 3, height * 2 / 3);
        if (this.article != null) {
            this.article.onSizeChanged(width, height);
        }
    }

    public void readChapter() {
        if (this.article != null) {
            Chapter currentChapter = this.article.getCurrentChapter();
            if (currentChapter != null) {
                String chapterId = currentChapter.getChapterId();
                if (!this.readed.contains(chapterId)) {
                    if (this.readed.size() < 3) {
                        this.readed.add(chapterId);
                    } else if (this.readWatcher != null) {
                        this.readWatcher.onReadSomeChapters();
                    }
                }
            }
        }
    }

    public final void reload(boolean z, int i) {
        if (this.article != null) {
            this.article.reload();
        }
    }

    public void reloadArticleChapter(int i, ArrayList<Chapter> arrayList) {
    }

    public void requestRepaint(Chapter chapter, LineBlock lineBlock, String str) {
        synchronized (this) {
            if (!(chapter == null || lineBlock == null)) {
                if (this.article != null) {
                    int currentOffsetY = this.article.getCurrentOffsetY();
                    int chapterTop = chapter.getChapterTop() + lineBlock.getY();
                    if ((chapter.getChapterTop() + lineBlock.getY()) + lineBlock.getHeight() >= currentOffsetY && chapterTop <= currentOffsetY + this.article.getViewHeight()) {
                        requestRepaint("ReaderView:requestRepaint: \u91cd\u7ed8\u56fe\u7247" + lineBlock.toString());
                    }
                }
            }
        }
    }

    public void requestRepaint(String str) {
        synchronized (this) {
            postInvalidate();
        }
    }

    public void setActivity(Activity activity) {
        this.activity = activity;
    }

    public void setBatteryLevel(int i) {
        this.batteryLevel = i;
    }

//    public void setBookEndView(View view) {
//        this.bottomView = view;
//    }

    public void setBookStartView(View view) {
        this.topView = view;
    }

    public abstract void setOffsetY(int i);

    public void setOnRectClickCallback(OnRectClickCallback onRectClickCallback) {
        this.onRectClickCallback = onRectClickCallback;
    }

    public OnRectClickCallback getOnRectClickCallback() {
        return onRectClickCallback;
    }

    public void setPreview(boolean z) {
        this.preview = z;
    }

    public void setReadChaptersWatcher(ReadChaptersWatcher readChaptersWatcher) {
        this.readWatcher = readChaptersWatcher;
    }

    public abstract boolean trunpage(boolean z, boolean anim);


    public void drawBackground(Canvas canvas, int scrollY) {
        if (this.backgroundBitmap != null) {
            if (scrollY > 0) {
                this.matrix.setScale((((float) getWidth()) * 1.0f) / ((float) this.backgroundBitmap.getWidth()), (((float) getHeight()) * 1.0f) / ((float) this.backgroundBitmap.getHeight()));
                this.matrix.postTranslate(0.0f, (float) scrollY);
                canvas.drawBitmap(this.backgroundBitmap, this.matrix, PaintHelper.getInstance().getBasePaint());
            } else {
                canvas.drawBitmap(this.backgroundBitmapH, 0, 0, PaintHelper.getInstance().getBasePaint());
            }
        }
    }

    public void setBackgroundDrawable(Bitmap bgBmp, Bitmap bgBmpH) {
        if (backgroundBitmap != null) {
            backgroundBitmap.recycle();
            backgroundBitmap = null;
        }

        if (backgroundBitmapH != null) {
            backgroundBitmapH.recycle();
            backgroundBitmapH = null;
        }
        backgroundBitmap = bgBmp;
        backgroundBitmapH = bgBmpH;
    }

    protected Runnable mLongPressRunnable = () -> getLongClickListener().onClick();

    protected void postLongPressCallBack() {
        postDelayed(mLongPressRunnable, ViewConfiguration.getLongPressTimeout());
    }

    protected void removeLongPressRunnable() {
        removeCallbacks(mLongPressRunnable);
    }

    protected OnLongClickListener getLongClickListener() {
        if (mOnLongClickListener == null) {
            mOnLongClickListener = this::handleLongPressAction;
        }
        return mOnLongClickListener;
    }

//    protected abstract void handleLongPressAction();

//    public abstract void clearSelection();

//    public void initBackgroundDrawable() {
//
//        if (Build.VERSION.SDK_INT>=0){
//            return;
//        }
//        if (backgroundBitmap != null) {
//            backgroundBitmap.recycle();
//            backgroundBitmap = null;
//        }
//
//        if (backgroundBitmapH!=null){
//            backgroundBitmapH.recycle();
//            backgroundBitmapH = null;
//        }
//
//        int backgroundRes = R.color.reader_sence_night_bg_color;
//        switch (TangYuanSharedPrefUtils.getInstance().getSceneMode(IReadConfig.SKIN_THEME_DAYTIME)) {
//            case IReadConfig.SKIN_THEME_DAYTIME /*69633*/:
//                backgroundRes = R.drawable.bg_read_white;
//                break;
//            case IReadConfig.SKIN_THEME_NIGHT /*69634*/:
//                backgroundRes = R.drawable.bg_read_black;
//                break;
//            case IReadConfig.SKIN_THEME_YELLOW /*69635*/:
//                backgroundRes = R.drawable.bg_read_yellow;
//                break;
//            case IReadConfig.SKIN_THEME_GREEN /*69636*/:
//                backgroundRes = R.drawable.bg_read_green;
//                break;
//            case IReadConfig.SKIN_THEME_PINK /*69637*/:
//                backgroundRes = R.drawable.bg_read_pink;
//                break;
//            case IReadConfig.SKIN_THEME_BLUE /*69638*/:
//                backgroundRes = R.drawable.bg_read_blue;
//                break;
//            default:
//                backgroundRes = R.drawable.bg_read_sblue;
//                break;
//        }
//        this.backgroundBitmap = BitmapUtil.drawableToBitmap(getContext().getResources().getDrawable(backgroundRes));
//        // 计算缩放比例
//        float scaleWidth = ((float) ScreenUtil.getScreenWidth(getContext())) / backgroundBitmap.getWidth();
//        float scaleHeight = ((float) ScreenUtil.getScreenHeight(getContext())) / backgroundBitmap.getHeight();
//        // 取得想要缩放的matrix参数
//        Matrix matrix = new Matrix();
//        matrix.postScale(scaleWidth, scaleHeight);
//        this.backgroundBitmapH = Bitmap.createBitmap(backgroundBitmap, 0, 0, backgroundBitmap.getWidth(), backgroundBitmap.getHeight(), matrix, true);
//
//    }

    public interface OnLongClickListener {
        void onClick();
    }

    public void log(String log) {
        LogUtils.d(log);
    }

    protected boolean hasSelectionArea() {
        if (this instanceof PaperView) {
            return article != null
                    && article.getCurrentChapter() != null
                    && article.getCurrentChapter().getSelectedParagraph() != null
                    && article.getCurrentChapter().getSelectedParagraph().isSelect();
        } else {
            if (article != null) {
                boolean ans = article.getCurrentChapter() != null
                        && article.getCurrentChapter().getSelectedParagraph() != null
                        && article.getCurrentChapter().getSelectedParagraph().isSelect();
                if (!ans) {
                    ans = article.getNextChapter() != null
                            && article.getNextChapter().getSelectedParagraph() != null
                            && article.getNextChapter().getSelectedParagraph().isSelect();
                }
                if (!ans) {
                    ans = article.getPreviousChapter() != null
                            && article.getPreviousChapter().getSelectedParagraph() != null
                            && article.getPreviousChapter().getSelectedParagraph().isSelect();
                }
                return ans;
            }
        }
        return false;
    }

    protected void handleLongPressAction() {
        // 关闭气泡显示时不能长按
        if (!TangYuanSharedPrefUtils.getInstance().showParaCommentCount()) return;
        if (isMenuShowing()) return;// 菜单在的时候，不响应长按
        if (canRepaint()) {
            article.getCurrentChapter().setLongPressSelect(mPressedPoint);
            Paragraph paragraph = article.getCurrentChapter().getSelectedParagraph();
            if (this instanceof YViewBiz) {
                // 垂直模式可能章节有偏差
                if (paragraph == null && article.getNextChapter() != null) {
                    article.getNextChapter().setLongPressSelect(mPressedPoint);
                    paragraph = article.getNextChapter().getSelectedParagraph();
                }
                if (paragraph == null && article.getPreviousChapter() != null) {
                    article.getPreviousChapter().setLongPressSelect(mPressedPoint);
                    paragraph = article.getPreviousChapter().getSelectedParagraph();
                }
            }
            if (paragraph == null || paragraph.getParagraphId() <= 0) return; // 没有段落id的段落不支持
            hasHandledLongPress = true;
            repaint();
            ReaderClient.getInstance().getCallBackContainer().getOnLongPressCallBack().onPress(mPressedPoint, paragraph);
        }
    }

    public boolean isMenuShowing() {
        return onRectClickCallback != null && onRectClickCallback.isMenuShowing();
    }

    protected boolean canRepaint() {
        if (article != null) {
            if (article.getCurrentChapter() != null && article.getCurrentChapter().containsPoint(mPressedPoint)) {
                return article.getCurrentChapter().getStatus() == ReaderConfigWrapper.STATUS_LOADED;
            }
            if (article.getPreviousChapter() != null && article.getPreviousChapter().containsPoint(mPressedPoint)) {
                return article.getPreviousChapter().getStatus() == ReaderConfigWrapper.STATUS_LOADED;
            }
            if (article.getNextChapter() != null && article.getNextChapter().containsPoint(mPressedPoint)) {
                return article.getNextChapter().getStatus() == ReaderConfigWrapper.STATUS_LOADED;
            }
        }
        return false;
    }

    public void clearSelection() {
        if (canRepaint()) {
            if (article.getCurrentChapter() != null) {
                article.getCurrentChapter().resetSelect();
            }
            if (article.getPreviousChapter() != null) {
                article.getPreviousChapter().resetSelect();
            }
            if (article.getNextChapter() != null) {
                article.getNextChapter().resetSelect();
            }
            repaint();
        }
    }

    protected abstract void repaint();

    public interface OnChangedListener {

        void onChapterChanged();

        void onPageChanged();
    }
}
