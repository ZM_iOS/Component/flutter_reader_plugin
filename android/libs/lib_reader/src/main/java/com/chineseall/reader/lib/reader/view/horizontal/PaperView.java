package com.chineseall.reader.lib.reader.view.horizontal;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.PointF;
import android.graphics.RectF;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.chineseall.reader.lib.reader.callbacks.OnInitChapterHelperCallBack;
import com.chineseall.reader.lib.reader.config.ReaderConfigWrapper;
import com.chineseall.reader.lib.reader.config.TangYuanSharedPrefUtils;
import com.chineseall.reader.lib.reader.core.ReaderClient;
import com.chineseall.reader.lib.reader.entities.Article;
import com.chineseall.reader.lib.reader.entities.Chapter;
import com.chineseall.reader.lib.reader.utils.LogUtils;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

public class PaperView extends PaperWidget implements OnTouchListener {
    private static final int MOVE_DISTANCE = 20;
    protected Canvas currcanvas;
    //    protected boolean handleEventByBottomView;
    protected boolean handleEventByLockView;
    private boolean init;
    protected RectF leftr;
    protected Canvas nextcanvas;
    protected RectF rightr;
    private boolean setpapervar;
    private boolean slide;

    public PaperView(Context context) {
        super(context);
        this.init = false;
        this.slide = false;
        this.setpapervar = false;
        this.leftr = new RectF();
        this.rightr = new RectF();
        init();
    }

    public PaperView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.init = false;
        this.slide = false;
        this.setpapervar = false;
        this.leftr = new RectF();
        this.rightr = new RectF();
        init();
    }

    public PaperView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.init = false;
        this.slide = false;
        this.setpapervar = false;
        this.leftr = new RectF();
        this.rightr = new RectF();
        init();
    }

    private void debugImg(String str) {
        int i = 2;
        if (i == 1)
            return;

    }

    private Bitmap getBitmap(int width, int height) {
        for (int i = 0; i < 3; i++) {
            try {
                Bitmap bm = Bitmap.createBitmap(width, height, Bitmap.Config.RGB_565);
                if (null == bm || bm.isRecycled()) {
                    System.gc();
                } else {
                    return bm;
                }
            } catch (OutOfMemoryError oom) {
                System.gc();
            }
        }
        return null;
    }

    /**
     * 判断是否为点击操作
     *
     * @param x
     * @param y
     * @return
     */
    private boolean
    isClick(float x, float y) {
        double value = Math.hypot((x - mPressedPoint.x), (y - mPressedPoint.y));
        return !slide && value <= MOVE_DISTANCE;
    }

    private boolean isFirstPage() {
        return this.article.isFirstPage();
    }

    private boolean isLastPage() {
        return this.article.isLastPage();
    }

    private void showStatus(String flag, int dir) {
        StringBuffer sb = new StringBuffer();
        sb.append(flag).append(" -->");
        sb.append(" dir: ").append(dir);
        sb.append(" lastpage: ").append(isLastPage());
//        sb.append(" bottomViewOnShow: ").append(bottomViewOnShow);
        sb.append(" firstpage: ").append(isFirstPage());
//        Logger.i("PaperView:showStatus()", sb.toString());
    }


    @Override
    protected void animationFinish(int dir) {
        super.animationFinish(dir);

//        LogUtils.d("test changed: animationFinish");

        if (dir > 0) { //向后翻页
            if (article.isLastPage()) { //尾页了
//                if (null != bottomView) {
//                    drawView(bottomView, currcanvas);
//                    EventBus.getDefault().post(new OnUserSeeBookEndViewEvent());
//                    bottomViewOnShow = true;
//                } else {
                //					article.paintPage(currcanvas, ReaderConfig.CURRENT_PAGE, true);
//                }
            } else {
//                bottomViewOnShow = false;
                article.pageTruned(dir);
                article.paintPage(currcanvas, ReaderConfigWrapper.CURRENT_PAGE, true);
//                Logger.i("PaperView:requestRepaint", "》》》》》》animationFinish  1");
                article.paintPage(nextcanvas, ReaderConfigWrapper.CURRENT_PAGE, true);
            }
        } else {
//            if (bottomViewOnShow) { //正好在显示尾页
//                article.paintPage(currcanvas, ReaderConfig.CURRENT_PAGE, true);
//                Logger.i("PaperView:requestRepaint", "》》》》》》animationFinish  2");
//
//            } else

            {
                article.pageTruned(dir);
                article.paintPage(currcanvas, ReaderConfigWrapper.CURRENT_PAGE, true);
//                Logger.i("PaperView:requestRepaint", "》》》》》》animationFinish  3");
            }
//            bottomViewOnShow = false;
        }

        //Log.e("PaperView", "article.getCurrentOffsetY(): " + article.getCurrentOffsetY());
        //showStatus("animationFinish", dir);
        readChapter();
//        if(delayRepaint){
//            this.requestRepaint("延时重绘");
//            //Logger.i("PaperView:requestRepaint", "animationFinish 延时重绘");
//        }
        //article.paintPage(currcanvas, Env.CURRENT, true);
        //debugImg("ani_end");
        //		postInvalidate();
        //		Logger.i("postInvalidate","postInvalidate-9");

        onPageChanged();
    }

    @Override
    public void
    articleHeightChanged(int newHeight) {
        if (!init) {
            //
            initCanvas(getWidth(), getHeight());

//            DisplayMetrics display = getContext().getResources().getDisplayMetrics();
//            int width = display.widthPixels;
//            int height = display.heightPixels;//用此方法,在有虚拟导航栏的手机上时,高度不准确,导致显示信息不完整,如小米mix
//            initCanvas(width, height);
        } else {
            if (newHeight != getHeight()) { //旋转了
                //initCanvas(getWidth(), getHeight());
            }
        }

        //requestRepaint();
    }

    @Override
    public void freeMemory() {
        super.freeMemory();
        if (null != mCurPageBitmap && !mCurPageBitmap.isRecycled()) {
            mCurPageBitmap.recycle();
        }
        mCurPageBitmap = null;

        if (null != mNextPageBitmap && !mNextPageBitmap.isRecycled()) {
            mNextPageBitmap.recycle();
        }
        mNextPageBitmap = null;
        currcanvas = null;
        nextcanvas = null;
    }

    protected void init() {
        setOnTouchListener(this);
    }

    public void initCanvas(int width, int height) {
        if (width * height == 0) { //不允许为0的长宽
            return;
        }
        sizeChanged(width, height);
        if (null != mCurPageBitmap && !mCurPageBitmap.isRecycled()) {
            mCurPageBitmap.recycle();
        }
        mCurPageBitmap = null;
        currcanvas = null;

        if (null != mNextPageBitmap && !mNextPageBitmap.isRecycled()) {
            mNextPageBitmap.recycle();
        }
        mNextPageBitmap = null;
        nextcanvas = null;

        System.gc();

        mCurPageBitmap = getBitmap(width, height);
        mNextPageBitmap = getBitmap(width, height);

        if (null == mCurPageBitmap || null == mNextPageBitmap) {
//            Logger.i("INIT", "width: " + width + ", height: " + height + " " + (null == mCurPageBitmap) + " " + (null == mNextPageBitmap));
            return;
        }

        currcanvas = new Canvas(mCurPageBitmap);
        nextcanvas = new Canvas(mNextPageBitmap);
//        currcanvas.drawColor(ReaderConfig.getInstance().getPageBgColor());
//        nextcanvas.drawColor(ReaderConfig.getInstance().getPageBgColor());

        //左边
        leftr.left = 0;
        leftr.top = 0;
        leftr.bottom = getHeight();
        leftr.right = getWidth() / 3;

        //右边
        rightr.left = (getWidth() * 2) / 3;
        rightr.right = getWidth();
        rightr.top = 0;
        rightr.bottom = getHeight();

        init = true;
    }

    @Override
    public void jumpChapter(String id, int progress) {
        if (!TextUtils.isEmpty(id) && article != null) {
            article.jumpToChapter(id);
            onPageChanged();
        } else {
            Toast.makeText(getContext(), "跳转失败!", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void loadArticle(int bookid, String chapterId, ArrayList<Chapter> arrayList, int progress, @NonNull OnInitChapterHelperCallBack callBack) {
        this.article = new Article(this, bookid, arrayList);
//        article.setNeedLoadBookFromNet(flag);
        this.article.setChapterHelper(callBack.init(this.article));
        this.article.load(chapterId, progress);
        onPageChanged();
    }

    public void loadNextChapter() {
        if (this.article != null) {
            this.article.loadNextChapter();
            onPageChanged();
        }
    }

    public void loadPreChapter() {
        if (this.article != null) {
            this.article.loadPreviousChapter();
            onPageChanged();
        }
    }

    public void onAttachedToWindow() {
        super.onAttachedToWindow();
//        EventBus.getDefault().register(this);
    }

    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
//        EventBus.getDefault().unregister(this);
    }

    protected void onDraw(Canvas canvas) {
        isDrawing = true;
        super.onDraw(canvas);
        if (this.article == null) {
            drawOpening(canvas);
            return;
        }
        isDrawing = false;
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        log("onTouch: " + event.getAction());
        if (null == currcanvas || null == article) {
            return false;
        }

        if (checkCanDraw(event)) return true;

        if (checkReadMode()) return true;

        int animateMode = TangYuanSharedPrefUtils.getInstance().getReaderPagerAnim(ReaderConfigWrapper.PAGER_ANIM_BOOK);
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            if (timer != null) {
                try {
                    timer.cancel();
                    timer = null;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if (hasSelectionArea()) {
                clearSelection();
                return true;
            }
            hasHandledLongPress = false;
            postLongPressCallBack();
            handleDownEvent(event, animateMode);
        } else if (event.getAction() == MotionEvent.ACTION_MOVE) {
            if (timer != null) {
                try {
                    timer.cancel();
                    timer = null;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if (!hasHandledLongPress) {
                handleMoveEvent(event, animateMode);
                if (slide) {
                    removeLongPressRunnable();
                }
            }
        } else if (event.getAction() == MotionEvent.ACTION_UP
                || event.getAction() == MotionEvent.ACTION_CANCEL) {
            if (!hasHandledLongPress) {
                removeLongPressRunnable();
                if (animateMode == ReaderConfigWrapper.PAGER_ANIM_BOOK) {
                    if (handleBookAnimUpEvent(event)) return true;
                } else {
                    handleOtherAnimUpEvent(event);
                }
            } else {
                norepaint = false;
            }
        }
        return true;
    }

    private void handleDownEvent(MotionEvent event, int animateMode) {
        slide = false;
        setpapervar = false;
        dispatchClickEvent = false;
        mPressedPoint.x = event.getX();
        mPressedPoint.y = event.getY();

        if (animateMode == ReaderConfigWrapper.PAGER_ANIM_BOOK) {
            checkEvent(event);
            Log.d("TAG=Scroll", "abortAnimation555555");
            abortAnimation();
            calcCornerXY(event.getX(), event.getY());
            doTouchEvent(event);
            canceltrun();
            if (getCurrentChapter() != null && getCurrentChapter().getStatus() == ReaderConfigWrapper.STATUS_LOCK) {
                handleEventByLockView = getLockView().dispatchTouchEvent(event);
            }
        } else {
            slidey = 0;
            Log.d("TAG=Scroll", "abortAnimation666666");
            abortAnimation();
            doTouchEvent(event);
            canceltrun();
            if (getCurrentChapter() != null && getCurrentChapter().getStatus() == ReaderConfigWrapper.STATUS_LOCK) {
                handleEventByLockView = getLockView().dispatchTouchEvent(event);
            }
        }
    }

    private void handleOtherAnimUpEvent(MotionEvent event) {
        float x = event.getX();
        float y = event.getY();
        if (handleEventByLockView) {
            getLockView().dispatchTouchEvent(event);
            handleEventByLockView = false;
            norepaint = false;
        } else {
            Log.d("TAG=Scroll", "abortAnimation777777");
            abortAnimation();
            boolean singeTouchMode = TangYuanSharedPrefUtils.getInstance().isSingleTouchMode();
            if (isClick(x, y)) {
                norepaint = false;
                boolean processedClickEvent = false; // 外部是否处理点击事件
                if (null != article) {
                    if (!article.onClick((int) x, (int) y + article.getCurrentOffsetY())) {
                        if (menu.contains((int) x, (int) y)) {
                            if (onRectClickCallback != null) {
                                onRectClickCallback.menuRectClick();
                                return;
                            }
                            //  Toast.makeText(getContext(), "弹菜单", Toast.LENGTH_SHORT).show();
                            processedClickEvent = true;
                        }
                    } else {
                        processedClickEvent = true;
//                        processedClickEvent = getCurrentChapter() == null || (getCurrentChapter().getStatus() != ReaderConfigWrapper.STATUS_LOCK);
                    }
                }

                if (!processedClickEvent) {
                    if (leftr.contains(x, y)) {
                        if (singeTouchMode) { // 单手模式，翻下一页
                            nextPage(true);
                        } else {
                            if (!article.isFirstPage()) {
                                if (onRectClickCallback != null) {
                                    onRectClickCallback.previousPageRectClick(3);
                                    onRectClickCallback.adGone(3);
                                    onRectClickCallback.adVisible(3);
                                }
                                trunpage(false);
                            }
                        }
                    } else if (rightr.contains(x, y)) { //向后翻
                        nextPage(true);
                    }
                }
            } else {
                float dx = x - mPressedPoint.x;
                if (dx > 0) {
                    if (singeTouchMode) { // 单手模式，翻下一页
                        nextPage(false);
                    } else {
                        if (!article.isFirstPage()) {
                            if (onRectClickCallback != null) {
                                onRectClickCallback.previousPageRectClick(4);
                                onRectClickCallback.adGone(4);
                                onRectClickCallback.adVisible(4);
                            }
                            trunpage(false);
                        }
                    }
                } else { //向后翻
                    nextPage(false);
                }
            }
        }
    }

    private boolean handleBookAnimUpEvent(MotionEvent event) {
        float x = event.getX();
        float y = event.getY();
        checkEvent(event);
        if (handleEventByLockView) {
            getLockView().dispatchTouchEvent(event);
            postInvalidate();
            handleEventByLockView = false;
            norepaint = false;
        } else {
            if (isClick(x, y)) {
                if (!setpapervar) {
                    norepaint = false;
                    boolean processedClickEvent = false; // 外部是否处理点击事件
                    if (null != article) {
                        dispatchClickEvent = article.onClick((int) x, (int) y + article.getCurrentOffsetY());
                        if (!dispatchClickEvent) {
                            if (menu.contains((int) x, (int) y)) {
                                if (onRectClickCallback != null) {
                                    onRectClickCallback.menuRectClick();
                                    return true;
                                }
                                processedClickEvent = true;
                            }
                        } else {
                            processedClickEvent = true;
//                                    getCurrentChapter() == null;
                            // || (getCurrentChapter().getStatus() != ReaderConfigWrapper.STATUS_LOCK)
                        }
                    }

                    if (!isTurnBack() && article.isLastPage()) {// 处理最后一页点击翘角的问题
                        processedClickEvent = true;
                        setpapervar = true;
                    }

                    if (!processedClickEvent) {
                        calcCornerXY(event.getX(), event.getY());
                        mTouch.x = x;
                        mTouch.y = y;
                        mTouch = adjust(mTouch);

                        setpapervar = true;
                    }
                }
            } else {
                if (!setpapervar) {
                    if (!isTurnBack()) {
                        if (article.isLastPage()) {
                            ReaderClient.getInstance().getCallBackContainer().getOnReadCompleteCallBack().complete(article == null ? "" : article.getBookid() + "");
                            return true;
                        }
                    }
                }
            }

            if (setpapervar) {
                int time = 0;//动画离开屏幕需要多长时间
                if (isTurnBack()) { // 向前翻
                    if (onRectClickCallback != null) {
                        onRectClickCallback.previousPageRectClick(1);
                        onRectClickCallback.adGone(1);
                        int width = getWidth();
                        int dx = (int) (width * 2 - x - 1);//x->dx 1000s  计算规则来自PagerWidget.startAnimation()
                        int mdx = (int) (width - x);//x->0或者x->width
                        int delayMillis = ReaderConfigWrapper.getInstance().getAnimateTime();
                        try {
                            time = mdx * delayMillis / Math.abs(dx);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        if (time <= 0) {
                            time = delayMillis;
                        }
                    }
                    if (article.isFirstPage()) {
                        return true;
                    } else {
                        article.paintPage(currcanvas, ReaderConfigWrapper.CURRENT_PAGE, true);
                        article.paintPage(nextcanvas, ReaderConfigWrapper.PREVIOUS_PAGE, false);
                    }
                } else { // 向后翻
                    if (onRectClickCallback != null) {
                        onRectClickCallback.nextPageRectClick(1);
                        onRectClickCallback.adGone(1);
                        int width = getWidth();
                        int dx = -(int) (width + x);//x->dx 1000s
                        int mdx = (int) x;//x->0或者x->width
                        int delayMillis = ReaderConfigWrapper.getInstance().getAnimateTime();
                        try {
                            time = mdx * delayMillis / Math.abs(dx);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        if (time <= 0) {
                            time = delayMillis;
                        }
                    }
                    if (article.isLastPage()) { // 文章到了最后
                        if (!dispatchClickEvent) {
                            ReaderClient.getInstance().getCallBackContainer().getOnReadCompleteCallBack().complete(article == null ? "" : article.getBookid() + "");
                        }
                        return true;
                    } else {
                        article.paintPage(currcanvas, ReaderConfigWrapper.CURRENT_PAGE, false); //继续显示当前页
                        article.paintPage(nextcanvas, ReaderConfigWrapper.NEXT_PAGE, false);
                    }
                }

                boolean isCancelTurn = doTouchEvent(event, time);//是否取消翻页
                if (!isCancelTurn) { //没有取消翻页
                    timerDo(time);
                }
            }
        }
        return false;
    }

    Timer timer;

    /**
     * 计时执行任务
     */
    private void timerDo(int time) {
        if (timer != null) {
            try {
                timer.cancel();
                timer = null;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                if (timer != null) {
                    if (onRectClickCallback != null) {
                        onRectClickCallback.adVisible(1); //子线程
                    }
                    try {
                        timer.cancel();
                        timer = null;
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }, time);
    }

    private void handleMoveEvent(MotionEvent event, int animateMode) {
        if (!isClick(event.getX(), event.getY())) { // 用户有滑动操作的企图
            slide = true;
            handleEventByLockView = false;
        }
        if (slide) {
            if (animateMode == ReaderConfigWrapper.PAGER_ANIM_BOOK) {
                checkEvent(event);
                if ((isTurnBack() && !isFirstPage()) || (!isTurnBack() && !isLastPage())) {
                    if (!setpapervar) {
                        calcCornerXY(event.getX(), event.getY());
                        setpapervar = true;

                        article.paintPage(currcanvas, ReaderConfigWrapper.CURRENT_PAGE, true);
                        if (isTurnBack()) { // 向前翻
                            if (onRectClickCallback != null) {
                                onRectClickCallback.previousPageRectClick(2);
                                onRectClickCallback.adGone(2);
                            }
                            article.paintPage(nextcanvas, ReaderConfigWrapper.PREVIOUS_PAGE, false);
                        } else { // 向后翻
                            if (!article.isLastPage()) { // 文章还没到最后
                                article.paintPage(nextcanvas, ReaderConfigWrapper.NEXT_PAGE, false);
                            }
                            if (onRectClickCallback != null) {
                                onRectClickCallback.nextPageRectClick(2);
                                onRectClickCallback.adGone(2);
                            }
                        }
                    }
                    doTouchEvent(event);
                    if (onRectClickCallback != null) {
                        onRectClickCallback.adGone(2);
                    }
                }
            } else {
                if (onRectClickCallback != null) {
                    onRectClickCallback.nextPageRectClick(3);
                    onRectClickCallback.adGone(3);
                }
                doTouchEvent(event);
            }
        }
    }

    private boolean checkCanDraw(MotionEvent event) {
        if (!article.canDraw) {
            if (article.getCurrentChapter() != null && article.getCurrentChapter().getStatus() == ReaderConfigWrapper.STATUS_FAILED && event.getAction() == MotionEvent.ACTION_UP) {
                if (article.getCurrentChapter().reloadRectF != null && article.getCurrentChapter().reloadRectF.contains(event.getX(), event.getY())) {
                    article.getChapterHelper().loadChapter(article.getCurrentChapter(), null, 0);
                } else if (onRectClickCallback != null) {
                    onRectClickCallback.menuRectClick();
                }
            } else {//不可绘制时，保证可以唤起菜单，进行操作，不至于给人一种假死的感觉
                float x = event.getX();
                float y = event.getY();
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (menu.contains((int) x, (int) y)) {
                        if (onRectClickCallback != null) {
                            onRectClickCallback.menuRectClick();
                        }
                    }
                }
            }
            return true;
        }
        return false;
    }

    private boolean checkReadMode() {
        boolean readModeOn = TangYuanSharedPrefUtils.getInstance().isReadModeOn();
        if (readModeOn) {
            if (onRectClickCallback != null) {
                onRectClickCallback.menuRectClick();
            }
            return true;
        }
        return false;
    }

    private void nextPage(boolean sendEvent) {
        if (onRectClickCallback != null) {
            onRectClickCallback.nextPageRectClick(4);
            onRectClickCallback.adGone(2);
            onRectClickCallback.adVisible(2);
        }
        if (sendEvent && article.isLastPage()) {
            ReaderClient.getInstance().getCallBackContainer().getOnReadCompleteCallBack().complete(article == null ? "" : article.getBookid() + "");
        }
        trunpage(true);
    }


    public void reloadArticleChapter(int i, ArrayList<Chapter> arrayList) {
        if (this.article != null) {
            this.article.reloadChapters(i, arrayList);
        }
    }


    @Override
    public void requestRepaint(String hint) {
        if (norepaint) {
//            Logger.i("PaperView:requestRepaint", "不允许此时重绘");
//            delayRepaint = true;
            return;
        }
//        delayRepaint = false;
        if (null != hint) {
//            Logger.i("PaperView:requestRepaint", hint);
        }

        repaint();
//        Logger.i("PaperView", "刷新View,requestRepaint()" + (hint == null ? "" : hint));
    }

    protected void repaint() {
        if (null == currcanvas || null == nextcanvas) {
            return;
        }

//        if (null != bottomView) {
//            if (bottomViewOnShow) {
//                drawView(bottomView, currcanvas);
//                drawView(bottomView, nextcanvas);
//            } else {
//                article.paintPage(currcanvas, ReaderConfig.CURRENT_PAGE, true);
//                article.paintPage(nextcanvas, ReaderConfig.CURRENT_PAGE, false);
//            }
//        } else

        {
            article.paintPage(currcanvas, ReaderConfigWrapper.CURRENT_PAGE, true);
            article.paintPage(nextcanvas, ReaderConfigWrapper.CURRENT_PAGE, false);
        }

        postInvalidate();
    }


    @Override
    public void setOffsetY(int top) {
//        bottomViewOnShow = false;
        article.jumpToOffset(top);
        requestRepaint("PaperView:setOffsetY() ->" + top);
        //debugImg("showat");
    }


    /**
     * @param b false表示翻到上一页，true表示翻到下一页
     * @return
     */
    public boolean trunpage(boolean b) {
        return trunpage(b, true);
    }

    public boolean trunpage(boolean b, boolean anim) {

        mIsAnim = anim;
        if (isAnimation()) {
            return true;
        }
        if (article == null) {
            return false;
        }
        //showStatus("trunpage", b ? 1 : -1);
        Log.d("TAG=Scroll", "abortAnimation888888");
        abortAnimation();
        PointF initPoint = new PointF(1, 1);
        if (b) {
            initPoint.x = getWidth() - 1;
            initPoint.y = getHeight() - 1;
        }
        initPoint = adjust(initPoint);
        mTouch.x = initPoint.x;
        mTouch.y = initPoint.y;
        mPressed.x = initPoint.x;
        mPressed.y = initPoint.y;
        calcCornerXY(initPoint.x, initPoint.y);

        int animateMode = TangYuanSharedPrefUtils.getInstance().getReaderPagerAnim(ReaderConfigWrapper.PAGER_ANIM_BOOK);

        boolean draw2 = (animateMode != ReaderConfigWrapper.PAGER_ANIM_NONE && !TangYuanSharedPrefUtils.getInstance().isReadModeOn());
        if (!anim) {
            draw2 = anim;
        }
        if (b) {
            if (article.isLastPage()) {
                article.paintPage(currcanvas, ReaderConfigWrapper.CURRENT_PAGE, true);
                if (draw2) {
//                    if (null != bottomView) {
//                        if (bottomViewOnShow) {
//                            return true;
//                        }
//                        drawView(bottomView, nextcanvas);
//                        bottomViewOnShow = true;
//                    } else {
                    return true;
                    //						article.paintPage(nextcanvas, ReaderConfig.CURRENT_PAGE, false);
//                    }
                }
            } else {
                article.paintPage(currcanvas, ReaderConfigWrapper.CURRENT_PAGE, true);
                if (draw2) {
                    article.paintPage(nextcanvas, ReaderConfigWrapper.NEXT_PAGE, false);
                }
            }

            startAnimation(true, anim);
            postInvalidate();
//            Logger.i("PaperView", "刷新View,trunpage()");
            return true;
        } else {
            if (article.isFirstPage()) {
                if (draw2) {
//                    if (bottomViewOnShow) { //可能是只有一页，所以也会显示最后一页
//                        drawView(bottomView, currcanvas);
//                    } else
                    {
                        return true;
                        //article.paintPage(currcanvas, ReaderConfig.CURRENT_PAGE, true);
                    }
//                    article.paintPage(nextcanvas, ReaderConfig.CURRENT_PAGE, false);
                } else {
                    article.paintPage(currcanvas, ReaderConfigWrapper.CURRENT_PAGE, true);
                }
            } else {
//                if (bottomViewOnShow) { //可能是只有一页，所以也会显示最后一页
//                    drawView(bottomView, currcanvas);
//                    article.paintPage(nextcanvas, ReaderConfig.CURRENT_PAGE, true);
//                } else
                {
                    article.paintPage(currcanvas, ReaderConfigWrapper.CURRENT_PAGE, true);
                    article.paintPage(nextcanvas, ReaderConfigWrapper.PREVIOUS_PAGE, false);
                }
            }

            startAnimation(false, anim);
            postInvalidate();
//            Logger.i("postInvalidate", "postInvalidate-8");
            return true;
        }
    }
}
