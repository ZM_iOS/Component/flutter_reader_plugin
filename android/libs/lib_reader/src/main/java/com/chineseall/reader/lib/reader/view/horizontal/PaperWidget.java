//
// Decompiled by Procyon v0.5.30
//

package com.chineseall.reader.lib.reader.view.horizontal;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.PointF;
import android.graphics.Region.Op;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.os.Handler;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Scroller;

import com.chineseall.reader.lib.reader.config.ReaderConfigWrapper;
import com.chineseall.reader.lib.reader.config.TangYuanSharedPrefUtils;
import com.chineseall.reader.lib.reader.utils.LogUtils;
import com.chineseall.reader.lib.reader.view.Path;
import com.chineseall.reader.lib.reader.view.ReaderView;

import java.util.Timer;
import java.util.TimerTask;

public abstract class PaperWidget extends ReaderView {
    protected int alpha;
//    protected boolean bottomViewOnShow;
    private boolean hasDestroy;
    int[] mBackShadowColors;
    GradientDrawable mBackShadowDrawableLR;
    GradientDrawable mBackShadowDrawableRL;
    private PointF mBezierControl1;
    PointF mBezierControl2;
    private boolean mIsTurnBack = true;
    private PointF mBezierEnd1;
    PointF mBezierEnd2;
    private PointF mBezierStart1;
    PointF mBezierStart2;
    private PointF mBeziervertex1;
    PointF mBeziervertex2;
    ColorMatrixColorFilter mColorMatrixFilter;
    private int mCornerX;
    private int mCornerY;
    private boolean mIsClicked;
    protected Bitmap mCurPageBitmap;
    float mDegrees;
    GradientDrawable mFolderShadowDrawableLR;
    GradientDrawable mFolderShadowDrawableRL;
    int[] mFrontShadowColors;
    GradientDrawable mFrontShadowDrawableHBT;
    GradientDrawable mFrontShadowDrawableHTB;
    GradientDrawable mFrontShadowDrawableVLR;
    GradientDrawable mFrontShadowDrawableVRL;
    private int mHeight;
    Matrix mMatrix;
    float[] mMatrixArray;
    float mMaxLength;
    float mMiddleX;
    float mMiddleY;
    protected Bitmap mNextPageBitmap;
    Paint mPaint;
    private Path mPath0;
    private Path mPath1;
    protected PointF mPressed;
    Scroller mScroller;
    protected PointF mTouch;
    protected PointF mFirstPressed;
    protected PointF mFirstMoved;
    float mTouchToCornerDis;
    private int mWidth;

    protected boolean dispatchClickEvent;
    protected int slidex;
    protected int slidey;
    private Timer timer;
    private boolean slide;
    protected boolean mIsAnim; //是否执行动画，用于音量键翻页

    public PaperWidget(final Context context) {
        super(context);
        this.mWidth = 480;
        this.mHeight = 800;
        this.mCornerX = 0;
        this.mCornerY = 0;
        this.mCurPageBitmap = null;
        this.mNextPageBitmap = null;
        this.norepaint = false;
        this.mTouch = new PointF();
        mFirstPressed = new PointF();
        mFirstMoved = new PointF();
        this.mPressed = new PointF();
        this.mBezierStart1 = new PointF();
        this.mBezierControl1 = new PointF();
        this.mBeziervertex1 = new PointF();
        this.mBezierEnd1 = new PointF();
        this.mBezierStart2 = new PointF();
        this.mBezierControl2 = new PointF();
        this.mBeziervertex2 = new PointF();
        this.mBezierEnd2 = new PointF();
        this.mMatrixArray = new float[]{0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f};
        this.mMaxLength = (float) Math.hypot(this.mWidth, this.mHeight);
        this.timer = null;
        this.alpha = 100;
        this.slidex = 0;
        this.slidey = 0;
        this.init();
    }

    public PaperWidget(final Context context, final AttributeSet set) {
        super(context, set);
        this.mWidth = 480;
        this.mHeight = 800;
        this.mCornerX = 0;
        this.mCornerY = 0;
        this.mCurPageBitmap = null;
        this.mNextPageBitmap = null;
        this.norepaint = false;
        this.mTouch = new PointF();
        this.mPressed = new PointF();
        mFirstPressed = new PointF();
        mFirstMoved = new PointF();
        this.mBezierStart1 = new PointF();
        this.mBezierControl1 = new PointF();
        this.mBeziervertex1 = new PointF();
        this.mBezierEnd1 = new PointF();
        this.mBezierStart2 = new PointF();
        this.mBezierControl2 = new PointF();
        this.mBeziervertex2 = new PointF();
        this.mBezierEnd2 = new PointF();
        this.mMatrixArray = new float[]{0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f};
        this.mMaxLength = (float) Math.hypot(this.mWidth, this.mHeight);
        this.timer = null;
        this.alpha = 100;
        this.slidex = 0;
        this.slidey = 0;
        this.init();
    }

    public PaperWidget(final Context context, final AttributeSet set, final int n) {
        super(context, set, n);
        this.mWidth = 480;
        this.mHeight = 800;
        this.mCornerX = 0;
        this.mCornerY = 0;
        this.mCurPageBitmap = null;
        this.mNextPageBitmap = null;
        this.norepaint = false;
        this.mTouch = new PointF();
        this.mPressed = new PointF();
        mFirstPressed = new PointF();
        mFirstMoved = new PointF();
        this.mBezierStart1 = new PointF();
        this.mBezierControl1 = new PointF();
        this.mBeziervertex1 = new PointF();
        this.mBezierEnd1 = new PointF();
        this.mBezierStart2 = new PointF();
        this.mBezierControl2 = new PointF();
        this.mBeziervertex2 = new PointF();
        this.mBezierEnd2 = new PointF();
        this.mMatrixArray = new float[]{0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f};
        this.mMaxLength = (float) Math.hypot(this.mWidth, this.mHeight);
        this.timer = null;
        this.alpha = 100;
        this.slidex = 0;
        this.slidey = 0;
        this.init();
    }

    private void calcPoints() {
        mMiddleX = (mTouch.x + mCornerX) / 2;
        mMiddleY = (mTouch.y + mCornerY) / 2;
        mBezierControl1.x = mMiddleX - (mCornerY - mMiddleY) * (mCornerY - mMiddleY) / (mCornerX - mMiddleX);
        mBezierControl1.y = mCornerY;
        mBezierControl2.x = mCornerX;
        mBezierControl2.y = mMiddleY - (mCornerX - mMiddleX) * (mCornerX - mMiddleX) / (mCornerY - mMiddleY);

        mBezierStart1.x = mBezierControl1.x - (mCornerX - mBezierControl1.x) / 2;
        mBezierStart1.y = mCornerY;

        // 当mBezierStart1.x < 0或者mBezierStart1.x > 480时
        // 如果继续翻页，会出现BUG故在此限制
        if (mTouch.x > 0 && mTouch.x < mWidth) {
            if (mBezierStart1.x < 0 || mBezierStart1.x > mWidth) {
                if (mBezierStart1.x < 0)
                    mBezierStart1.x = mWidth - mBezierStart1.x;

                float f1 = Math.abs(mCornerX - mTouch.x);
                float f2 = mWidth * f1 / mBezierStart1.x;
                mTouch.x = Math.abs(mCornerX - f2);

                float f3 = Math.abs(mCornerX - mTouch.x) * Math.abs(mCornerY - mTouch.y) / f1;
                mTouch.y = Math.abs(mCornerY - f3);

                mMiddleX = (mTouch.x + mCornerX) / 2;
                mMiddleY = (mTouch.y + mCornerY) / 2;

                mBezierControl1.x = mMiddleX - (mCornerY - mMiddleY) * (mCornerY - mMiddleY) / (mCornerX - mMiddleX);
                mBezierControl1.y = mCornerY;

                mBezierControl2.x = mCornerX;
                mBezierControl2.y = mMiddleY - (mCornerX - mMiddleX) * (mCornerX - mMiddleX) / (mCornerY - mMiddleY);
                mBezierStart1.x = mBezierControl1.x - (mCornerX - mBezierControl1.x) / 2;
            }
        }
        mBezierStart2.x = mCornerX;
        mBezierStart2.y = mBezierControl2.y - (mCornerY - mBezierControl2.y) / 2;

        mTouchToCornerDis = (float) Math.hypot((mTouch.x - mCornerX), (mTouch.y - mCornerY));

        mBezierEnd1 = getCross(mTouch, mBezierControl1, mBezierStart1, mBezierStart2);
        mBezierEnd2 = getCross(mTouch, mBezierControl2, mBezierStart1, mBezierStart2);

        mBeziervertex1.x = (mBezierStart1.x + 2 * mBezierControl1.x + mBezierEnd1.x) / 4;
        mBeziervertex1.y = (2 * mBezierControl1.y + mBezierStart1.y + mBezierEnd1.y) / 4;
        mBeziervertex2.x = (mBezierStart2.x + 2 * mBezierControl2.x + mBezierEnd2.x) / 4;
        mBeziervertex2.y = (2 * mBezierControl2.y + mBezierStart2.y + mBezierEnd2.y) / 4;
    }

    /**
     * 创建阴影的GradientDrawable
     */
    private void createDrawable() {
        int[] color = {0x333333, 0xb0333333};
        mFolderShadowDrawableRL = new GradientDrawable(GradientDrawable.Orientation.RIGHT_LEFT, color);
        mFolderShadowDrawableRL.setGradientType(GradientDrawable.LINEAR_GRADIENT);

        mFolderShadowDrawableLR = new GradientDrawable(GradientDrawable.Orientation.LEFT_RIGHT, color);
        mFolderShadowDrawableLR.setGradientType(GradientDrawable.LINEAR_GRADIENT);

        mBackShadowColors = new int[]{0xff111111, 0x111111};
        mBackShadowDrawableRL = new GradientDrawable(GradientDrawable.Orientation.RIGHT_LEFT, mBackShadowColors);
        mBackShadowDrawableRL.setGradientType(GradientDrawable.LINEAR_GRADIENT);

        mBackShadowDrawableLR = new GradientDrawable(GradientDrawable.Orientation.LEFT_RIGHT, mBackShadowColors);
        mBackShadowDrawableLR.setGradientType(GradientDrawable.LINEAR_GRADIENT);

        mFrontShadowColors = new int[]{0x80111111, 0x111111};
        mFrontShadowDrawableVLR = new GradientDrawable(GradientDrawable.Orientation.LEFT_RIGHT, mFrontShadowColors);
        mFrontShadowDrawableVLR.setGradientType(GradientDrawable.LINEAR_GRADIENT);
        mFrontShadowDrawableVRL = new GradientDrawable(GradientDrawable.Orientation.RIGHT_LEFT, mFrontShadowColors);
        mFrontShadowDrawableVRL.setGradientType(GradientDrawable.LINEAR_GRADIENT);

        mFrontShadowDrawableHTB = new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM, mFrontShadowColors);
        mFrontShadowDrawableHTB.setGradientType(GradientDrawable.LINEAR_GRADIENT);

        mFrontShadowDrawableHBT = new GradientDrawable(GradientDrawable.Orientation.BOTTOM_TOP, mFrontShadowColors);
        mFrontShadowDrawableHBT.setGradientType(GradientDrawable.LINEAR_GRADIENT);
    }

    /**
     * 绘制翻起页背面
     *
     * @param canvas
     * @param bitmap
     */
    private void drawCurrentBackArea(Canvas canvas, Bitmap bitmap) {
        int i = (int) (mBezierStart1.x + mBezierControl1.x) / 2;
        float f1 = Math.abs(i - mBezierControl1.x);
        int i1 = (int) (mBezierStart2.y + mBezierControl2.y) / 2;
        float f2 = Math.abs(i1 - mBezierControl2.y);
        float f3 = Math.min(f1, f2);
        mPath1.reset();
        mPath1.moveTo(mBeziervertex2.x, mBeziervertex2.y);
        mPath1.lineTo(mBeziervertex1.x, mBeziervertex1.y);
        mPath1.lineTo(mBezierEnd1.x, mBezierEnd1.y);
        mPath1.lineTo(mTouch.x, mTouch.y);
        mPath1.lineTo(mBezierEnd2.x, mBezierEnd2.y);
        mPath1.close();

        GradientDrawable mFolderShadowDrawable = null;
        int left = 0;
        int right = 0;
        if (mPressed.x > mWidth / 2 && mPressed.y < mHeight / 2) { //右上
            left = (int) (mBezierStart1.x - 1);
            right = (int) (mBezierStart1.x + f3 + 1);
            mFolderShadowDrawable = mFolderShadowDrawableLR;
        } else if (mPressed.x > mWidth / 2 && mPressed.y > mHeight / 2) { //右下
            left = (int) (mBezierStart1.x - f3 - 1);
            right = (int) (mBezierStart1.x + 1);
            mFolderShadowDrawable = mFolderShadowDrawableRL;
        } else if (mPressed.x < mWidth / 2 && mPressed.y < mHeight / 2) { //左上
            left = (int) (mBezierStart1.x - 1);
            right = (int) (mBezierStart1.x + f3 + 1);
            mFolderShadowDrawable = mFolderShadowDrawableLR;
        } else if (mPressed.x < mWidth / 2 && mPressed.y > mHeight / 2) { //左下
            left = (int) (mBezierStart1.x - f3 - 1);
            right = (int) (mBezierStart1.x + 1);
            mFolderShadowDrawable = mFolderShadowDrawableRL;
        }

        canvas.save();
        canvas.clipPath(mPath0);
        //Log.v("", pathlog.toString());
        canvas.clipPath(mPath1, Op.INTERSECT);
        //Log.v("", "444444444444");
        mPaint.setColorFilter(mColorMatrixFilter);

        float dis = (float) Math.hypot(mCornerX - mBezierControl1.x, mBezierControl2.y - mCornerY);
        float f8 = (mCornerX - mBezierControl1.x) / dis;
        float f9 = (mBezierControl2.y - mCornerY) / dis;
        mMatrixArray[0] = 1 - 2 * f9 * f9;
        mMatrixArray[1] = 2 * f8 * f9;
        mMatrixArray[3] = mMatrixArray[1];
        mMatrixArray[4] = 1 - 2 * f8 * f8;
        mMatrix.reset();
        mMatrix.setValues(mMatrixArray);
        mMatrix.preTranslate(-mBezierControl1.x, -mBezierControl1.y);
        mMatrix.postTranslate(mBezierControl1.x, mBezierControl1.y);
        canvas.drawBitmap(bitmap, mMatrix, mPaint);
        // canvas.drawBitmap(bitmap, mMatrix, null);
        mPaint.setColorFilter(null);
        canvas.rotate(mDegrees, mBezierStart1.x, mBezierStart1.y);
        if (mFolderShadowDrawable != null) {
            mFolderShadowDrawable.setBounds(left, (int) mBezierStart1.y, right, (int) (mBezierStart1.y + mMaxLength));
            mFolderShadowDrawable.draw(canvas);
        }
        canvas.restore();
    }

    /**
     * 绘制当前区域
     *
     * @param canvas
     * @param bitmap
     * @param path
     */
    private void drawCurrentPageArea(Canvas canvas, Bitmap bitmap, Path path) {
        //crash 1
        //09-09 15:16:01.441: VERBOSE/(2136): (248.0, 480.0), (272.0, 480.0, NaN, NaN), (272.0, 432.0), (296.0, 432.0), (320.0, 432.0, 320.0, 408.0), (320, 480)

        //crash 2
        //09-09 15:17:45.271: VERBOSE/(2205): (222.5, 480.0), (255.0, 480.0, NaN, NaN), (255.0, 415.0), (287.5, 415.0), (320.0, 415.0, 320.0, 382.5), (320, 480)

        if (mBezierEnd1.x == Float.NaN || mBezierEnd1.y == Float.NaN) {
            return;
        }

        mPath0.reset();
        //pathlog.delete(0, pathlog.length());

        mPath0.moveTo(mBezierStart1.x, mBezierStart1.y);
        //pathlog.append("(").append(mBezierStart1.x).append(", ").append(mBezierStart1.y).append("), ");

		/*mBezierEnd1.x = Float.NaN;
        mBezierEnd1.y = Float.NaN;*/

        mPath0.quadTo(mBezierControl1.x, mBezierControl1.y, mBezierEnd1.x, mBezierEnd1.y);
        //pathlog.append("(").append(mBezierControl1.x).append(", ").append(mBezierControl1.y).append(", ").append(mBezierEnd1.x).append(", ").append(mBezierEnd1.y).append("), ");

        mPath0.lineTo(mTouch.x, mTouch.y);
        //pathlog.append("(").append(mTouch.x).append(", ").append(mTouch.y).append("), ");

        mPath0.lineTo(mBezierEnd2.x, mBezierEnd2.y);
        //pathlog.append("(").append(mBezierEnd2.x).append(", ").append(mBezierEnd2.y).append("), ");

        mPath0.quadTo(mBezierControl2.x, mBezierControl2.y, mBezierStart2.x, mBezierStart2.y);
        //pathlog.append("(").append(mBezierControl2.x).append(", ").append(mBezierControl2.y).append(", ").append(mBezierStart2.x).append(", ").append(mBezierStart2.y).append("), ");

        mPath0.lineTo(mCornerX, mCornerY);
        //pathlog.append("(").append(mCornerX).append(", ").append(mCornerY).append(")");

        mPath0.close();

        canvas.save();
        clipPathXor(canvas,path);
        canvas.drawBitmap(bitmap, 0, 0, null);
        canvas.restore();
    }

    /**
     * 画一个从中间展开的Bitmap，从中间切割
     *
     * @param canvas
     * @param bitmap
     * @param dx     左/上半边的运动坐标
     * @param dy     右/下半边的运动坐标
     * @param hor    true 表示水平方向
     */
    private void drawExpand(Canvas canvas, Bitmap bitmap, int dx, int dy, boolean hor) {
        int cx = mWidth / 2;
        int cy = mHeight / 2;
        if (hor) {
            canvas.clipRect(dx, 0, dx + cx, mHeight, Op.REPLACE);
            canvas.drawBitmap(bitmap, dx, 0, null);
            canvas.clipRect(dy + cx, 0, dy + cx + cx, mHeight, Op.REPLACE);
            canvas.drawBitmap(bitmap, dy, 0, null);
        } else {
            canvas.clipRect(0, dx, mWidth, dx + cy, Op.REPLACE);
            canvas.drawBitmap(bitmap, 0, dx, null);
            canvas.clipRect(0, dy + cy, mWidth, dy + cy + cy, Op.REPLACE);
            canvas.drawBitmap(bitmap, 0, dy, null);
        }
    }

    private void drawFade(Canvas canvas, Bitmap bitmap, int alpha) {
        mPaint.setAlpha(alpha);
        canvas.drawBitmap(bitmap, 0, 0, mPaint);
    }

    /**
     * 绘制下页及阴影
     *
     * @param canvas
     * @param bitmap
     */
    private void drawNextPageAreaAndShadow(Canvas canvas, Bitmap bitmap) {
        final int dis = 5;
        mPath1.reset();
        mPath1.moveTo(mBezierStart1.x, mBezierStart1.y);
        mPath1.lineTo(mBeziervertex1.x, mBeziervertex1.y);
        mPath1.lineTo(mBeziervertex2.x, mBeziervertex2.y);
        mPath1.lineTo(mBezierStart2.x, mBezierStart2.y);
        mPath1.lineTo(mCornerX, mCornerY);
        mPath1.close();

        mDegrees = (float) Math.toDegrees(Math.atan2(mBezierControl1.x - mCornerX, mBezierControl2.y - mCornerY));
        int leftx = 0;
        int rightx = 0;
        GradientDrawable mBackShadowDrawable = null;
        if (mPressed.x > mWidth / 2 && mPressed.y < mHeight / 2) { //右上
            leftx = (int) (mBezierStart1.x);
            rightx = (int) (mBezierStart1.x + mTouchToCornerDis / dis);
            mBackShadowDrawable = mBackShadowDrawableLR;
        } else if (mPressed.x > mWidth / 2 && mPressed.y > mHeight / 2) { //右下
            leftx = (int) (mBezierStart1.x - mTouchToCornerDis / dis);
            rightx = (int) mBezierStart1.x;
            mBackShadowDrawable = mBackShadowDrawableRL;
        } else if (mPressed.x < mWidth / 2 && mPressed.y < mHeight / 2) { //左上
            leftx = (int) (mBezierStart1.x);
            rightx = (int) (mBezierStart1.x + mTouchToCornerDis / dis);
            mBackShadowDrawable = mBackShadowDrawableLR;
        } else if (mPressed.x < mWidth / 2 && mPressed.y > mHeight / 2) { //左下
            leftx = (int) (mBezierStart1.x - mTouchToCornerDis / dis);
            rightx = (int) mBezierStart1.x;
            mBackShadowDrawable = mBackShadowDrawableRL;
        }
        canvas.save();
        canvas.clipPath(mPath0);
        canvas.clipPath(mPath1, Op.INTERSECT);
        canvas.drawBitmap(bitmap, 0, 0, null);
        canvas.rotate(mDegrees, mBezierStart1.x, mBezierStart1.y);
        if (mBackShadowDrawable != null) {
            mBackShadowDrawable.setBounds(leftx, (int) mBezierStart1.y, rightx, (int) (mMaxLength + mBezierStart1.y));
            mBackShadowDrawable.draw(canvas);
        }
        canvas.restore();
        mPath0.draw(canvas, 0xFFFF0000);
        mPath1.draw(canvas, 0xFF0000FF);
    }

    private void drawSlide(Canvas canvas, Bitmap bitmap, int x, int y) {
        canvas.drawBitmap(bitmap, x, y, null);
    }

    private void init() {
        this.mPath0 = new Path();
        this.mPath1 = new Path();
        this.createDrawable();
        (this.mPaint = new Paint()).setStyle(Style.FILL);
        final ColorMatrix colorMatrix = new ColorMatrix();
        colorMatrix.set(new float[]{
                0.85f, 0.0f, 0.0f, 0.0f, 80.0f,
                0.0f, 0.85f, 0.0f, 0.0f, 80.0f,
                0.0f, 0.0f, 0.85f, 0.0f, 80.0f,
                0.0f, 0.0f, 0.0f, 0.8f, 0.0f});
        this.mColorMatrixFilter = new ColorMatrixColorFilter(colorMatrix);
        this.mMatrix = new Matrix();
        this.mScroller = new Scroller(this.getContext());
        this.mTouch.x = 0.01f;
        this.mTouch.y = 0.01f;
    }

    private TimerTask newScrollerListener() {
        return new TimerTask() {

            @Override
            public void run() {
                if (mScroller.isFinished() || hasDestroy) {
                    cancel();
                    stopTimmer();
                    animationFinish(isTurnBack() ? ReaderConfigWrapper.PREVIOUS_PAGE : ReaderConfigWrapper.NEXT_PAGE);
//                } else {
//                    postInvalidate();
                }
            }
        };
    }


    private void startAnimation(int delayMillis,int time) {
        norepaint = true;
//        Logger.i("Chapter:paintPage", "animationFinish：norepaint = true");
        int dx, dy;
        // dx 水平方向滑动的距离，负值会使滚动向左滚动
        // dy 垂直方向滑动的距离，负值会使滚动向上滚动
        if (!isTurnBack()) {
            dx = -(int) (mWidth + mTouch.x);
        } else {
            dx = (int) (mWidth - mTouch.x + mWidth - 1);
        }
        if (mCornerY > 0) {
            dy = (int) (mHeight - 1 - mTouch.y);
        } else {
            dy = (int) (1 - mTouch.y); // 防止mTouch.y最终变为0
        }
//        Log.d("TAG=Scroll", "time=" + delayMillis + ", w=" + mWidth + ", h=" + mHeight + ", x=" + (int) mTouch.x + ", y=" + (int) mTouch.y + ", dx=" + dx + ", dy=" + dy);
        mScroller.startScroll((int) mTouch.x, (int) mTouch.y, dx, dy, delayMillis);

        stopTimmer();
        timer = new Timer();
        timer.schedule(newScrollerListener(), 0, 60);
    }


    private synchronized void stopTimmer() {
        if (null != timer) {
            timer.cancel();
            timer = null;
        }
    }

    /**
     * 停止当前动画
     */
    public void abortAnimation() {
        if (!mScroller.isFinished()) {
            norepaint = false;
            mScroller.abortAnimation();
            stopTimmer();
            animationFinish(isTurnBack() ? ReaderConfigWrapper.PREVIOUS_PAGE : ReaderConfigWrapper.NEXT_PAGE);
        }
//        Logger.i("Chapter:paintPage", "animationFinish：norepaint = false");
        //Log.e("slidey", "slidey = " + slidey);
    }


    protected PointF adjust(PointF p) {
        p.x = p.x < 1 ? 1 : p.x;
        p.x = p.x > mWidth - 1 ? mWidth - 1 : p.x;
        p.y = p.y < 1 ? 1 : p.y;
        p.y = p.y > mHeight - 1 ? mHeight - 1 : p.y;
        return p;
    }

    @Override
    public void animateStoped() {
        LogUtils.d("paperwidget stop...");
    }

    protected void animationFinish(int dir) {
//        Logger.i("Chapter:paintPage", "animationFinish：norepaint = false");
        norepaint = false;
    }

    public void calcCornerXY(final float n, final float n2) {
        int mHeight;
        if (n2 <= this.mHeight / 2) {
            mHeight = 0;
        } else {
            mHeight = this.mHeight;
        }
        this.mCornerY = mHeight;
        this.mCornerX = this.mWidth;
    }

    /**
     * 判断是否能够翻页过去
     *
     * @return
     */
    public boolean canDragOver(float x, float y) {
        //zhangjian 2016.5.4 加入在四个角判断不让翻页
        if (isTurnBack()) {
            return true;
        }
        boolean ret;
        int w = Math.min(mWidth, mHeight) / 8;
        if (x < w && y < w) { //左上角
            mPressed.x = x;
            mPressed.y = y;
            return false;
        } else if (x < w && y > mHeight - w) { //左下角
            mPressed.x = x;
            mPressed.y = y;
            return false;
        } else if (x > mWidth - w && y < w) { //右上角
            mPressed.x = x;
            mPressed.y = y;
            return false;
        } else if (x > mWidth - w && y > mHeight - w) { //右下角
            mPressed.x = x;
            mPressed.y = y;
            return false;
        }

        ret = mTouchToCornerDis > mWidth / 5; //判断距离
        if (!ret) {
            int value = (int) (x - mPressed.x) + (int) (y - mPressed.y);
            mPressed.x = x;
            mPressed.y = y;
            ret = value <= 100;
        }
        return ret;
    }


    /**
     * 取消当前的翻页动画
     */
    protected void canceltrun() {
        mTouch.x = mCornerX - 0.09f;
        mTouch.y = mCornerY - 0.09f;
        this.postInvalidate();
    }

    public synchronized void computeScroll() {
        super.computeScroll();

        int animateMode = TangYuanSharedPrefUtils.getInstance().getReaderPagerAnim(ReaderConfigWrapper.PAGER_ANIM_BOOK);

        switch (animateMode) {
            case ReaderConfigWrapper.PAGER_ANIM_COVER:
                if (mScroller.computeScrollOffset()) {
                    slidex = mScroller.getCurrX();
                    slidey = mScroller.getCurrY();

                    if(mWidth+slidex==0||mWidth-slidex==0){
                        Log.d("TAG=Scroll","abortAnimation222222");
                        abortAnimation();
                    }else{
                        postInvalidate();
                    }
                }
                break;
            case ReaderConfigWrapper.PAGE_ORIENTATION_VERTICAL:
                if (mScroller.computeScrollOffset()) {
                    slidex = mScroller.getCurrX();
                    slidey = mScroller.getCurrY();
                    postInvalidate();
                }
                break;
            default:
                //Log.v(TAG, "" + mScroller.computeScrollOffset());
                if (mScroller.computeScrollOffset()) {
                    int x = mScroller.getCurrX();
                    int y = mScroller.getCurrY();
                    mTouch.x = x;
                    mTouch.y = y;
                    //Log.v(TAG, "" + x + ", " + y);
                    if((mWidth + x >= 100) && x < 0 || (2 * mWidth - x >= 100) && x > 0){
                        postInvalidate();
                    }else{
                        Log.d("TAG=Scroll","abortAnimation333333");
                        abortAnimation();
                    }
                    //postInvalidate();
                }
                break;
        }
    }

    public void destroy() {
        this.hasDestroy = true;
    }

    public void checkEvent(MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                slide = false;
                mFirstPressed.x = event.getX();
                mFirstPressed.y = event.getY();
                break;
            case MotionEvent.ACTION_MOVE:
                slide = true;
                if (mFirstMoved.x == 0) {
                    mFirstMoved.x = event.getX();
                    if (mFirstMoved.x - mFirstPressed.x > 0) {
                        mIsTurnBack = true;
                    } else {
                        mIsTurnBack = false;
                    }
                }
                break;
            case MotionEvent.ACTION_UP:
                mFirstMoved.x = 0;
                if (isClick(event.getX(), event.getY())) {
                    mIsClicked = true;
                } else {
                    mIsClicked = false;
                }
                break;

        }


    }

    /**
     * 处理事件
     *
     * @param event
     */
    public void doTouchEvent(MotionEvent event) {
        doTouchEvent(event,0);
    }

    public boolean doTouchEvent(MotionEvent event,int time) {
        float x = event.getX();
        float y = event.getY();

        //Log.v(TAG, "dotouch: " + event.getX() + ", " + event.getY() + ", " + mWidth + ", " + mHeight);

        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            mTouch.x = event.getX();
            mTouch.y = event.getY();
            mPressed.x = event.getX();
            mPressed.y = event.getY();

            mTouch = adjust(mTouch);
            mPressed = adjust(mPressed);
            norepaint = true;

        } else if (event.getAction() == MotionEvent.ACTION_MOVE) {
            mTouch.x = x;
            mTouch.y = y;
            mTouch = adjust(mTouch);
            this.postInvalidate();
        } else if (event.getAction() == MotionEvent.ACTION_UP
            || event.getAction() == MotionEvent.ACTION_CANCEL) {
            norepaint = false;

            if (canDragOver(x, y)) {
                startAnimation(ReaderConfigWrapper.getInstance().getAnimateTime(),time);
            } else {
                mTouch.x = mCornerX - 0.09f;
                mTouch.y = mCornerY - 0.09f;
                this.postInvalidate();
                return true;
            }

            this.postInvalidate();
        }
        return false;
    }

    /**
     * 绘制翻起页的阴影
     */
    public void drawCurrentPageShadow(Canvas canvas) {
        final int dist = 15;
        double degree = 0;
        if (mPressed.x > mWidth / 2 && mPressed.y < mHeight / 2) { //右上
            degree = Math.PI / 4 - Math.atan2(mBezierControl1.y - mTouch.y, mTouch.x - mBezierControl1.x);
        } else if (mPressed.x > mWidth / 2 && mPressed.y > mHeight / 2) { //右下
            degree = Math.PI / 4 - Math.atan2(mTouch.y - mBezierControl1.y, mTouch.x - mBezierControl1.x);
        } else if (mPressed.x < mWidth / 2 && mPressed.y < mHeight / 2) { //左上
            degree = Math.PI / 4 - Math.atan2(mBezierControl1.y - mTouch.y, mTouch.x - mBezierControl1.x);
        } else if (mPressed.x < mWidth / 2 && mPressed.y > mHeight / 2) { //左下
            degree = Math.PI / 4 - Math.atan2(mTouch.y - mBezierControl1.y, mTouch.x - mBezierControl1.x);
        }
        // 翻起页阴影顶点与touch点的距离
        double d1 = (float) dist * 1.414 * Math.cos(degree);
        double d2 = (float) dist * 1.414 * Math.sin(degree);
        float x = (float) (mTouch.x + d1);
        float y = 0;
        if (mPressed.x > mWidth / 2 && mPressed.y < mHeight / 2) { //右上
            y = (float) (mTouch.y + d2);
        } else if (mPressed.x > mWidth / 2 && mPressed.y > mHeight / 2) { //右下
            y = (float) (mTouch.y - d2);
        } else if (mPressed.x < mWidth / 2 && mPressed.y < mHeight / 2) { //左上
            y = (float) (mTouch.y + d2);
        } else if (mPressed.x < mWidth / 2 && mPressed.y > mHeight / 2) { //左下
            y = (float) (mTouch.y - d2);
        }
        mPath1.reset();
        mPath1.moveTo(x, y);
        mPath1.lineTo(mTouch.x, mTouch.y);
        mPath1.lineTo(mBezierControl1.x, mBezierControl1.y);
        mPath1.lineTo(mBezierStart1.x, mBezierStart1.y);
        mPath1.close();
        float rotateDegrees;
        canvas.save();

        clipPathXor(canvas,mPath0);
        canvas.clipPath(mPath1, Op.INTERSECT);
        int leftx = 0;
        int rightx = 0;
        GradientDrawable mCurrentPageShadow = null;
        if (mPressed.x > mWidth / 2 && mPressed.y < mHeight / 2) { //右上
            leftx = (int) (mBezierControl1.x);
            rightx = (int) mBezierControl1.x + dist;
            mCurrentPageShadow = mFrontShadowDrawableVLR;
        } else if (mPressed.x > mWidth / 2 && mPressed.y > mHeight / 2) { //右下
            leftx = (int) (mBezierControl1.x - dist);
            rightx = (int) mBezierControl1.x + 1;
            mCurrentPageShadow = mFrontShadowDrawableVRL;
        } else if (mPressed.x < mWidth / 2 && mPressed.y < mHeight / 2) { //左上
            leftx = (int) (mBezierControl1.x);
            rightx = (int) mBezierControl1.x + dist;
            mCurrentPageShadow = mFrontShadowDrawableVLR;
        } else if (mPressed.x < mWidth / 2 && mPressed.y > mHeight / 2) { //左下
            leftx = (int) (mBezierControl1.x - dist);
            rightx = (int) mBezierControl1.x + 1;
            mCurrentPageShadow = mFrontShadowDrawableVRL;
        }

        rotateDegrees = (float) Math.toDegrees(Math.atan2(mTouch.x - mBezierControl1.x, mBezierControl1.y - mTouch.y));
        canvas.rotate(rotateDegrees, mBezierControl1.x, mBezierControl1.y);
        if (mCurrentPageShadow != null) {
            mCurrentPageShadow.setBounds(leftx, (int) (mBezierControl1.y - mMaxLength), rightx, (int) (mBezierControl1.y));
            mCurrentPageShadow.draw(canvas);
        }
        canvas.restore();

        mPath1.reset();
        mPath1.moveTo(x, y);
        mPath1.lineTo(mTouch.x, mTouch.y);
        mPath1.lineTo(mBezierControl2.x, mBezierControl2.y);
        mPath1.lineTo(mBezierStart2.x, mBezierStart2.y);
        mPath1.close();
        canvas.save();
        clipPathXor(canvas,mPath0);
        canvas.clipPath(mPath1, Op.INTERSECT);
        //		if (mIsRTandLB) {
        //			leftx = (int) (mBezierControl2.y);
        //			rightx = (int) (mBezierControl2.y + dist);
        //			mCurrentPageShadow = mFrontShadowDrawableHTB;
        //		} else {
        //			leftx = (int) (mBezierControl2.y - dist);
        //			rightx = (int) (mBezierControl2.y + 1);
        //			mCurrentPageShadow = mFrontShadowDrawableHBT;
        //		}
        if (mPressed.x > mWidth / 2 && mPressed.y < mHeight / 2) { //右上
            leftx = (int) (mBezierControl2.y);
            rightx = (int) (mBezierControl2.y + dist);
            mCurrentPageShadow = mFrontShadowDrawableHTB;
        } else if (mPressed.x > mWidth / 2 && mPressed.y > mHeight / 2) { //右下
            leftx = (int) (mBezierControl2.y);
            rightx = (int) (mBezierControl2.y + dist);
            mCurrentPageShadow = mFrontShadowDrawableHTB;
        } else if (mPressed.x < mWidth / 2 && mPressed.y < mHeight / 2) { //左上
            leftx = (int) (mBezierControl2.y);
            rightx = (int) (mBezierControl2.y + dist);
            mCurrentPageShadow = mFrontShadowDrawableHTB;
        } else if (mPressed.x < mWidth / 2 && mPressed.y > mHeight / 2) { //左下
            leftx = (int) (mBezierControl2.y);
            rightx = (int) (mBezierControl2.y + dist);
            mCurrentPageShadow = mFrontShadowDrawableHTB;
        }

        rotateDegrees = (float) Math.toDegrees(Math.atan2(mBezierControl2.y - mTouch.y, mBezierControl2.x - mTouch.x));
        canvas.rotate(rotateDegrees, mBezierControl2.x, mBezierControl2.y);
        float temp;
        if (mBezierControl2.y < 0)
            temp = mBezierControl2.y - mHeight;
        else
            temp = mBezierControl2.y;

        int hmg = (int) Math.hypot(mBezierControl2.x, temp);
        if (mCurrentPageShadow != null) {
            if (hmg > mMaxLength) {
                mCurrentPageShadow.setBounds((int) (mBezierControl2.x - dist) - hmg, leftx, (int) (mBezierControl2.x + mMaxLength) - hmg, rightx);
            } else {
                mCurrentPageShadow.setBounds((int) (mBezierControl2.x - mMaxLength), leftx, (int) (mBezierControl2.x), rightx);
            }
            mCurrentPageShadow.draw(canvas);
        }
        canvas.restore();
    }

    /**
     * 求解直线P1P2和直线P3P4的交点坐标
     */
    public PointF getCross(PointF P1, PointF P2, PointF P3, PointF P4) {
        PointF CrossP = new PointF();
        // 二元函数通式： y=ax+b
        float a1 = (P2.y - P1.y) / (P2.x - P1.x);
        float b1 = ((P1.x * P2.y) - (P2.x * P1.y)) / (P1.x - P2.x);

        float a2 = (P4.y - P3.y) / (P4.x - P3.x);
        float b2 = ((P3.x * P4.y) - (P4.x * P3.y)) / (P3.x - P4.x);
        CrossP.x = (b2 - b1) / (a1 - a2);
        CrossP.y = a1 * CrossP.x + b1;
        return CrossP;
    }

    public boolean isAnimation() {
        return !this.mScroller.isFinished();
    }

    public boolean isTurnBack() {
//        return this.mPressed.x <= this.mWidth / 2;
//        return mFirstMoved.x - mFirstPressed.x > 0;
        if (TangYuanSharedPrefUtils.getInstance().isSingleTouchMode()) {
            return false;
        } else {
            if (TangYuanSharedPrefUtils.getInstance().getReaderPagerAnim(ReaderConfigWrapper.PAGER_ANIM_BOOK) == ReaderConfigWrapper.PAGER_ANIM_BOOK) {
                if (mIsClicked) {
                    return (int) this.mPressed.x <= this.mWidth / 2;
                }
                return mIsTurnBack;
            }
            return (int) this.mPressed.x <= this.mWidth / 2;
        }
    }

    private boolean isClick(float f, float f2) {
        return !this.slide && Math.hypot((double) (f - this.mFirstPressed.x), (double) (f2 - this.mFirstPressed.y)) <= 20.0d;
    }

    @Override
    protected void onDraw(final Canvas canvas) {
        canvas.drawColor(ReaderConfigWrapper.getInstance().getPageBgColor());
        if (null == mCurPageBitmap || null == mNextPageBitmap) {
//			canvas.drawColor(ReaderConfig.getInstance().getPageBgColor());
            return;
        }


        int animateMode = TangYuanSharedPrefUtils.getInstance().getReaderPagerAnim(ReaderConfigWrapper.PAGER_ANIM_BOOK);
        if (!mIsAnim) {
            animateMode = ReaderConfigWrapper.PAGER_ANIM_NONE;
        }
        switch (animateMode) {

            case ReaderConfigWrapper.PAGER_ANIM_NONE: {
                canvas.drawBitmap(this.mCurPageBitmap, 0.0f, 0.0f, null);
                mIsAnim = true;
                break;
            }
            case ReaderConfigWrapper.PAGER_ANIM_BOOK: {
//                    this.calcPoints();
                if (!isTurnBack()) {
                    this.calcPoints();
                    this.drawCurrentPageArea(canvas, this.mCurPageBitmap, this.mPath0);
                    this.drawNextPageAreaAndShadow(canvas, this.mNextPageBitmap);
                    this.drawCurrentPageShadow(canvas);
                    this.drawCurrentBackArea(canvas, this.mCurPageBitmap);
                    break;
                }
                this.mTouch.y = this.mHeight - 0.1f;
//                    this.mPressed.y = mTouch.y;
                this.calcPoints();
                this.drawCurrentPageArea(canvas, this.mNextPageBitmap, this.mPath0);
                this.drawNextPageAreaAndShadow(canvas, this.mCurPageBitmap);
                this.drawCurrentPageShadow(canvas);
                this.drawCurrentBackArea(canvas, this.mNextPageBitmap);
                break;
            }
            case ReaderConfigWrapper.PAGER_ANIM_COVER: {
                this.drawSlide(canvas, this.mCurPageBitmap, 0, 0);
                this.drawSlide(canvas, this.mNextPageBitmap, this.slidey, 0);
                break;
            }
            case ReaderConfigWrapper.PAGE_ORIENTATION_VERTICAL: {
                this.drawSlide(canvas, this.mCurPageBitmap, 0, 0);
                this.drawSlide(canvas, this.mNextPageBitmap, 0, this.slidey);
                break;
            }
            default: {
                canvas.drawBitmap(this.mCurPageBitmap, 0.0f, 0.0f, (Paint) null);
                break;
            }
        }


    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);
//        if (null != bottomView) {
//            bottomView.layout(left, top, right, bottom);
//        }
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);

//        if (null != bottomView) {
//            bottomView.measure(widthMeasureSpec, heightMeasureSpec);
//        }
    }

    public void setBitmaps(final Bitmap mCurPageBitmap, final Bitmap mNextPageBitmap) {
        this.mCurPageBitmap = mCurPageBitmap;
        this.mNextPageBitmap = mNextPageBitmap;
    }

    public void setScreen(final int mWidth, final int mHeight) {
        this.mWidth = mWidth;
        this.mHeight = mHeight;
    }

    public void sizeChanged(final int mWidth, final int mHeight) {
        this.mWidth = mWidth;
        this.mHeight = mHeight;
        this.mMaxLength = (float) Math.hypot(this.mWidth, this.mHeight);
    }



    public void startAnimation(boolean next,boolean anim) {
        norepaint = true;
        int animateTime = ReaderConfigWrapper.getInstance().getAnimateTime();
        int animateMode = TangYuanSharedPrefUtils.getInstance().getReaderPagerAnim(ReaderConfigWrapper.PAGER_ANIM_BOOK);

        if (anim && !TangYuanSharedPrefUtils.getInstance().isReadModeOn()) {
            switch (animateMode) {
                case ReaderConfigWrapper.PAGE_ORIENTATION_VERTICAL:
                    if (next) {
                        mScroller.startScroll(0, mHeight, -mHeight, -mHeight, animateTime);
                    } else {
                        mScroller.startScroll(0, -mHeight, mHeight, mHeight, animateTime);
                    }
                    break;
                case ReaderConfigWrapper.PAGER_ANIM_COVER:
                    if (next) {
                        mScroller.startScroll(0, mWidth, -mWidth, -mWidth, animateTime);
                    } else {
                        norepaint = true;
                        mScroller.startScroll(0, -mWidth, mWidth, mWidth, animateTime);
                    }
                    break;
                case ReaderConfigWrapper.PAGER_ANIM_BOOK:
                    final int dx = mWidth / 7,
                            dy = mHeight / 7;
                    if (next) {
                        mScroller.startScroll(mWidth - dx, mHeight - dy, -mWidth * 2 + dx, dy - 1, animateTime);
                    } else {
                        mScroller.startScroll(dx, dy, mWidth * 2 - dx, 1 - dy, animateTime);
                    }
                    break;
            }
        }
        stopTimmer();

        if (!mScroller.isFinished()) {
            timer = new Timer();
            timer.schedule(newScrollerListener(), 0, 30);
        } else {
            animationFinish(next ? ReaderConfigWrapper.NEXT_PAGE : ReaderConfigWrapper.PREVIOUS_PAGE);
        }
    }

    private void clipPathXor(Canvas canvas,Path path){
        if(Build.VERSION.SDK_INT >= 26){
            Path mPathXOR = new Path();
            mPathXOR.moveTo(0,0);
            mPathXOR.lineTo(getWidth(),0);
            mPathXOR.lineTo(getWidth(),getHeight());
            mPathXOR.lineTo(0,getHeight());
            mPathXOR.close();
            //以上根据实际的Canvas或View的大小，画出相同大小的Path即可
            mPathXOR.op(path, Path.Op.XOR);
            canvas.clipPath(mPathXOR);
        }else {
            canvas.clipPath(path, Op.XOR);
        }
    }
}
