package com.chineseall.reader.lib.reader.view.vertical;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewConfiguration;
import android.view.animation.LinearInterpolator;
import android.widget.Scroller;

import com.chineseall.reader.lib.reader.config.ReaderConfigWrapper;
import com.chineseall.reader.lib.reader.config.TangYuanSharedPrefUtils;
import com.chineseall.reader.lib.reader.core.Constants;
import com.chineseall.reader.lib.reader.core.ReaderClient;
import com.chineseall.reader.lib.reader.entities.Chapter;
import com.chineseall.reader.lib.reader.utils.LogUtils;
import com.chineseall.reader.lib.reader.view.ReaderView;

public abstract class YView extends ReaderView implements OnTouchListener {
    private int CLICK_DIS;
    protected View activeview;
    private boolean cancelClick;
    /**
     * 当前事件是否交给bottomView处理
     */
    protected boolean handleEventByBottomView;
    protected boolean handleEventByLockView;
    protected boolean handleEventByChapterEndView;
    private float mLastMotionY;
    private int mMaximumVelocity;
    private int mMinimumVelocity;
    private Scroller mScroller;
    private VelocityTracker mVelocityTracker;
    protected int maxY;
    protected int minY;

    public YView(Context context) {
        super(context);
        this.minY = 0;
        this.maxY = Integer.MAX_VALUE;
        this.mLastMotionY = 0.0f;
        this.mScroller = new Scroller(getContext());
        this.CLICK_DIS = 400;
        this.cancelClick = true;
        init();
    }

    public YView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.minY = 0;
        this.maxY = Integer.MAX_VALUE;
        this.mLastMotionY = 0.0f;
        this.mScroller = new Scroller(getContext(), new LinearInterpolator());
        this.CLICK_DIS = 400;
        this.cancelClick = true;
        init();
    }

    public YView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.minY = 0;
        this.maxY = Integer.MAX_VALUE;
        this.mLastMotionY = 0.0f;
        this.mScroller = new Scroller(getContext());
        this.CLICK_DIS = 400;
        this.cancelClick = true;
        init();
    }

    private void init() {
        setOnTouchListener(this);
        ViewConfiguration viewConfiguration = ViewConfiguration.get(getContext());
        this.mMinimumVelocity = viewConfiguration.getScaledMinimumFlingVelocity();
        this.mMaximumVelocity = viewConfiguration.getScaledMaximumFlingVelocity();
    }

    private void obtainVelocityTracker(MotionEvent motionEvent) {
        if (this.mVelocityTracker == null) {
            this.mVelocityTracker = VelocityTracker.obtain();
        }
        this.mVelocityTracker.addMovement(motionEvent);
    }

    private void releaseVelocityTracker() {
        if (this.mVelocityTracker != null) {
            this.mVelocityTracker.recycle();
            this.mVelocityTracker = null;
        }
    }

    public void animateStoped() {
    }

    public void computeScroll() {
        if (this.mScroller.computeScrollOffset()) {
            getScrollX();
            getScrollY();
            scrollTo(this.mScroller.getCurrX(), this.mScroller.getCurrY());
            postInvalidate();
            return;
        }
        animateStoped();
    }

    public void fling(int velocityY) {
        //mScroller.fling(getScrollX(), getScrollY(), 0, velocityY, 0, 0, Integer.MIN_VALUE, Integer.MAX_VALUE);
        mScroller.fling(getScrollX(), getScrollY(), 0, velocityY, 0, 0, minY, maxY - getHeight());
        invalidate();
    }

    public void onAttachedToWindow() {
        super.onAttachedToWindow();
//        EventBus.getDefault().register(this);
    }

    protected void onClick(int x, int y) {

    }

    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
//        EventBus.getDefault().unregister(this);
    }

//    public void onEventMainThread(ReadEndPageLayoutMessage readEndPageLayoutMessage) {
//        requestLayout();
//        requestRepaint("\u5c3e\u9875\u66f4\u65b0");
//    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);
//        if (null != bottomView) {
//            bottomView.layout(left, top, right, bottom);
//        }
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);

//        if (null != bottomView) {
//            bottomView.measure(widthMeasureSpec, heightMeasureSpec);
//        }
        int height = View.MeasureSpec.getSize(heightMeasureSpec);
        if (maxY == Integer.MAX_VALUE) {
            maxY = height * 2;
        }
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN && event.getEdgeFlags() != 0) {
            return false;
        }

        if (null == article) {
            return false;
        }

        obtainVelocityTracker(event);
        final int action = event.getAction();
        final float x = event.getX();
        final float y = event.getY();

        if (checkCanDraw(event, x, y)) return true;

        switch (action) {
            case MotionEvent.ACTION_DOWN:
                if (hasSelectionArea()) {
                    clearSelection();
                    return true;
                }
                hasHandledLongPress = false;
                postLongPressCallBack();
                handleDownEvent(event, x, y);
                break;
            case MotionEvent.ACTION_MOVE:
                if (!hasHandledLongPress) {
                    handleMoveEvent(x, y);
                    if (cancelClick) {
                        removeLongPressRunnable();
                    }
                }
                break;
            case MotionEvent.ACTION_UP:
            case MotionEvent.ACTION_CANCEL:
                if (!hasHandledLongPress) {
                    removeLongPressRunnable();
                    handleUpEvent(event, x, y);
                }
                break;
        }

        return true;
    }

    private boolean checkCanDraw(MotionEvent event, float x, float y) {
        if (!article.canDraw) {
            if (article.getCurrentChapter().getStatus() == ReaderConfigWrapper.STATUS_FAILED && event.getAction() == MotionEvent.ACTION_UP) {
                if (article.getCurrentChapter().reloadRectF != null && article.getCurrentChapter().reloadRectF.contains(x, y)) {
                    article.getChapterHelper().loadChapter(article.getCurrentChapter(), null, 0);
                } else if (onRectClickCallback != null) {
                    onRectClickCallback.menuRectClick();
                }
            } else {//不可绘制时，保证可以唤起菜单，进行操作，不至于给人一种假死的感觉
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (menu.contains((int) x, (int) y)) {
                        if (onRectClickCallback != null) {
                            onRectClickCallback.menuRectClick();
                        }
                    }
                }
            }
            return true;
        }
        return false;
    }

    private void handleUpEvent(MotionEvent event, float x, float y) {
        if (handleEventByBottomView) {
//                    int offy = article.getArticleHeight() - getScrollY();
//                    event.setLocation(x, y - offy); //强制将事件转换为view适用的坐标
//                    handleEventByBottomView = bottomView.dispatchTouchEvent(event);
//                    postInvalidate();
            ReaderClient.getInstance().getCallBackContainer().getOnReadCompleteCallBack().complete(article == null ? "" : article.getBookid() + "");
            handleEventByBottomView = false;
        } else if (handleEventByLockView) {
            getLockView().dispatchTouchEvent(event);
            handleEventByLockView = false;
        }
//                else if (handleEventByChapterEndView) {
//                    ChapterEndView chapterEndView = getChapterEndView();
//                    if (chapterEndView!=null) {
//                        chapterEndView.dispatchTouchEvent(event);
//                    }
//                    handleEventByChapterEndView = false;
//                }
        else {
            final VelocityTracker velocityTracker = mVelocityTracker;
            velocityTracker.computeCurrentVelocity(1000, mMaximumVelocity);
            int initialVelocity = (int) velocityTracker.getYVelocity();
            if ((Math.abs(initialVelocity) > mMinimumVelocity)) {
                fling(-initialVelocity);
            } else if (!cancelClick) {
                float dis = (x - mPressedPoint.x) * (x - mPressedPoint.x) + (y - mPressedPoint.y) * (y - mPressedPoint.y);
                if (dis < CLICK_DIS) {
                    onClick((int) mPressedPoint.x, (int) mPressedPoint.y);
                }
            }
            releaseVelocityTracker();
        }
        postInvalidate();
    }

    private void handleMoveEvent(float x, float y) {
        final int deltaY = (int) (mLastMotionY - y);
        mLastMotionY = y;
        if (!cancelClick) {
            float dis = (x - mPressedPoint.x) * (x - mPressedPoint.x) + (y - mPressedPoint.y) * (y - mPressedPoint.y);
            if (dis > CLICK_DIS) {
                cancelClick = true;
            }
        } else {
            if (onRectClickCallback != null) {
                onRectClickCallback.hideMenu();
            }
        }
        if (deltaY < 0) {
            if (getScrollY() > 0) {
                scrollBy(0, deltaY);
            }
        } else if (deltaY > 0) {
            boolean mIsInEdge = getScrollY() < maxY - getHeight();
            if (mIsInEdge) {
                scrollBy(0, deltaY);
            } else {
                handleEventByBottomView = true;
            }
        }
        //scrollBy(0, deltaY);
        postInvalidate();
    }

    private void handleDownEvent(MotionEvent event, float x, float y) {
        if (!mScroller.isFinished()) {
            Log.d("TAG=Scroll","abortAnimation444444");
            mScroller.abortAnimation();
        }

        cancelClick = false;
        mPressedPoint.x = x;
        mPressedPoint.y = y;
        mLastMotionY = y;

        int currentChapterTop = article.getCurrentChapter().getChapterTop();
        int scrolly = getScrollY();
        boolean isCurrentChapter = true;

        if (y + scrolly < currentChapterTop) {
            LogUtils.d("前一章节");
            isCurrentChapter = false;
            article.setClickPreviousChapter(true);
        } else {
            LogUtils.d("当前章节");
            article.setClickPreviousChapter(false);
        }

        Chapter temChapter = isCurrentChapter ? article.getCurrentChapter() : article.getPreviousChapter();

        if (getScrollY() > article.getArticleHeight() - getHeight()) {
//                    if(bottomView!=null){
//                        int offy = article.getArticleHeight() - getScrollY();
//                        event.setLocation(x, y - offy); //强制将事件转换为view适用的坐标
//                        handleEventByBottomView = bottomView.dispatchTouchEvent(event);
//                    }

//                    postInvalidate();
        } else if (temChapter != null && temChapter.getStatus() == ReaderConfigWrapper.STATUS_LOCK) {
            int offy = getScrollY() - temChapter.getChapterTop();
            event.setLocation(x, y + offy); //强制将事件转换为view适用的坐标
            handleEventByLockView = getLockView().dispatchTouchEvent(event);
        } else {
//                    int offy = getScrollY() - temChapter.getChapterTop();
//                    int chapterHeight = temChapter.getChapterHeight();
//                    int leftHeight = chapterHeight - offy;
//                    if (leftHeight<getHeight()){//在最后一页
//                        event.setLocation(x, y + offy%getHeight()); //强制将事件转换为view适用的坐标
//                        ChapterEndView chapterEndView = getChapterEndView();
//                        if (chapterEndView!=null) {
//                            handleEventByChapterEndView = chapterEndView.dispatchTouchEvent(event);
//                        }
//                    }

        }

			/*
			if (getScrollY() > article.getArticleHeight() - getHeight()) {
				if(bottomView!=null){
					int offy = article.getArticleHeight() - getScrollY();
					event.setLocation(x, y - offy); //强制将事件转换为view适用的坐标
					handleEventByBottomView = bottomView.dispatchTouchEvent(event);
				}
				postInvalidate();
			} else if(article.getCurrentChapter()!=null&&article.getCurrentChapter().getStatus()==ReaderConfig.STATUS_LOCK){
				int offy=getScrollY()-article.getCurrentChapter().getChapterTop();
				event.setLocation(x, y+offy); //强制将事件转换为view适用的坐标
				handleEventByLockView=getLockView().dispatchTouchEvent(event);
			} else if(article.getCurrentChapter()!=null&&article.getCurrentChapter().getStatus()==ReaderConfig.STATUS_SUBSCRIBE){
				int offy=getScrollY()-article.getCurrentChapter().getChapterTop();
				event.setLocation(x, y+offy); //强制将事件转换为view适用的坐标
				handleEventBySubscribeView=getSubscribeView().dispatchTouchEvent(event);
			}
			*/
			/*int realy = (int) y + getScrollY() - article.getArticleHeight();
			activeview = findActive(bottomView, (int) x, realy);
			if (null != activeview) {
				boolean handle = bottomView.dispatchTouchEvent(event);
				Logger.i("pressed.handle", "" + handle);
			}*/
    }

    public boolean performClick() {
        return super.performClick();
    }

    public void scroll(boolean z) {
        int min;
        if (!this.mScroller.isFinished()) {
            this.mScroller.forceFinished(true);
        }

        int pageTopBarHeight = ReaderConfigWrapper.getInstance().getPageTopBarHeight();
        pageTopBarHeight = (getHeight() - pageTopBarHeight) - ReaderConfigWrapper.getInstance().getPageBottomBarHeight();
        int scrollY = getScrollY();
        if (z) {
            int i = maxY - scrollY;
            if (i / pageTopBarHeight == 1) {
                min = i - getHeight();
            } else {
                min = Math.min((this.maxY - pageTopBarHeight) - scrollY, pageTopBarHeight);
            }
        } else {
            min = Math.max(-pageTopBarHeight, 0 - scrollY);
        }
        this.mScroller.startScroll(0, scrollY, 0, min, ReaderConfigWrapper.getInstance().getAnimateTime());
        invalidate();
    }

    public void startScroll() {
        int startY = getScrollY();
        int dy = TangYuanSharedPrefUtils.getInstance().getInt(Constants.AUTO_SCROLL_SPEED, 200);
        if (startY + dy <= this.maxY - getHeight()) {
            this.mScroller.startScroll(0, startY, 0, dy, ReaderConfigWrapper.getInstance().getAnimateTime());
            invalidate();
        }
    }

    public void stopScroll() {
        if (!this.mScroller.isFinished()) {
            this.mScroller.forceFinished(true);
        }
    }

    public void setRange(int i, int i2) {
//        Logger.i("YView:setRange", "\u603b\u9ad8\u5ea6\uff1a" + i2 + ", \u5355\u9875\u9ad8\u5ea6\uff1a" + this.article.getViewHeight(), new Object[0]);
        this.minY = i;
//        if (this.bottomView != null) {
//            this.maxY = getHeight() + i2;
//        } else
        {
            this.maxY = i2;
        }
    }

    @Override
    protected void onScrollChanged(int l, int t, int oldl, int oldt) {
        if (TangYuanSharedPrefUtils.getInstance().getBoolean(Constants.AUTO_SCROLL, false)) {
            if (getCurrentChapter().isVip()) {
                ReaderClient.getInstance().getCallBackContainer().getOnVerticalViewCallBack().exitAutoScrollMode();
            } else if (getCurrentChapter().getStatus() != ReaderConfigWrapper.STATUS_LOADED) {//章节内容未获取时，暂停自动滚动
                ReaderClient.getInstance().getCallBackContainer().getOnVerticalViewCallBack().exitAutoScrollMode();
            } else {
                startScroll();
            }
        }
    }
}
