package com.chineseall.reader.lib.reader.view.vertical;

import android.content.Context;
import android.graphics.Canvas;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.chineseall.reader.lib.reader.callbacks.OnInitChapterHelperCallBack;
import com.chineseall.reader.lib.reader.core.ReaderClient;
import com.chineseall.reader.lib.reader.entities.Article;
import com.chineseall.reader.lib.reader.entities.Chapter;
import com.chineseall.reader.lib.reader.utils.LogUtils;

import java.util.ArrayList;

public class YViewBiz extends YView {
    private boolean isShowBottom = false;
    private Canvas mCanvas;

    public YViewBiz(Context context) {
        super(context);
    }

    public YViewBiz(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public YViewBiz(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    /**
     * @param id
     * @param defid 默认阅读的ID
     */
    @Override
    public void loadArticle(final int id, final String defid, ArrayList<Chapter> chapters, int progress, @NonNull OnInitChapterHelperCallBack callBack) {
        article = new Article(this, id, chapters);
        article.setChapterHelper(callBack.init(article));
        article.load(defid, progress);

        post(() -> scrollTo(0, 0));
    }

    public void reloadArticleChapter(final int id, ArrayList<Chapter> chapters) {
        if (article != null) {
            article.reloadChapters(id, chapters);
        }
    }

    @Override
    public void articleHeightChanged(int newHeight) {
        setRange(0, newHeight);
        // postInvalidate();
    }

    @Override
    public void jumpChapter(String id, int progress) {
        if (!TextUtils.isEmpty(id) && article != null) {
            article.jumpToChapter(id);
        } else {
            Toast.makeText(getContext(), "跳转失败!", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onClick(int x, int y) {
        if (null != article) {
            if (!article.onClick(x, y + getScrollY())) {
                //Toast.makeText(getContext(), "你点击了我", Toast.LENGTH_LONG).show();
                if (menu.contains(x, y)) {
                    if (onRectClickCallback != null) {
                        onRectClickCallback.menuRectClick();
                    }
//					Toast.makeText(getContext(), "弹菜单", Toast.LENGTH_SHORT).show();
                } else {
                    boolean autoScrollMenuShow = ReaderClient.getInstance().getCallBackContainer().getOnVerticalViewCallBack().isAutoScrollMenuShow(this);
                    if (autoScrollMenuShow) {//如果自动菜单已经显示，禁用其他操作
                        if (onRectClickCallback != null) {
                            onRectClickCallback.menuRectClick();
                        }
                    } else {
                        if (y + getScrollY() < article.getArticleHeight()) {
                            scroll(y > getHeight() / 2);
                        }
                    }
                }
            }
        } else {
            if (menu.contains(x, y)) {
                if (onRectClickCallback != null) {
                    onRectClickCallback.menuRectClick();
                }
            }
        }

    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
//        canvas.drawColor(ReaderConfig.getInstance().getPageBgColor());
        mCanvas = canvas;
        if (null == article) {
            drawOpening(canvas);
            return;
        } else {
            article.paint(canvas, getScrollY());
        }
    }

    @Override
    public void setOffsetY(final int top) {
//        Logger.i("YViewBiz:setOffsetY", "top: " + top);
        post(() -> scrollTo(0, top));

        postInvalidate();
    }

    @Override
    public void animateStoped() {
        readChapter();
        if (null != article) {
            //article.freeMemory();
        }
    }

    public void loadNextChapter() {
        if (this.article != null) {
            this.article.loadNextChapter();
        }
    }

    public void loadPreChapter() {
        if (this.article != null) {
            this.article.loadPreviousChapter();
        }
    }

    @Override
    public boolean trunpage(boolean b, boolean anim) {
        if (b) {
            // scroll(y > getHeight() / 2);
        } else {

        }
        return false;
    }

    @Override
    public void requestRepaint(String str) {
        super.requestRepaint(str);
//        if (canRepaint()) {
//            repaint();
//        }
    }

    @Override
    protected void repaint() {
        super.requestRepaint("");
//        if (mCanvas != null) {
//            LogUtils.d(mCanvas + ", " + getScrollY());
//            article.paint(mCanvas, getScrollY());
//            postInvalidate();
//        }
    }
}
