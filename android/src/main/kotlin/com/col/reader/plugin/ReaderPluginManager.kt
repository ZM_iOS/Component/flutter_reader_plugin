package com.col.reader.plugin

import android.app.Application
import android.content.Context
import android.view.MotionEvent
import android.view.View
import androidx.annotation.NonNull
import com.chineseall.reader.lib.reader.callbacks.OnAdViewCallBack
import com.chineseall.reader.lib.reader.callbacks.OnAdViewReceiver
import com.chineseall.reader.lib.reader.callbacks.OnLockViewCallBack
import com.chineseall.reader.lib.reader.config.ReaderConfigWrapper
import com.chineseall.reader.lib.reader.config.TangYuanSharedPrefUtils
import com.chineseall.reader.lib.reader.core.ReaderClient
import com.chineseall.reader.lib.reader.entities.Chapter
import com.chineseall.reader.lib.reader.utils.PaintHelper
import com.chineseall.reader.lib.reader.view.ILockView
import com.chineseall.reader.lib.reader.view.ReaderView
import com.col.reader.plugin.read.PluginReadConfig
import com.col.reader.plugin.read.ReaderViewPlugin
import io.flutter.embedding.android.FlutterActivity
import io.flutter.embedding.engine.FlutterEngine
import splitties.init.appCtx

object ReaderPluginManager {

    fun initReadConfig(@NonNull activity: FlutterActivity, @NonNull flutterEngine: FlutterEngine) {
        initSdk(activity.application)
        //跳转到原生Android页面
        flutterEngine.plugins.add(ReaderViewPlugin(activity))
    }

    private fun initSdk(
        application: Application
    ) {

        TangYuanSharedPrefUtils.initSharePref(appCtx)
        ReaderConfigWrapper.init(PluginReadConfig(application))
        PaintHelper.init(ReaderConfigWrapper.getInstance())

        ReaderClient.Builder(appCtx)
            .drawLockView(object : OnLockViewCallBack {
                override fun draw(
                    readerView: ReaderView,
                    lockView: ILockView,
                    chapter: Chapter,
                ) {
                }

                override fun initInstance(context: Context): ILockView {
                    return object : ILockView {
                        override fun setCanDraw(flag: Boolean) {}

                        override fun getLockHeight(): Int {
                            return 0
                        }

                        override fun dispatchTouchEvent(ev: MotionEvent): Boolean {
                            return false
                        }
                    }
                }
            })
            .readComplete {

            }
            .log { tag, msg ->

            }
            .adViewProvider(object : OnAdViewCallBack {
                override fun get(receiver: OnAdViewReceiver?) {

                }

                override fun adClick(adView: View?) {

                }

                override fun adShow(adView: View?) {

                }
            })
            .build()
    }

}