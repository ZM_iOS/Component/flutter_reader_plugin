package com.col.reader.plugin.helper

import android.os.Handler
import android.os.Looper
import com.chineseall.reader.lib.reader.config.ReaderConfigWrapper
import com.chineseall.reader.lib.reader.config.TangYuanSharedPrefUtils
import com.chineseall.reader.lib.reader.entities.*
import com.chineseall.reader.lib.reader.utils.IChapterHelper
import com.chineseall.reader.lib.reader.utils.PaintHelper
import com.chineseall.reader.lib.reader.utils.StringUtil
import com.col.lib_book.utils.CacheFileUtils
import io.flutter.plugin.common.MethodChannel
import java.util.ArrayList
import java.util.concurrent.CopyOnWriteArrayList

open class IChapterHelperImpl(
    private val article: Article,
    private val mMethodChannel: MethodChannel,
    private val block: (chapter: Chapter?) -> Unit,
) : IChapterHelper {

    private val mHandler: Handler = Handler(Looper.getMainLooper())
    override fun isVerticalScrollMode(): Boolean {
        return article.isVerticalScrollMode
    }

    override fun loadChapter(self: Chapter?, next: Chapter?, progress: Int) {

        if (self == null) {
            return
        }

        mMethodChannel.invokeMethod(
            "getContent",
            "chapterId:${self.chapterId}",
            object : MethodChannel.Result {
                override fun success(content: Any?) {
                    updateContent(content, self, next, progress)
                }

                override fun error(errorCode: String, errorMessage: String?, errorDetails: Any?) {
                }

                override fun notImplemented() {
                }
            })
    }

    private fun updateContent(arguments: Any?, self: Chapter, next: Chapter?, progress: Int) {
        if (arguments is HashMap<*, *>) {
            if (arguments.contains("title")) {
                val title = arguments["title"].toString()
            }
            if (arguments.contains("content")) {
                val content = arguments["content"].toString().replace("<br><br>", "\n")
                val paragraphs: List<Paragraph> = CopyOnWriteArrayList()
                try {
                    val chapterContent =
                        String(content.toByteArray(), charset(CacheFileUtils.CHARSET_NAME))
                    val padding = ReaderConfigWrapper.getInstance().padding
                    val elements = ArrayList<RElement>()
                    elements.add(RText(PaintHelper.getInstance().contentPaint, chapterContent))
                    var status = ReaderConfigWrapper.STATUS_LOADED
                    if (self.isVip) {
                        status = ReaderConfigWrapper.STATUS_LOCK
                    }
                    if (status == ReaderConfigWrapper.STATUS_LOADED || status == ReaderConfigWrapper.STATUS_LOCK) {
                        elements.add(
                            0,
                            RTitle(PaintHelper.getInstance().titlePaint, self.chapterName)
                        )

                        // elements.add(RAD())

                        /** 连续空行数>=1, 保留一个  */
                        var previousElementIsBlankLine = false //上一个是否是空行
                        for (ele in elements) {
                            if (ele is RText) {
                                if (StringUtil.isBlank(ele.text)) {
                                    if (!previousElementIsBlankLine) {
                                        val blankTextBlock =
                                            LineBlock(article.view, self, LineBlock.TEXT, ele)
                                        blankTextBlock.str = ""
                                        blankTextBlock.height = 20 //文字有时太大了，所以写死
                                    }
                                    previousElementIsBlankLine = true
                                } else {
                                    previousElementIsBlankLine = false
                                }
                            } else {
                                previousElementIsBlankLine = false
                            }
                            ele.layout(
                                article.view,
                                self,
                                paragraphs,
                                article.pageWidth - padding * 2
                            )
                        }
                        //可能会要求跳转
                        val newOffsetY = if (isVerticalScrollMode) {
                            self.onePageLayout(
                                paragraphs,
                                article.pageWidth - padding * 2,
                                article.viewHeight,
                                next,
                                progress,
                                status
                            )
                        } else {
                            self.morePagesLayout(
                                paragraphs,
                                article.pageWidth - padding * 2,
                                article.viewHeight,
                                next,
                                progress,
                                status
                            )
                        }
                        self.status = status
                        if (progress > -1 && newOffsetY >= 0) { //跳转到进度
                            article.view.setOffsetY(newOffsetY + self?.chapterTop!!)
                        }
                    } else {
                        self.status = ReaderConfigWrapper.STATUS_FAILED
                    }
                    if (TangYuanSharedPrefUtils.getInstance()
                            .getReaderPagerAnim(ReaderConfigWrapper.PAGER_ANIM_BOOK) != ReaderConfigWrapper.PAGER_ANIM_COVER
                    ) {
                        mHandler.post {
                            article.view.requestRepaint("确保刷新")
                        }
                        mHandler.postDelayed({
                            block(self)
                        }, 100)
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                    self.status = ReaderConfigWrapper.STATUS_FAILED
                    mHandler.post {
                        article.view.requestRepaint("确保刷新")
                    }
                }

            }
        }
    }
}