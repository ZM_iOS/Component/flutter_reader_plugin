package com.col.reader.plugin.read

import android.content.Context
import android.graphics.BitmapFactory
import android.graphics.Color
import android.graphics.Paint
import android.graphics.Typeface
import com.chineseall.reader.lib.reader.config.IReadConfig
import com.chineseall.reader.lib.reader.config.ReaderConfigWrapper
import com.chineseall.reader.lib.reader.config.TangYuanSharedPrefUtils
import com.chineseall.reader.lib.reader.core.Constants
import com.col.lib_book.R
import com.col.lib_book.utils.DensityUtil
import com.col.lib_book.utils.MotifyAvatar

class PluginReadConfig(private val context: Context) : IReadConfig {
    private val pageWidth: Int // 阅读页面绘制区域宽度 (默认屏幕宽度)

    private val pageHight: Int // 阅读页面绘制区域高度 (默认屏幕高度)

    private var screenDensity = 1f // 屏幕密度
    private val topBarHeight: Int // 顶部显示 章节进度，时间，电量的 bar 的高度

    private val bottomBarHeight: Int // 底部显示 页码进度 的 bar 的高度

    private val padding: Int // 页面内边距
    private val lineSpace: Int // 正文的行间距

    private val paragraphSpace: Int // 正文中的段落间距

    var mBgColor: Int=Color.WHITE// 页面的背景色

    var mTextSize: Int = 18 // 正文字号大小

    var mTextColor: Int = Color.BLACK// 正文的文字颜色

    private val lineColor: Int
    private val textTypeface: Typeface? = null // 正文的文字的字体
    private val titleTextSize: Int// 正文标题的字号大小

    var mTitleTextColor: Int // 正文标题的文字颜色

    private val titleTypeface: Typeface? = null // 正文标题的字体
    private val tailTextSize: Int // 作者有话说的字号大小

    private val tailTextColor: Int //作者有话说的文字颜色

    private var tailTitle = "作者有话说:" //作者有话说的作者名
    private val headerTextSize: Int // 页眉文字大小

    private val footerTextSize: Int // 页脚文字大小

    private val pageTopBottomExtraTextColor: Int //阅读页面顶部，章节信息，时间以及底部页码的文字颜色

    private val imageMargin: Int // 图片绘制时，四周边距

    private val animateTime: Int // 横向翻页时，翻页动画效果时长(毫秒)

    private val verticalChapterMargin: Int // 垂直翻页时，两个章节的间距

    private val titleBetweenContentGapSpace: Int // 章节标题 与 正文之间的间距
    private val isCatalogPageShowBookInfoBlock //在章节目录列表最上面，是否显示书籍简介信息块
            : Boolean
    private val isShowChapterTransitPage //阅读过程中，章节结束，是否显示章节过渡页
            : Boolean
    private val isShowBookEndRecommendPage //阅读过程中，书籍结束，是否显示书尾推荐页
            : Boolean
    private val isShowFloatCollectLayer //阅读过程中，是否弹出喜欢收藏的浮层
            : Boolean
    private val isShowCollectItemMenu //是否显示收藏弹出菜单
            : Boolean
    private var isShowReaderImage // 阅读章节中的图片，是否显示
            = false
    private var defaultReaderImageResId = 0 // 阅读章节中的图片如果不显示，替代的默认图显示
    private var defaultReaderImageWidth = 0
    private var defaultReaderImageHeight = 0


    private fun initDefaultImageResouce() {
        if (defaultReaderImageResId == 0) {
            defaultReaderImageResId = R.drawable.col_ic_bookshelf
            val options = BitmapFactory.Options()
            options.inJustDecodeBounds = true
            BitmapFactory.decodeResource(context.resources, defaultReaderImageResId, options)
            defaultReaderImageWidth = options.outWidth
            defaultReaderImageHeight = options.outHeight
        }
    }

    override fun getPageTopBarHeight(): Int {
        return topBarHeight
    }

    override fun getPageBottomBarHeight(): Int {
        return bottomBarHeight
    }

    override fun getPageBgColor(): Int {
        return mBgColor
    }

    override fun getPadding(): Int {
        return padding
    }

    override fun getLineSpace(): Int {
        return lineSpace
    }

    override fun getParagraphSpace(): Int {
        return paragraphSpace
    }

    override fun getImageMargin(): Int {
        return imageMargin
    }

    override fun getPageHeight(): Int {
        return pageHight
    }

    override fun getPageWidth(): Int {
        return pageWidth
    }

    override fun getScreenDensity(): Float {
        return screenDensity
    }

    override fun getTextSize(): Int {
        return mTextSize
    }


    override fun getLineColor(): Int {
        return lineColor
    }

    override fun getTextTypeface(): Typeface {
        return textTypeface!!
    }

    override fun getTitleTextSize(): Int {
        return titleTextSize
    }

    override fun getTitleColor(): Int {
        return mTitleTextColor
    }

    override fun getTailTextSize(): Int {
        return tailTextSize
    }

    override fun getTailColor(): Int {
        return tailTextColor
    }

    override fun getTailTitle(): String {
        return tailTitle
    }

    override fun getTextColor(): Int {
        return mTextColor
    }

    override fun setTailTitle(tailTitle: String) {
        this.tailTitle = tailTitle
    }

    override fun getTitleTypeface(): Typeface {
        return titleTypeface!!
    }

    override fun getPageTopBottomExtraTextColor(): Int {
        return pageTopBottomExtraTextColor
    }

    override fun getPageHeaderTextSize(): Int {
        return headerTextSize
    }

    override fun getPageFooterTextSize(): Int {
        return footerTextSize
    }

    override fun getAnimateTime(): Int {
        return animateTime
    }

    override fun getVerticalChapterMargin(): Int {
        return verticalChapterMargin
    }

    override fun getTitleBetweenContentGapSpace(): Int {
        return titleBetweenContentGapSpace
    }

    override fun isCatalogPageShowBookInfoBlock(): Boolean {
        return isCatalogPageShowBookInfoBlock
    }

    override fun isShowChapterTransitPage(): Boolean {
        return isShowChapterTransitPage
    }

    override fun isShowBookEndRecommendPage(): Boolean {
        return isShowBookEndRecommendPage
    }

    override fun isShowFloatCollectLayer(): Boolean {
        return isShowFloatCollectLayer
    }

    override fun isShowCollectItemMenu(): Boolean {
        return isShowCollectItemMenu
    }

    override fun isShowContentImage(): Boolean {
        return isShowReaderImage
    }

    override fun getDefaultImageResId(): Int {
        return defaultReaderImageResId
    }

    override fun getDefaultImageWidth(): Int {
        return defaultReaderImageWidth
    }

    override fun getDefaultImageHight(): Int {
        return defaultReaderImageHeight
    }

    companion object {
        const val AUTO_SCROLL_SPEED_LEVEL = "auto_scroll_speed_level"
    }

    init {
        val display = context.resources.displayMetrics
        pageWidth = display.widthPixels
        pageHight = display.heightPixels
        screenDensity = display.density
        val sharedPrefUtil = TangYuanSharedPrefUtils.getInstance()
        padding = DensityUtil.dip2px(context, 16f)


        mTextSize = DensityUtil.dip2px(
            context,
            sharedPrefUtil.getFontSize(IReadConfig.TEXT_SIZE_DEFAULT).toFloat()
        )
        val paint = Paint()
        paint.isAntiAlias = true

        paint.textSize = mTextSize.toFloat()
        val ceil = paint.measureText("啊").toInt()
        val size = ceil * 2 / 3
        lineSpace = sharedPrefUtil.getPageLineGap(size)
        paragraphSpace = lineSpace // 段间距和行间距一样
        val speedLevel = sharedPrefUtil.getInt(AUTO_SCROLL_SPEED_LEVEL, 5)
        var lineNumber = 48
        when (speedLevel) {
            1 -> lineNumber = 35
            2 -> lineNumber = 38
            3 -> lineNumber = 42
            4 -> lineNumber = 45
            5 -> lineNumber = 48
            6 -> lineNumber = 51
            7 -> lineNumber = 54
            8 -> lineNumber = 57
            9 -> lineNumber = 60
            10 -> lineNumber = 63
        }
        sharedPrefUtil.putInt(
            Constants.AUTO_SCROLL_SPEED,
            lineNumber * (mTextSize + lineSpace) / 60
        )
        titleTextSize = DensityUtil.dip2px(
            context,
            (sharedPrefUtil.getFontSize(IReadConfig.TEXT_SIZE_DEFAULT) + 4).toFloat()
        )
        tailTextSize = DensityUtil.dip2px(
            context,
            sharedPrefUtil.getFontSize(IReadConfig.TEXT_SIZE_DEFAULT).toFloat()
        )
        topBarHeight = DensityUtil.dip2px(context, 42f)
        bottomBarHeight = DensityUtil.dip2px(context, 42f)
        headerTextSize = DensityUtil.dip2px(context, 12f)
        footerTextSize = DensityUtil.dip2px(context, 12f)
        imageMargin = DensityUtil.dip2px(context, 18f)
        animateTime = 1000
        verticalChapterMargin = topBarHeight
        titleBetweenContentGapSpace = DensityUtil.dip2px(context, 26f)
        val sceneMode = sharedPrefUtil.getSceneMode(ReaderConfigWrapper.SKIN_THEME_DAYTIME)
        lineColor = context.resources.getColor(R.color.col_reader_voice_line_bg)
        mBgColor = context.resources.getColor(R.color.col_white)
        mTextColor = context.resources.getColor(R.color.col_reader_sence_text_color)
        mTitleTextColor = context.resources.getColor(R.color.col_reader_sence_title_color)
        tailTextColor = context.resources.getColor(R.color.col_reader_sence_tail_color)
        pageTopBottomExtraTextColor = context.resources.getColor(R.color.col_reader_sence_top_bottom_extra_txt_color)
        isCatalogPageShowBookInfoBlock = true
        isShowChapterTransitPage = false
        isShowBookEndRecommendPage = true
        isShowFloatCollectLayer = true
        isShowCollectItemMenu = true
        val showImageType = sharedPrefUtil.getShowImageType(MotifyAvatar.CAMERA_WITH_DATA)
        if (showImageType == MotifyAvatar.CAMERA_WITH_DATA) {
            isShowReaderImage = true
        } else if (showImageType == MotifyAvatar.GALLERY_WITH_DATA_KITKAT) {
            isShowReaderImage = false
        } else if (showImageType == MotifyAvatar.GALLERY_WITH_DATA) {
            isShowReaderImage = true
        }
        if (!isShowReaderImage) {
            initDefaultImageResouce()
        }
    }
}