package com.col.reader.plugin.read

import android.content.Context
import android.graphics.Color
import android.text.TextUtils
import android.util.Log
import android.view.View
import com.chineseall.reader.lib.reader.config.IReadConfig
import com.chineseall.reader.lib.reader.config.ReaderConfigWrapper
import com.chineseall.reader.lib.reader.entities.Chapter
import com.chineseall.reader.lib.reader.utils.PaintHelper
import com.chineseall.reader.lib.reader.view.ReaderView
import com.chineseall.reader.lib.reader.view.horizontal.PaperView
import com.col.lib_book.utils.DensityUtil
import com.col.reader.plugin.helper.IChapterHelperImpl
import io.flutter.embedding.android.FlutterActivity
import io.flutter.plugin.common.BinaryMessenger
import io.flutter.plugin.common.MethodCall
import io.flutter.plugin.common.MethodChannel
import io.flutter.plugin.platform.PlatformView
import splitties.init.appCtx

/**
 * 提供 AndroidView 与 flutter 间的交互能力
 */
class ReaderViewController(
    private val context: Context,
    messenger: BinaryMessenger,
    val id: Int,
    val params: HashMap<String, Any>,
    activity: FlutterActivity?,
    ) : PlatformView, MethodChannel.MethodCallHandler, ReaderView.OnRectClickCallback {

    private val TAG = "ReaderViewController"
    private var paperView: PaperView? = null
    private var mActivity: FlutterActivity? = null
    private var chapterId: String? = null
    private var bookId: Int = 0
    private var progress: Int = 0
    var mChapterLists = arrayListOf<Chapter>()
    private val mMethodChannel: MethodChannel = MethodChannel(
        messenger, "com.col.custom.readerView$id"
    )

    init {
        // 如果需要在自定义view交互中申请监听权限可以加上下面这句话
        // CustomShared.binding?.addRequestPermissionsResultListener(this)
        mMethodChannel.setMethodCallHandler(this)
        params.entries.forEach {
            Log.i(TAG, "CustomView初始化接收入参：${it.key} - ${it.value}")
        }
        mActivity = activity
    }


    override fun getView(): View = initCustomView()

    private fun initCustomView(): View {
        if (paperView == null) {
            paperView = PaperView(context, null)
            paperView?.activity = mActivity
            paperView?.onRectClickCallback = this
        }

        return paperView!!
    }

    override fun dispose() {
        // flutterView dispose 生命周期 在此响应
        Log.i(TAG, "flutterView on Dispose")
        mMethodChannel.setMethodCallHandler(null)
    }

    /**
     * 接收 flutter 调用原生的方法
     */
    override fun onMethodCall(call: MethodCall, result: MethodChannel.Result) {
        when (call.method) {
            "methodFromFlutterViewLoadChapter" -> {
                Log.e(TAG, call.arguments.toString())
                if (call.arguments is HashMap<*, *>) {
                    val params = call.arguments as HashMap<*, *>
                    if (params.contains("bookId")) {
                        bookId = params["bookId"].toString().toInt()
                    }
                    if (params.contains("chapterId")) {
                        chapterId = params["chapterId"].toString()
                    }
                    if (params.contains("progress")) {
                        progress = params["progress"].toString().toInt()
                    }
                }
                getChapterList(bookId.toString())
                result.success(true)
            }

            "loadPreChapter" -> {
                paperView?.loadPreChapter()
            }

            "loadNextChapter" -> {
                paperView?.loadNextChapter()
            }

            "updateConfig" -> {
                updateReadPageConfig(call.arguments)
            }

            "setContent" -> {
                //updateContent(call.arguments)
            }

            else -> result.notImplemented()
        }
    }

    private fun getChapterList(bookId: String) {
        mMethodChannel.invokeMethod(
            "getChapterList",
            "bookId:${bookId}",
            object : MethodChannel.Result {
                override fun success(chaperList: Any?) {
                    if (chaperList is ArrayList<*>) {
                        mChapterLists.clear()
                        chaperList.forEachIndexed { index, ch ->
                            if (ch is HashMap<*, *>) {
                                val chapter = Chapter()
                                chapter.index = index
                                chapter.bookId = ch["bookId"].toString()
                                chapter.bookName = ""
                                chapter.chapterId = ch["chapterId"].toString()
                                chapter.timeStamp_value = ch["updateTimeValue"].toString().toLong()
                                chapter.publishTime = ch["createTimeValue"].toString().toLong()
                                var chapterName: String = ch["chapterName"].toString()

                                if (TextUtils.isEmpty(chapterName)) {
                                    chapterName = "无标题"
                                }
                                chapter.nextId = if (index >= chaperList.size - 1) 0
                                else (chaperList[index + 1] as HashMap<*, *>)["chapterId"].toString()
                                    .toLong()
                                chapter.preId =
                                    if (index <= 0) 0 else (chaperList[index - 1] as HashMap<*, *>)["chapterId"].toString()
                                        .toLong()
                                chapter.chapterName = chapterName
                                mChapterLists.add(chapter)
                            }
                        }
                        loadChapterContent(chapterId)
                    }
                }

                override fun error(errorCode: String, errorMessage: String?, errorDetails: Any?) {
                }

                override fun notImplemented() {
                }
            })
    }

    private fun loadChapterContent(chapterId: String?) {
        paperView?.loadArticle(bookId, chapterId, mChapterLists, 0) { article ->
            IChapterHelperImpl(article, mMethodChannel) {}
        }
    }



    /**
     * 更新阅读页面主题
     *
     *
     * {
     *'contentFontSize': '16',
     *'contentFontName': '',
     *'contentColor': '0x0ffffff',
     *
     *'titleFontSize': '16',
     *'titleFontName': '',
     *'titleColor': '0x0ffffff',
     *
     *'backgroundColor': '0x000000',
    }
     */
    private fun updateReadPageConfig(arguments: Any) {
        val pluginReadConfig = PluginReadConfig(appCtx)
        if (arguments is HashMap<*, *>) {


            if (arguments.contains("contentColor")) {
                val contentColor = arguments["contentColor"] as String
                pluginReadConfig.mTextColor = Color.parseColor(contentColor)
            }

            var titleFontName = arguments["titleFontName"]

            if (arguments.contains("titleColor")) {
                val titleColor = arguments["titleColor"] as String
                pluginReadConfig.mTitleTextColor = Color.parseColor(titleColor)
            }

            if (arguments.contains("backgroundColor")) {
                val titleColor = arguments["backgroundColor"] as String
                pluginReadConfig.mBgColor = Color.parseColor(titleColor)
            }

            if (arguments.contains("contentFontSize")) {
                val fontSize = arguments["contentFontSize"].toString().toFloat()
                pluginReadConfig.mTextSize = DensityUtil.dip2px(
                    context,
                    fontSize
                )
                //字体大小变化，需要reload
                paperView?.reload(true,0)
            }

            ReaderConfigWrapper.init(pluginReadConfig)
            PaintHelper.getInstance().resetConfig(ReaderConfigWrapper.getInstance())
            //PaintHelper.init(ReaderConfigWrapper.getInstance())
            paperView?.requestRepaint(null)
        }
    }


    override fun hideMenu() {
        mMethodChannel.invokeMethod("hideMenu", "隐藏菜单")
    }

    override fun menuRectClick() {
        mMethodChannel.invokeMethod("menuRectClick", "点击菜单区域")
    }

    override fun nextPageRectClick(w: Int) {

    }

    override fun previousPageRectClick(w: Int) {
    }

    override fun isMenuShowing(): Boolean {
        return false
    }

    override fun adGone(w: Int) {
    }

    override fun adVisible(w: Int) {
    }
}