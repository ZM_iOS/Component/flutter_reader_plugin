package com.col.reader.plugin.read

import android.content.Context
import com.col.reader.plugin.read.ReaderViewController
import io.flutter.embedding.android.FlutterActivity
import io.flutter.plugin.common.BinaryMessenger
import io.flutter.plugin.common.StandardMessageCodec
import io.flutter.plugin.platform.PlatformView
import io.flutter.plugin.platform.PlatformViewFactory

class ReaderViewFactory(
    private val messenger: BinaryMessenger,
    activity: FlutterActivity
) : PlatformViewFactory(StandardMessageCodec.INSTANCE) {
    private var mActivity: FlutterActivity?

    init {
        mActivity = activity
    }

    override fun create(
        context: Context?,
        viewId: Int,
        args: Any?
    ): PlatformView {

        @Suppress("UNCHECKED_CAST")
        val params = args as HashMap<String, Any>

        return ReaderViewController(
            context = requireNotNull(context),
            id = viewId,
            messenger = messenger,
            params = params,
            activity = mActivity
        )
    }
}