package com.col.reader.plugin.read

import androidx.annotation.NonNull
import io.flutter.embedding.android.FlutterActivity
import io.flutter.embedding.engine.plugins.FlutterPlugin
import io.flutter.embedding.engine.plugins.activity.ActivityAware
import io.flutter.embedding.engine.plugins.activity.ActivityPluginBinding

class ReaderViewPlugin(activity: FlutterActivity) : FlutterPlugin, ActivityAware {
    private var mActivity: FlutterActivity

    init {
        mActivity = activity
    }

    override fun onAttachedToEngine(@NonNull flutterPluginBinding: FlutterPlugin.FlutterPluginBinding) {
        /// 将 Android 控件进行注册，提供 flutter 层使用
        flutterPluginBinding.platformViewRegistry.registerViewFactory(
            VIEW_TYPE_ID,
            ReaderViewFactory(flutterPluginBinding.binaryMessenger, mActivity)
        )
    }


    companion object {
        // 通过唯一值id进行控件注册
        private const val VIEW_TYPE_ID = "com.col.custom.android/readerView"
    }

    override fun onDetachedFromEngine(binding: FlutterPlugin.FlutterPluginBinding) {

    }

    override fun onAttachedToActivity(binding: ActivityPluginBinding) {
    }

    override fun onDetachedFromActivityForConfigChanges() {
    }

    override fun onReattachedToActivityForConfigChanges(binding: ActivityPluginBinding) {
    }

    override fun onDetachedFromActivity() {
    }
}