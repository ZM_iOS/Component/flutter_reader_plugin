package com.col.reader.plugin.flutter_reader_plugin_example

import androidx.annotation.NonNull
import com.col.reader.plugin.ReaderPluginManager
import io.flutter.embedding.android.FlutterActivity
import io.flutter.embedding.engine.FlutterEngine

class MainActivity : FlutterActivity() {

    override fun configureFlutterEngine(@NonNull flutterEngine: FlutterEngine) {
        super.configureFlutterEngine(flutterEngine)
        ReaderPluginManager.initReadConfig(this,flutterEngine)
    }
}
