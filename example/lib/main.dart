import 'package:flutter/material.dart';
import 'package:flutter_reader_plugin/app/getx_route_observer.dart';
import 'package:flutter_reader_plugin/router.dart';
import 'package:flutter_smart_dialog/flutter_smart_dialog.dart';
import 'package:get/get.dart';



void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      title: 'read',
      initialRoute: ReaderRouter.bookDetail,
      getPages: ReaderRouter.getPages,
      navigatorObservers: [FlutterSmartDialog.observer, GetXRouteObserver()],
    );
  }
}