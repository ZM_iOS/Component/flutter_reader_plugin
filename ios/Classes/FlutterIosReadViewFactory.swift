//
//  FlutterIosReadViewFactory.swift
//  read_plugin
//
//  Created by Ink on 2023/2/13.
//

import UIKit
import Flutter

class FlutterIosReadViewFactory: NSObject, FlutterPlatformViewFactory {
    
    let messenger: FlutterBinaryMessenger
    
    init(messenger: FlutterBinaryMessenger) {
        self.messenger = messenger
        
        super.init()
    }
    
    
    func create(withFrame frame: CGRect, viewIdentifier viewId: Int64, arguments args: Any?) -> FlutterPlatformView {
        return ReadCenterView(frame: frame, viewIdentifier: viewId, arguments: args, messenger: messenger)
    }
}
