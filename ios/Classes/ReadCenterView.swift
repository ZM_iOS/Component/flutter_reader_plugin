//
//  ReadCenterView.swift
//  read_plugin
//
//  Created by Ink on 2023/2/13.
//

import UIKit
import Flutter
import ZNReader

let LFEndViewHeight: CGFloat = 600

let topOffset: CGFloat = 20
let leftOffset: CGFloat = 20
let bottomOffset: CGFloat = 20
let rightOffset: CGFloat = 20
var readerContentFrame: CGRect = .zero

class ReadCenterView: UIView, FlutterPlatformView {
    
    fileprivate enum ReadHandleMatch: String {
        case updateConfig
        case reload
        case reversePage
        case forwardPage
        case loadNextChapter
        case loadPreChapter
        case pageMsg
        case transitionStyleChange
    }
    
    class var nameString: String {
        return "ReadCenterView"
    }
    
    var configContentBackgroundColor: UIColor = .white
    
    var staticTransitionStyle: TransitionStyle = .pageCurl {
        didSet {
            switch staticTransitionStyle {
            case .pageCurl:
                transitionStyleButton.setTitle("仿真", for: .normal)
            case .horizontalScroll:
                transitionStyleButton.setTitle("横向", for: .normal)
            case .verticalScroll:
                transitionStyleButton.setTitle("纵向", for: .normal)
            case .horizontalPageScroll:
                transitionStyleButton.setTitle("横向页", for: .normal)
            case .verticalPageScroll:
                transitionStyleButton.setTitle("纵向页", for: .normal)
            case .horizontalCover:
                transitionStyleButton.setTitle("横覆盖", for: .normal)
            case .verticalCover:
                transitionStyleButton.setTitle("纵覆盖", for: .normal)
            case .noneAnimation:
                transitionStyleButton.setTitle("无动画", for: .normal)
            case .noneAnimation_dragForbid:
                transitionStyleButton.setTitle("无动画_禁止拖拽", for: .normal)
            case .autoGradually:
                transitionStyleButton.setTitle("自动", for: .normal)
            }
        }
    }
    lazy var transitionStyleButton: UIButton = {
        let btn = UIButton(type: .system)
        btn.frame = CGRect(origin: CGPoint(x: UIScreen.main.bounds.size.width / 2 - 30, y: topOffset), size: CGSize(width: 60, height: 40))
        btn.backgroundColor = .red
        btn.addTarget(self, action: #selector(nextTransitionStyleChange), for: .touchUpInside)
        addSubview(btn)
        return btn
    }()
    
    var flutterChannel: FlutterMethodChannel?
    func view() -> UIView { self }
            
    var bookId:String?
    var chapterId:String?
    var bookName:String?
    // 本次阅读开始时间
    var currentStartReadDate = Date()
    var readChapterCache = NSHashTable<ZMReadChapter>(options: NSHashTableWeakMemory)
    
    var config = ZMReadConfig()
    var isVoiceRead = false
    // 语音朗读标记
    var voiceRange:NSRange?
    
    // 阅读时长 秒
    var readedMilliseconds:Int = 0
    private var readCycleTimer: DispatchSourceTimer?
    
    var requestRefreshIng = false
    var readStartTime = 0.0
    private var _currentContentView: ZMReadContentView?
    private var selectedParagraphChapter:ZMReadChapter?
    
    var chapterModels = [IdNameModel]()
    
    var currentContentView: ZMReadContentView? {
        return _currentContentView ?? (pageViewController.shownViews.last as? ZMReadContentView)
    }

    var currentChapter:ZMReadChapter?{
         return currentContentView?.chapter
    }

//    var hintHeaderFooterView: ReaderHintHeaderFooterView?
//
//    @objc lazy var  coverView:ReaderCoverView = {
//        let readerCover = ReaderCoverView.loadFromNib()
//        return readerCover
//    }()
    
    lazy var pageViewController: ZNPageViewController = {
        let pageViewController = ZNPageViewController.init(transitionStyle: config.transitionStyle)
        pageViewController.view.backgroundColor = UIColor.clear
        pageViewController.dataSource = self
        pageViewController.delegate = self
        pageViewController.isDoubleSided = true
        pageViewController.speed = 0.48
        return pageViewController
    }()
    
    
    public init(frame: CGRect, viewIdentifier viewId: Int64, arguments args: Any?, messenger:FlutterBinaryMessenger) {
        super.init(frame: frame)
        
        let channelName = "com.col.custom.readerView\(viewId)"
        let channel = FlutterMethodChannel(name: channelName, binaryMessenger: messenger)
        channel.setMethodCallHandler {[weak self] call, result in
            print(call.method)
            self?.method(call: call, result: result)
        }
        flutterChannel = channel
        setup()
    }
    
    func method(call: FlutterMethodCall, result: FlutterResult) {
        
        print(call.method)
        if let arguments = call.arguments {
            print(arguments)
        }
        let arguments = call.arguments as? [String: Any]
        
        switch ReadHandleMatch(rawValue: call.method) {
        case .reload:
            reload()
        case .updateConfig:
            updateConfig(arguments: arguments)
            break
        case .forwardPage:
            forwardPage()
        case .reversePage:
            reversePage()
        case .loadPreChapter:
            seekToPrevChapter()
        case .loadNextChapter:
            seekToNextChapter()
        case .pageMsg:
            let pageIndex: Int? = arguments?[util:"pageIndex"]
            let resultarguments = pageMsg(pageIndex: pageIndex)
            result(resultarguments)
        case .transitionStyleChange:
            let styleValue = (call.arguments as? [String: Any])?["type"] as? Int ?? 0
            transitionStyleChange(style: TransitionStyle(rawValue: styleValue))
        default:
            break
        }
    }
    
    
    func value<T>(from arguments: Any?, key : String) -> T? {
        return (arguments as? [String: Any])?[key] as? T
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

fileprivate extension [String:Any] {
    subscript<T:LosslessStringConvertible>(util key: String) -> T? {
        set { self[key] = newValue }
        get {
            guard let value = self[key] else { return nil }
            if let value = value as? T { return value }
            return T("\(value)".description)
        }
    }
}

extension ReadCenterView {
        
    override func layoutSubviews() {
        super.layoutSubviews()
    }
    
    func pageMsg(pageIndex: Int?) -> [String:Any] {
        var arguments = [String:Any]()
        
        var currentPageView = _currentContentView
        if let pageIndex = pageIndex {
            currentPageView = getContentView(_currentContentView?.chapter[page: pageIndex], chapter: _currentContentView?.chapter)
        }
        if let currentPageView = currentPageView {
            arguments["pageIndex"] = "\(currentPageView.page.index)"
            arguments["content"] = currentPageView.page.contentText ?? ""
        }

        return arguments
    }
    
    func chapter(model: IdNameModel?)  -> ZMReadChapter? {
        if let model = model {
            return ZMReadChapter(model: model, readCenterView: self, delegate: self)
        }
        return nil
    }
    
    func updateConfig(arguments:[String:Any]?) {
        if let fontSize: Float = arguments?[util:"contentFontSize"]{
            config.fontSize = Int(fontSize)
        }
        
        if let fontName: String = arguments?[util:"contentFontName"]{
            config.fontName = fontName
        }
        
        if let fontSize: Float = arguments?[util:"titleFontSize"],
           let fontName: String = arguments?[util:"titleFontName"] {
            if let font = UIFont(name: fontName, size: CGFloat(fontSize)) {
                config.readConfig.firstTitle.font = font
            }
        }
        
        if let colorStr: String = arguments?[util:"backgroundColor"] {
            config.bgColor = UIColor(colorStr)
        }
        
        if let colorStr: String = arguments?[util:"contentColor"] {
            config.contentColor = UIColor(colorStr)
        }
        
        if let colorStr: String = arguments?[util:"titleColor"] {
            config.titleColor = UIColor(colorStr)
        }
        
        if let colorStr: String = arguments?[util:"paragraphSelectedColor"] {
            config.paragraphSelectedColor = UIColor(colorStr)
        }
        
        reload()
    }
    
    func setup() {
    
        config.readConfig.firstTitle.font = UIFont.systemFont(ofSize: 30)
        config.readConfig.mainBody.font = UIFont.systemFont(ofSize: 17)
        config.readConfig.mainBody.textColor = UIColor.gray
        config.readConfig.adjustsTopIndent = true
        config.readConfig.paragraphSelectedColor = .lightGray
        
        addSubview(pageViewController.view)
        
//        NotificationCenter.default.addObserver(self, selector: #selector(paragraphCancelSelected), name: UIMenuController.willHideMenuNotification, object: nil)
        
        
        let tapGestureRecognizer = UITapGestureRecognizer()
        tapGestureRecognizer.addTarget(self, action: #selector(tapGestureRecognizerAction(tapGestureRecognizer:)))
        pageViewController.view.addGestureRecognizer(tapGestureRecognizer)
        
        DispatchQueue.main.async {[weak self] in
            self?.loadReadBookData()
        }
    }
    
    
    private func loadReadBookData() {
        
        DispatchQueue.main.async {[weak self] in
            self?.reload()
        }
    }
    
    @objc func nextTransitionStyleChange() {
        let style = TransitionStyle(rawValue: (staticTransitionStyle.rawValue + 1)%(TransitionStyle.autoGradually.rawValue + 1))
        transitionStyleChange(style: style)
    }
     
    func transitionStyleChange(style: TransitionStyle?) { // 切换翻页模式
        staticTransitionStyle = style ?? .pageCurl
        reload()
    }
    
    @objc func tapGestureRecognizerAction(tapGestureRecognizer: UITapGestureRecognizer) {
        guard self.config.transitionStyle != .autoGradually else {//自动阅读
            pageViewController.speed = 0
            return ;
        }
        
        if requestRefreshIng == true {
            return ;
        }
        
        let point = tapGestureRecognizer.location(in: tapGestureRecognizer.view)
        let oneThirdOfWidth = bounds.size.width/3.0
        if point.x < oneThirdOfWidth { // 左侧点击
            if currentContentView?.page.operating == true {
                return ;
            }
            if self.config.singleTapMode == true {
                seekToNextPage()
            }else{
                seekToPrevPage()
            }
        }
        else if point.x > oneThirdOfWidth*2 { // 右侧点击
            if currentContentView?.page.operating == true {
                return ;
            }
            seekToNextPage()
        }
        else {
            flutterChannel?.invokeMethod("menuRectClick", arguments: nil)
//            self.coverView.hideOrShowBars()
        }
    }
    
    // 下一页
    private func seekToNextPage(animated: Bool = true) {
        let netxPage = currentContentView?.page.nextPage
        seekTo(page: netxPage?.page,chapter: netxPage?.chapter,direction : .forward,animated: animated)
    }
    
    // 上一页
    private func seekToPrevPage() {
        let prevPage = currentContentView?.page.prevPage
        seekTo(page: prevPage?.page,chapter: prevPage?.chapter,direction : .reverse,animated: true)
    }
    
    func seekToPrevChapter() {
        let chapter = currentContentView?.chapter.prevChapter
        seekTo(page: chapter?.firstPage,chapter: chapter)
     }

    func seekToNextChapter() {
        let chapter = currentContentView?.chapter.nextChapter
        seekTo(page: chapter?.firstPage,chapter: chapter)
    }
    
    /// 跳转页
    func seekTo(page:ZMReadPage?,chapter:ZMReadChapter?, direction: Direction = .forward, animated:Bool = false,complation:((ZMReadContentView?)->Void)? = nil){
        guard let view = getContentView(page,chapter: chapter) else {
            complation?(nil)
            return ;
        }
        pageViewController.setView(view: view, direction: direction, animated: animated)
        complation?(view)
    }
    
    func chapter(chapterId:String?) -> ZMReadChapter? {
        if let chapterId = chapterId,
            let model =  chapterModels.first(where: { $0.id ==  chapterId }) {
            return readChapterCache.allObjects.first(where: {$0.chapterId == chapterId}) ?? ZMReadChapter(model: model, readCenterView: self, delegate: self)
        }
        return nil
    }
    
    func reversePage() {
        seekToPrevPage()
    }
    
    func forwardPage() {
        seekToNextPage()
    }
    
    func reload(clearCache:Bool = true) {
        
        ZMReadConfig.size = bounds.size
        
        var progress: Int = 0
        var tempChapterId: String?
                
        if chapterId != nil {
            tempChapterId = chapterId
            progress = 0
        }
        else if let currentChapter = currentChapter {
            tempChapterId = currentChapter.chapterId
            progress = currentContentView?.page.progress ?? 0
        }
        
        guard chapterModels.count > 0 else {
            flutterChannel?.invokeMethod("getChapterList", arguments: nil,result: {[weak self] result in
                if let result = result as? [[String: Any]] {
                    self?.chapterModels.removeAll()
                    result.forEach { data in
                        if let id: Int = data[util:"chapterId"],
                           let name: String = data[util:"chapterName"] {
                            self?.chapterModels.append(IdNameModel(id:"\(id)",name:name))
                        }
                    }
                    if (self?.chapterModels.count ?? 0) > 0 {
                        self?.reload()
                    }
                }
            })
            return
        }
        
        guard let tempChapterId = tempChapterId ?? chapterModels.first?.id else {
            return ;
        }
        
        pageViewController.view.frame = config.transitionStyle == .verticalScroll ? CGRect(x: 0, y: ZMReadConfig.contentInsets.top, width: bounds.width, height: ZMReadConfig.contentSize.height) : bounds
        pageViewController.transitionStyle = config.transitionStyle
        backgroundColor = config.bgColor ?? config.skinType.bgColor
        
        clearCache ? readChapterCache.removeAllObjects() : nil
        guard let chapter = chapter(chapterId: tempChapterId),
              let contentView = getContentView(chapter[progress:progress],chapter: chapter) else {
                  return ;
        }
        
        chapterId = nil
        pageViewController.setView(view: contentView, direction: .forward, animated: false)
    }

    private func getContentView(_ forPage: ZMReadPage?,chapter:ZMReadChapter?,isBackSide: Bool = false) -> ZMReadContentView? {
        guard let page = forPage,let chapter = chapter else { return nil }
        let contentView = ZMReadContentView(page: page,isBackSide: isBackSide, chapter: chapter, frame: pageViewController.view.bounds)
        return contentView
    }
    
    @objc func btnHandler() {
        print("dasdasdasdasd")
    }
    
//    @objc func shareAction() {
//        commandAction()
//    }
//
//    @objc func commandAction() {
//        let selectedParagraph = segments?.flatMap({$0.paragraphInfos}).first(where: {$0.isSelected == true})
//        print(selectedParagraph?.contextFullText ?? "")
//        paragraphCancelSelected()
//    }
//
//    @objc func paragraphCancelSelected() {
//        segments?.forEach({$0.paragraphInfos.forEach({$0.isSelected = false})})
//        pageVC.shownViews.forEach { view in
//            view.subviews.filter({$0 is ZNReaderView}).forEach({
//                if let readView = $0 as? ZNReaderView {
//                    let segment = readView.contextData
//                    readView.contextData = segment
//                }
//            })
//        }
//    }
    
    open override var canBecomeFirstResponder: Bool {
        return true
    }
    
//    open override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
//        return UIMenuController.shared.menuItems?.first(where: {$0.action == action}) != nil
//    }
//
}

// MARK: - ZMReadChapterDelegate
extension ReadCenterView: ZMReadChapterDelegate {
    func readChapter(_ readChapter: ZMReadChapter,
                     fetchContentHandler: @escaping (_ chapterContent:String?, _ isNetError: Bool) -> Void) {
        let arguments = ["chapterId":readChapter.chapterId]
        flutterChannel?.invokeMethod("getContent", arguments: arguments,result: { result in
            let result = result as? [String:Any]
            fetchContentHandler(result?[util:"content"],false)
        })
    }
    
    func pegesDidChanged(_ readChapter: ZMReadChapter) {
        if let page = currentContentView?.page, readChapter == currentContentView?.chapter{
            updateReadRecord()
            voiceRange = nil
            let arguments = ["pageIndex":"\(page.index)","chapterId":readChapter.chapterId]
            flutterChannel?.invokeMethod("currentPageChange", arguments: arguments)
        }
    }
}

extension ReadCenterView: ZNPageViewControllerDataSource, ZNPageViewControllerDelegate{
    
    func currentView(in pageViewController: ZNPageViewController, didChangeTo view: UIView) {
        
        guard let contentView = view as? ZMReadContentView else { return ; }
        
        let isSwitchChapter = contentView.chapter.chapterId != currentContentView?.chapter.chapterId // 判断是否是切换章节
        
        _currentContentView = contentView
        
        if isSwitchChapter {
            let arguments = ["chapterId":"\(contentView.page.chapter?.chapterId ?? "")"]
            flutterChannel?.invokeMethod("currentChapterChange", arguments: arguments)
        }
        
        if currentChapter?.content != nil,
           currentChapter?.operating == false {

            updateReadRecord()
        }
        voiceRange = nil
    }
    
    func beforeView(in pageViewController: ZNPageViewController, view: UIView) -> UIView? {
        guard let contentView = view as? ZMReadContentView else { return nil }
        let prevPage = contentView.page.prevPage
        if prevPage.page == nil {
            flutterChannel?.invokeMethod("loadPreChapter", arguments: nil)
        }
        return getContentView(prevPage.page,chapter: prevPage.chapter)
    }
    
    func afterView(in pageViewController: ZNPageViewController, view: UIView) -> UIView? {
        guard let contentView = view as? ZMReadContentView else { return nil }
        let nextPage = contentView.page.nextPage
        if nextPage.page == nil { // 后一页为空
            flutterChannel?.invokeMethod("loadNextChapter", arguments: nil)
            return nil
        }
        return getContentView(nextPage.page,chapter: nextPage.chapter)
    }
    
    func backgroundView(in pageViewController: ZNPageViewController, for view: UIView) -> UIView? {
        guard let contentView = view as? ZMReadContentView else { return nil }
        return getContentView(contentView.page,chapter: contentView.chapter,isBackSide: true)
    }
}

// MARK: - record
extension ReadCenterView {
    // 更新阅读进度
    fileprivate func updateReadRecord() {
        flutterChannel?.invokeMethod("updateReadRecord", arguments: nil)
    }
}
