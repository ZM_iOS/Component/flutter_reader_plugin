import Flutter
import UIKit

public class SwiftFlutterReaderPlugin: NSObject, FlutterPlugin {
  public static func register(with registrar: FlutterPluginRegistrar) {
    let channel = FlutterMethodChannel(name: "flutter_reader_plugin", binaryMessenger: registrar.messenger())
    let instance = SwiftFlutterReaderPlugin()
    registrar.addMethodCallDelegate(instance, channel: channel)
    registrar.register(FlutterIosReadViewFactory(messenger: registrar.messenger()), withId: ReadCenterView.nameString)
  }

  public func handle(_ call: FlutterMethodCall, result: @escaping FlutterResult) {
    result("iOS " + UIDevice.current.systemVersion)
  }
}
