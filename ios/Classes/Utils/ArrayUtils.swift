//
//  ArrayUtils.swift
//  novelReader
//
//  Created by Ink on 2020/8/1.
//  Copyright © 2020 ChineseAll. All rights reserved.
//

extension Array {
    subscript(safe index: Int) -> Element? {
        return indices ~= index ? self[index] : nil
    }
}

extension Array {
    func merge(_ element: Element?) -> [Element] {
        if let element = element {
            return self + [element]
        }
        return self
    }
    
    func changeArray(_ step:Int,pageCount:Int) -> [Element] {
        
        var startIdx = step * pageCount
        var endIdx   = startIdx + pageCount - 1
        if endIdx >= self.count {
            startIdx = 0
            endIdx   = pageCount - 1
        }
        let data = Array(self[startIdx...endIdx])
        return data
    }
    
}
