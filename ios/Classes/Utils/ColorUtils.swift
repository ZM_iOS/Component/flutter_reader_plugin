//
//  ColorUtils.swift
//  novelReader
//
//  Created by Ink on 2019/9/6.
//  Copyright © 2019 ChineseAll. All rights reserved.
//

import UIKit

extension UIColor {
    
    static var themeColor_17k = UIColor(0xF14C36)
        
    /// convenience color with hex
    ///
    /// - Parameter:
    ///     - hex:  16进制 支持 {0xRRGGBBAA 、0xRRGGBB、0xRGB、0xAARRGGBB、0xARGB}
    ///     - alpha: 透明度 default = 1.0
    ///     - hexa: (ahex | hexa) default is ahex
    /// - Returns: UIColor 返回颜色
    convenience init(_ val: UInt, alpha: CGFloat = 1.0, hexa: Bool = false) {
        
        var r: UInt = 0, g: UInt = 0, b: UInt = 0;
        var a: UInt = UInt(alpha * 255) & 0xFF
        var rgb = val
        if (val & 0xFFFF0000) == 0 {
            a = UInt(alpha * 15) * 0xF
            
            if val & 0xF000 > 0 {
                
                if hexa == true {
                    a = val & 0xF
                    rgb = val >> 4
                }
                else {
                    a = (val & 0xF000) >> 12
                    a = (a << 4) | a
                }
                
            }
            
            r = (rgb & 0xF00) >> 8
            r = (r << 4) | r
            
            g = (rgb & 0xF0) >> 4
            g = (g << 4) | g
            
            b = rgb & 0xF
            b = (b << 4) | b
            
        } else {
            
            if val & 0xFF000000 > 0 {
                if hexa == true {
                    a = val & 0xFF
                    rgb = val >> 8
                }
                else {
                    a = (val & 0xFF000000) >> 24
                }
            }
            
            r = (rgb & 0xFF0000) >> 16
            g = (rgb & 0xFF00) >> 8
            b = rgb & 0xFF
        }
                
        self.init(red: CGFloat(r) / 255.0,
                       green: CGFloat(g) / 255.0,
                       blue: CGFloat(b) / 255.0,
                       alpha: CGFloat(a) / 255.0)
    }
    
    /// convenience color with hexStr
    ///
    /// - Parameter:
    ///     - cStr: 6位 16进制字符串
    ///     - alpha: 透明度 default = 1.0
    /// - Returns: UIColor 返回颜色
    convenience init(_ hexStr: String, alpha: CGFloat = 1.0, hexa: Bool = false) {
        let hexStr = hexStr.replacingOccurrences(of: "#", with: "")
        
        var hex :UInt64 = 0
        Scanner(string: hexStr).scanHexInt64(&hex)
        
        self.init(UInt(hex), alpha: alpha,hexa:hexa)
    }
}

extension UIColor {
    
    @objc static func caculateColor(startColor: UIColor, endColor: UIColor, franch: CGFloat) -> UIColor {
        var startR : CGFloat = 0.0
        var startG : CGFloat = 0.0
        var startB : CGFloat = 0.0
        var startA : CGFloat = 0.0
        startColor.getRed(&startR, green: &startG, blue: &startB, alpha: &startA)
        
        var endR : CGFloat = 0.0
        var endG : CGFloat = 0.0
        var endB : CGFloat = 0.0
        var endA : CGFloat = 0.0
        endColor.getRed(&endR, green: &endG, blue: &endB, alpha: &endA)
        
        let r = (endR - startR) * franch + startR
        let g = (endG - startG) * franch + startG
        let b = (endB - startB) * franch + startB
        let a = (endA - startA) * franch + startA
        
        return UIColor(red: r, green: g, blue: b, alpha: a)
    }
}
