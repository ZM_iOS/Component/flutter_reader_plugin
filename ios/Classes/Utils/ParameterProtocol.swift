//
//  ParameterProtocol.swift
//  novelReader
//
//  Created by Ink on 2019/9/18.
//  Copyright © 2019 ChineseAll. All rights reserved.
//

protocol ParameterProtocol {
    
    associatedtype ValueType
    
    var key: String { get }
    var value: ValueType? { get }
    func update(value: ValueType?)
}

extension ParameterProtocol {
    
    static var typeName: String {
        return "\(String(reflecting: self))"
    }
}

/// UserDefaults 存储
protocol UserDefaultsParameterProtocol {
    
    associatedtype ValueType
    
    var userDefaultsKey: String { get }
    var userDefaultsValue: ValueType? { get }
    func updateUserDefaults( value: ValueType?)
    static var typeName: String { get }
}
    
extension UserDefaultsParameterProtocol {

    static var typeName: String {
        return "\(String(reflecting: self))"
    }
    
    var userDefaultsKey: String { return Self.typeName }
    
    var userDefaultsValue: ValueType? {
        return UserDefaults.standard.value(forKey: userDefaultsKey) as? ValueType
    }
    
    func updateUserDefaults(value: ValueType?) {
        UserDefaults.standard.set(value, forKey: userDefaultsKey)
    }
}

protocol UserDefaultsStringParameterProtocol: UserDefaultsParameterProtocol where ValueType == String {}
protocol UserDefaultsIntParameterProtocol: UserDefaultsParameterProtocol where ValueType == Int {}
protocol UserDefaultsFloatParameterProtocol: UserDefaultsParameterProtocol where ValueType == Float {}
protocol UserDefaultsDoubleParameterProtocol: UserDefaultsParameterProtocol where ValueType == Double {}
protocol UserDefaultsBoolParameterProtocol: UserDefaultsParameterProtocol where ValueType == Bool {}
