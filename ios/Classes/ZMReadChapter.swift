//
//  ZMReadChapter.swift
//  novelReader
//
//  Created by Ink on 2020/8/5.
//  Copyright © 2020 ChineseAll. All rights reserved.
//

import UIKit
@_exported import ZNReader

protocol ZMReadChapterDelegate: NSObjectProtocol {
    func readChapter(_ readChapter: ZMReadChapter,
                     fetchContentHandler: @escaping (_ chapterContent:String?, _ isNetError: Bool) -> Void)

    func pegesDidChanged(_ readChapter: ZMReadChapter)
}

protocol ZMReadChapterPlaceholderDelegate: NSObjectProtocol {
    
    func showReaderContentAd(from viewController: UIViewController) -> Bool
    func placeholderView(from viewController: UIViewController,parameter:NSMutableDictionary?, index: Int,totalCount: Int, spaceFrame: CGRect) -> ((_ placeholder: ZNReaderPlaceholder) -> UIView?)?
}

typealias IdNameModel = (id:String, name: String)

class ZMReadChapter: NSObject {
    
    let chapterId:String
    let title: String
    var content: String?
    
    var operating = false
    
    var paragraphInfosCount : Int {
        guard let lastIndex = paragraphInfos.last?.paragraphIndex else {
            return 0
        }
        return lastIndex + 1
    }
    
    var paragraphInfos: [ZNReaderParagraphInfo] {
        let segments = pageArray.compactMap({return $0.segment})
        return segments.flatMap({$0.paragraphInfos})
    }
    
    weak var readCenterView : ReadCenterView?
    weak var delegate: ZMReadChapterDelegate?
    weak var placeholderViewDelegate: ZMReadChapterPlaceholderDelegate?
    
    var textCount: Int { return content?.count ?? 0 }
    var pagesCount: Int { return pageArray.count }
    var index : Int { return readCenterView?.chapterModels.firstIndex(where: {chapterId == $0.id}) ?? 0}
    var prevChapter: ZMReadChapter? {return readCenterView?.chapter(model: readCenterView?.chapterModels[safe:index - 1])}
    var nextChapter: ZMReadChapter? {return readCenterView?.chapter(model: readCenterView?.chapterModels[safe:index + 1])}
    
    var firstPage: ZMReadPage? {
        return operate(page: pageArray.first) {[weak self] in
            self?.update(pages: $0, operatePage: $0?.first, page: $1)
        }
    }
    var lastPage: ZMReadPage? {
        return operate(page: pageArray.last) {[weak self] in
            self?.update(pages: $0, operatePage: $0?.last, page: $1)
        }
    }
    
    subscript(page index: Int) -> ZMReadPage?{
        return operate(page: pageArray.first(where: {$0.index == index}) ?? pageArray.first) {[weak self] in
            self?.update(pages: $0, operatePage: $0?[safe: index] ?? $0?.first, page: $1)
        }
    }
    /// 0 - 100
    subscript(progress progress: Int) -> ZMReadPage?{
        assert(progress >= 0 || progress <= 100, "progress is in 0~100")
        let progress = min(100, max(0, progress))
        let index: Int =  lroundf(Float(pagesCount * progress) / 100.0)
        
        return operate(page: pageArray[safe: index] ?? pageArray.first) {[weak self] in
            let newIndex: Int = ($0?.count ?? 0) * progress / 100
            self?.update(pages: $0, operatePage: $0?[safe: newIndex] ?? $0?.first, page: $1)
        }
    }
    
    subscript(offset offset: Int) -> ZMReadPage?{
        return operate(page: pageArray.first(where: {$0.segment?.range.contains(offset) ?? false}) ?? pageArray.first) {[weak self] in
            self?.update(pages: $0, operatePage: $0?.first(where: {$0.segment?.range.contains(offset) ?? false}) ?? $0?.first, page: $1)
        }
    }
    
    fileprivate lazy var pageArray: [ZMReadPage] = {
        let page = ZMReadPage(chapter: self)
        let pages = [page]
        parsePage(){[weak page] in page?.segment = $0.first?.segment}
        return pages
    }()
    
    var free:Bool {
        return true
    }
    
    func operate(page:ZMReadPage?, updateHandler:@escaping (_ pages: [ZMReadPage]?,_ page:ZMReadPage?) -> Void) -> ZMReadPage? {
        if pageArray.count <= 1 {
            self.operating = true
            page?.operating = true
            loadContent(){[weak page,weak self] in
                page?.operating = false
                updateHandler($0,page)
                self?.operating = false
            }
        }
        return page
    }
    
    private func update(pages:[ZMReadPage]?,operatePage:ZMReadPage?,page:ZMReadPage?) {
        guard let pages = pages,
            let originalPage = page,
            let operatePage = operatePage,
            let index = pages.firstIndex(of: operatePage) else {
                page?.reloadContentView()
                return
        }
        
        pageArray = pages
        pageArray.remove(at: index)
        pageArray.insert(originalPage, at: index)
        originalPage.netErrorCallback = operatePage.netErrorCallback
        originalPage.segment = operatePage.segment
        delegate?.pegesDidChanged(self)
    }
    
    init(model: IdNameModel, readCenterView : ReadCenterView?, delegate: ZMReadChapterDelegate?) {
        self.chapterId = model.id
        self.title = model.name
        super.init()
        self.readCenterView = readCenterView
        self.delegate = delegate
//        self.placeholderViewDelegate = readCenterView
        readCenterView?.readChapterCache.add(self)
    }
    
    private func loadContent(resultHandler: @escaping (_ pages: [ZMReadPage]) -> Void) {
        
        delegate?.readChapter(self) {[weak self] (chapterContent, isNetError) in
            guard let self = self else {
                resultHandler([])
                return
            }
            self.content = chapterContent?.replacingOccurrences(of: self.title + "\n\n", with: "") ?? ""
            self.parsePage(self.content,isNetError:isNetError) { pages in
                resultHandler(pages)
            }
        }
    }
    
    // 解析 分页
    private func parsePage(_ content: String? = nil,isNetError:Bool = false,resultHandler:@escaping (_ pages: [ZMReadPage]) -> Void) {
        
        let data = ZNReaderData.init(firstTitle: title + "\n", secondTitle: nil, mainBody: content)
        let parser = ZNReaderParser.init(data: data, config: readCenterView?.config.readConfig ?? ZNReaderConfig())

        parser.parse(placeholderBlock: {(index, totalCount, segment) -> [ZNReaderPlaceholder]? in

            return nil
        }, drawData: {[weak self] (segments) in
            var pages = [ZMReadPage]()
            if let self = self {
                segments.forEach({
                    $0.paragraphInfos.forEach({$0.canSelected = true})
                    let page = ZMReadPage(segment: $0, chapter: self)
                    if isNetError == true {
                        page.netErrorCallback = {[weak self] in
                            self?.loadContent(resultHandler: resultHandler)
                        }
                    }
                    pages.append(page)
                })
            }
            resultHandler(pages)
        })
    }
    
    func selected(paragraph: ZNReaderParagraphInfo) {
        pageArray.forEach({
            $0.segment?.paragraphInfos.forEach({$0.isSelected = $0.paragraphIndex == paragraph.paragraphIndex})
            $0.reloadContentView()
        })
    }
    
    func paragraphsCancelSelected() {
        pageArray.forEach({
            $0.segment?.paragraphInfos.forEach({$0.isSelected = false})
            $0.reloadContentView()
        })
    }
    
    func reloadPages(removeExtraAttributes:Bool = false) {
        pageArray.forEach({
            $0.segment?.removeExtraAttributes()
            $0.reloadContentView()
        })
    }
}

class ZMReadPage: NSObject {
    
    weak var chapter: ZMReadChapter?
    var operating = false
    var netErrorCallback:(()->Void)?
    
    var segment: ZNReaderSegment? { didSet{ reloadContentView()}}
    private var segmentContentViews = NSHashTable<ZMReadContentView>.weakObjects()
    var contentText: String? { return segment?.contentText }
    var index: Int { return chapter?.pageArray.firstIndex(where: {$0 == self}) ?? 0 }
    // 阅读内容进度 0 - 100
    var progress: Int {return Int(min(100,Int(ceil(max(0,Double(index * 100) / Double(max(1,totalCount)))))))}
    var totalCount: Int { return chapter?.pagesCount ?? 0 }
    var isLast:Bool {
        return self == chapter?.pageArray.last
    }
    var prevPage:(page:ZMReadPage?,chapter:ZMReadChapter?) {
        if let page = chapter?.pageArray[safe:index - 1] {
            return (page:page,chapter:page.chapter)
        }
        else {
            let temChapter = chapter?.prevChapter
            return (page:temChapter?.lastPage,chapter:temChapter)
        }
    }
    var nextPage:(page:ZMReadPage?,chapter:ZMReadChapter?) {
        if let page = chapter?.pageArray[safe:index + 1] {
            return (page:page,chapter:page.chapter)
        }
        else {
            let temChapter = chapter?.nextChapter
            return (page:temChapter?.firstPage,chapter:temChapter)
        }
    }
    
    init(segment: ZNReaderSegment? = nil, chapter: ZMReadChapter? = nil) {
        self.chapter = chapter
        self.segment = segment
        super.init()
    }
    
    func reloadContentView() {
        segmentContentViews.allObjects.forEach({ $0.reload()})
    }
    
    func addObserver(readContentView:ZMReadContentView) {
        segmentContentViews.add(readContentView)
    }
}

class ChapterParagraphIdCountModel: NSObject/*,YYModel*/ {
    /// 段落ID
    @objc var paragraphId: Int = 0
    /// 段落评论数量
    @objc var threadReplyCount: Int = 0
}

extension ZNReaderParagraphInfo {
        
    private struct AssociatedKeys {
        static var paragraphId = "ZNReaderParagraphInfo.paragraphId"
        static var threadReplyCount = "ZNReaderParagraphInfo.threadReplyCount"
    }
    
    /// 段落ID （long类型转string存储）
    var paragraphId: String {
        get {
            return objc_getAssociatedObject(self, &AssociatedKeys.paragraphId) as? String ?? "0"
        }
        set {
            objc_setAssociatedObject(self,
                                     &AssociatedKeys.paragraphId,
                                     newValue,
                                     objc_AssociationPolicy.OBJC_ASSOCIATION_COPY)
        }
    }
    
    /// 段落评论数量
    var threadReplyCount: Int {
        get {
            return objc_getAssociatedObject(self, &AssociatedKeys.threadReplyCount) as? Int ?? 0
        }
        set {
            objc_setAssociatedObject(self,
                                     &AssociatedKeys.threadReplyCount,
                                     newValue,
                                     objc_AssociationPolicy.OBJC_ASSOCIATION_ASSIGN)
        }
    }
}

