//
//  ZMReadConfig.swift
//  novelReader
//
//  Created by Ink on 2020/8/20.
//  Copyright © 2020 ChineseAll. All rights reserved.
//

import UIKit

struct kBaiDuTTS {
    static let APP_ID = "10814711"
    static let API_KEY = "BZALIj0OUDaEHmxnKEZREmMj";
    static let SECRET_KEY = "Ydfv2XHSbskjRyHsupxkkGNdMuuOTghC";
}

let kReaderBrightness                 = "readerBrightness"
let kReaderFollowSystem               = "readerFollowSystem"
let kReaderSingleTapMode              = "singleTapMode"
let kReaderEyeMode                     = "readerEyeMode"

struct KFontName {
    static let system = "PingFangSC-Regular"
    static let kyqh = "HYQiHei-EZS"
}

enum ReaderVoiceType:Int {
    case voiceMale,voiceFemale,voiceNormalMale,voiceGirl
}

//enum ReaderVoiceTime:Int {
//    case voiceNone,voiceFifteen,voiceSixty,voiceThreeHours
//}


//enum ReaderFontType: Int {
//
//    case system,alipht,hyzs,hyqh
//
//    private static let fontTypesName = [
//       "PingFangSC-Regular",
//       "AlibabaPuHuiTiR",
//       "HYa3gj",
//       "HYQiHei-EZS"
//    ]
//
//    var fontName: String {
//        return ReaderFontType.fontTypesName[self.rawValue]
//    }
//
//}

enum ReaderSkinType: Int {
    
    ///（ 背景色，字体颜色，选中颜色 , 设置颜色）
    typealias SkinItem = (bgColor: UIColor, textColor: UIColor, selectedColor: UIColor,
                          settingBgColor: UIColor,settingImageColor: UIColor,settingLineColor: UIColor)
    
    case normal,green,blue,pink,yellow,sBlue,night
    
    var bgColor: UIColor { return item.bgColor }
    var textColor: UIColor { return item.textColor }
    var selectedColor: UIColor { return item.selectedColor }
    var settingBgColor: UIColor { return item.settingBgColor }
    var settingImageColor: UIColor { return item.settingImageColor }
    var settingLineColor: UIColor { return item.settingLineColor }
    
    private var item:SkinItem {
        switch self {
        case .normal:
            return SkinItem(bgColor: UIColor(0xf7f7f7), textColor: UIColor(0x4a4a4a), selectedColor: UIColor(0xe4e4e4),
                            settingBgColor: UIColor(0xFFFFFF),settingImageColor: UIColor(0x4A4A4A),settingLineColor: UIColor(0xD7D7D7))
        case .green:
            return SkinItem(bgColor: UIColor(0xd5e9d4), textColor: UIColor(0x425340), selectedColor: UIColor(0xb1cdb0),
                            settingBgColor: UIColor(0xF7FFF6),settingImageColor: UIColor(0x577953),settingLineColor: UIColor(0xD7D7D7))
        case .blue:
            return SkinItem(bgColor: UIColor(0xcee7ff), textColor: UIColor(0x52648a), selectedColor: UIColor(0x9ec0e1),
                            settingBgColor: UIColor(0xF5FAFF),settingImageColor: UIColor(0x607BB3),settingLineColor: UIColor(0xD7D7D7))
        case .pink:
            return SkinItem(bgColor: UIColor(0xffe1dc), textColor: UIColor(0x814156), selectedColor: UIColor(0xe2b5ad),
                            settingBgColor: UIColor(0xFFF9F8),settingImageColor: UIColor(0xA44665),settingLineColor: UIColor(0xFFE1DC))
        case .yellow:
            return SkinItem(bgColor: UIColor(0xfbe6b5), textColor: UIColor(0x6a482e), selectedColor: UIColor(0xe9cc88),
                            settingBgColor: UIColor(0xFFFCF6),settingImageColor: UIColor(0xC16F30),settingLineColor: UIColor(0xF3DFB0))
        case .sBlue:
            return SkinItem(bgColor: UIColor(0x28334c), textColor: UIColor(0x607799), selectedColor: UIColor(0x3f4d6e),
                            settingBgColor: UIColor(0x5B6782),settingImageColor: UIColor(0xA7C3EC),settingLineColor: UIColor(0x808CA7))
        case .night:
            return SkinItem(bgColor: UIColor(0x292929), textColor: UIColor(0x959595), selectedColor: UIColor(0x484848),
                            settingBgColor: UIColor(0x191818),settingImageColor: UIColor(0xD7D7D7),settingLineColor: UIColor(0x646464))
        }
    }
    
    /// 是否为深色
    var isDark: Bool {
        switch self {
        case .sBlue,.night:
            return true
        default:
            return false
        }
    }
}

extension ZMReadConfig {
    
    static var size: CGSize = UIScreen.main.bounds.size
    static let contentInsets: UIEdgeInsets = UIEdgeInsets(top: 20, left: 20, bottom: 20, right: 20)
    static var contentSize: CGSize { CGSize(width: size.width - contentInsets.left - contentInsets.right, height: size.height - contentInsets.top - contentInsets.bottom) }
}

class ZMReadConfig: NSObject {
        
    // 分页绘制文本配置
    let readConfig = ZNReaderConfig()
    
    /// 皮肤（颜色）
    var skinType = ReaderSkinType.normal {
        didSet {
            if skinType == oldValue {
                return
            }
            updateReadConfig()
            
            if skinType != .night {
                ZMUserDefaults.int.skinType.updateUserDefaults(value: skinType.rawValue)
            }
        }
    }
    
    var titleColor: UIColor?
    var contentColor: UIColor?
    var bgColor: UIColor?
    var paragraphSelectedColor: UIColor?
    
    /// 夜间模式
    var isNight = false {
        didSet {
            updateReadConfig()
            
            ZMUserDefaults.bool.isNight.updateUserDefaults(value: isNight)
//            NotificationCenter.default.post(name: .ChangeSkinColorNotification, object: nil)
        }
    }
    
    /// 亮度跟随系统
    var brightnessFollowSystem = false{
        didSet{
            ZMUserDefaults.bool.brightnessFollowSystem.updateUserDefaults(value: brightnessFollowSystem)
        }
    }
    
    var brightness = Float(0.5){
        didSet{
            ZMUserDefaults.float.brightness.updateUserDefaults(value: brightness)
        }
    }
    
    /// 单手模式
    var singleTapMode = false{
        didSet{
            ZMUserDefaults.bool.singleTapMode.updateUserDefaults(value: singleTapMode)
        }
    }
    
    /// 护眼模式
    var safeEyeMode = false {
        didSet{
            ZMUserDefaults.bool.safeEyeMode.updateUserDefaults(value: safeEyeMode)
        }
    }
    
    var voiceType:ReaderVoiceType = .voiceMale{
        didSet{
            ZMUserDefaults.int.voiceType.updateUserDefaults(value: voiceType.rawValue)
        }
    }

    var fontSize = 22 {
        didSet{
            ZMUserDefaults.int.fontSize.updateUserDefaults(value: fontSize)
            updateReadConfig()
        }
    }
    
    var fontName: String = KFontName.system {
        didSet {
            ZMUserDefaults.string.fontName.updateUserDefaults(value: fontName)
            updateReadConfig()
        }
    }

    var voiceSpeed = Float(4.5){
        didSet{
            ZMUserDefaults.float.voiceSpeed.updateUserDefaults(value: voiceSpeed)
        }
    }
    
    var autoReadSpeed = 5 {
        didSet{
            ZMUserDefaults.int.autoReadSpeed.updateUserDefaults(value: autoReadSpeed)
        }
    }

   //有声/自动之前的翻页方式
    var lastTransitionStyle:TransitionStyle = .pageCurl{
        didSet{
            ZMUserDefaults.int.lastTransitionStyle.updateUserDefaults(value: lastTransitionStyle.rawValue)
        }
    }

    /// 翻页方式
    var transitionStyle:TransitionStyle = .pageCurl{
        didSet{
            readConfig.adjustsTopIndent = transitionStyle == .verticalScroll
            if transitionStyle != .autoGradually {
                ZMUserDefaults.int.transitionStyle.updateUserDefaults(value: transitionStyle.rawValue)
            }
        }
    }
    
    override init() {
        super.init()
        
        readConfig.textSize = ZMReadConfig.contentSize
        readConfig.spaceBeforeFirstTitle = 0
        readConfig.spaceBetweenSecondTitleAndMainBody = 20
        readConfig.paragraphPrefix = "　　"
        
        fontSize = ZMUserDefaults.int.fontSize.userDefaultsValue ?? 22
        fontName = ZMUserDefaults.string.fontName.userDefaultsValue ?? KFontName.system
        brightnessFollowSystem = ZMUserDefaults.bool.brightnessFollowSystem.userDefaultsValue ?? true
        brightness = ZMUserDefaults.float.brightness.userDefaultsValue ?? 0.5
        singleTapMode = ZMUserDefaults.bool.singleTapMode.userDefaultsValue ?? false
        safeEyeMode   = ZMUserDefaults.bool.safeEyeMode.userDefaultsValue ?? false
        voiceType   = ReaderVoiceType(rawValue: ZMUserDefaults.int.voiceType.userDefaultsValue ?? 0) ?? .voiceMale
        voiceSpeed  = Float(ZMUserDefaults.float.voiceSpeed.userDefaultsValue ?? 4.5)
        autoReadSpeed = ZMUserDefaults.int.autoReadSpeed.userDefaultsValue ?? 5
        if autoReadSpeed == 0 {
            autoReadSpeed = 5
        }
        transitionStyle = TransitionStyle(rawValue: ZMUserDefaults.int.transitionStyle.userDefaultsValue ?? 0) ?? .pageCurl
        
        readConfig.adjustsTopIndent = transitionStyle == .verticalScroll

        singleTapMode = ZMUserDefaults.bool.singleTapMode.userDefaultsValue ?? false
        isNight  = ZMUserDefaults.bool.isNight.userDefaultsValue ?? false
        if isNight == true{
            skinType = .night
        }else{
            skinType = ReaderSkinType(rawValue: ZMUserDefaults.int.skinType.userDefaultsValue ?? 4) ?? .yellow
        }
        updateReadConfig()
    }
    
    func updateReadConfig() {
        
        readConfig.firstTitle.textColor = titleColor ?? skinType.textColor
        readConfig.mainBody.textColor  = contentColor ?? skinType.textColor
        readConfig.paragraphSelectedColor = paragraphSelectedColor ?? skinType.selectedColor
        
        
        readConfig.firstTitle.font = UIFont(name: fontName, size: CGFloat(fontSize+4)) ?? UIFont.systemFont(ofSize: CGFloat(fontSize+4))
        readConfig.firstTitle.style.lineSpacing = readConfig.firstTitle.font.pointSize * 3.0 / 8.0
        readConfig.firstTitle.style.paragraphSpacing = readConfig.firstTitle.style.lineSpacing * 2.0
        
        readConfig.mainBody.font = UIFont(name: fontName, size: CGFloat(fontSize)) ?? UIFont.systemFont(ofSize: CGFloat(fontSize))
        readConfig.mainBody.style.lineSpacing = readConfig.mainBody.font.pointSize * 3.0 / 8.0
        readConfig.mainBody.style.paragraphSpacing = readConfig.mainBody.style.lineSpacing * 2.0
    }
}

extension ZMReadConfig {
    
    // 存储
    class ZMUserDefaults {
        
        enum bool: String , UserDefaultsBoolParameterProtocol{
            // 夜间模式
            case isNight

            /// 亮度跟随系统
            case brightnessFollowSystem
            
            /// 单手模式
            case singleTapMode
            
            // 护眼模式
            case safeEyeMode
            
            var userDefaultsKey: String {
                switch self {
                case .safeEyeMode:
                    return kReaderEyeMode
                case .brightnessFollowSystem:
                    return kReaderFollowSystem
                case .singleTapMode:
                    return kReaderSingleTapMode
                default:
                    return Self.typeName + ".\(self.rawValue)"
                }
            }
        }
        
        enum string: String , UserDefaultsStringParameterProtocol{
            
            // 字体
            case fontName
                        
            var userDefaultsKey: String { return Self.typeName + ".\(self.rawValue)"}
        }
        
        enum int: String , UserDefaultsIntParameterProtocol{
                    
            // 皮肤
            case skinType
            
            // 字号
            case fontSize
            //有声朗读方式
            case voiceType
            //自动翻页速度
            case autoReadSpeed
            // 翻页方式
            case transitionStyle
            
            case lastTransitionStyle
            //有声朗读时长.
//            case ttsFixedReadTime
            
            var userDefaultsKey: String { return Self.typeName + ".\(self.rawValue)"}
        }
        
        enum float: String , UserDefaultsFloatParameterProtocol {
                    
            /// 亮度
            case brightness
            
            //语音语速
            case voiceSpeed
            
            var userDefaultsKey: String {
                switch self {
                case .brightness:
                    return kReaderBrightness
                default:
                    return Self.typeName + ".\(self.rawValue)"
                }
            }
        }
    }
}
