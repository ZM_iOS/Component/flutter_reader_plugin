//
//  ZMReadContentView.swift
//  novelReader
//
//  Created by Ink on 2020/8/5.
//  Copyright © 2020 ChineseAll. All rights reserved.
//

import UIKit

protocol ZMReadContentViewDelegate: NSObjectProtocol {
    func paragraphSelected(paragraph:ZNReaderParagraphInfo?,contentView:ZMReadContentView?)
    func paragraphCountTapHandler(paragraph:ZNReaderParagraphInfo?,contentView:ZMReadContentView?)
}

class ZMReadContentView: BaseView {
    
    var page: ZMReadPage
    var chapter: ZMReadChapter
    var config: ZMReadConfig? {
        return chapter.readCenterView?.config
    }
    
    var drawTextView: ZNReaderView = ZNReaderView()
//    weak var chargeView: SZChargeHintView?
//    var hintHeaderFooterView: ReaderHintHeaderFooterView?
    
    weak var delegate:ZMReadContentViewDelegate?
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    init(page: ZMReadPage,isBackSide:Bool = false,chapter:ZMReadChapter, frame: CGRect, delegate:ZMReadContentViewDelegate? = nil) {
        self.page = page
        self.chapter = chapter
        super.init(frame: frame)
        self.delegate = delegate
        drawTextView.isBackground = isBackSide
        backgroundColor = config?.bgColor ?? (config?.skinType ?? .normal).bgColor
        page.segment?.config = chapter.readCenterView?.config.readConfig
        let textContentView = UIView()
        textContentView.backgroundColor = UIColor.clear
        addSubview(textContentView)
        textContentView.frame = bounds
//        textContentView.snp.makeConstraints { (make) in
//            make.edges.equalTo(self)
//        }
        textContentView.addSubview(drawTextView)
        drawTextView.frame = CGRect(x: ZMReadConfig.contentInsets.left, y:config?.transitionStyle == .verticalScroll ? 0 : ZMReadConfig.contentInsets.top, width: ZMReadConfig.contentSize.width, height: ZMReadConfig.contentSize.height)
        page.addObserver(readContentView: self)
        
//        drawTextView.paragraphTailView = {[weak self] (paragraph,lastLineFrame) in
//            return self?.paragraphTailButton(paragraph, lastLineFrame: lastLineFrame)
//        }

        drawTextView.paragraphSelected = {[weak self] (paragraph,readerView) in
            self?.delegate?.paragraphSelected(paragraph: paragraph, contentView: self)
        }
        
//        if config?.transitionStyle != .verticalScroll {
//            let hintView = ReaderHintHeaderFooterView()
//            addSubview(hintView)
//            hintView.frame = bounds
//            hintHeaderFooterView = hintView
//        }
        
        if page.chapter?.readCenterView?.config.transitionStyle == .autoGradually {
            textContentView.isUserInteractionEnabled = false
        }
        
        reload()
    }
    
//    func paragraphTailButton(_ paragraph:ZNReaderParagraphInfo,lastLineFrame:CGRect) -> UIButton {
//        let btn = UIButton()
//
//        btn.setTitle("\(min(999, paragraph.threadReplyCount))", for: .normal)
//
//        btn.addTouchUpInsideHandler {[weak paragraph,weak self] _ in
//            self?.delegate?.paragraphCountTapHandler(paragraph: paragraph,contentView: self)
//        }
//
//        let image = UIImage(named: "17k_segment_bubble")?.withRenderingMode(.alwaysTemplate)
//        btn.setBackgroundImage(image, for: .normal)
//        if let readConfigMainBody = config?.readConfig.mainBody {
//            btn.titleLabel?.font = readConfigMainBody.font.withSize(readConfigMainBody.font.pointSize / 2.2)
//            btn.setTitleColor(readConfigMainBody.textColor, for: .normal)
//            btn.tintColor = readConfigMainBody.textColor
//        }
//
//        let width = min(24.0, lastLineFrame.height * 3 / 4);
//        btn.frame = CGRect(origin: .zero, size: CGSize(width: width, height: width * 5 / 6))
//
//        btn.isHidden = paragraph.threadReplyCount <= 0
//        return btn
//    }
    
//    @objc func
    
    func reload() {
        
//        let title = page.index == 0 ? chapter.readCenterView?.book?.bookName : chapter.title
//        hintHeaderFooterView?.render(tintColor: (config?.skinType ?? .normal).textColor, title: title, offset: "\(page.index + 1) / \(chapter.pagesCount)",hiddenBottom: page.isLast )
//
//        if let callBack = page.netErrorCallback {
//            hideToastActivity()
//            showFaildView { _ in callBack() }
//            return
//        }
//
//        self.hideErrorRetryView()
        if let _ = page.segment {
            drawTextView.contextData = page.segment
        }
//        if page.segment != nil , page.operating == false{
//            hideToastActivity()
//        }
//        else {
//            makeToastActivity()
//        }
    }
}
