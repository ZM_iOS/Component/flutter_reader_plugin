import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../flutter_reader_plugin.dart';
import '../router.dart';
import 'logic.dart';

class BookDetailPage extends StatelessWidget {
  final logic = Get.put(BookDetailLogic());
  final state = Get.find<BookDetailLogic>().state;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("书详情"),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            GestureDetector(
              child: Text("可以原生"),
              onTap: _jumpToAndroidMethod,
            ),
            TextButton(
                onPressed: () {
                  Get.toNamed(ReaderRouter.read);
                },
                child: Text('也可以flutter')),
          ],
        ),
      ),
      // This trailing comma makes auto-formatting nicer for build methods.
    );
  }

  //跳转到Android activity页面
  Future<void> _jumpToAndroidMethod() async {
    FlutterReaderPlugin().openBook("1797752", "0", 0);
  }
}
