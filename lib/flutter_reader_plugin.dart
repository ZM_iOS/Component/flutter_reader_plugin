
import 'flutter_reader_plugin_platform_interface.dart';

class FlutterReaderPlugin {
  Future<String?> getPlatformVersion() {
    return FlutterReaderPluginPlatform.instance.getPlatformVersion();
  }

  Future<void> openBook(String bookId, String chapterId,int progress) async {
     FlutterReaderPluginPlatform.instance.openBook(bookId,chapterId,progress);
  }
}
