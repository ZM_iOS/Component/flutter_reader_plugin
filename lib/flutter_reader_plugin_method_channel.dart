import 'package:flutter/foundation.dart';
import 'package:flutter/services.dart';

import 'flutter_reader_plugin_platform_interface.dart';

/// An implementation of [FlutterReaderPluginPlatform] that uses method channels.
class MethodChannelFlutterReaderPlugin extends FlutterReaderPluginPlatform {
  /// The method channel used to interact with the native platform.
  @visibleForTesting
  final methodChannel = const MethodChannel('flutter_reader_plugin');

  @override
  Future<String?> getPlatformVersion() async {
    final version =
        await methodChannel.invokeMethod<String>('getPlatformVersion');
    return version;
  }

  @override
  Future<void> openBook(String bookId, String chapterId, int progress) async {
    await methodChannel.invokeMethod<void>(
      'openBook',
      <String, dynamic>{
        'bookId': bookId,
        "chapterId": chapterId,
        "progress": progress
      },
    );
  }
}
