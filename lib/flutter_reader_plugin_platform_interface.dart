import 'package:plugin_platform_interface/plugin_platform_interface.dart';

import 'flutter_reader_plugin_method_channel.dart';

abstract class FlutterReaderPluginPlatform extends PlatformInterface {
  /// Constructs a FlutterReaderPluginPlatform.
  FlutterReaderPluginPlatform() : super(token: _token);

  static final Object _token = Object();

  static FlutterReaderPluginPlatform _instance =
      MethodChannelFlutterReaderPlugin();

  /// The default instance of [FlutterReaderPluginPlatform] to use.
  ///
  /// Defaults to [MethodChannelFlutterReaderPlugin].
  static FlutterReaderPluginPlatform get instance => _instance;

  /// Platform-specific implementations should set this with their own
  /// platform-specific class that extends [FlutterReaderPluginPlatform] when
  /// they register themselves.
  static set instance(FlutterReaderPluginPlatform instance) {
    PlatformInterface.verifyToken(instance, _token);
    _instance = instance;
  }

  Future<String?> getPlatformVersion() {
    throw UnimplementedError('platformVersion() has not been implemented.');
  }

  Future<void> openBook(String bookId, String chapterId, int progress) {
    throw UnimplementedError('openBook() has not been implemented.');
  }
}
