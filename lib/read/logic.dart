import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';

import 'state.dart';

class ReadLogic extends GetxController {
  final ReadState state = ReadState();

  void onViewCreated(int id) {}

  Future<void> onPlatformViewCreated(int viewId) async {
    state.mMethodChannel = MethodChannel('com.col.custom.readerView$viewId');
    state.mMethodChannel.setMethodCallHandler(flutterMethod);
    onViewCreated(viewId);

    sendMessageToReadView(<String, dynamic>{
      'bookId': "1398783",
      'chapterId': "21555988",
      "progress": 0,
    });
  }

  Future<dynamic> flutterMethod(MethodCall methodCall) async {
    print('原生调用了flutterMethod方法！！！${methodCall.method}');
    print('原生传递给flutter的参数是：${methodCall.arguments}');
    switch (methodCall.method) {
      case 'menuRectClick':
        state.showMenu = !state.showMenu;
        _showBottomMenu();
        break;
      case 'hideMenu':
        break;
      case 'currentPageChange':
        break;
      case 'loadPreChapter':
        break;
      case 'loadNextChapter':
        break;

      case 'getChapterList':
        var chapters = <Map<String, dynamic>>[
          <String, dynamic>{
            "bookId": 1398783,
            "chapterId": "21555988",
            "isVIP": "N",
            "chapterName": "第一章 丹帝记忆",
            "updateDate": "2021-07-18 15:02:09",
            "chapterIndex": 1,
            "volumeCode": 200,
            "volumeId": 5818348,
            "updateTimeValue": 1626591729000,
            "createTimeValue": 1441059471000
          },
          <String, dynamic>{
            "bookId": 1398783,
            "chapterId": 21557345,
            "isVIP": "N",
            "chapterName": "第二章 卑鄙小人",
            "updateDate": "2021-07-18 14:54:24",
            "chapterIndex": 2,
            "volumeCode": 200,
            "volumeId": 5818348,
            "updateTimeValue": 1626591264000,
            "createTimeValue": 1441074144000
          },
          <String, dynamic>{
            "bookId": 1398783,
            "chapterId": 21567672,
            "isVIP": "N",
            "chapterName": "第三章 锋芒初现",
            "updateDate": "2019-11-13 19:58:03",
            "chapterIndex": 3,
            "volumeCode": 200,
            "volumeId": 5818348,
            "updateTimeValue": 1573646283000,
            "createTimeValue": 1441156056000
          },
        ];

        return chapters;
      case 'getContent':
        return {
          'content': "${methodCall.arguments}  "
              "　　一人一刀，气势冲霄，仿佛整个天地之间，只有一个人的存在，充满了傲视天下，睥睨群伦的意志。<br><br>　　人们呆呆的看着那个背影，一时间这个画面，永远地定格在他们的脑海中，永远也无法磨灭。<br><br>　　直到他们老了，依旧能向他们的儿孙，讲诉着那个充满了霸气的身影。<br><br>　　“桀桀桀，有意思，我感觉到了你的愤怒，却没感受到你的恐惧，还真是无知者无畏啊，就凭你们也敢与老夫对抗？”鬼沙桀桀怪笑道。<br><br>　　“老鬼，枉你连死带活的过了这么多年，自吹自擂是什么高手，你连修行者力量的源泉是什么都不知道”龙尘走到鬼沙前方站定，一脸嘲讽的道。<br><br>　　“那你说是什么？”<br><br>　　鬼沙冷笑道，他现在早就看穿了形势，大阵随时落下，他需要抓一个护身符才行，选来选去，他还是决定选龙尘，他在等一个机会。<br><br>　　龙尘嘴角浮现一抹冷笑道：“修行者最强大的力量，不是天赋、不是意志、更不是功法和战技，而是守护。<br><br>　　一个修行者可能在生死攸关面前，爆发十成的力量，而心中有了守护，他就会爆发出十倍，百倍的能量。<br><br>　　因为在我们需要守护的目标前，一切都可以不要，包括我们自己的生命，这就是守护的力量”<br><br>　　别人听着龙尘的话，听得一脸茫然，但是唐婉儿、叶知秋等人，双目之中，闪过一丝明悟。<br><br>　　图方长老更是心头狂震，他终于明白，为什么唐婉儿和叶知秋会同时复苏祖纹了。<br><br>　　为了同伴的安危，自己的生命已经变得不重要了，那已经超越了对生死的领悟，到了更高一种层次的境界。<br><br>　　图方不禁一叹，龙尘的年龄，还不足他们的零头，却可以把修行看的如此透彻，如果他不陨落，将来绝对是一个惊天动地的大人物，可惜……。<br><br>　　“有了守护，才有了努力的方向，有了守护，才有了悍不畏死的决心，有了守护，我们的道心更加坚定，可以在修行的道路上一路披荆斩棘，勇往直前。<br><br>　　而你们这些没人性的邪徒，把自己的同类当成修炼工具，你们永远无法理解什么是守护，所以你这个老鬼，有什么资格嘲笑我？你才是真正的无知。”龙尘冷冷的道。<br><br>　　“放屁，这是你们这些伪君子冠冕堂皇的措辞而已，修行路上，谁不是踩着别人的肩膀向上爬？<br><br>　　这是一个弱肉强食的世界，适者生存，你们没资格指责我们。<br><br>　　如果你们的理论真的是正确的，这无数年来，也不会被我们邪派一直压着打了。<br><br>　　弱者，没资格说大话，一个初入修行界的小鬼，也敢大放厥词？真是让人笑掉牙齿。<br><br>　　既然你说什么是守护，那就拿出你口中守护的力量，那到底是什么垃圾”鬼沙冷冷地嘲讽道。<br><br>　　自古以来，正邪对立，两者间的争斗从未断过，都认为自己的想法是正确的，对方才是错误的。<br><br>　　所以当龙尘说到这里时，鬼沙不禁勃然大怒，甚至比龙尘从他那里骗走幽冥鬼影步，更加愤怒，这是对他信仰的一种亵渎。<br><br>　　“心有所守，意有所归，那才是修行者力量的源泉，既然你想看，那我就成全你”<br><br>　　“轰”<br><br>　　龙尘的声音，宛若来自九天之上，震撼所有的人心灵，当龙尘说完，一道恐怖的气浪崩碎了大地，一道长达百丈的光环，冲天而起。<br><br>　　光环浮现在龙尘的背后，天地不停地颤动，空间在剧烈的轰鸣，整个世界仿佛变成了海洋，巨浪以龙尘为核心，不停地向周围冲刷。<br><br>　　恐怖的力量，不停地碾压着这个世界，一些人感觉自己的身体仿佛被大山压着，喘息都开始变得困难了。<br><br>　　“好强大的威压”<br><br>　　图方一惊，此时龙尘的力量，已经远远超过了凝血境该有的标准，这太不可思议了。<br><br>　　他身后的光环到底是什么？怎么可以不停地吸纳天地灵气，这到底是功法？还是战技？<br><br>　　唐婉儿和叶知秋也一脸震惊的看着，那个被神环包围的身影，这还是她们认识的那个龙尘吗？<br><br>　　光凭身上散发的威压，就可以让她们呼吸困难，这还是那个油嘴滑舌的家伙吗？<br><br>　　郭然站在人群中，一脸崇拜的看着那个身影，双手握的紧紧的，这就是无敌的老大。<br><br>　　所有长老也都一脸震惊的看着龙尘，不过有一人，震惊过后，陷入了沉思，双目之中闪过一丝贪婪，<br><br>　　“为了守护，无畏无惧，心境通明，方得大道，你们这些邪魅，又岂能懂得？看刀”<br><br>　　龙尘大喝一声，一步迈出，手中的长刀爆长，刀气漫天，一瞬间将天地间的肃杀之气，都凝聚在一刀之中，如天刀斩落星河。<br><br>　　“轰”<br><br>　　一刀斩落，坚固的大地，直接被斩出了一道数百丈长的沟壑，而鬼沙则在沟壑的尽头，胸前被砍出了一个大口子，不过却没有鲜血流出。<br><br>　　人们一脸惊骇的看着地上的那条大沟，久久说不出话来，这么恐怖的破坏力，他们做梦也想不到。<br><br>　　如果换成是他们，恐怕就算有一千条命，也要被秒杀，这个龙尘比怪物还怪物。<br><br>　　雷千伤与齐信不禁脸色大变，心头不禁有些后怕，当初他们与龙尘交战，如果龙尘爆发，他们恐怕已经死了。<br><br>　　看着那个如同天帝转世的身影，两人同时泛起了极大的妒忌之心，他们也是高傲的天才，如今被这样压制了，心中极为恼火。<br><br>　　“混蛋，如果不是这具肉身太垃圾，老夫可以一巴掌拍死你，你信不？”鬼沙愤怒的咆哮道。<br><br>　　他乃是邪道强者，空有一身本事，奈何如今只剩下灵魂，又被阵法压制，这具尸体也不给力，气得他暴跳如雷。<br><br>　　“切，说空话谁不会啊，我如果活了跟你一样的年月，我放个屁就能摆平你，你信不？”龙尘长刀往肩膀上一抗，冷笑道。<br><br>　　唐婉儿和叶知秋不禁对视一眼，好像那个熟悉的龙尘，又回来了。<br><br>　　“混蛋，去死”<br><br>　　鬼沙暴怒一声，向龙尘扑来，浑身诡异的纹路再次亮起，邪恶的气息越来越恐怖。<br><br>　　“你都人不人鬼不鬼了，都不去死，我死干啥？”<br><br>　　龙尘一横手中长刀，对着鬼沙斩去，如今鬼沙身上诡异的纹路浮现，与龙尘对战。<br><br>　　不过人们可以清楚的看到，两人对战之时，不时有细小的东西掉落。<br><br>　　“那是什么？”<br><br>　　“好像……好像是那人身上的肉”<br><br>　　“我明白了，龙尘现在战力提升，已经可以破开那邪尸的防御了”一人忽然道。<br><br>　　众人定睛一看，果然只见鬼沙的拳头上，已经开始变得坑坑洼洼，原来那些细小的东西，是鬼沙拳头上的碎肉。<br><br>　　邪尸本身被药水泡过，坚韧异常，刀剑难伤，又有灵魂之力辅助，防御会变得更加强大。<br><br>　　尤其鬼沙的灵魂修养了一千多年，灵魂之力浩瀚如海，强大的灵魂之力加持，让尸体更加强大。<br><br>　　可是如今经过重重消耗后，龙尘的力量增强，而他的灵魂之力开始锐减，此消彼长之下，他已经防御不住龙尘的攻击了。<br><br>　　唐婉儿和叶知秋不禁大喜，这就预示着胜利在望，对方的灵魂已经开始衰竭，已经成了强弩之末。<br><br>　　她们都击杀过邪尸，知道这是胜利即将到来的标志，虽然经过重重波折，还是要成功了。<br><br>　　图方也暗中点点头，真不愧是传说中的异数，如果他能够得到核心级的培养，那么玄天别院，在排名上不光要提升一个大大的高度，甚至有机会问鼎前十吧。<br><br>　　激动的同时，图方心中也不禁暗自庆幸，自己把龙尘带回了玄天别院，给别人一个机会的同时，也给自己玄天别院一个机会，一饮一啄，莫非天定？<br><br>　　龙尘长刀挥舞，纵横来去，刀气震天，有着神环的加持，他几乎有着用不完的灵气。<br><br>　　这里是玄天别院，灵气极为充沛，只要不使用风府战身，完全可以达到收支平衡，短时间内不用担心灵气枯竭。<br><br>　　所以时间越长，对于龙尘来说就越有利，因为鬼沙消耗的是灵魂之力，每消耗一点，战力就弱一分，他没地方补充能量。<br><br>　　“砰”<br><br>　　鬼沙一拳击在龙尘的长刀上，人倒退几步，此时拳头上已经光剩下骨骼，肉都消失不见了，全部都被震飞了。<br><br>　　他已经没办法消耗更多的魂力，去护着这具尸体，如今他真的陷入了绝境。<br><br>　　“幽冥噬生”<br><br>　　鬼沙忽然一声厉喝，背后竟然浮现出黑色的一道虚影，那道虚影，仿佛恶魔之眸，在它出现的一刻，鬼沙的气息，忽然再次暴涨，一股恐怖的气机将龙尘锁定。<br><br>　　龙尘心头一震，他能够感觉到鬼沙的力量数以倍计的提升，而他的灵魂之力，也正以数倍的速度，急速衰退。<br><br>　　这绝对是一记狠招，牺牲了大量的灵魂之力，来疯狂提升力量，这个老家伙实在太恐怖了，只剩下灵魂，还有这么多招数，如果活着的时候，得多强啊。<br><br>　　想到这里，龙尘不禁有些懊悔，早知道，就多套点东西了，光是得到一套幽冥鬼影步，实在有些亏了。<br><br>　　“黄泉掌”<br><br>　　鬼沙一声厉啸，一只只剩下骨头干巴巴的爪子，对着龙尘拍落，恐怖的力量崩碎了虚空。<br><br>　　龙尘深吸了一口气，如今只能二选一了，不使用风府战身，就只能用它了。<br><br>　　长刀缓缓举起，风府星内的能量再无保留，沿着经络，冲过九道窍穴，注入手中长刀。<br><br>　　长刀立刻无风自鸣，浮现一抹奇异的颜色，当那抹颜色出现，整个长刀如同活了一般，一道刀气直冲天际，点亮天宇。<br><br>　　“开天！”<br><br>　　龙尘心中默念一声，手中的长刀斩落！<br><br>　　",
        };
    }
  }

  // 发送数据给native
  Future<void> sendMessageToReadView(dynamic message) async {

    Timer(Duration(seconds: 1), () async {
      await state.mMethodChannel
          .invokeMethod("reload", null);
    });

    await state.mMethodChannel.invokeMethod(
      'methodFromFlutterViewLoadChapter',
      message,
    );
  }

  // 发送数据给native
  Future<void> updateConfig(Map<String, dynamic> config) async {
    await state.mMethodChannel.invokeMethod(
      'updateConfig',
      config,
    );
  }

  Future<void> loadNextChapter() async {
    await state.mMethodChannel.invokeMethod("loadNextChapter");
  }

  Future<void> loadPreChapter() async {
    await state.mMethodChannel.invokeMethod("loadPreChapter");
  }

  Future<void> _showBottomMenu() async {
    showModalBottomSheet<int>(
      backgroundColor: Colors.transparent,
      isScrollControlled: true,
      context: Get.context!,
      builder: (BuildContext context) {
        return Container(
          clipBehavior: Clip.antiAlias,
          decoration: const BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(2.0),
              topRight: Radius.circular(2.0),
            ),
          ),
          height: MediaQuery.of(context).size.height / 3.0,
          child: Column(children: [
            SizedBox(
              height: 50,
              child: Stack(
                textDirection: TextDirection.rtl,
                children: [
                  const Center(
                    child: Text(
                      '设置',
                      style: TextStyle(
                          fontWeight: FontWeight.bold, fontSize: 16.0),
                    ),
                  ),
                  IconButton(
                      icon: const Icon(Icons.close),
                      onPressed: () {
                        Navigator.of(context).pop();
                      }),
                ],
              ),
            ),
            const Divider(height: 1.0),
            Row(
              children: [
                TextButton(
                    onPressed: () {
                      loadPreChapter();
                      Navigator.of(context).pop();
                    },
                    child: const Text("上一章")),
                const Expanded(
                  child: LinearProgressIndicator(
                    backgroundColor: Colors.cyanAccent,
                    valueColor: AlwaysStoppedAnimation<Color>(Colors.red),
                    value: 0.2,
                  ),
                ),
                TextButton(
                    onPressed: () {
                      loadNextChapter();
                      Navigator.of(context).pop();
                    },
                    child: const Text("下一章")),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                GestureDetector(
                  onTap: () {
                    var config = <String, dynamic>{
                      'contentColor': "#ff4a4a4a",
                      'backgroundColor': "#fff7f7f7",
                      "titleColor": "#ff4a4a4a",
                      "contentFontSize": 12,
                    };
                    updateConfig(config);
                  },
                  child: Container(
                    width: 30,
                    height: 30,
                    decoration: const BoxDecoration(
                        color: Colors.grey,
                        borderRadius: BorderRadius.all(Radius.circular(15.0))),
                  ),
                ),
                GestureDetector(
                  onTap: () {
                    var config = <String, dynamic>{
                      'contentColor': "#ff6A482E",
                      'backgroundColor': "#ffFBE6B5",
                      "titleColor": "#ff6A482E",
                      "contentFontSize": 22,
                    };
                    updateConfig(config);
                  },
                  child: Container(
                    width: 30,
                    height: 30,
                    decoration: const BoxDecoration(
                        color: Colors.orangeAccent,
                        borderRadius: BorderRadius.all(Radius.circular(15.0))),
                  ),
                ),
                GestureDetector(
                  onTap: () {
                    var config = <String, dynamic>{
                      'contentColor': "#ff425340",
                      'backgroundColor': "#ffD5E9D4",
                      "titleColor": "#ff425340",
                    };
                    updateConfig(config);
                  },
                  child: Container(
                    width: 30,
                    height: 30,
                    decoration: const BoxDecoration(
                        color: Colors.green,
                        borderRadius: BorderRadius.all(Radius.circular(15.0))),
                  ),
                ),
                GestureDetector(
                  onTap: () {
                    var config = <String, dynamic>{
                      'contentColor': "#ff814156",
                      'backgroundColor': "#ffFFE1DC",
                      "titleColor": "#ff814156",
                    };
                    updateConfig(config);
                  },
                  child: Container(
                    width: 30,
                    height: 30,
                    decoration: const BoxDecoration(
                        color: Colors.pinkAccent,
                        borderRadius: BorderRadius.all(Radius.circular(15.0))),
                  ),
                ),
                GestureDetector(
                  onTap: () {
                    var config = <String, dynamic>{
                      'contentColor': "#ff607799",
                      'backgroundColor': "#ff28334c",
                      "titleColor": "#ff607799",
                    };
                    updateConfig(config);
                  },
                  child: Container(
                    width: 30,
                    height: 30,
                    decoration: const BoxDecoration(
                        color: Colors.blue,
                        borderRadius: BorderRadius.all(Radius.circular(15.0))),
                  ),
                ),
                GestureDetector(
                  onTap: () {
                    var config = <String, dynamic>{
                      'contentColor': "#ff959595",
                      'backgroundColor': "#ff292929",
                      "titleColor": "#ff959595",
                    };
                    updateConfig(config);
                  },
                  child: Container(
                    margin: EdgeInsets.symmetric(horizontal: 10),
                    width: 30,
                    height: 30,
                    decoration: const BoxDecoration(
                        color: Colors.black,
                        borderRadius: BorderRadius.all(Radius.circular(15.0))),
                  ),
                ),
              ],
            )
          ]),
        );
      },
    );
  }
}
