import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';

import 'logic.dart';

class ReadPage extends StatelessWidget {
  final logic = Get.put(ReadLogic());
  final state = Get.find<ReadLogic>().state;

  ReadPage({super.key});

  @override
  Widget build(BuildContext context) {
    return contentView();
  }

  Widget contentView() {
    return Stack(
      children: [
        Container(
          color: Colors.white,
          child: buildReadView(),
        ),
      ],
    );
  }

  Widget buildReadView() {
    return readView(
      logic.onPlatformViewCreated,
      const <String, dynamic>{'initParams': '来了，老弟'},
      const StandardMessageCodec(),
    );
  }

  Widget readView(PlatformViewCreatedCallback? onPlatformViewCreated,dynamic creationParams,StandardMessageCodec? creationParamsCodec) {
    if (Platform.isIOS) {
      return UiKitView(
        //设置标识
        viewType: "ReadCenterView",
        onPlatformViewCreated: onPlatformViewCreated,
        creationParams: creationParams,
        creationParamsCodec: creationParamsCodec,
      );
    }
    else if (Platform.isAndroid) {
      return AndroidView(
        //设置标识
        viewType: "com.col.custom.android/readerView",
        onPlatformViewCreated: onPlatformViewCreated,
        creationParams: creationParams,
        creationParamsCodec: creationParamsCodec,
      );
    }

    return Container();
  }

  Future<dynamic> method(Future<dynamic> Function(MethodCall call) handler) {
    return Future((){});
  }
}
