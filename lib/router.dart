import 'package:flutter_reader_plugin/book_detail/view.dart';
import 'package:flutter_reader_plugin/read/view.dart';
import 'package:get/get_navigation/src/routes/get_route.dart';

class ReaderRouter {
  static const String bookDetail = "/book_detail";
  static const String read = "/read";
  static final List<GetPage> getPages = [
    GetPage(name: bookDetail, page: () => BookDetailPage()),
    GetPage(name: read, page: () => ReadPage()),
  ];
}
