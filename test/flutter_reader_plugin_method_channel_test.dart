import 'package:flutter/services.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:flutter_reader_plugin/flutter_reader_plugin_method_channel.dart';

void main() {
  MethodChannelFlutterReaderPlugin platform = MethodChannelFlutterReaderPlugin();
  const MethodChannel channel = MethodChannel('flutter_reader_plugin');

  TestWidgetsFlutterBinding.ensureInitialized();

  setUp(() {
    channel.setMockMethodCallHandler((MethodCall methodCall) async {
      return '42';
    });
  });

  tearDown(() {
    channel.setMockMethodCallHandler(null);
  });

  test('getPlatformVersion', () async {
    expect(await platform.getPlatformVersion(), '42');
  });
}
