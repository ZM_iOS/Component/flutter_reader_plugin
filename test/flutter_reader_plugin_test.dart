import 'package:flutter_test/flutter_test.dart';
import 'package:flutter_reader_plugin/flutter_reader_plugin.dart';
import 'package:flutter_reader_plugin/flutter_reader_plugin_platform_interface.dart';
import 'package:flutter_reader_plugin/flutter_reader_plugin_method_channel.dart';
import 'package:plugin_platform_interface/plugin_platform_interface.dart';

class MockFlutterReaderPluginPlatform
    with MockPlatformInterfaceMixin
    implements FlutterReaderPluginPlatform {

  @override
  Future<String?> getPlatformVersion() => Future.value('42');
}

void main() {
  final FlutterReaderPluginPlatform initialPlatform = FlutterReaderPluginPlatform.instance;

  test('$MethodChannelFlutterReaderPlugin is the default instance', () {
    expect(initialPlatform, isInstanceOf<MethodChannelFlutterReaderPlugin>());
  });

  test('getPlatformVersion', () async {
    FlutterReaderPlugin flutterReaderPlugin = FlutterReaderPlugin();
    MockFlutterReaderPluginPlatform fakePlatform = MockFlutterReaderPluginPlatform();
    FlutterReaderPluginPlatform.instance = fakePlatform;

    expect(await flutterReaderPlugin.getPlatformVersion(), '42');
  });
}
